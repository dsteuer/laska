
#if !defined(__MTDF_H)
#define __MTDF_H

#include "movetree.h"

class MTDF : public MoveTree {
public:
	MTDF	(const unsigned int board_size = 7);
	MTDF	(std::string	pos, const unsigned int board_size = 7);
	Move	findBestMove	();

protected:
	int			evaluateMoveTree	(unsigned int depth, int alpha, int beta);
	int			evaluateMoveTreeQuiesce	(	  int depth, int alpha, int beta);
	std::pair<int, Move>	evaluateMoveTreeRoot	(unsigned int depth, int alpha, int beta);
	Move			search			(unsigned int depth, int v0);
};

#endif
