
#include "mtdf.h"

#include <iomanip>

MTDF::MTDF (const unsigned int board_size)			: MoveTree (board_size)    	{ } 
MTDF::MTDF (std::string pos, const unsigned int board_size)	: MoveTree (pos, board_size)	{ }

int MTDF::evaluateMoveTreeQuiesce(int depth, int alpha, int beta) {
	std::list<Move> l = possibleJumps();

	if (l.empty()) {							// no jumps, so quiesence search over. node is (at least) quiet
		l = possibleSteps();

		if (l.empty()) { return -mateValue(tree_depth-depth); }		// game over, node is terminal. return mate in plies

		return evaluatePosition();
	}

	for (auto m: l) {						// forced part, moves hit
		move(m);
		int v = -evaluateMoveTreeQuiesce(depth-1, -beta, -alpha);
		undo (m);

		if (v >= beta)  { return v; }					// fail-soft
		if (v >  alpha) { alpha = v; }
	}

	return alpha;
}

int MTDF::evaluateMoveTree(unsigned int depth, int alpha, int beta) {
	std::list<Move> l = possibleMoves();

	if (l.empty()) { return -mateValue(tree_depth-depth);		}	// game over, node is terminal

	if (!depth) {								// end of search depth
		if (!Move(l.front()).isJump()) {				// a normal move
			return evaluatePosition();
		}
		for (auto m: l) {
			move(m);
			int v = -evaluateMoveTreeQuiesce(depth-1, -beta, -alpha);
			undo (m);

			if (v >= beta)  { return beta; }
			if (v >  alpha) { alpha = v; }
		}

		return alpha;			// alpha holds best value
	}

	for (auto m: l) {
		move(m);
		int v = -evaluateMoveTree(depth-1, -beta, -alpha);
		undo (m);

		if (v >= beta)  { return v; }
		if (v >  alpha) { alpha = v; }
	}

	return alpha;
}

std::pair<int, Move> MTDF::evaluateMoveTreeRoot(unsigned int depth, int alpha, int beta) {
	std::list<Move> l = possibleMoves();	// by findBestMove exists and contains more than 1 move.

	std::list<std::pair<int, Move> > sl;

	for (auto m: l) {
		move(m);
		int v =  -evaluateMoveTree(0, -beta, -alpha);
		undo(m);
		
		std::cerr << moveValueString	(m, v) << "\t";

		sl.push_back(std::pair<int, Move>(v, m));
	}
	std::cerr << std::endl;

	sl.sort(std::greater<std::pair<int, Move> >());

	return sl.front();
}

Move MTDF::search(unsigned int depth, int v0) {
	int bound[2] = { -maxValue(), maxValue() };
	int v = v0;

	std::pair<int, Move> p(v, NoMove); 
	while (bound[0] < bound[1]) {
		int beta = p.first;
		if (p.first == bound[0]) beta = p.first + 1;

		p = evaluateMoveTreeRoot (depth, beta-1, beta);

		(p.first < beta) ?  bound[1] = p.first : bound[0] = p.first;
	}

	return p.second;
}

Move MTDF::findBestMove() {
	std::list<Move> l = possibleMoves();

	if (l.empty())		return NoMove;		// game over. i lost
	if (l.size() == 1)	return l.back();	// if only one move possible, return at once

	Move m(NoMove);

	unsigned int r = tree_depth;
	for (tree_depth = 1; tree_depth <=r; tree_depth++) {
		std::cerr << "depth=" << std::setw(2) << tree_depth << "\t";
		m =  search(tree_depth, evaluatePosition());
	}

	return m;
}

