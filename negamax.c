
#include <iomanip>

#include "negamax.h"

// negamax movetree evaluation

NegaMax::NegaMax (const unsigned int board_size)			: MoveTree (board_size)    { } 
NegaMax::NegaMax (std::string pos, const unsigned int board_size)	: MoveTree (pos, board_size), value_sum(0){ }

int NegaMax::evaluateMoveTree(unsigned int depth) {
	if (!depth)	{
		move_count++;
		value_sum += evaluatePosition();
		return evaluatePosition();			// end of search depth, return terminal value
	}

	std::list<Move> l = possibleMoves();

	if (l.empty())	return -mateValue(tree_depth-depth);		// game over

	int best_value = -maxValue();

	for (auto m: l) {
		// if (isPermutation (Move(m))) { continue; }		// needs history, see movetree.c how to activate
		move(m);
		best_value = std::max(best_value, -evaluateMoveTree(depth-1));
		undo(m);
	}

	return best_value;
}

Move NegaMax::findBestMove() {
	std::list<Move> l = possibleMoves();

	if (l.empty())	   return NoMove;					// no more moves, game over.
	if (l.size() == 1) return l.back();					// only one move possible. return at once.

	int	best_value = -maxValue();					// start with worst possible outcome
	Move	best_move  = NoMove;						// and no move

	for (auto m: l) {
		move(m);
		int v = -evaluateMoveTree(tree_depth-1);
		undo(m);

		if (v > best_value) {
			best_value = v;
			best_move  = m;
		}

		std::cerr << moveValueString(m, v) << "\t";
	}
	std::cerr << std::endl;
	float r = ((float) value_sum/(float)move_count);
	std::cerr <<	"value: " << setw(6) << evaluatePosition() << " - " << setw(5) << move_count <<
			" moves. value sum " << setw(10) << value_sum <<
			" avg. " << setw(10) << r <<
			" diff " << (r-value) << std::endl;

	return best_move;
}

