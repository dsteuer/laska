
#if !defined(__ABQ_BOARD_H)
#define __ABQ_BOARD_H

#include "movetree.h"

class AlphaBetaQuiesce : public MoveTree {
public:
	AlphaBetaQuiesce	(const unsigned int board_size = 7);
	AlphaBetaQuiesce	(std::string	pos, const unsigned int board_size = 7);
	Move	findBestMove	();

protected:
	int	evaluateMoveTree	(unsigned int depth, int alpha, int beta);
	int	evaluateMoveTreeQuiesce	(	  int depth, int alpha, int beta);
};

#endif
