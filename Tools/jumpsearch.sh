#!/bin/bash

# file containing search start positions (here all unique leaves of first 3 root moves sorted of a -d14 search)
pfile=m14-3sm

poscount=`wc -l $pfile | cut -d" " -f1`

echo $poscount start positions

for i in `seq 1 $poscount`; do
	p=`head -n$i $pfile | tail -n1`
	echo position $i: `date +"%Y/%m/%d %H:%M:%S"` $p  -- log to data/log/$i
	time  ./search -g -d20 -A"$p" > data/log/$i 2>&1
done

