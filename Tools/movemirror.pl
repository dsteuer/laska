
# perl script to mirror moves at d - files

use strict;
use warnings;
use Getopt::Long;

while (<>) {								# read stdin and store into pos ans moves
	s/a/_/g;
	s/g/a/g;
	s/_/g/g;
	s/b/_/g;
	s/f/b/g;
	s/_/f/g;
	s/c/_/g;
	s/e/c/g;
	s/_/e/g;

	print;
}

