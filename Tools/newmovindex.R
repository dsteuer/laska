olddata <- read.csv("oldmovedata.txt", sep=",", header = FALSE)
moveindex <- cbind(olddata, matrix(0, nrow=NROW(olddata),ncol=5))
moveindex <- cbind(moveindex, matrix(-1, nrow=NROW(olddata),ncol=3))
moveindex <- cbind(moveindex, matrix(0, nrow=NROW(olddata),ncol=2))
colnames(moveindex) <- c("length","from","to1", "to2", "to3", "to4", "to5", "to6", "to7", "own", "opp", "startw", "startb","starto","endw","endb","endo", "ispromow", "ispromob")



#sink(file="nmoveindex.txt")
for ( movenumber in 1:NROW(moveindex)) {
  if ( moveindex$"length"[movenumber] < 7) { ## if length >= 7 no follow-up
    cur.length <- moveindex$"length"[movenumber]
    cur.steps <- moveindex[movenumber, 2:9]

    for ( pos.start in which(moveindex$"length" == (cur.length +1) )){
      if (all (moveindex[movenumber, 2:(2+cur.length)] == moveindex[pos.start, 2:(2+cur.length)])) {
        ## longer move is continuation of shorter move

        ## officer
        if (moveindex[movenumber, "starto"] == 0 ) moveindex[movenumber, "starto"] <- 72+pos.start-1
        moveindex[movenumber, "endo"] <- 72+pos.start-1 +1 ###  the last +1 is for the comparison in the index in
                                                           ###  possibleSteps/Jumps

        ## white private
        if (moveindex[movenumber, 2+cur.length] - moveindex[movenumber, 2] >= cur.length*6 ){
          if (moveindex[pos.start, 2+cur.length+1] - moveindex[pos.start, 2] >= (1+cur.length)*6 ){
            if (moveindex[movenumber, "startw"] == 0 ) moveindex[movenumber, "startw"] <- 72+pos.start-1
            moveindex[movenumber, "endw"] <- 72+pos.start
          }
        }
        ## black private
        if (moveindex[movenumber, 2] - moveindex[movenumber, 2+cur.length] >= cur.length*6 ){
          if (moveindex[pos.start, 2] - moveindex[pos.start, 2+cur.length+1] >= (1+cur.length)*6 ){
            if (moveindex[movenumber, "startb"] == 0 ) moveindex[movenumber, "startb"] <- 72+pos.start-1
          moveindex[movenumber, "endb"] <- 72+pos.start
          }
        }
      }
    }
  }
}

for ( movenumber in 1:NROW(moveindex)) {
  cur.length <- moveindex$"length"[movenumber]
  cur.steps <- moveindex[movenumber, 2:9]

  ## white private
  if (moveindex[movenumber, 2+cur.length] - moveindex[movenumber, 2] >= cur.length*6 ){
    if (moveindex[movenumber, 2+cur.length] > 20) moveindex$ispromow[movenumber] <-1
  }

  ## black private
  if (moveindex[movenumber, 2] - moveindex[movenumber, 2+cur.length] >= cur.length*6 ){
    if (moveindex[movenumber, 2+cur.length] < 4) moveindex$ispromob[movenumber] <-1
  }
}


for ( movenumber in 1:NROW(moveindex)) {
  cat( "{ ", moveindex[movenumber, "length"], ", { ", sep="")
  for (step in 2:8) {
    cat( format(moveindex[movenumber, step], width=2), ",  "  , sep="")
  }
  cat( format(moveindex[movenumber,9], width=2), " }, ",
      format(moveindex[movenumber,10], width=2), ", ",
      format(moveindex[movenumber,11], width=2), ", {  ", sep="")
  cat(  format(moveindex[movenumber,"startw"], width=3), ",  ",
      format(moveindex[movenumber,"startb"], width=3), ",  ",
      format(moveindex[movenumber,"starto"], width=3), " }, { ",
      format(moveindex[movenumber,"endw"], width=3), ",  ",
      format(moveindex[movenumber,"endb"], width=3), ",  ",
      format(moveindex[movenumber, "endo"], width=3), " }, { ",
      format(moveindex[movenumber, "ispromow"], width=3), ", ",
      format(moveindex[movenumber, "ispromob"], width=3)," }}, //",
      format(72+movenumber-1, width=3), sep="")
  cat("\n")

}
#sink(NULL)



#### below old stuff
moveindex <- read.csv(file="movetmp.txt", header=FALSE, sep=",")
colnames(moveindex) <- c("length","from","to1", "to2", "to3", "to4", "to5", "to6", "to7", "own", "opp", "start", "end")

moveindex$start <- 0
moveindex$end <- 0

sink(file="nmindex.txt")
for ( movenumber in 1:NROW(moveindex))
{
#  cat( movenumber, " ")
  if ( moveindex$"length"[movenumber] < 7) {
    cur.length <- moveindex$"length"[movenumber]
    cur.steps <- moveindex[movenumber, 2:9]

    for (pos.start in which(moveindex$"length" == (cur.length+1)) ){
                                        #    if (moveindex$"length"[pos.start] == cur.length) next
                                        #    if (moveindex$"length"[pos.start] > (cur.length +1)) break
      if (all (moveindex[movenumber, 2:(2+cur.length)] == moveindex[pos.start, 2:(2+cur.length)])) { #found start
        if (moveindex[movenumber, "start"] == 0 ) moveindex[movenumber, "start"] <- pos.start-1
        moveindex[movenumber, "end"] <- pos.start-1
      } 
    }
  }
###    cat("MOvenumber ", movenumber-1,"\n")
###    print ( moveindex[movenumber,] )
##    print ( moveindex[moveindex[movenumber, "start"]+1,    ] )
###    print ( moveindex[moveindex[movenumber, "end"]+1,    ] )
  cat( "{ ", moveindex[movenumber, "length"], ", { ", sep="")
  for (step in 2:8) { cat( format(moveindex[movenumber, step], width=2), ",  "  , sep="")}
  cat(format(moveindex[movenumber,9], width=2), " }, ", format(moveindex[movenumber,10], width=2), ", ", format(moveindex[movenumber,11], width=2), ", {  ", sep="")
  cat(format(moveindex[movenumber,"start"], width=3), ",  ", format(moveindex[movenumber, "end"], width=3), " }}, //", format(movenumber-1, width=3), sep="")
  cat("\n")

}
sink(NULL)

olddata <- read.csv("oldmovedata.txt", sep=",")
