
# m: m > position value > -m
m=1000
# number of games (random) played
G=500
# moves per  game
d=30

for p in `./laska -R -M $G -d$d 2>&1  | grep -v "game over" `; do
	v=`./laska -e -p "$p" 2>/dev/null`;
	if [ $v -lt $m -a $v -gt -$m ]; then
		echo $p $v;
	fi;
done | sort -n -k2

