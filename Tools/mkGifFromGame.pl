
# perl script to convert a game history of the form
# [<compact position>] <move> [<move>]*
# and so on, separated my any whitespace,
# to an animated gif
# you need to have installed GD and Switch modules


use strict;
use warnings;
use GD;
use Switch;
use Getopt::Long;

# global vars

my	$verbose	= 0;
my	$tmpdir		= "/home/lk/src/laska/tmp-img";		# dir, where to write image
my	$outputfile	= "game.gif";				# gif image file name
our	$w		= 50;					# width of a stone. all sizes calculated from here

GetOptions (
	'stonewidth=i'	=> \$w,
	'verbose'	=> \$verbose,
	'tmpdir=s'	=> \$tmpdir,
	'output=s'	=> \$outputfile,
);

our	$h		= $w/5;					# height of stone

my	$shift_count	= 10;					# 10 images for a move from a square to another adjecent square
our	$step_size	= $w/10;				# thus pixel per shift.
our	$margin		= 3*$w/4;				# margin around board

our	%square2index = (
	"a1" =>  0, "c1" =>  1, "e1" =>  2, "g1" =>  3,
	    "b2" =>  4, "d2" =>  5, "f2" =>  6,
	"a3" =>  7, "c3" =>  8, "e3" =>  9, "g3" => 10,
	    "b4" => 11, "d4" => 12, "f4" => 13,
	"a5" => 14, "c5" => 15, "e5" => 16, "g5" => 17,
	    "b6" => 18, "d6" => 19, "f6" => 20,
	"a7" => 21, "c7" => 22, "e7" => 23, "g7" => 24
);

our	$image		= new GD::Image	(8*$w+$w/2, 8*$w+$w);		# around 8.5 x stonewidth image;
our	$white		= $image->colorAllocate (255, 255, 255);
our	$black		= $image->colorAllocate ( 10,  10,  10);
our	$white_stone	= $image->colorAllocate (218, 165,  32);	# stone color now
our	$black_stone	= $image->colorAllocate ( 93, 71,  139);       
our	$text_color	= $image->colorAllocate (233, 214,  91);	# (unused)

our	$image_count	= 0;						# count images written	(unused)
our	$animation	= $image->gifanimbegin; #(0, 1);		# no global color map, 1 loop;

my	$pos		= "o.o.o.o.o.o.o.o.o.o.o....x.x.x.x.x.x.x.x.x.x.x";
my	@moves		= ();

while (<>) {								# read stdin and store into pos ans moves
	s/\s+$//;
	my @words = split /\s+/;

	foreach my $x (@words) {
		if ($x =~ /o/)	{
			$pos = $x;
		} elsif ($x =~ /[a-g][1-7][x-][a-g][1-7]/) {
			push @moves, $x;
		} else {
			print "unknown word: \"" . $x . "\" ignored\n";
		}
	}
}

if ($verbose) {
	print $pos . "\n";
	foreach my $m (@moves) { print $m . " "; } print "\n";
}

our @towers = split /[\.|]/, $pos;

sub tower2position {		# towers square index -> ll corner of square on board in pixel
	my ($i) = @_;

	my $sw  = $w;
	my $row = 2 * int($i/7);
	my $col = $i%7;
	if ($col > 3) { $row++; }

	$row = (7-$row)*$sw;					 #  + $margin;
	if ($col > 3) { $col = (2*($col-4)*$sw + $sw); }
	else { $col = (2*$col*$sw); }
	$col += 3*$sw/4 + 5;
	return ($row, $col);
}

sub oneFrame {
	my	($square, $dx, $dy) = @_;			# shift tower at square by dx / dy

	my	$im = new GD::Image($image->getBounds);		# 8.5 x stonewidth square image, copy from image

	# first print board background

	$im->filledRectangle (0, 0, 8*$w+$w/2, 8*$w+$w/2, $white);		# white background

	$im->rectangle($margin, $margin/2, $margin + 7*$w, $margin/2 + 7*$w, $black);	# thin border

	my	$l = "1234567";
	my	$n = "GFEDCBA";

	for (my $i=0; $i<7; $i++) {
		$im->string(gdMediumBoldFont, $w/4, 7*$w-$w/4 -$i*$w,      substr($l, $i, 1), $black);
		$im->string(gdMediumBoldFont, 7*$w+$w/4 -$i*$w, 8*$w-$w/4, substr($n, $i, 1), $black);
	}


	for(my $i=0; $i<7; $i++) {					# print some dark squares, checkers like
	for(my $j=0; $j<7; $j++) {
	        my $b = ($i%2) ? (($j%2) ? 0 : 1) : (($j%2) ? 1 : 0);

		if ($b == 1) {
			$im->filledRectangle ($margin + $i*$w, $margin/2 + $j*$w, $margin + $i*$w + $w , $margin/2 + $j*$w + $w, $black);
		}
	}}

	for(my $t = 0; $t < 25; $t++) {					# print the towers
		my $s = reverse $towers[$t];				# revert string, draw from bottom to top

		my @offset = tower2position ($t);
		$offset[0] -= $h*(length($s)-1) - $h/2;			# add towers height

		my $mx = 0;
		my $my = 0;
		if ($t == $square) {					# the "moving" tower (index)
			$mx = $dx;					# shift
			$my = $dy;
		}

		for(my $i = 0; $i < length($s); $i++) {
			my $x1 = $offset[1] +             1 + $mx;
			my $x2 = $offset[1] +         $w-12 + $mx;	# 6 pixel margin around stone
			my $y1 = $offset[0] +     ($i*$h)+1 + $my;
			my $y2 = $offset[0] + (($i+1)*$h)-2 + $my;	# square - border pixel, both sides

			switch (substr ($s, $i, 1)) {
			case 'o' { $im->filledRectangle($x1,          $y1,          $x2,         $y2,        $white_stone);	next; }
			case 'O' { $im->filledRectangle($x1,          $y1,          $x2,         $y2,        $white_stone);
				   $im->filledEllipse  ($x1 + $w/2-6, $y1 + $h/2-1, $h-3,        $h-3,       $black_stone);	next; }
			case 'x' { $im->filledRectangle($x1,          $y1,          $x2,         $y2,        $black_stone);	next; }
			case 'X' { $im->filledRectangle($x1,          $y1,          $x2,         $y2,        $black_stone);
				   $im->filledEllipse  ($x1 + $w/2-6, $y1 + $h/2-1, $h-3,        $h-3,       $white_stone);	next; }
			}
		}
	}

	$animation	.= $im->gifanimadd (0, 0, 0, 10); # , 1, prev_im);
	$image_count++;
}

sub oneFlightForward {		# move a stone from sqaure $from in direction $dx, $dy. if target square has a tower (hit move) the shift up by $dy_extra, to get on top of that tower
	my ($from, $stepcount, $dx, $dy, $dy_extra) = @_;

	for (my $j=1; $j<=$stepcount; $j++) {					# calc position of tower in current image
		my	$cx = ($dx*($j));					# in offset from endposition of last step
		my	$cy = ($dy*($j) - int($dy_extra*($j)+.5));		# on hit moves move should end on top of captures tower, so a bit upward

		oneFrame ($from, $cx, $cy);
	}
}

sub oneFlightBackward {		# capture move only: move a tower from top of captures stone to target square.
	my ($from, $stepcount, $dx, $dy, $dy_extra) = @_;

	for (my $j= $stepcount; $j > 0; $j--) {					# calc position of tower in current image
		my	$cx = ($dx*($j));					# in offset from endposition of last step
		my	$cy = ($dy*($j) - int($dy_extra*($j)+.5));		# on hit moves move should end on top of captures tower, so a bit upward

		oneFrame ($from, $cx, $cy);
	}
}

oneFrame (-1, 0, 0);								# print start position

foreach my $move (@moves) {
	my @steps = ();
	my $hit = 0;
	if (index ($move, "x") == 2) {	# hit move ?
		@steps = split /x/, $move;
		$hit = 1;
	} else {
		@steps = split /-/, $move;
	}
	# array steps holds now all squares move passes.

	for (my $i = 0; $i < $#steps; $i++) {			# loop over steps
		my $file_1 = substr($steps[$i],   0, 1);	# extract file and rank info of current movestep
		my $file_2 = substr($steps[$i+1], 0, 1);
		my $rank_1 = substr($steps[$i],   1, 1);
		my $rank_2 = substr($steps[$i+1], 1, 1);

		# offest of current moving tower from start position, in pixel. stepsize between images is 6 currently
		my $dx =  $step_size;				# calc file / rank differenct -> move direction
		   $dx = -$step_size if $file_1 gt $file_2;
		my $dy = -$step_size;
		   $dy =  $step_size if $rank_1 gt $rank_2;
	
		my $from = $square2index{$steps[$i]};
		my $to   = $square2index{$steps[$i+1]};

		if (!$hit) {
			oneFlightForward ($from, $shift_count, $dx, $dy, 0);
			my $swap	= $towers[$from];
			$towers[$from]	= $towers[$to];
			$towers[$to]	= $swap;
		} else {
			my $over	= ($from + $to) / 2;
			my $extra	= $h*(length($towers[$over]))/$shift_count;
			oneFlightForward ($from, $shift_count, $dx, $dy, $extra);
			my $top		= substr ($towers[$over], length($towers[$over]) -1, 1);
			$towers[$over]	= substr ($towers[$over], 0, length($towers[$over]) -1);
			$towers[$from]	= $top . $towers[$from];
			$extra		= $h*(length($towers[$over]))/$shift_count;
			my $swap	= $towers[$from];
			$towers[$from]	= $towers[$to];
			$towers[$to]	= $swap;
			oneFlightBackward ($to, $shift_count, -$dx, -$dy, $extra);	# after capture tower's square is $to. so flight "backward" home
		}

		if ($to > 20 || $to < 4) {		# promotion check. at end of partmove. (if, does not continue.)
			my $top = substr ($towers[$to], length($towers[$to]) - 1, 1);		# uppercase top stone
			$top = uc($top);
			$towers[$to] = substr ($towers[$to], 0, length($towers[$to]) - 1) . $top;
		}
	}
	oneFrame (-1, 0, 0);				# draw position after move
}

$animation	.= $image->gifanimend;

print "write output to $tmpdir/$outputfile\n";
open (my $A, '>', "$tmpdir/$outputfile") or die "no open\n";
print $A $animation;
close $A;


