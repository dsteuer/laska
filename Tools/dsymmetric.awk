#!/usr/bin/awk -f
# prints all positions which are d-rank-symmetric, reads stdin, prints stdout
{
    pos = $NF
    n = split(pos, lines, "|")
    symmetric=1;

    for (i=1; i<=n; i++){
	m = split(lines[i], towers, ".");
	if (m == 3) {
	    if (towers[1] != towers[3]) {
		symmetric = 0;
		break
	    }
	} else {
	    if (towers[1] != towers[4]){
		symmetric = 0;
		break
	    }
	    if (towers[2] != towers[3]){
		symmetric = 0;
		break
	    }
	}
    }
    if (symmetric == 1) print $0
}
