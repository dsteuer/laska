
# perl script to convert a "readable" ascii-board
# to caompact laska-position

use strict;
use warnings;
use Getopt::Long;

# global vars

my	$verbose	= 0;
my	$pos		= 0;					# width of a stone. all sizes calculated from here
my	$reverse	= 0;					# width of a stone. all sizes calculated from here

GetOptions (
	'verbose'	=> \$verbose,
	'pos=s'		=> \$pos,
);

my $n = 0;
my $p_str = "";
my $lines = 0;

while (<>) {								# read stdin and store into pos ans moves
	$n ++ if (/^  7/);

	if ($n == $pos) {
		if ($lines < 26) {
			$lines ++;
			# print $lines . " " . $_;
			if ($lines%4 == 2) {
				# s/\|_______\|$//;
				s/\|_______\|/./g;
				s/^       \|//;
				s/\|$//;
				s/\s+//g;
				$p_str = $_ . $p_str;
			}
		}
		last if ($lines == 27);
	}
}

print $p_str . "\n";

