
#if !defined(__PVS_H)
#define __PVS_H

#include "alphabetaq-id.h"

class PVS : public AlphaBetaQId {
public:
	PVS	(const unsigned int board_size = 7);
	PVS	(std::string	pos, const unsigned int board_size = 7);

protected:
	int	evaluateMoveTree	(unsigned int depth, int alpha, int beta);
};

#endif
