
POS="o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x"
SUFFIX=$$

if [ $# -ge 1 ]; then POS=$1;    fi;
if [ $# -eq 2 ]; then SUFFIX=$2; fi;

P2=./laska
P1=../laska-master/laska

PROG1="stdbuf -i0 -o0 -e0 $P1 -d5 -p \"$POS\" -l -m 50"
PROG2="stdbuf -i0 -o0 -e0 $P2 -d5 -p \"$POS\" -l -m 50 -b"

# echo $2 $1

# echo $PROG1
# echo $PROG2

# thx to http://unix.stackexchange.com/questions/53641/how-to-make-bidirectional-pipe-between-two-programs - post of "user873942"
ioloop () 
{ 
    FIFO=$(mktemp -u /tmp/ioloop_$$_XXXXXX ) && trap "rm -f $FIFO" EXIT && mkfifo $FIFO && ( : < $FIFO & ) && exec > $FIFO < $FIFO
}

# ( ioloop && $PROG1 | $PROG2 ) 2>&1 | grep \\. | tee game-$$
( ioloop && $PROG1 2>out/prog-1-log-$SUFFIX | $PROG2 2>out/prog-2-log-$SUFFIX)
