
#include "evaltest.h"

#include "negamax.h"
#include "alphabeta.h"
#include "alphabetaq.h"
#include "alphabetaq-id.h"


EvalTest::EvalTest (std::string p, unsigned int n_games, unsigned int max_len, unsigned int depth) {
	unsigned int    victories[3] = { 0, 0, 0 };

	std::cerr << "EvalTest on position " << p << ' ' << n_games << " games of maximum length " << max_len << std::endl;

	position = p;
	num_games = n_games;
	max_game_length = max_len;
	tree_depth = depth;
	for (unsigned int game=0; game < num_games; game++) {
		std::cerr << "EvalTest game " << game << std::endl;
		victories[oneGame()] ++;
	}
	std::cerr << "EvalTest result " << num_games << ": " << victories[0] << ' ' << victories[1] << ' ' << victories[2] << std::endl;
}


unsigned int EvalTest::oneGame () {
	std::cerr << "EvalTest oneGame " << std::endl;
	// NegaMax	b0(position);
	// NegaMax	b1(position);
	AlphaBetaQuiesce	b0(position);
	AlphaBetaQuiesce	b1(position);
	b0.readConfig ("cfg0.txt");
	b1.readConfig ("cfg1.txt");
//	b0.setPosition (position);
//	b1.setPosition (position);
	b0.setTurn(0);
	b1.setTurn(0);
	b0.setTreeDepth(tree_depth);
	b1.setTreeDepth(tree_depth);
	
	for (unsigned int c = 0; c < max_game_length; c++) {
		Move m = b0.findBestMove();
		if (m.empty()) {
			std::cerr << " b wins" << std::endl;
			return 1;
		}
		std::cerr << m << ' ';
		b0.moveRecorded(m); // b0.print(); std::cerr << std::endl; b0.print(1); std::cerr << std::endl;
		b1.moveRecorded(m); // b1.print();
		m = b1.findBestMove();
		if (m.empty()) {
			std::cerr << " w wins" << std::endl;
			return 0;
		}
		std::cerr << m << ' ';
		b0.moveRecorded(m); // b0.print(); std::cerr << std::endl; b0.print(1); std::cerr << std::endl;
		b1.moveRecorded(m); // b1.print();
	}

	std::cerr << "max moves reached -> draw" << std::endl;
	return 2;
}

