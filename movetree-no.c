#include <iostream>
#include <list>
#include <string>

#include "movetree-no.h"

// testing possibleMoves() finder

static unsigned  int	record_depth;
// move statistics: counter and in depth counter
static long long int	mc=0;
static long long int	depthc=0;
static clock_t		startclock, endclock;
static std::vector<int>				record_count;
static std::vector<std::list<std::string>>	record_values;

MoveTreeNoOfficer::MoveTreeNoOfficer (		       const unsigned int board_size) : PositionNoOfficer (     board_size) { initStats(); }
MoveTreeNoOfficer::MoveTreeNoOfficer (std::string pos, const unsigned int board_size) : PositionNoOfficer (pos, board_size) { initStats(); }

void MoveTreeNoOfficer::initStats ()	{
	record_depth	= 1000;			// a search depth unreachable (initial search depth (tree_depth) will be lesser)
	record_count.resize(100);
	// record_values.resize(100);
	for (unsigned int i=0; i<100; i++) {
		std::list<std::string> l;
		record_values.push_back(l);
		record_count[i] = 0;
	}
}

void MoveTreeNoOfficer::setTreeDepth		(const unsigned int d)	{
	tree_depth = d;
}

bool MoveTreeNoOfficer::isPermutation	(const Move m) {
	// std::cerr << "MoveTree::symmetryCheck " << Move(m).toString() << std::endl;

	if (move_history.size() < 3) return false;	// need at least three moves

	Move other (move_history.back());
	std::list<Move>::const_reverse_iterator i = ++(move_history.rbegin());
	Move m0(*i);							// second last move, undo 2 times, and reverse moves (m0, other, m) to (m, other, m0)
	
	if (m0.index  () <  m.index  ()) { return false; }		// the variation to keep 
	if (m0.isStep () != m.isStep ()) { return false; }		// exchanded moves both be steps or jumps
	if (m0.to     () == m.from   ()) { return false; }		// current move is a continuation of former, cannot exchange

	undo(other);							// undo 2 moves and apply in reversed order below
	undo(m0);

	if (!isLegal(m)) {						// m not legal in this position
		move(m0);						// restore position
		move(other);
		return false;
	}
	move(m);
	if (!isLegal(other)) {						// opponents move not legal after playing m
		undo(m);						// restore position
		move(m0);
		move(other);
		return false;
	}
	if (other.isStep()) {
		if (!possibleJumps().empty()) {				// m provokes a jump, but m0 doesn't. (move above already called switchTurn() )
			undo(m);
			move(m0);
			move(other);
			return false;
		}
	}
	move(other);
	if (!isLegal(m0)) {
		undo(other);
		undo(m);
		move(m0);
		move(other);
		return false;
	}

	undo(other);							// restore position
	undo(m);
	move(m0);
	move(other);

	// std::cerr << "MoveTree::symmetryCheck is permutation." << std::endl;

	return true;
}

void MoveTreeNoOfficer::generateMoveTree(unsigned int depth) {
	// cerr << "generateMoveTree(" << depth << ")" << endl;

	if (!depth) {
		std::cerr << "limit too low " << std::endl;
		for (const auto& mi: move_history) {
			std::cerr << (mi) << ' ';
                }
		std::cerr << std::endl;
		print (1);
		return;
	}

	if (depth < record_depth) {
		std::cerr << "new record: " << (tree_depth - depth) << " plies. (" << move_history.size() << " moves in history.)" << std::endl;
		for (const auto& mi: move_history) { std::cerr << (mi) << ' '; } std::cerr << std::endl;
		print(1);
		record_depth = depth;
		record_count[(tree_depth - depth)] += 1;
		// std::string s;
		// for (const auto& mi: move_history) { s += (mi).toString() + ' '; }
		// record_values[(tree_depth - depth)].push_back(s);
	} else if (depth == record_depth) {
		std::cerr << "record: " << (tree_depth - depth) << " plies. (" << move_history.size() << " moves in history.)" << std::endl;
		for (const auto& mi: move_history) { std::cerr << (mi) << ' '; } std::cerr << std::endl;
		print(1);
		record_count[(tree_depth - record_depth)] += 1;
	}

	std::list<Move> l = possibleMoves();

	if (l.empty()) { /* cerr << "game over." << endl; */ return; }		// game over

	for (const auto& m: l) {
		if (isPermutation (m)) { continue; }

		//for (const auto& mi: move_history) { cerr << (mi) << ' '; }
		// move_count ++; cerr << move_count << ". ";
		//cerr << m.toString() << endl;

		moveRecorded(m);
		//move(m);

		// print(1);

		mc++;
		depthc += tree_depth-depth;
		if (mc%10000000 == 0) {
			for (const auto& mi: move_history) { std::cerr << (mi) << ' '; } std::cerr << std::endl;
			std::cerr << (mc / 10000000)  << "\t" << depthc / mc << std::endl;
			endclock = clock();
			std::cerr << "time:\t" << ((unsigned) (endclock-startclock)/1e6) << " s (cpu)\t";
			std::cerr << (mc*1e6 / (unsigned) (endclock-startclock)) << " N/s" << std::endl ;
		}

		generateMoveTree(depth-1);

		undo();
		//undo(m);
	}
}

