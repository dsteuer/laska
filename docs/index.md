Exploring Laska
===============

Laska is a checkers like game invented by the former german chess world
champion Emanuel Lasker. It is said to have been very popular around 1910,
but was supressed by the nazis, because Lasker was a jew.

Laska is played on a 7 x 7 checkers board, with 11 pieces each player.
It is intentionally a very agressive checkers variation and has a lot
of tactical variation. Since there is no fixed set of pieces, indeed each
capture (eventually) creates a piece, it is also quite difficult to
imagine the outcome of variations.


Rules
-----

On the 7x7 checkers board each player starts with eleven soldiers (privates)
on white squares.

A soldier diagonally can move diagonally forward one square.
If the next square is occupied by an opponents piece, and the square behind
that piece is empty, the soldier can jump over the opponent piece to that
empty square behind. In the process it captures the opponent piece, which is put
below the jumping piece. If such a capture is available, it has priority over
non-capturing moves. If there are several captures in a row, they have to
be played till the end. If there a several different capturing moves of
different length, there is no priority. Each, and not necessaryly the longest,
may be chosen.

Captured opponents pieces are stacked below the capturing piece. In case of
re-capturing from such a stack, only the topmost stone is removed.
(In the process lost pieces may be freed again.)


If a soldier reaches the opponent's back row, it is promoted to an officer,
which can move diagonally in all directions.

The game continues until either a player can't move or a player has no pieces
remaining on the board. In both cases, this player loses the game.

Conveniance
-----------

In the following, we call soldiers also privates. And stacked pieces a 'tower'.
Capturing moves may be called jump moves or in short jumps, as well as non
captunring moves may be called steps or step moves.

[Theoretical Results](theo.md)
