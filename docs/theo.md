Exploring Laska
===============


We show some theretical results we found while developing the laska engine.
As there are:



Shortest mate sequence from start position
------------------------------------------

A brute force search over the complete move tree of laska revealed
that the shortest possible laska games have 14 plys.
(Before 16 plys was said to be the shortest possible game.)  

Here is an (primitive) animated version of the first of the games listed below.

![game](7-move-game-1.gif).

We improved on a result of Bruce Willford from 1999 (cf. Book of Angerstein)
The games ending after 14 plies are the following:  

01.  c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 f6xd4 e1-d2 b6-a5 g3-f4 a5xc3xe1 c1-d2 e1xg3 g1-f2 g3xe1xc3 mate
02.  c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 a5xc3xe1 c1-d2 f6xd4 g3-f4 e1xg3 g1-f2 g3xe1xc3 mate
03.  c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 a5xc3xe1 g3-f4 e1xg3 g1-f2 f6xd4 c1-d2 g3xe1xc3 mate
04.  c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 a5xc3xe1 g3-f4 f6xd4 c1-d2 e1xg3 g1-f2 g3xe1xc3 mate
05.  c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 f6xd4 g3-f4 a5xc3xe1 c1-d2 e1xg3 g1-f2 g3xe1xc3 mate
06.  c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 g3-f4 e1xg3 g1-f2 g3xe1 c1-d2 e1xc3 mate
07.  c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 c3-d4 a5xc3xe1 g3-f4 e1xg3 g1-f2 g3xe1 c1-d2 e1xc3 mate
08.  c3-d4 e5xc3 d2xb4 f6-e5 e1-d2 e7-f6 c3-d4 a5xc3xe1 g3-f4 e1xg3 g1-f2 g3xe1 c1-d2 e1xc3 mate
09.  c3-d4 e5xc3 d2xb4 f6-e5 e1-d2 g7-f6 c3-d4 a5xc3xe1 g3-f4 e1xg3 g1-f2 g3xe1 c1-d2 e1xc3 mate
10.  e3-d4 c5xe3 d2xf4 b6-c5 c1-d2 a7-b6 e3-d4 g5xe3xc1 a3-b4 c1xa3 a1-b2 a3xc1 e1-d2 c1xe3 mate
11.  e3-d4 c5xe3 d2xf4 b6-c5 c1-d2 c7-b6 e3-d4 g5xe3xc1 a3-b4 c1xa3 a1-b2 a3xc1 e1-d2 c1xe3 mate
12.  e3-f4 g5xe3 d2xf4 c5-d4 e3xc5 b6xd4 c1-d2 f6-g5 a3-b4 g5xe3xc1 e1-d2 c1xa3 a1-b2 a3xc1xe3 mate
13.  e3-f4 g5xe3 d2xf4 f6-g5 c1-d2 c5-d4 e3xc5 g5xe3xc1 e1-d2 b6xd4 a3-b4 c1xa3 a1-b2 a3xc1xe3 mate
14.  e3-f4 g5xe3 d2xf4 f6-g5 c1-d2 c5-d4 e3xc5 g5xe3xc1 a3-b4 c1xa3 a1-b2 b6xd4 e1-d2 a3xc1xe3 mate
15.  e3-f4 g5xe3 d2xf4 f6-g5 c1-d2 c5-d4 e3xc5 g5xe3xc1 a3-b4 b6xd4 e1-d2 c1xa3 a1-b2 a3xc1xe3 mate
16.  e3-f4 g5xe3 d2xf4 f6-g5 c1-d2 c5-d4 e3xc5 b6xd4 a3-b4 g5xe3xc1 e1-d2 c1xa3 a1-b2 a3xc1xe3 mate
17.  e3-f4 g5xe3 d2xf4 f6-g5 c1-d2 e7-f6 e3-d4 g5xe3xc1 a3-b4 c1xa3 a1-b2 a3xc1 e1-d2 c1xe3 mate
18.  e3-f4 g5xe3 d2xf4 f6-g5 c1-d2 g7-f6 e3-d4 g5xe3xc1 a3-b4 c1xa3 a1-b2 a3xc1 e1-d2 c1xe3 mate

The latter 7 differ only in move order and/or symmetry to d - column from the first.


Maximum size of a tower
-----------------------

It has been proven for a long time, that it is impossible to get a single tower
at the end of the game. At least, there are two towers remaining.
We proved that this is the only restriction.

A listing of a 47 moves game, that results in a 21 stone giant tower and
a single second one:

c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 e1xc3 c7-d6 g3-f4 e5xg3xe1 b4-a5 c5-d4
a5xc7 d6-c5 c3xe5 f6xd4 a3-b4 e1xc3xa5 b2-c3 d4xb2 a1xc3 e5-f4 c3-b4 a5xc3xa1
c7-b6 a1xc3xe1 b6xd4 a7-b6 c1-d2 e1xc3xe5 g1-f2 d4-e3 f2xd4xf6 e7-d6 e5xc7
g7xe5 c7xa5xc3 e5-d4 c3xe5xg3 e3-f2 g3xe5xg7 g5-f4 g7xe5xg3xe1 f2-g1 d4-c5
g1-f2 e1xg3
resulting tower is XxxXxxxxxxxOooooooooO

Here is an animation of the

![game](max-tower.gif).


First occurrence of /n/ - stones tower

 n | plies | tower            | plies
---|-------|------------------|------
 1 |  1 | o                   | c3-b4
 2 |  2 | ox                  | c3-b4 a5xc3
 3 |  5 | xxo                 | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6
 4 | 10 | oooX                | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 g1-f2 e1xg3
 5 | 10 | ooooX               | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5
 6 | 10 | oooooX              | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 c3-d4 e1xc3xe5xg3
 7 | 12 | ooooooX             | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 g1-f2 e1xg3xe5 c3-d4 e5xc3xe1
 8 | 14 | oooooooX            | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 c3-d4 e1xc3xe5xg3 g1-f2 g3xe1xc3
 9 | 16 | ooooooooX           | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 c3-d4 e1xc3xe5xg3 g1-f2 g3xe1xc3 c1-d2 c3xe1
10 | 19 | xxxxxxxxxO          | c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 c1xe3 g5-f4 e3xg5 e5-f4 b4xd6 e7xc5 g5xe7 c5-d4 e7xc5xe3xg5 g7-f6 g5xe7xc5 c7-d6 c5xe7
11 | 20 | ooooooooooX         | e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 a5-b4 c3xa5 e5xc3xe1 g3-f4 e1xg3xe5 g1-f2 c5-b4 a3xc5 e5-d6 b2-c3 d6xb4xd2xf4 a1-b2 f4-g3 c1-d2 g3xe1xc3xa1
12 | 21 | xxxxXooooooO        | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 c3-d4 e1xc3xe5xg3 c1-d2 c7-b6 g1-f2 g3xe1xc3 b2xd4 f4xd2 a1-b2 c5xe3 c3xe5xc7 a5-b4 c7xa5xc3xe1
13 | 23 | xxxxXoooooooO       | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5 a3-b4 c5xa3 c1-b2 a3xc1 e3-d4 c1xe3xc5 c3-b4 c5xa3 e1-d2 a5-b4 a1-b2 a3xc1xe3 f2xd4 e5xc3 e3xc5xa7 c3-b2 a7xc5xa3xc1
14 | 25 | xxxxxXxxxooooO      | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 c1-d2 a5xc3xe1 e3-d4 e5xc3 b2xd4 c5xe3 c3xa5xc7 a7-b6 c7xe5xc3 e7-d6 c3-b2 g5-f4 b2-c1 e3-d2 c1xe3xg5xe7xc5xa7
15 | 25 | xxxXxxxoooooooO     | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-f4 c7xe5xg7 a7-b6 g7xe5xc7xa5
16 | 27 | xxxxXxxxoooooooO    | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-f4 c7xe5xg7 a7-b6 g7xe5xc7xa5 c5-b4 a5xc3
17 | 27 | xxxxxXxxxoooooooO   | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-d4 c7xe5xg7 a7-b6 g7xe5xc7xa5 c5-b4 a5xc3xe5
18 | 29 | xxxxxxXxxxoooooooO  | c3-d4 e5xc3 d2xb4 d6-e5 b4xd6 e7xc5 a3-b4 c5xa3 e3-d4 g5-f4 c1-d2 a3xc1 c3-b4 c1xe3xc5xa3 e1-d2 b6-c5 a1-b2 a3xc1xe3 f2xd4xb6 a7xc5 e3xg5xe7 a5-b4 e7-f6 c5-d4 f6-e7 e5-f4 e7xc5xa7 g7-f6 a7xc5xe3xg5xe7xc5xa3
19 | 33 | xxxxxxxXxxxoooooooO | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-f4 c7xe5xg7 a7-b6 g7xe5xc7xa5 e7-f6 a5-b4 f4-g3 b4xd6 g5-f4 d6-e7 g3-f2 e7xg5xe3xg1


First occurrence of n - captured stones with a single tower
-----------------------------------------------------------

 n | plies | tower        | plies
---|-------|--------------|---------------
 0 |     1 | o            | c3-b4
 1 |     2 | ox           | c3-b4 a5xc3
 2 |     5 | xxo          | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6
 3 |    10 | oooX         | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 g1-f2 e1xg3
 4 |    10 | ooooX        | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5
 5 |    10 | oooooX       | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 c3-d4 e1xc3xe5xg3
 6 |    12 | ooooooX      | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 g1-f2 e1xg3xe5 c3-d4 e5xc3xe1
 7 |    14 | oooooooX     | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 c3-d4 e1xc3xe5xg3 g1-f2 g3xe1xc3
 8 |    16 | ooooooooX    | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 c3-d4 e1xc3xe5xg3 g1-f2 g3xe1xc3 c1-d2 c3xe1
 9 |    19 | xxxxxxxxxO   | c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 c1xe3 g5-f4 e3xg5 e5-f4 b4xd6 e7xc5 g5xe7 c5-d4 e7xc5xe3xg5 g7-f6 g5xe7xc5 c7-d6 c5xe7
10 |    20 | ooooooooooX  | e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 a5-b4 c3xa5 e5xc3xe1 g3-f4 e1xg3xe5 g1-f2 c5-b4 a3xc5 e5-d6 b2-c3 d6xb4xd2xf4 a1-b2 f4-g3 c1-d2 g3xe1xc3xa1
11 |    22 | oooooooooooX | g3-f4 e5xg3 a3-b4 c5xa3 e3-d4 g5-f4 d2-e3 f4xd2 c1xe3 a3xc1 e3-f4 c1xe3xg5 e1-d2 g3xe1 a1-b2 e1-f2 g1xe3 f4-g3 e3-f4 g5xe3xg1 c3-b4 g1xe3xc1xa3xc5xe3

Listing is complete.


First occurrence of n - stones of same color on top of tower
------------------------------------------------------------

 n | plies | tower        | plies         | comment
---|-------|--------------|---------------|------------------------------
 0 |  2    |              | c3-b4 a5xc3   | first deletion of a tower.
 1 |  1    | o            |               | exists at start position.
 2 |  6    | xx           | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5 | -
 3 | 11    | ooo          | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 c3-d4 e5xc3xe1 c1-d2 e1xc3 b2xd4 | -
 4 | 13    | oooo         | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 b2-c3 e5-d4 c3xe5 | -
 5 | 13    | ooooo        | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 e3-d4 e5xc3 b2xd4 | -
 6 | 15    | oooooo       | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5 a3-b4 c5xa3 c1-b2 a3xc1 e3-d4 c1xe3xc5 c3-d4 c5xe3 f2xd4 | -
 7 | 15    | ooooooo      | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 g1-f2 e1xg3xe5 c3-d4 e5xc3xe1 c1-d2 e1xc3 b2xd4xb6 | -
 8 | 19    | oooooooo     | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5 a3-b4 c5xa3 c1-b2 a3xc1 a1-b2 c1xa3 c3-b4 a3xc5 e3-d4 c5xe3xc1 e1-d2 c1xe3 f2xd4 | -
 9 | 21    | ooooooooo    | g3-f4 e5xg3 c3-d4 a5-b4 d2-c3 b4xd2 e1xc3 g3xe1 c3-b4 e1xc3xa5 b2-c3 b4xd2 a3-b4 a5xc3xe5 e3-f4 e5xg3 c1xe3 g3-f2 d2-c3 f2xd4xb2 a1xc3 | -
10 | 24    | xxxxxxxxxx   | c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 c1xe3 g5-f4 b4xd6 e7xc5 e3xg5xe7 c5-d4 e7xc5xe3 b6-c5 d4xf6 g7xe5 c3-b4 c5-d4 e3xc5xe7xg5 a7-b6 g5-f6 b6-c5 f6xd4xb6 c7xa5xc3 | -

Listing is complete. (11 stones of same color on top of a tower are impossible, due to ruleset.)


Maximum number of possible moves on a position
----------------------------------------------

It is easy to layout a position, where, say black, has
eight officers on the second, forth and six row of the
board, and a single opponent officer in a corner.

Black then has 32 possible moves when it's her turn.

But it was unknown if such a position is reachable by legal play.

The answer is yes!

An example listing of such a (again cooperative) game:

a3-b4 c5xa3 g3-f4 e5xg3 c3-b4 a5xc3 d2xb4 d6-c5 b4xd6 e7xc5 c3-d4 d6-e5 e1-d2
e5xc3xe1 e3-f4 g5xe3 f2xd4 f6-e5 d4xf6 g7xe5 g1-f2 e5-d4 c1-d2 e1xc3 e3-f4
a3xc1 f4-g5 g3xe1 g5xe7 f6-g5 e7-f6 c3-d2 f6-g7 b6-a5 a1-b2 c1xa3 g7-f6 e1-f2
f6-g7 g5-f4 g7-f6 f2-g1 f6-g7 d2-e1 g7-f6 d4-c3 f6-g5 c3-b2 g5xe3 b2-a1 e3-f4
c5-b4 f4-g5 b4-c3 g5-f6 c3-d2 f6-g7 d2-c1 g7-f6 c1-b2 f6-g7 c7-d6 g7-f6 d6-c5
f6-g7 c5-d4 g7-f6 d4-c3 f6-g7 c3-d2 g7-f6 d2-c1 f6-g7 c1-d2 g7-f6 d2-e3 f6-g7
e3-f2 g7-f6 a5-b4 f6-g7 b4-c3 g7-f6 c3-d2 f6-g7 d2-c1 g7-f6 c1-d2 f6-g7 d2-e3
g7-f6 a7-b6 f6-g7 b6-a5 g7-f6 a5-b4 f6-g7 b4-c3 g7-f6 c3-d2 f6-g7 d2-c1 g7-f6
a3-b4 f6-g7 b4-a5 g7-f6 a5-b6 f6-g7 b2-a3 g7-f6 a3-b4 f6-g7 a1-b2 g7-f6 c1-d2
f6-g7 d2-c3 g7-f6 c3-d4 f6-g7 d4-c5 g7-f6 c5-d6 f6-g7 e1-d2 g7-f6 e3-d4 f6-g7
f2-g3 g7-f6 g3-f4 f6-g7 g1-f2 g7-f6

Animated ![max-possible-moves-game](32-possible-moves.gif)


(Remark: It may still be possible to extend this to 35 possible moves.)

Listing of first occurrence of /n/ - possible non capturing moves

 n | plies | moves | possible moves
---|-------|-------|---------------
 1 | 10 | c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 f6xd4 c1-d2 b6-a5 g3-f4 a5xc3 | f2-g3
 2 |  8 | c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 f6xd4 e1-d2 b6-a5 | e3-f4 f2-g3
 3 |  5 | e3-d4 c5xe3 d2xf4 a5-b4 c3xa5 | e5-d4 b6-c5 d6-c5
 4 |  3 | c3-b4 a5xc3 b2xd4 | e5-f4 c5-b4 g5-f4 b6-a5
 5 |  2 | g3-f4 e5xg3 | c3-b4 e3-d4 a3-b4 c3-d4 e3-f4
 6 |  4 | c3-b4 a5xc3 d2xb4 b6-a5 | e1-d2 c1-d2 e3-d4 g3-f4 c3-d4 e3-f4
 7 |  5 | g3-f4 e5xg3 e3-d4 c5xe3 f2xd4 | a5-b4 g5-f4 g3-f2 b6-c5 d6-e5 d6-c5 f6-e5
 8 |  6 | c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5 | c1-b2 a1-b2 c3-b4 e3-d4 g3-f4 a3-b4 c3-d4 e3-f4
 9 |  8 | e3-d4 c5xe3 d2xf4 b6-c5 c3-d4 e5xc3 b2xd4xb6 a7xc5 | c1-b2 e1-d2 a1-b2 c1-d2 c3-b4 e3-d4 a3-b4 c3-d4 f4-e5
10 |  9 | e3-d4 c5xe3 f2xd4 d6-c5 g3-f4 e5xg3 d4-e5 f6xd4xf2 g1xe3 | c7-d6 e7-f6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5
11 | 12 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 c3-d4 e5xc3 b2xd4xb6 a7xc5 f2-e3 f6-e5 | c1-b2 e1-d2 g1-f2 a1-b2 c1-d2 e1-f2 e3-d4 g3-f4 c3-d4 e3-f4 g5-f6
12 | 13 | g3-f4 e5xg3 c3-b4 a5xc3 d2xb4 f6-e5 e1-d2 g3xe1 e3-f4 e5xg3 b4-a5 d6-e5 d2-e3 | c7-d6 e7-f6 e7-d6 g7-f6 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 g3-f2 e1-d2 e1-f2
13 | 13 | e3-d4 c5xe3 f2xd4 d6-c5 g3-f4 e5xg3 e1-f2 g3xe1 g1-f2 e1xg3 d4-e5 f6xd4xf2 d2-e3 | c7-d6 e7-f6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 f2-g1 f2-e1 g3-f4
14 | 16 | e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 d2-e3 e7-d6 g5xe7 g7-f6 e7xg5 c5-b4 a3xc5xe7 b6-c5 d4xb6 a7xc5 | e1-d2 g1-f2 c1-d2 e1-f2 c3-b4 e3-d4 g3-f4 c3-d4 e3-f4 g5-f6 b2-a3 e7-f6 e7-d6 g5-f4
15 | 15 | c3-b4 a5xc3 d2xb4 b6-a5 g3-f4 e5xg3 c1-d2 f6-e5 e3-d4 c5xe3xc1 b4-c5 d6xb4xd2 e1xc3 g3xe1 d2-e3 | a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 e1-d2 c1-d2 e1-f2
16 | 17 | c3-b4 a5xc3 d2xb4 b6-a5 c1-d2 a7-b6 e3-d4 c5xe3xc1 f2-e3 e5-d4 e3xc5xa7 f6-e5 e1-d2 c1xe3 b4-c5 d6xb4xd2 b2-c3 | c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 e3-f2 d2-e1 d2-c1 e3-d4 e3-f4
17 | 19 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 g3-f4 e5xg3 c1-d2 f6-e5 c3-d4 c5xe3xc1 e1-d2 a5xc3xe1 a3xc5 d6xb4 b2-a3 b4-c3 f2-e3 | a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 c5-d4 e5-f4 c5-b4 e5-d4 c3-d2 c3-b2 g3-f2 c1-b2 e1-d2 c1-d2 e1-f2
18 | 19 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 d6-c5 c1-d2 c5-d4 e3xc5 g5xe3xc1 c3-d4 f6-g5 e1-d2 g3xe1 g1-f2 e1xc3xe5 f2-e3 b6xd4xf2 b2-c3 | a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 c5-b4 e5-d4 f4-g3 f4-e3 f2-g1 f2-e1 c1-b2 c1-d2 e5-d6 e5-f6
19 | 21 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 g3-f4 e5xg3 c1-d2 f6-e5 c3-d4 c5xe3xc1 e1-d2 g3xe1 b2-c3 c1xe3 b4-c5 d6xb4xd2 a1-b2 d2-c1 b2-c3 | a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 e3-f2 e3-d2 c1-b2 e1-d2 c1-d2 e1-f2 e3-d4 e3-f4
20 | 23 | c3-b4 a5xc3 d2xb4 b6-a5 g3-f4 e5xg3 e1-d2 g3xe1 c3-d4 e1xc3xe5 e3-d4 c5xe3 g1-f2 e3xg1 b2-c3 g1-f2 c1-b2 g5-f4 b4-c5 d6xb4xd2 b2-c3 f6-g5 a1-b2 | f2-e3 e5-d6 f2-g3 e5-f6 d2-e1 f2-g1 f4-g3 a5-b4 c5-d4 a7-b6 c7-d6 e7-f6 d2-c1 f2-e1 f4-e3 c5-b4 e5-d4 c7-b6 e7-d6 g7-f6
21 | 25 | c3-b4 a5xc3 d2xb4 b6-a5 c3-d4 e5xc3 b2xd4xb6 a7xc5 e1-d2 f6-e5 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 a1-b2 a5xc3xa1 c1-d2 a1-b2 e3-d4 c5xe3xc1 a3xc5xa7 c1-d2 a7xc5 d6xb4 g1-f2 | b2-a3 d2-c3 e5-d6 b2-c3 d2-e3 e5-f6 b2-c1 d2-e1 b4-c3 c5-d4 e5-f4 c7-d6 e7-f6 b2-a1 d2-c1 b4-a3 e5-d4 g5-f4 c7-b6 e7-d6 g7-f6
22 | 25 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 c1-d2 e5-d4 c3xe5 d6-c5 e3-d4 c5xe3xc1 e1-d2 g3xe1 g1-f2 g5xe3xg1 d2-c3 e1-d2 c3-d4 g1-f2 d4-c5 b6xd4 a3-b4 c1xa3xc5 a1-b2 | d2-c3 f2-e3 c5-b6 d2-e3 f2-g3 c5-d6 d2-e1 f2-g1 d4-e3 f4-g3 a5-b4 a7-b6 c7-d6 e7-f6 d2-c1 f2-e1 d4-c3 f4-e3 c5-b4 c7-b6 e7-d6 g7-f6
23 | 27 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 c3-d4 a5xc3xe1 b2-c3 b4xd2 a1-b2 e5xc3xa1 g3-f4 e1xg3xe5 g1-f2 a1-b2 e3-d4 c5xe3xg1 c1xe3 g1-f2 e3-f4 g5xe3xc1 a3-b4 c1-d2 b4-a5 e5-d4 a5xc7 | b2-a3 d2-c3 f2-e3 d4-c5 b2-c3 d2-e3 f2-g3 d4-e5 b2-c1 d2-e1 f2-g1 d4-e3 f4-g3 d6-e5 f6-g5 a7-b6 b2-a1 d2-c1 f2-e1 d4-c3 f4-e3 d6-c5 f6-e5
24 | 29 | g3-f4 e5xg3 a3-b4 c5xa3 e3-f4 g5xe3 d2xf4 b6-c5 e1-d2 g3xe1 f4-g5 e1-f2 e3-f4 f6-e5 g1xe3 e5xg3xe1 e3-d4 c5xe3xg1 c3xe5 d6xf4 d2-c3 g1-f2 c1-d2 a3xc1 c3-d4 c1xe3xc5 a1-b2 e1-d2 b2-a3 e7xg5 | a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 c5-b4 e5-d4 f4-g3 f4-e3 d2-e1 f2-g1 d2-c1 f2-e1 c5-b6 e5-d6 c5-d6 e5-f6 d2-c3 f2-e3 d2-e3 f2-g3
25 | 29 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 e3-d4 c5xe3 g1-f2 e3xg1 d2-e3 d6-c5 c1-d2 g1-f2 e3-d4 c5xe3xc1 a3-b4 c1xa3xc5 f4-e5 f6xd4xb2 a1xc3 e1-d2 c3-b4 a5xc3xa1 b2-c3 a1-b2 c3xa5 | a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 b2-c1 d2-e1 f2-g1 b2-a1 d2-c1 f2-e1 c5-b6 c5-d6 b2-a3 d2-c3 f2-e3 b2-c3 d2-e3 f2-g3



Maximum number of possible capturing moves on a position
--------------------------------------------------------

Listing of first occurrence of /n/ possible capturing moves

 n | plies | moves | possible moves
---|-------|-------|---------------
 1 |  2 | c3-b4 | a5xc3
 2 |  3 | c3-b4 a5xc3 | d2xb4 b2xd4
 3 |  5 | e3-d4 c5xe3 d2xf4 e5-d4 e3xc5 | g5xe3 b6xd4 d6xb4xd2
 4 |  7 | e3-d4 c5xe3 d2xf4 e5-d4 e3xc5 g5xe3 g3xe5 | b6xd4 d6xf4 d6xb4xd2 f6xd4
 5 | 11 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 a5xc3xe1 c1-d2 e1xc3 b2xd4xb6 | a7xc5 c7xa5 d6xf4xd2 f6xd4xb2 b4xd2
 6 | 11 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 f2-e3 e5-d4 c3xe5 a5xc3 b2xd4xb6 | b4xd2 d6xf4xd2 a7xc5 f6xd4xb2 f6xd4xf2 c7xa5
 7 | 15 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 c1-d2 e7-f6 a3-b4 b6-c5 e3-d4 c5xe3xc1 e1-d2 g3xe1 c3-d4 | c1xa3xc5xe3xc1 e1xc3 c1xe3xc5xa3xc1 a5xc3 e5xg3 e5xc3 g5xe3
 8 | 17 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 c1-d2 e7-f6 a3-b4 b6-c5 e3-d4 c5xe3xc1 e1-d2 g3xe1 g1-f2 c1xa3xc5 c3-d4 | e1xc3 e1xg3 c5xe3xc1 c5xe3xg1 e5xg3 e5xc3 g5xe3xc1 g5xe3xg1
 9 | 21 | c3-b4 a5xc3 b2xd4 e5-f4 g3xe5 d6xf4 c1-b2 f4-g3 e3-f4 g5xe3xc1 a3-b4 c5xa3 f2-e3 b6-a5 g1-f2 c1-d2 c3-b4 d2xf4 e1-d2 a3xc1 a1-b2 | c1xa3xc5xe3xc1 c1xa3xc5xe3xg1 c1xe3xg1 c1xe3xc5xa3xc1 a5xc3xa1 a5xc3xe1 g3xe1 e5xc3xa1 e5xc3xe1
10 | 21 | e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 c7-b6 c3-d4 e5xc3xe1 b2-c3 d6-e5 f4xd6 e1-d2 a3-b4 d2xf4 g3xe5 c5xa3 c1-d2 f6xd4xb2 a1xc3 a3xc1 c3-d4 | c1xa3 e5xc7 c1xe3xc5 e5xg3xe1xc3xa1 e5xg3xe1xc3xe5xg3 e5xg3xe1xc3xe5xc7 e5xc3xa1 e5xc3xe1xg3xe5xc7 g5xe3 e7xc5xe3
11 | 24 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 c3-b4 a5xc3 b2xd4 e1-f2 a1-b2 f6-e5 d4xf6 f2xd4 c3xe5 c5xe3 a3-b4 b6-a5 g1-f2 e3xg1 e5-d6 e1xg3 | c7xe5xc3xa1 c7xe5xc3xe1 c7xe5xg3 e7xc5xa3 e7xc5xe3 g7xe5xc3xa1 g7xe5xc3xe1 g7xe5xg3 a5xc3xa1 a5xc3xe1 g5xe3
12 | 23 | e3-d4 c5xe3 d2xf4 b6-c5 c1-d2 a7-b6 e3-d4 c5xe3xc1 f2-e3 c1-d2 a3-b4 d6-c5 f4xd6 d2xf4 g3xe5 c5xa3xc1 e1-d2 f6xd4xb2 a1xc3 c1xa3 c3-d4 e5xg3 g1-f2 | g3xe5xc3xa1 g3xe5xc3xe1xg3xe5 a3xc1xe3xg1 a3xc1xe3xc5 c7xe5xc3xa1 c7xe5xc3xe1 g3xe1xc3xa1 g3xe1xc3xe5xg3 g5xe3xc1 g5xe3xg1 e7xc5xe3xc1 e7xc5xe3xg1
13 | 24 | e3-d4 c5xe3 d2xf4 b6-c5 c3-b4 a5xc3 b2xd4xb6 c7xa5 c3-d4 e5xc3 a3-b4 d6-e5 f4xd6 e7xc5xa3 e3-d4 a5-b4 d4-c5 b6xd4 c1-d2 a3-b2 d2-e3 c3-d2 e3xc5xe7 g5-f4 | e1xc3xa5 e1xc3xe5xc7 g3xe5xc7 a1xc3xa5 a1xc3xe5xc7 e7xg5xe3xc1xa3xc5xe3 e7xg5xe3xc1xa3xc5xe7 e7xg5xe3xc5xa3xc1xe3 e7xg5xe3xc5xe7 e7xc5xa3xc1xe3xc5 e7xc5xa3xc1xe3xg5xe7 e7xc5xe3xc1xa3xc5 e7xc5xe3xg5xe7
14 | 25 | g3-f4 e5xg3 c3-d4 d6-e5 d2-c3 c7-d6 c1-d2 a5-b4 c3xa5xc7 e5xc3 d2xb4 c5-d4 e3xc5 d6-e5 e1-d2 g3xe1 c7-d6 e1-f2 g1xe3 e5-f4 c3-d4 f4-g3 c5-b6 g3xe1 e3-f4 | e1xc3xa5xc7xe5xc3 e1xc3xa5xc7xe5xg3xe1 e1xc3xe5xg3xe1 e1xc3xe5xc7xa5xc3 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5 a7xc5xe3xc1 a7xc5xe3xg1 g5xe3xc1 g5xe3xg1 e7xc5xe3xc1 e7xc5xe3xg1
15 | 27 | c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 e1xc3 c5-d4 b4-a5 b6-c5 g3-f4 g5xe3 d2xf4xd6 c7xe5 c3-b4 f6-g5 c1-d2 e5-f4 a5-b6 d4-c3 b2xd4 e7-f6 a1-b2 f4-g3 e3-f4 g3xe1 g1-f2 | e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xg3xe1 e1xc3xe5xg3xe1 e1xc3xe5xc7xa5xc3xa1 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5 c5xe3xc1 c5xe3xg1 g5xe3xc1 g5xe3xg1
16 | 26 | c3-b4 a5xc3 d2xb4 c5-d4 e3xc5 b6xd4 e1-d2 c7-b6 g3-f4 e5xg3xe1 c3xe5xc7 c5xe3 d2xf4 e1-d2 c7xa5 e7-d6 e3-d4 g5xe3 d4-e5 f6xd4 a5-b6 a7xc5 b4-a5 e3-f2 a5xc7 c5-b4 | g1xe3xc5xa7 g1xe3xc5xe7 g1xe3xg5 c1xe3xc5xa7 c1xe3xc5xe7 c1xe3xg5 a3xc5xa7 a3xc5xe7 c7xe5xc3xe1xg3xe5 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5 c7xa5xc3xe1xg3xe5xc3 c7xa5xc3xe1xg3xe5xc7xa5 c7xa5xc3xe5xg3xe1xc3 c7xa5xc3xe5xc7xa5
17 | 29 | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 a3-b4 g7-f6 e1-d2 g3xe1 e3-d4 g5xe3 b2-a3 f6-g5 d4xf6 f4-g3 d2xf4 e1-d2 a1-b2 d6-c5 b4xd6 d2xb4 a3xc5 b6xd4xf2 g1xe3 g3xe1 e3-d4 c5xa3 c1-d2 | c7xe5xc3xa1 c7xe5xg3 e7xc5xe3xc1 e7xc5xe3xg1 a5xc3xa1 g5xe3xc1 g5xe3xg1 a3xc1xe3xg1 a3xc1xe3xc5xa3 e1xc3xa1 e1xc3xe5xg3xe1 e1xc3xe5xg7 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xg7 a3xc5xe3xc1xa3xc5 a3xc5xe3xg1
18 | 29 | g3-f4 e5xg3 c3-d4 a5-b4 d4-e5 d6xf4 d2-c3 f4xd2 c3xa5 c7-d6 a5xc7 c5-d4 e1xc3xe5 f6xd4 b2-c3 g3xe1 a1-b2 d6-c5 c7-b6 e1-f2 g1xe3 e5-f4 c3xe5 e7-f6 e5-d6 f4-g3 e3-f4 g3xe1 a3-b4 | c5xe3xg1 c5xa3 g5xe3xg1 e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xc3xe1xg3xe5 e1xc3xa5xc7xe5xg3xe1xc3xa1 e1xc3xa5xc7xe5xg3xe1xc3xe5 e1xc3xe5xg3xe1xc3xa1 e1xc3xe5xg3xe1xc3xa5xc7xe5 e1xc3xe5xc7xa5xc3xa1 e1xc3xe5xc7xa5xc3xe1xg3xe5 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5
19 | 28 | c3-b4 a5xc3 d2xb4 c5-d4 e3xc5 b6xd4 e1-d2 c7-b6 g3-f4 e5xg3xe1 c3xe5xc7 c5xe3 d2xf4 e1-d2 c7xa5 e7-d6 e3-d4 g5xe3 d4-e5 f6xd4 a5-b6 a7xc5 b4-a5 g7-f6 a5xc7 e3-f2 c7xa5 c5-b4 | g1xe3xc5xa7 g1xe3xc5xe7 g1xe3xg5xe7 c1xe3xc5xa7 c1xe3xc5xe7 c1xe3xg5xe7 a3xc5xa7 a3xc5xe7 a5xc7xe5xc3xe1xg3xe5xg7 a5xc7xe5xc3xa5 a5xc7xe5xg3xe1xc3xa5 a5xc7xe5xg3xe1xc3xe5xg7 a5xc7xe5xg7 a5xc3xe1xg3xe5xc3 a5xc3xe1xg3xe5xc7xa5 a5xc3xe1xg3xe5xg7 a5xc3xe5xg3xe1xc3 a5xc3xe5xc7xa5 a5xc3xe5xg7
20 | 29 | g3-f4 e5xg3 c3-d4 a5-b4 d4-e5 d6xf4 b2-c3 c7-d6 c3xa5xc7 d6-e5 a1-b2 e5-d4 c7-b6 d4-c3 d2xb4xd6 f4xd2 e1xc3 g3xe1 b6-c5 f6-e5 a3-b4 e1-f2 g1xe3 e5-f4 c3-d4 f4-g3 e3-f4 g3xe1 c5-b6 g1xe3 | a7xc5xa3 a7xc5xe3xg1 e7xc5xa3 e7xc5xe3xg1 g5xe3xg1 e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xc3xe1xg3xe5 e1xc3xa5xc7xe5xg3xe1xc3xa1 e1xc3xa5xc7xe5xg3xe1xc3xe5 e1xc3xe5xg3xe1xc3xa1 e1xc3xe5xg3xe1xc3xa5xc7xe5 e1xc3xe5xc7xa5xc3xa1 e1xc3xe5xc7xa5xc3xe1xg3xe5 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5

Maximum stones captured with a single move
------------------------------------------

first occurence of n - capture move at m - plies

 n | plies  | moves 
---|--------|-------
 1 |  2     | c3-b4 a5xc3 
 2 |  6     | c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 
 3 | 10     | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 c3-d4 e1xc3xe5xg3 
 4 | 12     | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 c3-d4 a7-b6 e1-d2 g3xe1 g1-f2 e1xc3xe5xg3xe1 
 5 | 16     | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 b2-a3 e1-f2 c3-d4 f2-e1 g1-f2 e1xc3xe5xg3xe1xc3 
 6 | 18     | g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 b2-a3 e1-f2 c3-d4 c7-d6 a1-b2 f2-e1 g1-f2 e1xc3xe5xg3xe1xc3xa1 
 7 | 22     | e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 c7-b6 c3-b4 a5xc3xe1 b2-c3 c5-b4 c3xa5xc7 d6-c5 f4xd6 e1-d2 a3-b4 d2xf4 g3xe5 f6xd4 c7-b6 g5xe3 c1-d2 e5xg3xe1xc3xa5xc7xe5xg3 
 8 | 25     | g3-f4 e5xg3 c3-d4 g5-f4 e3xg5 c5xe3 f2xd4 d6-e5 b2-c3 e7-d6 g5xe7 a5-b4 d4xf6 g7xe5 e7xc5 b6xd4xf2 c3xa5 c5-d4 g1xe3xc5 e5-f4 c5-d6 c7xe5 f2-g1 g3-f2 g1xe3xc5xe7xg5xe3xc5xe7xg5 
 9 | 26 (*) | a3-b4 c5xa3 c3-d4 e5xc3 d2xb4 g5-f4 g3xe5 d6xf4xd2 c1xe3 a3xc1 c3-d4 a5xc3 f2-g3 c3-b2 a1xc3xa5 e5xc3xa1 g3-f4 a1-b2 f4-e5 f6xd4xf2 d2xf4 c1-d2 e1xc3 b2xd4 g1xe3xc5 c3xe5xg3xe1xc3xe5xg3xe1xc3xe5 
10 | 30 (*) | g3-f4 e5xg3 e3-f4 g5xe3 d2xf4 c5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 e3-d4 c5xe3 b2-a3 f6-g5 a3-b4 e7-f6 c3-d4 a5xc3 d2xb4 e1-d2 g1-f2 e3xg1 c1xe3 g1-f2 d4-e5 f6xd4xb2 f4xd6 f2xd4 a1xc3xe5 e3xc5xa3xc1xe3xc5xa3xc1xe3xc5xe7 
11 | 35     | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 e5-f4 g3xe5xc7 a5-b4 c7xe5 f6xd4 c3xa5xc7 e5xg3xe1 c7-d6 d4xf2 d6xb4 e1xc3xa5 g1xe3 e7-d6 a3xc5xe7 a7-b6 e7-f6 g7xe5 c1-d2 e5-d4 e3xc5xa7 g5-f4 d4-e5 f6xd4 a7-b6 a5xc7 f2-g3 c7-d6 g3xe5xc7xa5xc3xe5xc7xa5xc3xe5xc7xa5

Listing gives examples for optimal short sequences.
(*) up to d-file symmetric move, the only solutions at 26 and 30 plies respectively.


Longest Sequence of forced moves
--------------------------------

Listing of first occurrence of /n/ forced moves in a row. Plies gives last move of such a sequence.

 n | plies | moves
---|-------|------
 1 |  1 | c3-b4 a5xc3
 2 |  5 | e3-f4 g5xe3 d2xf4 e5-d4 c3xe5 f6xd4
 3 |  6 | c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 c5xe3 d2xf4
 4 |  7 | c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 c5xe3 d2xf4 d4xb2
 5 |  8 | c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 c5xe3 d2xf4 d4xb2 a1xc3
 6 | 14 | a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c3-b4 a5xc3 c1-d2 a3xc1 d2xb4 c5xa3 c3xa5xc7 e5xc3 c7xe5
 7 | 15 | a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c3-b4 a5xc3 c1-d2 a3xc1 d2xb4 c5xa3 c3xa5xc7 e5xc3 c7xe5 f6xd4 
 8 | 16 | a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c3-b4 a5xc3 c1-d2 a3xc1 d2xb4 c5xa3 c3xa5xc7 e5xc3 c7xe5 f6xd4 e3xc5
 9 | 21 | a3-b4 c5xa3 c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 e5-f4 g3xe5 d6xf4 c3-b4 e5xc3 d2-e3 f4xd2 c1xe3xc5 b6xd4 b4xd6 c7xe5 d2xb4 d4xb2 a1xc3 a3xc1
10 | 23 | a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c1-d2 a3xc1 a1-b2 c1xa3 c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 c5xe3 f2xd4 a3xc5 c3xa5xc7 e5xc3 c7xe5 d4xf2 g1xe3 f6xd4
11 | 25 | c3-d4 e5xc3 b2xd4 f6-e5 d4xf6 g7xe5 c1-b2 a5-b4 c3xa5 e5-d4 d2-c3 f6-e5 e3-f4 g5xe3 g3-f4 e5xg3 c3xe5 d6xf4 d4xf6 e7xg5 f2xd4 f4xd2 e1xc3 c5xe3xc1 c3xe5xg7 
12 | 26 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 e3-d4 c5xe3 d2xf4 f6-e5 e3-d4 e5xg3 d4-c5 b6xd4 c3xe5 a5xc3 a3xc5 d6xb4 d4xb6 c7xa5 b2xd4 b4xd2 c1xe3 e1xc3 
13 | 27 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 e3-d4 c5xe3 d2xf4 f6-e5 e3-d4 e5xg3 d4-c5 b6xd4 c3xe5 a5xc3 a3xc5 d6xb4 d4xb6 c7xa5 b2xd4 b4xd2 c1xe3 e1xc3 d2xb4 
14 | 31 | e3-d4 c5xe3 d2xf4 b6-c5 c3-d4 e5xc3 b2xd4xb6 a7xc5 c3-b4 a5xc3 a1-b2 c3xa1 c1-d2 f6-e5 d2-c3 e5-d4 c3xe5 c5-d4 e3xc5xa7 g5xe3 a7xc5 d6xb4 d4xb6 c7xa5 f2xd4 f4xd2 e1xc3 c5xe3xc1 a3xc5xa7 d4xb2 b4xd2 
15 | 35 | a3-b4 c5xa3 c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 f6xd4 e3xc5 g5-f4 d4xf6 e7xg5 c5xe7 b6-a5 e7-d6 c7xe5 c3-d4 e5xc3 b2xd4 f4-e3 b4-c5 d6xb4xd2 d4xb6 a7xc5 f2xd4 c5xe3 c3xe5 f6xd4 e1xc3 d4xb2 g1-f2 e3xg1 c1xe3 c3xe1 a1xc3 
16 | 40 | g3-f4 e5xg3 e3-d4 c5xe3 f2xd4 f6-e5 d4xf6 g7xe5 e1-f2 g3xe1 g1-f2 e1xg3 c3-b4 a5xc3xe1 e3-f4 g5xe3 b2-c3 e3-f2 a3-b4 e1-d2 c1xe3 f2-g1 e3-d4 g1-f2 d4-c5 b6xd4xb2 a1xc3 c5xa3xc1xe3 d2xf4 f2xd4 e3xc5 d6xb4xd2 f4xd6 c3xa1 e5xg7 c7xe5xc3 g7xe5xc7 d6xb4 d4xb6 a7xc5 c7xa5
17 | 41 | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 a5-b4 c3xa5xc7 c5-b4 a3xc5 d6xb4 e3-d4 e5xc3xe1 c7-b6 c5-d4 b2-c3 a7xc5 c3xa5xc7 b6-a5 c7-b6 b4-c3 c1-d2 g5-f4 g3xe5 e1xg3 d2xb4xd6 d4xb2 a1xc3 f6xd4 c5xe3 e7xc5 d4xf6 g7xe5 b6xd4 d6xb4xd2 e3xc1 c3xa1 c1xe3 e5xc3xe1 e3xc5 e1xc3
18 | 42 | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 a5-b4 c3xa5xc7 c5-b4 a3xc5 d6xb4 e3-d4 e5xc3xe1 c7-b6 c5-d4 b2-c3 a7xc5 c3xa5xc7 b6-a5 c7-b6 b4-c3 c1-d2 g5-f4 g3xe5 e1xg3 d2xb4xd6 d4xb2 a1xc3 f6xd4 c5xe3 e7xc5 d4xf6 g7xe5 b6xd4 d6xb4xd2 e3xc1 c3xa1 c1xe3 e5xc3xe1 e3xc5 e1xc3 c5xe3
19 | 43 | e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 a5-b4 c3xa5xc7 c5-b4 a3xc5 d6xb4 e3-d4 e5xc3xe1 c7-b6 c5-d4 b2-c3 a7xc5 c3xa5xc7 b6-a5 c7-b6 b4-c3 c1-d2 g5-f4 g3xe5 e1xg3 d2xb4xd6 d4xb2 a1xc3 f6xd4 c5xe3 e7xc5 d4xf6 g7xe5 b6xd4 d6xb4xd2 e3xc1 c3xa1 c1xe3 e5xc3xe1 e3xc5 e1xc3 c5xe3 d4xf2
20 | 50 | c3-d4 e5xc3 d2xb4 g5-f4 e3xg5 f6-e5 e1-d2 e7-f6 g5xe7 c5-d4 e7xc5xe3 e5-d4 c3xe5 a5xc3xe1 a3xc5 b6xd4 e3-d2 e1xc3 c1xe3 c3-b4 e5-f6 g7xe5 e3-f4 f6-g5 f4xd6 b4-c3 e5-f6 c7xe5 f6-e7 c3-b4 b2-c3 d4xb2 a1xc3xa5 c5xa3xc1 e7xc5 c1xa3 c5xe7 a3xc5 b4xd6xf4 g5xe3 f2xd4 f4xd6xb4 e3xc1 b4xd6 c1xe3 d6xb4 e3xc1 b4xd6 e7xc5 d6xb4 d4xb6
21 | 63 | g3-f4 e5xg3 e3-f4 g5xe3 f2xd4 d6-e5 c3-b4 a5xc3 d2xb4xd6 e7xc5 c3-b4 e5xc3 b4-a5 d4xf2 g1xe3 c5-d4 e3xc5xe7 c3-d2 e1xc3 g3xe1 e7xg5 d6-c5 d2-e3 g7-f6 g5xe7 e1-f2 c3-b4 f2-g3 b4xd6 c7xe5xc3 b2xd4 b6-c5 d4xb6 a7xc5 a5xc7 g3-f2 c7xa5 f2xd4xb2 a5-b4 b2xd4 b4-c3 d4xb2 a3-b4 c5xa3 e7xc5 d6xb4 c1-d2 c3xe1 a1xc3xa5 a3xc1 b4xd6 c1xa3 d6xb4 a3xc1 b4xd6 c1xa3 d6xb4 a3xc1 b4xd6 c1xa3 d6xb4 a3xc5 a5-b6 c5xa7
22 | 75 | e3-f4 g5xe3 f2xd4 f6-g5 d4xf6 g7xe5 e1-f2 e5-d4 c3xe5xg7 d6-e5 g3-f4 e5xc3xe1 g7xe5 e1xg3 e3-d4 g5xe3 e5-d6 c7xe5xc3 b2xd4 g3-f2 a3-b4 c5xa3 c3-b4 a5xc3 d4-e5 d6xf4 a1-b2 c3xa1 b4-a5 e7-f6 a5xc7 f4-g3 c1-b2 a1xc3 c7-d6 a3-b2 d6xf4xd2xb4 f2xd4 c3xa1 f6-e5 b2-c3 d4xb2 a1xc3 a7-b6 g1-f2 g3xe1 b4-a5 e1-f2 a5xc7 f2-e3 c7-d6 e3-f2 d6xf4 f2-e3 f4xd2 e3xc1 c3xe1 c1xa3 e1xc3 a3xc1 c3xe1 c1xa3 e1xc3 a3xc1 c3xe1 c1xa3 e1xc3 d2xb4 c3xa5 a3xc5 a5-b6 c5xa7 b6-c7 a7-b6 c7xa5 b6-c5
23 | 79 | c3-d4 e5xc3 b2xd4 d6-e5 c3-b4 a5xc3 d2xb4xd6 e7xc5 a1-b2 b6-a5 d4xb6 d6xb4xd2 e1xc3 a7xc5 c3-b4 a5xc3xa1 g3-f4 e5xg3xe1 e3-d4 c5xe3 a3xc5xa7 b6-a5 c1-b2 a1xc3 g1-f2 e3xg1 a7-b6 c7-d6 b6-c7 f6-e5 c7-b6 d6-c5 b6xd4xb2 e5-f4 c3-d4 g1-f2 d4-c5 f2-g1 b2-c3 e1-f2 c3xe1xg3xe5 g1xe3xc1 c5-b6 c1xe3xg1 b6-c7 g5-f4 e5xg3 g7-f6 c7-d6 f6-e5 d6xf4 g1-f2 g3xe1 a5-b4 f4-e3 b4-c3 e1-d2 c3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 d2xf4 e3xg5 g3xe5 g5-f6 e5xg7 f6-e7 g7-f6
24 | 80 | c3-d4 e5xc3 b2xd4 d6-e5 c3-b4 a5xc3 d2xb4xd6 e7xc5 a1-b2 b6-a5 d4xb6 d6xb4xd2 e1xc3 a7xc5 c3-b4 a5xc3xa1 g3-f4 e5xg3xe1 e3-d4 c5xe3 a3xc5xa7 b6-a5 c1-b2 a1xc3 g1-f2 e3xg1 a7-b6 c7-d6 b6-c7 f6-e5 c7-b6 d6-c5 b6xd4xb2 e5-f4 c3-d4 g1-f2 d4-c5 f2-g1 b2-c3 e1-f2 c3xe1xg3xe5 g1xe3xc1 c5-b6 c1xe3xg1 b6-c7 g5-f4 e5xg3 g7-f6 c7-d6 f6-e5 d6xf4 g1-f2 g3xe1 a5-b4 f4-e3 b4-c3 e1-d2 c3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 d2xf4 e3xg5 g3xe5 g5-f6 e5xg7 f6-e7 g7-f6 e7xg5
25 | 98 | a3-b4 c5xa3 c3-d4 e5xc3 b2xd4 d6-c5 a1-b2 g5-f4 g3xe5 c5-b4 f2-g3 b6-c5 d4xb6 a7xc5 e3-d4 c5xe3 d2xf4 b4xd2 e1xc3 f6xd4xf2 f4xd6 c7xe5 e3-d4 g7-f6 g1xe3 d6-c5 c3-b4 a5xc3xe1 g3-f4 e1xg3 f4xd6 g3xe1 e5xg7 e1-f2 b2-c3 b4xd2 d4-e5 f2xd4xf6 g7xe5 e7xg5 d6-c7 g5-f4 e5xg3 c5-b4 c7xa5xc3xe1 b4-c3 d2xb4 b6-a5 g3-f2 a5xc3 f2xd4xb2 b4xd2 c1xe3 a3xc1 d2-c3 c1-d2 e1-f2 d2xb4 f4-e5 b4-c3 e3-d4 b2-a1 f6-e7 c3-b4 f2-e3 b4-a5 e5-d6 a1-b2 e7-f6 a5-b6 e3-d2 b6-c5 d4xb6 b2-c3 d2xb4 c3xa5xc7xe5xg7 c5xa7 g7xe5 a7xc5xa3 e5xg7 a3xc5 g7xe5 c5xa3 e5xg7 a3xc5 b4xd6 c5xe7 g7-f6 e7xg5 f6-e5 d6xf4 e5xg3 g5xe3 g3-f2 e3xg1 f2-e1 g1-f2 e1xg3 f2-e3


Officers
--------

As shown below, all 22 soldiers can promote.

But, a little more difficult question is, how many officer towers can be simultaniously on the board.
Brute force search reached a '9 officers at 33 plies'. After that, search gets too time consuming
and new techniques had to be applied.

We started from the opposite direction. The first forced capture in at ply 2, and reduces the number of
towers to 21. Another few more moves an there is a next forces capture of a single stone tower,
and the number is reduces to 20. At this point the data get's uncomprehesible,
cause there are several officers on the board, which can move undlessly around free squares
and without any possibility to make any further promotions.

A good critera to stop the unneccessary searches was needed. 

First each number of officers was searched separately.
A level of required towers was introduced, so it was possible to search for 20 officers, 19 officers,
etc. down to 13 officers separately.
If the number of towers fell below the required level, search was stoped.

Second, a simple offset function was introduced:
Count the number of moves for each soldier until promotion (assuming no resistence on the path).
If an officer moves, this number will not reduce. If there are no more possibilities for
another promotion, then this number may get constant or cyclic.
(Cyclic if there are short pathes of soldiers to capture. Which are started every 2nd oder 4th
ply when deepening search.)

Using this two measures, it is easy to show that there are no 20 officers.

At 19 officers things again get much more difficult.
A lot of positions survived 50 plies and more.
So the search was devided into steps.
First all unique position on ply 50 were generated and sorted by the offset function.
Then the positions with the best few offsets were again searched 25 plies deeper, and so on,
until a position with 19 officers was found.

A last probem was, that the best offsets at the restart points of searches (75, 100 ... plies and so on)
were possibly positions in a dead end (Needing some unneccessary moves to get out there again.)
So different cutting plies were tested, undtil best variations stabilzed.

Finally a best game was found, where the 19th soldier promoted at ply 116.
(With a lot of different move orders possible.)
This is probalby the shortes game possible. At least,
it seems quite hard to find a shorter game.


The same technique was applies for the lesser numbers of officers, and as well some quite hard
to top boundaries esstablished. See listing below.



 

First occurrence of /n/-th officer
----------------------------------

 n | plies | moves | position
---|-------|-------|---------
 1 |  8 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 | o.o.ooX.o|o.o.|o.o.o.|xo..|x.x..x|x.x.x|.x.x.x
 2 | 10 | g3-f4 e5xg3 c3-d4 d6-e5 d2-c3 e7-d6 c1-d2 g5-f4 e3xg5xe7 c5xe3xc1 | o.ooX.o.o|o..o|o.o..ox|..|x..x.|x.x.|x.x.xxO.x
 3 | 13 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 c1-d2 c5xe3xc1 a3xc5xa7 | o.ooX.ooX.o|o..o|...o|..|..x.xo|.x.x|xxO.x.x.x
 4 | 15 | e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 g3-f4 e1xg3 c1-d2 e5xc3xe1 a3-b4 c5xa3xc1 e3xc5xe7 | o.ooX.ooX.o|..|...oooX|..o|...x|x..x|x.x.xxO.x
 5 | 18 | c3-b4 a5xc3 d2xb4 b6-a5 c1-d2 a7-b6 c3-d4 a5xc3 e3-f4 c5xe3xc1 a3xc5xa7 c1xa3 e1-d2 g5xe3xc1 a1-b2 c3xa1 g3-f4 e5xg3xe1 | ooX.ooX.ooX.o|..|oooX...|..|...|.x.x|xxO.x.x.x
 6 | 22 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 c3-d4 e5xc3xe1 b2-c3 c5-d4 e3xc5xa7 d6-e5 a1-b2 e1-d2 c1xe3 e7-d6 c3-d4 a5xc3xa1 a3xc5xe7 e5xc3xe1 e3-f4 g5xe3xc1 | ooX.ooX.ooX.o|..o|...o|..X|...|..x|xxO.x.xxO.x
 7 | 26 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c5-d4 e3xc5 c7-b6 g3-f4 e5xg3xe1 g1-f2 b6xd4 c3xe5xc7 c5xe3xg1 c7-d6 e7xc5 d2-c3 c5-d4 c3xe5xc7 a5xc3 a3xc5xe7 e1-d2 c1xe3 c3xe1 e3-f4 g5xe3xc1 | o.ooX.ooX.ooX|o..|...|.O.X|...|..x|x.xxO.xxO.x
 8 | 29 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 c1-d2 e1xc3 g3-f4 c5xe3 a3xc5xa7 e5xg3xe1 b2xd4 e1-f2 c3-b4 e3-d2 g1xe3 d2-c1 e3-f4 d6-c5 b4xd6 e7xc5xe3xg1 g5xe7 c7xe5xg3xe1 d6-c7 | o.oX.oooX.oooX|..|...|.X.X|...|..|xxO.xO.xxO.x


Maximum number of promotions
----------------------------

Can all private stones promote to officer ?

Again, the answer is a surprinsing yes. For example this 86 plies listing:

e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2
a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2
f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7
b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2
a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 g5-f4 f6-g5 f4-g3 g5-f6 f2-g1 c7-b6 e1-d2
b6-a5 g1-f2 f6-e5 f2-e3 d4xf2 d2xf4xd6 e5xc7 g3xe1 d6-e5 f2xd4xf6 e5xg7 b2-a1
f6-e7 a1-b2 a5-b4 c1-d2 b4-c3 d2xb4 a7-b6 b2-a3 g7-f6 b4-c5 b6xd4xb2 c3xa1
e7-d6 c5xe7xg5 d6-e5 g5xe7 f6-g7 b2-c1

resulting in position OX.XXXX.OX.|..|X...|..|..O.|..|.XOO.OOOOXX.XOO

Animated ![22-promotions](22-promotions.gif)


Listing of first occurrence of /n/ promotions

 n | plies | moves | position
---|-------|-------|---------
 1 |  8 | c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 | o.o.ooX.o|o.o.|o.o.o.|xo..|x.x..x|x.x.x|.x.x.x
 2 | 10 | g3-f4 e5xg3 c3-d4 d6-e5 d2-c3 e7-d6 c1-d2 g5-f4 e3xg5xe7 c5xe3xc1 | o.ooX.o.o|o..o|o.o..ox|..|x..x.|x.x.|x.x.xxO.x
 3 | 13 | c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 c1-d2 c5xe3xc1 a3xc5xa7 | o.ooX.ooX.o|o..o|...o|..|..x.xo|.x.x|xxO.x.x.x
 4 | 15 | e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 g3-f4 e1xg3 c1-d2 e5xc3xe1 a3-b4 c5xa3xc1 e3xc5xe7 | o.ooX.ooX.o|..|...oooX|..o|...x|x..x|x.x.xxO.x
 5 | 18 | c3-b4 a5xc3 d2xb4 b6-a5 c1-d2 a7-b6 c3-d4 a5xc3 e3-f4 c5xe3xc1 a3xc5xa7 c1xa3 e1-d2 g5xe3xc1 a1-b2 c3xa1 g3-f4 e5xg3xe1 | ooX.ooX.ooX.o|..|oooX...|..|...|.x.x|xxO.x.x.x
 6 | 21 | e3-d4 c5xe3 f2xd4 d6-c5 g1-f2 c7-d6 e3-f4 g5xe3xg1 g3-f4 c5xe3 e1-f2 e5xg3xe1 c3xe5xc7 e1xc3 b2xd4 e3-f2 c1-d2 f2-e1 c7-d6 e7xc5xe3xc1 c3xe5xc7 | o.ooOX.oX.ooX|..|o...|..|x...|x.x.x|x.xXooO..x
 7 | 23 | e3-d4 c5xe3 f2xd4 d6-c5 g1-f2 c7-d6 e3-f4 g5xe3xg1 g3-f4 c5xe3 e1-f2 e5xg3xe1 c3xe5xc7 e1xc3 b2xd4 e3-f2 c1-d2 f2-e1 c7-d6 e7xc5xe3xc1 c3xe5xc7 a5-b4 a3xc5xe7 | o.ooOX.oX.ooX|..|...|..|...|x..x|x.xXooO.xxO.x
 8 | 27 | e3-d4 c5xe3 f2xd4 d6-c5 g1-f2 c7-d6 e3-f4 g5xe3xg1 g3-f4 c5xe3 e1-f2 e5xg3xe1 c3xe5xc7 e1xc3 b2xd4 e3-f2 c1-d2 f2-e1 c7-d6 e7xc5xe3xc1 c3xe5xc7 c1-b2 a1xc3 f6-e5 c3-b4 a5xc3xa1 a3xc5xe7 | OoX..oX.ooX|oo..|...|..|..x.|x..|x.xXooO.xXO.x


Different towers after n plies
------------------------------

We calculatetd the following table of different towers after n plies

|plies| number of different towers|
|-----|---------------------------|
| 1   |  2 |
| 2   |  3 |
| 3   |  4 |
| 4   |  4 |
| 5   |  5 |
| 6   |  7 |
| 7   |  8 |
| 8   |  9 |
| 9   | 10 |
|10   | 15 |
|11   | 24 |
|12   | 31 |
|13   | 45 |
|14   | 58 |
|15   | 83 |
|16   |114 |
|17   |158 |
|18   |218 |
|19   |315 |
|20   |444 |

