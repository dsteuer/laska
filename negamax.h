
#if !defined(__NEGAMAX_H)
#define __NEGAMAX_H

#include "movetree.h"

class NegaMax : public MoveTree {
public:
	NegaMax		(const unsigned int board_size = 7);
	NegaMax		(std::string	pos, const unsigned int board_size = 7);
	Move		findBestMove	();

protected:
	int		evaluateMoveTree (unsigned int depth);
	int		value_sum;
};

#endif
