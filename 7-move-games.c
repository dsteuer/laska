
#include "games/laska/position.h"


#include <string>
#include <stdlib.h>
#include <getopt.h>

std::string gl[18][15] = {
	{ "c3-b4", "a5xc3", "d2xb4", "e5-d4", "c3xe5", "f6xd4", "e1-d2", "b6-a5", "g3-f4", "a5xc3xe1", "c1-d2", "e1xg3", "g1-f2", "g3xe1xc3" },
	{ "c3-b4", "a5xc3", "d2xb4", "b6-a5", "e1-d2", "e5-d4", "c3xe5", "a5xc3xe1", "c1-d2", "f6xd4", "g3-f4", "e1xg3", "g1-f2", "g3xe1xc3" },
	{ "c3-b4", "a5xc3", "d2xb4", "b6-a5", "e1-d2", "e5-d4", "c3xe5", "a5xc3xe1", "g3-f4", "e1xg3", "g1-f2", "f6xd4", "c1-d2", "g3xe1xc3" },
	{ "c3-b4", "a5xc3", "d2xb4", "b6-a5", "e1-d2", "e5-d4", "c3xe5", "a5xc3xe1", "g3-f4", "f6xd4", "c1-d2", "e1xg3", "g1-f2", "g3xe1xc3" },
	{ "c3-b4", "a5xc3", "d2xb4", "b6-a5", "e1-d2", "e5-d4", "c3xe5", "f6xd4", "g3-f4", "a5xc3xe1", "c1-d2", "e1xg3", "g1-f2", "g3xe1xc3" },
	{ "c3-b4", "a5xc3", "d2xb4", "b6-a5", "e1-d2", "a7-b6", "c3-d4", "a5xc3xe1", "g3-f4", "e1xg3", "g1-f2", "g3xe1", "c1-d2", "e1xc3" },
	{ "c3-b4", "a5xc3", "d2xb4", "b6-a5", "e1-d2", "c7-b6", "c3-d4", "a5xc3xe1", "g3-f4", "e1xg3", "g1-f2", "g3xe1", "c1-d2", "e1xc3" },
	{ "c3-d4", "e5xc3", "d2xb4", "f6-e5", "e1-d2", "e7-f6", "c3-d4", "a5xc3xe1", "g3-f4", "e1xg3", "g1-f2", "g3xe1", "c1-d2", "e1xc3" },
	{ "c3-d4", "e5xc3", "d2xb4", "f6-e5", "e1-d2", "g7-f6", "c3-d4", "a5xc3xe1", "g3-f4", "e1xg3", "g1-f2", "g3xe1", "c1-d2", "e1xc3" },
	{ "e3-d4", "c5xe3", "d2xf4", "b6-c5", "c1-d2", "a7-b6", "e3-d4", "g5xe3xc1", "a3-b4", "c1xa3", "a1-b2", "a3xc1", "e1-d2", "c1xe3" },
	{ "e3-d4", "c5xe3", "d2xf4", "b6-c5", "c1-d2", "c7-b6", "e3-d4", "g5xe3xc1", "a3-b4", "c1xa3", "a1-b2", "a3xc1", "e1-d2", "c1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "c5-d4", "e3xc5", "b6xd4", "c1-d2", "f6-g5", "a3-b4", "g5xe3xc1", "e1-d2", "c1xa3", "a1-b2", "a3xc1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "f6-g5", "c1-d2", "c5-d4", "e3xc5", "g5xe3xc1", "e1-d2", "b6xd4", "a3-b4", "c1xa3", "a1-b2", "a3xc1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "f6-g5", "c1-d2", "c5-d4", "e3xc5", "g5xe3xc1", "a3-b4", "c1xa3", "a1-b2", "b6xd4", "e1-d2", "a3xc1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "f6-g5", "c1-d2", "c5-d4", "e3xc5", "g5xe3xc1", "a3-b4", "b6xd4", "e1-d2", "c1xa3", "a1-b2", "a3xc1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "f6-g5", "c1-d2", "c5-d4", "e3xc5", "b6xd4", "a3-b4", "g5xe3xc1", "e1-d2", "c1xa3", "a1-b2", "a3xc1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "f6-g5", "c1-d2", "e7-f6", "e3-d4", "g5xe3xc1", "a3-b4", "c1xa3", "a1-b2", "a3xc1", "e1-d2", "c1xe3" },
	{ "e3-f4", "g5xe3", "d2xf4", "f6-g5", "c1-d2", "g7-f6", "e3-d4", "g5xe3xc1", "a3-b4", "c1xa3", "a1-b2", "a3xc1", "e1-d2", "c1xe3" },
};

void print_7_move_games () {

	for (unsigned int j = 0; j<18; j++) {
	std::cout << "game " << j << std::endl;
	Position p("o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x");
	p.print();
	for (unsigned int i = 0; i<14; i++) {
		std::cout << j << " " << i << " ";
		std::cout << gl[j][i] << " ";
		p.moveRecorded(Move(gl[j][i]));
		p.print();
	}
	std::cout << std::endl << std::endl;
	}
	std::cout << "done" << std::endl << std::endl;
}

void usage () {
	std::cout << "7-move-games: just print them.\n";
	exit(0);
}

int main (int argc, char **argv) {
	int c;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{0, 0, 0, 0}
		};

		c = getopt_long (argc, argv, "h?", long_options, &option_index);

		if (c == -1) break;

		switch (c) {
		case 'h': usage();	break;
		case '?': usage();	break;
		default: usage();
		}
	}

	print_7_move_games();

	return 0;
}

