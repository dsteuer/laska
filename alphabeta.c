
#include "alphabeta.h"

#include <iomanip>

// negamax with alpha/beta pruning

AlphaBeta::AlphaBeta (const unsigned int board_size)			: MoveTree (board_size)    	{ } 
AlphaBeta::AlphaBeta (std::string pos, const unsigned int board_size)	: MoveTree (pos, board_size)	{ }

int AlphaBeta::evaluateMoveTree(unsigned int depth, int alpha, int beta) {
	if (!depth) { return evaluatePosition(); }				// end of search depth, return node value

	std::list<Move> l = possibleMoves();

	if (l.empty()) { return -mateValue(tree_depth-depth); }			// no more moves, game over. return mate - (mate in plies)

	for (auto m: l) {
		move(m);
		int v = -evaluateMoveTree(depth-1, -beta, -alpha);
		undo(m);

		if (v >= beta)  { return v;  }
		if (v >  alpha) { alpha = v; }
	}

	return alpha;
}

Move AlphaBeta::findBestMove() {
	std::list<Move> l = possibleMoves();

	if (l.empty())	   return NoMove;					// no more moves, game over.
	if (l.size() == 1) return l.back();					// only one move possible. return at once.

	int	best_value = -maxValue();					// start with worst possible outcome
	Move	best_move  = NoMove;						// and no move

	for (auto m: l) {
		move(m);
		int v = -evaluateMoveTree(tree_depth-1, -maxValue(), maxValue());
		undo(m);

		std::cerr << moveValueString(m, v) << "\t";

		if (v > best_value) {
			best_value = v;
			best_move  = m;
		}
	}
	std::cerr << std::endl;

	return best_move;
}

