<?php

$w  = 60;	// width
$h  = $w/5;	// height of stone
$s  = '';
$p  = '';	// position
$tw = -1;	// square of moving tower
$dx =  0;	// moving tower shift
$dy =  0;	// "

$bg =  0;

if (isset($_REQUEST['s']))  { $s  = $_REQUEST['s'];  }
if (isset($_REQUEST['w']))  { $w  = $_REQUEST['w'];  }
if (isset($_REQUEST['h']))  { $h  = $_REQUEST['h'];  }
if (isset($_REQUEST['b']))  { $bg = 1; }
if (isset($_REQUEST['p']))  { $p  = $_REQUEST['p'];  }
if (isset($_REQUEST['tw'])) { $tw = $_REQUEST['tw']; }
if (isset($_REQUEST['dx'])) { $dx = $_REQUEST['dx']; }
if (isset($_REQUEST['dy'])) { $dy = $_REQUEST['dy']; }

if ($p !== '') { $bg = 1; }

function t2p($i) {
	$sw = 60;
	$row = 2*floor($i/7);
	$col = $i%7;
	if ($col > 3) { $row++; }
	// print "$row.$col ";

	$row = 6*$sw - ($row*$sw) + 80;
	if ($col > 3) { $col = (2*($col-4)*$sw + $sw); }
	else { $col = (2*$col*$sw); }
	// $col += 600 + 25;
	$col += 3*$sw/4 + 5;
	// print "$row.$col<br>";
	return array($row, $col);
}

$turn   = 'x';
$p	= str_replace(".", "|", $p);
$towers = explode('|', $p);


header ('Content-type: image/png');

if ($bg === 1) {
	$im = @imagecreatetruecolor(8*$w+$w/2, 8*$w+$w/2)
	      or die('Cannot Initialize new GD image stream');
	$white = imagecolorallocate($im, 255, 255, 255);
	$black = imagecolorallocate($im,  10,  10,  10);
	// imagefill($im, 0, 0, $white);
	imagefilledrectangle($im, 0, 0, 8*$w+$w/2, 8*$w+$w/2, $white);

	$off = 3*$w/4;
	imagerectangle($im, $off, $off/2, $off + 7*$w, $off/2 + 7*$w, $black);

	$l = "1234567";
	$n = "GFEDCBA";
	for ($i=0; $i<7; $i++) {
		imagestring($im, 5, $w/4, 7*$w-$w/4 -$i*$w,      $l[$i], $black);
		imagestring($im, 5, 7*$w+$w/4 -$i*$w, 8*$w-$w/4, $n[$i], $black);
	}

for($i=0; $i<7; $i++) {
for($j=0; $j<7; $j++) {
        $color = ($i%2) ? (($j%2) ? 'white' : 'black') : (($j%2) ? 'black' : 'white');

	if ($color === 'black') {
		imagefilledrectangle($im, $off + $i*$w, $off/2 + $j*$w, $off + $i*$w + $w , $off/2 + $j*$w + $w, $black);
	}
}
}

if ($p !== '') {
	$text_color = imagecolorallocate($im, 233, 214, 91);
	$white = imagecolorallocate($im, 218, 165,  32);
	$black = imagecolorallocate($im,  93, 71,  139);
for($t = 0; $t < 25; $t++) {
$s = strrev($towers[$t]);	// revert string, draw from bottom to top

$off = t2p($t);
// imagestring($im, 1, 5, 5,  $s, $text_color);
$off[0] -= 12*strlen($s);

$mx = 0;
$my = 0;
if ($t==$tw) {
	$mx = $dx;
	$my = $dy;
}

for($i = 0; $i < strlen($s); $i++) {
	switch ($s[$i]) {
	case 'o': imagefilledrectangle($im, $off[1] +      1 + $mx, $off[0] + ($i*$h)+1    + $my,    $off[1] + $w-12 + $mx, $off[0] + (($i+1)*$h)-2 + $my, $white);		break;
	case 'O': imagefilledrectangle($im, $off[1] +      1 + $mx, $off[0] + ($i*$h)+1    + $my,    $off[1] + $w-12 + $mx, $off[0] + (($i+1)*$h)-2 + $my, $white);
		  imagefilledellipse  ($im, $off[1] + $w/2-5 + $mx, $off[0] + ($i*$h)+$h/2 + $my,              $h- 3 + $mx,                    $h-3 + $my, $black);		break;
	case 'x': imagefilledrectangle($im, $off[1] +      1 + $mx, $off[0] + ($i*$h)+1    + $my,    $off[1] + $w-12 + $mx, $off[0] + (($i+1)*$h)-2 + $my, $black);		break;
	case 'X': imagefilledrectangle($im, $off[1] +      1 + $mx, $off[0] + ($i*$h)+1    + $my,    $off[1] + $w-12 + $mx, $off[0] + (($i+1)*$h)-2 + $my, $black);
		  imagefilledellipse  ($im, $off[1] + $w/2-5 + $mx, $off[0] + ($i*$h)+$h/2 + $my,              $h- 3 + $mx,                    $h-3 + $my, $white);		break;
	default:  imagefilledrectangle($im, $off[1] +      1 + $mx, $off[0] + ($i*$h)+1    + $my,    $off[1] + $w-12 + $mx, $off[0] + (($i+1)*$h)-2 + $my, $white);		break;
	// default:  imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $text_color);	break;
	}
}

}
}
	imagepng($im);
	imagedestroy($im);

	exit();

}

if ($s === '') {
	$im = @imagecreatetruecolor(1, 1)
	      or die('Cannot Initialize new GD image stream');
	$c = imagecolortransparent($im);
	imagefill($im, 0, 0, $c);
	imagecolortransparent($im, $c); 

	imagepng($im);
	imagedestroy($im);

	exit();
}

$im = @imagecreatetruecolor($w, strlen($s) * $h)
      or die('Cannot Initialize new GD image stream');
$text_color = imagecolorallocate($im, 233, 214, 91);
$white = imagecolorallocate($im, 255, 255,   0);
$black = imagecolorallocate($im, 210, 210, 210);
$white = imagecolorallocate($im, 218, 165, 32);
$black = imagecolorallocate($im,  93, 71,  139);


$s = strrev($s);	// revert string, draw from bottom to top

for($i = 0; $i < strlen($s); $i++) {
	switch ($s[$i]) {
	case 'o': imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $white);		break;
	case 'O': imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $white);
		  imagefilledellipse  ($im, $w/2, ($i*$h)+$h/2, $h-3, $h-3,    $black);		break;
		  // imagefilledellipse  ($im, $w/2, ($i*$h)+$h/2, $h-3, $h-3,    $text_color);	break;
	case 'x': imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $black);		break;
	case 'X': imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $black);
		  imagefilledellipse  ($im, $w/2, ($i*$h)+$h/2, $h-3, $h-3,    $white);		break;
	default:  imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $white);		break;
	// 	  imagefilledellipse  ($im, $w/2, ($i*$h)+$h/2, $h-3, $h-3,    $text_color);	break;
	// default:  imagefilledrectangle($im, 1, ($i*$h)+1, $w-2, (($i+1)*$h)-2, $text_color);	break;
	}
}
// imagestring($im, 1, 5, 5,  $s, $text_color);

imagepng($im);
imagedestroy($im);
?>

