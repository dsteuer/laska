
#include "movetree-no.h"
#include "movetree.h"

#include "position.h"

#include <list>
#include <iostream>
#include <sstream>
#include <fstream>

#include <getopt.h>
#include <unistd.h>

/*
 */

int init (string pos) {
	return 0; 
}                 

void compact2readable(string p, std::string moves, unsigned int board_size) {
	Position b(p, board_size);
	b.applyMoves (moves);
	b.print();
}

void usage () {
	cout << "laska options:"										<< endl;
	cout << "	[-A <movedata>]	apply move(s) in <movedata> before starting game"			<< endl;
	cout << "	[-b]		computer plays black. (default is white, white begins)"			<< endl;
	cout << "	[-d <n>]	tree_depth, search depth of possible moves tree"			<< endl;
	cout << "	[-D]		print (compact) position as a text board, optinal given by -p <pos>"	<< endl;
	cout << "	[-g]		generate the possible moves tree"					<< endl;
	cout << "	[-p <p>]	use (compact) position <p> as start position"				<< endl;
	cout << "	[-P] 		debug possible move generator"						<< endl;
	cout << "	[-s]		call special debug function and exit"					<< endl;
	cout << "	[-5]		play 5x5 laska"								<< endl;
	cout << "	[-3]		play 3x3 laska"								<< endl;
	cout << "	[-?|-h]		print this string"							<< endl;
	exit(0);
}

int main (int argc, char **argv) {
	int c;
	string	position		= "o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x",
		str_move		= "",
		to			= "",
		egtb_towers		= "";
	unsigned int depth = 14;

	bool	compact2textboard	= 0,
		evaluate_position	= 0,
		debug_possible_moves	= 0,
		noofficer_search	= 1;
	bool	special_debug_function	= 0;
	int	board_size		= 7;
	unsigned int print_style	= 0,
		max_moves_per_game	= 100,
		number_of_random_games	= 100;
	unsigned int turn		= 0;	// white

	if (std::string(argv[0]).find("laska5"  ) != std::string::npos) { board_size	   = 5; }
	if (std::string(argv[0]).find("laska-no") != std::string::npos) { noofficer_search = 1; } 

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"depth",	 1, 0, 0},
			{"position",	 1, 0, 0},
			{"file",	 1, 0, 0},
			{0, 0, 0, 0}
		};

		c = getopt_long (argc, argv, "aA:Bbd:DeEilL:m:M:p:ghHjPrRsS:t:T:tvw53?", long_options, &option_index);

		if (c == -1) break;

		switch (c) {
		case 'A': str_move = optarg;	break;
		case 'b': turn = 1;		break;	// play as black
		case 'd': depth = atoi(optarg); if (depth <= 0) { cerr << "treedepth <= 0 not an option\n"; return -1; } break;
		case 'D': compact2textboard	= 1;	break;
		case 'e': evaluate_position	= 1;	break;
		case 'm': max_moves_per_game	 = atoi(optarg); if (max_moves_per_game	    <= 0) { cerr << "max_moves_per_game <= 0 not an option\n"; return -1; } break;
		case 'M': number_of_random_games = atoi(optarg); if (number_of_random_games <= 0) { cerr << "number_of_random_games <= 0 not an option\n"; return -1; } break;
		case 'p': position		= optarg;	break;
		case 'P': debug_possible_moves	= 1;	break;
		case 's': special_debug_function= 1;	break;
		case 'S': print_style		= atoi(optarg);	break;
		case 'T': egtb_towers		= optarg;	break;
		case '5': board_size		= 5;	break;
		case '3': board_size		= 3;	break;
		case 'h': usage();	break;
		case '?': usage();	break;
		default: cerr << "what is -" << c << " for" << endl; usage();
		}
	}

	if (optind < argc) {
		cout << "Non Options/Values in ARGV: ";
		while (optind < argc)
			cout << argv[optind++] << ' ';
		cout << endl;
	}

	if (position == "o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x") {	// maybe set another default position, if there was position arg, don't change anything
		if (board_size == 5) {		// on smaller board sizes...
			position = "o|o|o|o|o||||x|x|x|x|x";
		} else if (board_size == 3) {
			position = "o|o||x|x";
		} else if ((std::string(argv[0]).find("mill") != std::string::npos || std::string(argv[0]).find("nmm")!= std::string::npos)) {
			position = "........*........*........";
		}
	}


	// when starting through autoplay.sh (scripted args, programms connect by named pipes, i.e. dump terminal) then shell leaves the quotes around position.
	// remove quotes
	if (position[0] == '"')			position.erase(position.begin());
	if (position[position.size()-1] == '"') position.erase(position.end()-1);

	initializeGame (board_size);

	if (special_debug_function) {
		init(position);
		return 0;
	}

	if (compact2textboard) {
		compact2readable(position, str_move, board_size);
		return 0;
	}

	if (print_style == 3) {
		Position p(position);
		p.print(print_style);
		return 1;
	}

	if (evaluate_position) {
		Position p(position);
		p.applyMoves (str_move);
		if (turn) p.setTurn(turn);
		cout << p.evaluatePosition () << endl;
	}

	if (debug_possible_moves) {
		cerr << "possibleMoves for " << (turn ? "black" : "white") << endl;
		Position p(position);
		p.applyMoves (str_move);
		if (turn) p.setTurn(turn);
		p.print();
		for (const auto& m: p.possibleMoves()) { cout <<  m.toString() << ' '; } cout << endl;

		return 0;
	}

	if (noofficer_search) {
		MoveTreeNoOfficer mt(position, board_size);
		mt.applyMoves (str_move);
		if (turn) mt.setTurn(turn);		// if not otherwise stated program take white, see below
		mt.setTreeDepth(depth);
		mt.generateMoveTree(depth);
	}

	return 0;
}

