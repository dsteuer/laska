CXX=g++ 
GAME=laska
# HEXIOOPT= -D__HEX_IO
HEXIOOPT=
GAMEOPTS= ${HEXIOOPT} -DUSE_POSITION_KEY -Igames/${GAME}
# GAMEOPTS= ${HEXIOOPT} -Igames/${GAME}

CXXFLAGS= -g -Wall ${GAMEOPTS} -std=c++0x
#CXXFLAGS= -g -Wall ${GAMEOPTS} -std=c++0x -DNDEBUG
#CXXFLAGS= -g -Wall ${GAMEOPTS} -std=c++0x -DNDEBUG -pg
OPTFLAGS=-O3 -funroll-loops -march=native
#OPTFLAGS= -Ofast -funroll-loops -march=native -fsanitize=undefined
OBJ_STARTUP=games/startup/position.o games/startup/move.o
OBJ_NMM=games/nmm/position.o games/nmm/ring.o games/nmm/move.o
# OBJ_7x7=games/laska/config.o games/laska/position-no.o games/laska/position.o games/laska/tower.o games/laska/bitb.o games/laska/move.o
OBJ_7x7=games/laska/liblaska.a
OBJ_FILES=main.o evaltest.o tt.o mc.o mtdf.o pvs.o alphabetaq-id.o alphabetaqtt.o alphabetaq.o alphabeta.o negamax.o movetree.o
#LDFLAGS= -pg
LDFLAGS=
TOBJ_FILES=test.o ${OBJ_7x7}

ifeq "$(MAKECMDGOALS)" "noofficer"
	CXXFLAGS += -DNOEVAL -DNDEBUG -D__MT_NOOFFICER
	GAMEOPTS = -Igames/${GAME}
endif

ifeq "$(MAKECMDGOALS)" "search"
	CXXFLAGS += -DNDEBUG -D__MT_JUMPSEARCH -D__MT_MAXMOVESEARCH -D__MC_SPECIAL
	GAMEOPTS = -Igames/${GAME}
endif

ifeq "$(MAKECMDGOALS)" "searchtt"
	CXXFLAGS += -DNDEBUG -D__MT_JUMPSEARCH -D__MT_MAXMOVESEARCH -D__MC_SPECIAL -DUSE_POSITION_KEY
endif

ifeq "$(MAKECMDGOALS)" "gen-leaves"
	CXXFLAGS += -DNDEBUG -D__MT_LEAVES -D__MT_LIST_COMPACT -D__MT_LIST_MOVES
	GAMEOPTS = -Igames/${GAME}
endif

ifeq "$(MAKECMDGOALS)" "gen-leaves-tt"
	# CXXFLAGS += -DNDEBUG -D__SEARCH -D__MT_LEAVES -D__MT_LIST_COMPACT -D__MT_LIST_MOVES -DUSE_POSITION_KEY
	CXXFLAGS += -DNDEBUG -D__MT_LEAVES -D__MT_LIST_COMPACT -D__MT_LIST_MOVES -DUSE_POSITION_KEY
endif

ifeq "$(MAKECMDGOALS)" "gen-tree"
	CXXFLAGS += -DNDEBUG -D__MT_ALL_NODES -D__MT_LIST_COMPACT -D__MT_LIST_MOVES
	GAMEOPTS = -Igames/${GAME}
endif

ifeq "$(MAKECMDGOALS)" "gen-tree-tt"
	CXXFLAGS += -DNDEBUG -D__MT_ALL_NODES -D__MT_LIST_COMPACT -D__MT_LIST_MOVES -DUSE_POSITION_KEY
endif

all: laska

laska: 7x7 ${OBJ_FILES}
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o laska
#	strip laska

7x7:
	$(MAKE) -C games/laska

index: genindex.c
	${CXX}  ${OPTFLAGS} ${CXXFLAGS} genindex.c -o genindex
	./genindex   > moveindex.h
	./genindex 2 > over.h

# noofficer: 7x7-no ${OBJ_FILES} ${OBJ_7x7_NO}
# 	${CXX} ${LDFLAGS} ${OBJ_FILES} -o laska-no

noofficer: 7x7 ${OBJ_FILES}
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o laska-no

7x7-no:
	$(MAKE) -C games/laska GAME=noofficer
	$(MAKE) -C games/laska-no

search: ${OBJ_FILES}
	$(MAKE) -C games/laska search
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o search

searchtt: ${OBJ_FILES}
	$(MAKE) -C games/laska searchtt
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o searchtt

gen-leaves: ${OBJ_FILES}
	$(MAKE) -C games/laska search
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o gen-leaves

gen-leaves-tt: ${OBJ_FILES}
	$(MAKE) -C games/laska searchtt
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o gen-leaves-tt

gen-tree: ${OBJ_FILES}
	$(MAKE) -C games/laska search
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o gen-tree

gen-tree-tt: ${OBJ_FILES}
	$(MAKE) -C games/laska searchtt
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_7x7} -o gen-tree-tt

mill: nmm ${OBJ_FILES}
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_NMM} -o mill

nmm:
	$(MAKE) -C games/nmm

startup: ${OBJ_FILES} ${OBJ_STARTUP}
	$(MAKE) -C games/startup
	${CXX} ${LDFLAGS} ${OBJ_FILES} ${OBJ_STARTUP} -o startup

clean:
	$(MAKE) -C games/laska clean
	rm -f *.o *~ *.d laska

Worldrecord: 7-move-games.c ${OBJ_7x7}
	${CXX} 7-move-games.c ${OBJ_7x7} -o 7-move-games

test: ${TOBJ_FILES}
	${CXX} ${CXXFLAGS} ${LDFLAGS} ${TOBJ_FILES} -o test

%.o: %.c  
	${CXX} ${OPTFLAGS} ${CXXFLAGS} -c $<
