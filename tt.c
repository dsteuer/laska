
#include <cstring>
#include <iostream>

#include "tt.h"

TranspositionTable::TranspositionTable () {
#ifdef USE_POSITION_KEY
	resize(26);
#endif
}

void TranspositionTable::resize(unsigned int to_size) {	// allocate 2^to_size entries
	// std::cerr << "TranspositionTable::resize " << to_size << " (" << (1<<to_size) << " entries)" << std::endl;
	table.resize (1<<to_size);
}

void TranspositionTable::clear() {
//	table.clear();
//	resize(20);
#ifdef USE_POSITION_KEY
	TTEntry e;
	e.key	= 0;
	e.move	= 0;
	e.turn	= 0;
	e.depth	= 0;
	e.value	= 0;

	for (unsigned int i=0; i<table.size(); i++) {
		table[i] = e;
	}
#endif
}

const TTEntry* TranspositionTable::probe(const Key key) const {
#ifdef USE_POSITION_KEY
	// std::cerr << "TT: ? " << key;
	unsigned int index = key%table.size();

	const TTEntry* e = &table[index];
	// std::cerr << e << std::endl;
	return e;
#endif
	return 0ULL;
}

void TranspositionTable::store(const Key key, const int move, const int turn, const unsigned int depth, const int value) {
#ifdef USE_POSITION_KEY
	TTEntry e;
	e.key	= key;
	e.move	= move;
	e.turn	= turn;
	e.depth	= depth;
	e.value	= value;

	unsigned int index = key%table.size();

	table[index] = e;
#endif
}

