
#include "movetree.h"

class EGTB : public MoveTree {
public:
	EGTB (string top_stones, string pos);

	unsigned int positionIndex ();
	unsigned int indexSize();

private:
	unsigned int tower_count[4];
};

