
#include "pvs.h"

#include <utility>
#include <iomanip>

// #define NDEBUG
#include <assert.h>

// negascout with alpha/beta pruning and quiescence search

PVS::PVS (const unsigned int board_size)			: AlphaBetaQId (board_size)   		{ } 
PVS::PVS (std::string pos, const unsigned int board_size)	: AlphaBetaQId (pos, board_size)	{ }

int PVS::evaluateMoveTree (unsigned int depth, int alpha, int beta) {
#ifdef USE_POSITION_KEY
	int w;
	if (probe(w, depth)) return w;
#endif
	if (!depth) { return evaluateMoveTreeQuiesce(depth, alpha, beta); }	// end of search depth. just forward all args to qsearch.

	std::list<Move> l = possibleMoves();

	if (l.empty()) { return -mateValue(tree_depth-depth);		}	// game over, node is terminal

	int v = 0;
	for (auto m: l) {
		move(m);
		if (m != l.front()) {
			v = -evaluateMoveTree (depth-1, -alpha-1, -alpha);
			if ((v > alpha) && (v < beta)) {			// fail high
				v = -evaluateMoveTree (depth-1, -beta, -v);
			}
		} else {
			v = -evaluateMoveTree (depth-1, -beta, -alpha);
		}
		store(m, turn, v, depth);
		undo(m);
		if (v >= beta)  { return v; }
		if (v >  alpha)	{ alpha = v; }
	}

	return alpha;
}

