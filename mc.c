#include "mc.h"

#include <vector>
#include <iomanip>
#include <stdlib.h>
#include <sys/time.h>
#include "games/laska/rkiss.h"
#include "readme.h"


void MCBoard::init() {	// random number generation
	timeval tv;
        gettimeofday(&tv, 0);
	srand((unsigned int)tv.tv_usec); 
}

MCBoard::MCBoard ()		   : Position(),		    num_roll_outs(NUM_ROLL_OUTS) { init(); }
MCBoard::MCBoard (std::string pos) : Position(pos), start_pos(pos), num_roll_outs(NUM_ROLL_OUTS) { init(); }

void MCBoard::setNumberOfRollouts     (const unsigned int n) {	num_roll_outs	= n; }
void MCBoard::setMaxGameLength        (const unsigned int n) {	max_game_length	= n; }

Move MCBoard::findBestMove() {
	Move best_move = NoMove;
	std::list<Move> l = possibleMoves();

	int best_value = -maxValue();
	for (auto m: l) {
		move(m);
		std::cerr << m.toString() << ' '; // std::endl;
		int r = evaluatePosition(turn);
		if (r > best_value) {
			best_value = r;
			best_move = m;
		}
		std::cerr << r << " - " << best_value << std::endl;

		undo(m);
	}

	return best_move;
}

// play num_roll_outs games (default 100)
// col: 0 w 1 b
// uses randomGame() and return value 0 draw, 1 w win, 2 b win
int MCBoard::evaluatePosition(const unsigned int col) {
	unsigned int	victories[3] = { 0, 0, 0 };
	// std::cerr << "evaluatePosition(" << num_roll_outs << ", " << col << ')' << std::endl;
	// print();
	MCBoard bak = *this;
	for (unsigned int game=0; game < num_roll_outs; game++) {
		++victories[randomGame()];
		(*this) = bak;
	}
	std::cerr << "MC result: " << victories[0] << ' ' << victories[1] << ' ' << victories[2] << ' ';
	// return (100*victories[col+1])/num_roll_outs;
	unsigned int other = !col;
	return victories[col] - victories[other];
}

unsigned int MCBoard::randomGame() {
	for (unsigned int i = 0; i < max_game_length; i++) {
		Move m = randomMove();
		if (m.empty()) return turn;		// winner

		move(m);
	}
	
	return 2;		// no win
}

//std::vector<unsigned int> eog_stats(1000, 0);

#ifdef __MC_SPECIAL
	static unsigned int max_steps = 23;		// thresholds for output, set to a value, where optimal solutions are known
	static unsigned int max_jumps = 16;
	static unsigned int max_jump_length = 8;

	static unsigned int forced_seq = 0;		// counter used below
#endif

Move MCBoard::randomMove() {
	static PRNG rk(time(0));

	std::list<Move> l = possibleMoves();

#ifdef __MC_SPECIAL
//	if (l.size() == 1) {
//		forced_seq ++;
//		if (forc_move_records[forced_seq] > (move_history.size())) {
//			forc_move_records[forced_seq] = move_history.size();
//			std::cout << forced_seq << " forced moves at "<< (move_history.size()) << " plies." << std::endl;
//			for (auto i: move_history) { std::cout << i << ' '; } std::cout << l.front() << std::endl;
//		}
//		return l.front();
//	} else {
//		forced_seq = 0;
//	}

	if (!l.front().isStep()) {
		if (l.size() > max_jumps && jump_move_records[l.size()] > (move_history.size())) {
			std::cout << l.size() << " jump moves at " << (move_history.size()) << " plies." << std::endl;
			for (auto i: move_history) { std::cout << i << ' '; } std::cout << " -> ";
			for (auto i: l) { std::cout << i << ' '; } std::cout << std::endl;
		}
	} else {
		if (l.size() >= max_steps && step_move_records[l.size()] > (move_history.size())) {
			std::cout << l.size() << " step moves at " << (move_history.size()) << " plies." << std::endl;
			for (auto i: move_history) { std::cout << i << ' '; } std::cout << " -> ";
			for (auto i: l) { std::cout << i << ' '; } std::cout << std::endl;
		}
	}
#endif

	if (!l.empty()) {
		unsigned int choose = rk.rand<unsigned int>() % l.size();
		unsigned int c = 0;
		for (const auto& m: l) {
			if (c == choose) { return m; }
			c++;
		}
	}	

	return NoMove;
}

// #define __MC_SPECIAL
// #define __MC_FIND_EVEN_POSITIONS

static unsigned int max_gl = 0;
static unsigned int max_os = 0;

void MCBoard::genetrateRandomPosition(const unsigned int n_moves) {
	unsigned int num_moves = n_moves ? n_moves : rand() % max_game_length;

	// move_history.clear();

	int n_offi   = 0;
	for (unsigned int i = 0; i < num_moves; i++) {
		Move m = randomMove();
		if (m.empty()) {
			// std::cout << toString(1) << " - game over, sorry. " << move_history.size()  << std::endl;
	//		eog_stats[move_history.size()]++;
			return;
		}
		moveRecorded(m);
#ifdef __MC_SPECIAL
		int nt = __builtin_popcount(bitb.bits[0]) + __builtin_popcount(bitb.bits[1]); 
		if (nt < 19) { break; }
		else {
			if (i>= max_gl) {
				max_gl = i;
				int no = __builtin_popcount(bitb.bits[2]) + __builtin_popcount(bitb.bits[3]); 
				if (no > max_os) {
					max_os = no;
					std::cout << no << ' '; for (auto i: move_history) { std::cout << i << ' '; } print(1);
				}
			}
		}
		static int max_offi = 7;
		static unsigned int max_promotions[] = { 1, 8, 10, 13, 15, 18, 21, 23, 30, 38, 43, 47, 50, 61, 65, 73, 79, 83, 94, 107, 118, 129, 143 };
		static int num_off = 7;
		static unsigned int max_officers[]   = { 1, 8, 10, 13, 15, 18, 22, 26, 36, 42, 57, 63, 75, 87, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000 };
		int no = __builtin_popcount(bitb.bits[2]) + __builtin_popcount(bitb.bits[3]); 
		if (no > num_off && max_officers[no] > move_history.size()) {
			max_officers[no] = move_history.size();
			std::cout << no << " officers at " << (move_history.size()) << " plies." << std::endl;
			for (auto i: move_history) { std::cout << i << ' '; } print(1);
		}
		if (max_tower_records[bitb.tower[m.to()].size()] > move_history.size()) {
			std::cout << bitb.tower[m.to()].size() << " size tower at " << (move_history.size()) << " plies." << std::endl;
			max_tower_records[bitb.tower[m.to()].size()] = move_history.size();
			for (auto i: move_history) { std::cout << i << ' '; } std::cout << "-> " << bitb.tower[m.to()].toString()  << std::endl;
		}
		if (m.isPromotion()) {
			n_offi ++;
			if (n_offi > max_offi && max_promotions[n_offi] >= move_history.size()) {
				max_promotions[n_offi] = move_history.size();
				std::cout << n_offi << " promotions at " << (move_history.size()) << " plies." << std::endl;
				for (auto i: move_history) { std::cout << i << ' '; } print(1);
			}
		}
		if (m.size() > max_jump_length) {
			if (move_history.size() < long_jump_records[m.size()]) {
				std::cout << "bigmove: " << m.size()  << ' ' << m << ' ' << move_history.size() << std::endl;
				for (auto i: move_history) { std::cout << i << ' '; } std::cout << std::endl;
			}
		}
#endif
	}

	// for (auto i: move_history) { std::cout << i << ' '; } std::cout << std::endl;
	// print(1);
#ifdef __MC_FIND_EVEN_POSITIONS
	int v = Position::evaluatePosition();
	if (v < 1000 && v > -1000) {
		std::list<Move> l = possibleJumps();
		std::cout << toString(1) << " " << v << ' ' << l.size() << std::endl;
	}
#endif
}

// void MCBoard::stats() {
// 	for (auto
// }


