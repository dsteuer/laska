#ifndef __EVAL_TEST_H
#define __EVAL_TEST_H

#include <string>

class EvalTest {
public:
	EvalTest (std::string p, unsigned int n_games = 100, unsigned int max_len = 200, unsigned int depth = 10);
	unsigned int	oneGame			();
	void		setNumberOfGames	(unsigned int n) { num_games       = n; }
	void		setMaxGameLength	(unsigned int n) { max_game_length = n; }

private:
	std::string	position;
 	unsigned int	num_games;
 	unsigned int	max_game_length;
	unsigned int	tree_depth;
};

#endif
