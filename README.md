laska
laska
=====

Engine to play and explore the game of Laska

Authors
=======

Lambert Klasen and Detlef Steuer


Resources
=========

Theoretical Results
===================

For quite a while, we thought, in Laska it is impossible to "lose a move"
like in chess. Each position defines, which player has the right to move.
For the very most of positions this is true, but, in general, false.
For example in position "o..o.o|Ooxx..oxx|..x.|..|ox...|.xxoo.|x.O..xxO"
the moves a5-b4 a1xc3xa5 a7-b6, as well as a7-b6 a1xc3 a5-b4 c3xa5
lead to the same position. One sequence has an odd number of plies, the other
an even number of plies.

Usage
=====

Playing strength
================

Interfaces
==========

Web-Interface
-------------

In the web-ui subdirectory there are two files: ask-laska.php and tower-img.php.
Copy these files to some directory under control of your webserver, say
http://127.0.0.1/~myhome/laska . In ask-laska you must adjust a) the path to
the laska executable and to tower-img.php.
If not already present you must install php and php-gd for this to work.

Now point your browser to http://127.0.0.1/~myhome/laska/ask-laska.php .
If all went well, you can enjoy a nice game of laska against the engine.

Note: This is only a very, very, very rudimentary interface. But it works.

Autoplay
--------
There is a script in the sources, ~autoplay.sh~, that couples two istances
of laska to play each other.

Replay in Vim
-------------

Using this little gem, let´s call it gameview.vim

:map c nzt
:map C Nzt
:set updatetime=1000
:autocmd CursorHold * call Timer()
:function! Timer()
call feedkeys("cf\e")
endfunction

one can step forward and backward through a game.
(you have to prepare a log file with
laska -D -A "&lt;movesequenz&gt;" > gamelog.txt
and then
vim -s gameview.vim gamelog.txt)


Tablebases
==========

Opening
-------

Endgame
-------

Developer Notes
===============

Positions for testing pvs - abq
-------------------------------

...|o..|.oxx..ooOX|.oo.|.xx.ox.|xXo..xo|..xxO.
o.oX..o|.o.|ox..oo.xo|..|xo.ox..xx|x..xxo|.x..
o.ooX.ooX.o|.ox.|.o..|xxO..|x...x|xxo..|xO...x
.o.o.|oo..|o.xo.ox.|o.Ox.ox|.xxxx.xo.|..|x.x..
o.o.oxX.ooxX|xO..|ox.xo..o|..|.o.xo.|.x.|x...x

popcount
--------
Read somewhere __builtin_popcountll is slow.
May be not needed anymore with proposed towers
Otherwise, one day, test alternative implementations.

sort
----
very short vectors must be merge sorted all the time in search
tree. There must be better things than std::sort.

Internal representation
-----------------------

A tower is a long long int. Stones are coded as
001 = o 101 = O  x = 011 and X = 111, so the rightmost bit is always 1.
(see tower.c)

Proposed change (in branch evalarray):
data =  bit 0-9 own stones, bit 0 is top, 1 is officer
   bit 10-13 own stonecount
   bit 14-23 other stones
   bit 24-27 other stonecount
   bit 28 color.

Missing stuff
-------------

Releases
========

v0.1: Proof of concept. Works, well, kind of.

v0.2: Move generater solidified. Up to depth 20 consistent.

v0.3: Order of stones in tower reverted. top stone is in lowest bits now.

v0.4: Whole game loop installed. Option -l. Autoplay.sh for two programs
evaluation tests.

Roadmap
=======

Evaluation
----------

- evaluate size of tower and officers
- offset in plies to next forced capture


Evaluation tests
---------------

laska -l goes into a whole game loop. opponents moves are read from stdin.
autoplay.sh connects two progs by named pipe. so two programs with different
evaluation functions can play a tournament.

autoplay accepts a position argument and starts both programs with this
position.

(the old idea of a build in evaluation test has to wait until the game engine
is stable incl. tranposition tables.)



Ideas
-----

Extend quiescence search: A sequence of captures counts as 1 ply.
Reason: Capture sequences are forced or have a very small branching factor.
If each capture counts as a ply in such a sequence a relativ small number of
variations remains to evaluate the resulting position. Especially in the
endgame this could lead to great advances. 
Further extension: currently the first non jump move is considered quiet.
But often, expecially in opening and midgame, this moves forces at once the
next capture move. And these jumps lead to disadvantage branches quite often
(in an already relatively unfavourable position).
resulting in a few good moves with a lot of bad moves for a given position.
a few moves later this horizon effect vanishes and the position collapses.
either catch this problem in evaluation or in qsearch analysing further forced moves.


Simple Promotion check without turn: Because a private always moves forward
and can never reach the own baseline it is enough to check
isPrivate && squares(move) & (15 | 15 << 21))
Nevertheless nearly no speedup.

Targets
-------

Branches (and starter) and their description
--------------------------------------------

master: master always contains a working copy of laska. All developement happens in branches.

evalarray (dsteuer): experiement with proposed change in tower representation

bitmove2: bitboard testing

Early Results
=============

Calculate Number of Positions
-----------------------------

Leafs on exact ply were printed as compact boards.

Counting nodes:
for number in `seq 1 20` ; do echo $number ; xzcat pos-$number.txt.xz | wc -l ; done

Counting unique nodes (ply 20 needed some other parameters for sort):
for number in `seq 1 20` ; do echo $number ; xzcat pos-$number.txt.xz | sort -u | wc -l ; done

Depth 		    nodes	unique nodes		 new nodes	unique new nodes (*)
0			1		1	 	         1	     	       1
1		        6	        6	 	         6		       6
2		       12	       12	 	         6		       6		
3		       30	       30	 	        18		      18
4		       84	       83	 	        54		      53
5		      200	      195	 	       116		     112
6		      524	      497	 	       324		     302
7		     1370	     1249	 	       846		     752
8		     3518	     2965	 	      2148		    1728
9		     9210	     7153	 	      5692		    4196
10		    23108	    16443	 	     13898		    9364
11		    60232	    39185	 	     37124		   22910
12		   157672	    92997	 	     97440		   54592
13		   427184	   226466	 	    269512		  135605
14		  1169822	   549872	 	    742638		  331296
15		  3282548	  1364416		   2112726		  834652
16		  9228288	  3369343		   5945740	         2075073
17		 26492040	  8476400		  17263752	         5291008
18		 76999312	 21278367		  50507272	        13426979
19		228297780	 54301389		 151298468	        34718106
20		692593000	138597875   		 464295220	        89933374	 
21	       2130688682				1438095682	       234132452
									       135324296 (**)
22									       607429029

(*) unique new nodes consider only positions on exact ply. same positions
with other number of plies aber not compared. (see also "Move permutations" below.)

(**) unique positions after only considering the starting moves a3-b4, c3-b4, c3-d4.
The number is bigger than half the unique positions. We have positions missing
that are symmetric around the d column that are once in the first half and would
be once in the second half of the search tree. The other missing positions are
those cases when we have a leaf and the symmetric version folded at the d column
once each in the half-tree. They both are unique and both exist in the other
half of the search tree and wouldn´t have counted again. The exact number of
unique positions is unknown at the moment. The described cases have to be
identified in those 135 Mio of leafs. Not such an easy task. 

(***) when using only 3 root nodes, omitting 3 d-line symmetric cases.


Different towers after n plies
------------------------------

We calculatetd the following table of different towers after n plies

plies number of different towers
1     2 
2     3 
3     4 
4     4 
5     5 
6     7 
7     8 
8     9 
9    10 
10    15 
11    24 
12    31 
13    45 
14    58 
15    83 
16   114 
17   158 
18   218 
19   315 
20   444 

Maximum size of a tower / Can a single tower capture all opponents towers
-------------------------------------------------------------------------

There are towers of 21 stones in laska. I.e., only this giant and a single stone of a color is remaining at the end of game.
This also answers the question if a single stone can capture all opponents stones.

Here is a sample 47 plies listing:
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 e1xc3 c7-d6 g3-f4 e5xg3xe1 b4-a5 c5-d4
a5xc7 d6-c5 c3xe5 f6xd4 a3-b4 e1xc3xa5 b2-c3 d4xb2 a1xc3 e5-f4 c3-b4 a5xc3xa1
c7-b6 a1xc3xe1 b6xd4 a7-b6 c1-d2 e1xc3xe5 g1-f2 d4-e3 f2xd4xf6 e7-d6 e5xc7
g7xe5 c7xa5xc3 e5-d4 c3xe5xg3 e3-f2 g3xe5xg7 g5-f4 g7xe5xg3xe1 f2-g1 d4-c5
g1-f2 e1xg3
resulting in tower XxxXxxxxxxxOooooooooO

Here is a 82 plies listing for all opponents stones captured by a single stone. (See also 11 jump move below.)

e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 c3-d4 e5xc3 b2xd4 a5-b4 c3xa5xc7 a7-b6 c7xa5 d6-e5 d4xb6 g5-f4 e3xg5 e5-d4 a1-b2 d4-c3 b2xd4
c3-b2 a5-b4 f6-e5 d4xf6 g7xe5 g3-f4 e5xg3 b4-c3 b2-a1 f2-e3 f6-e5 g5-f6 e7xg5 e3-f4 g5xe3 c1-b2 e5-d4 c3xe5xg7 a1xc3 g7-f6 c3-b2
b6-c7 b2-c3 f6-e5 c3-b4 a3xc5 d4-c3 b4-a5 c3-d2 e1xc3 g3-f2 e5-d4 f2-e1 d4xf2 e1xg3 a5-b6 g3xe1 c5-d6 e1-d2 b6-a7 d2xb4 e3-d4
b4-c5 g1xe3 c5xe7 c7xe5 f2-g1 e5-f6 e7xg5 a7-b6 f6-e5 d4xf6 g5xe7 b6-a7 e7xg5 e3-f4 g5xe3 a7-b6 e3-d4 b6-c5 d4xb6 


Solutions were obtained using monte carlo methods, so most likely not most effecient.


First occurrence of n - stones tower
------------------------------------

1 size tower at 1 plies.
c3-b4 -> o
2 size tower at 2 plies.
c3-b4 a5xc3 -> ox
3 size tower at 5 plies.
c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 -> xxo
4 size tower at 10 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 g1-f2 e1xg3 -> oooX
5 size tower at 10 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 -> ooooX
6 size tower at 10 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 c3-d4 e1xc3xe5xg3 -> oooooX
7 size tower at 12 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 g1-f2 e1xg3xe5 c3-d4 e5xc3xe1 -> ooooooX
8 size tower at 14 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 c3-d4 e1xc3xe5xg3 g1-f2 g3xe1xc3 -> oooooooX
9 size tower at 16 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 e1xc3 g3xe1 c3-d4 e1xc3xe5xg3 g1-f2 g3xe1xc3 c1-d2 c3xe1 -> ooooooooX
10 size tower at 19 plies.
c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 c1xe3 g5-f4 e3xg5 e5-f4 b4xd6 e7xc5 g5xe7 c5-d4 e7xc5xe3xg5 g7-f6 g5xe7xc5 c7-d6 c5xe7 -> xxxxxxxxxO
11 size tower at 20 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 a5-b4 c3xa5 e5xc3xe1 g3-f4 e1xg3xe5 g1-f2 c5-b4 a3xc5 e5-d6 b2-c3 d6xb4xd2xf4 a1-b2 f4-g3 c1-d2 g3xe1xc3xa1 -> ooooooooooX
12 size tower at 21 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 c3-d4 e1xc3xe5xg3 c1-d2 c7-b6 g1-f2 g3xe1xc3 b2xd4 f4xd2 a1-b2 c5xe3 c3xe5xc7 a5-b4 c7xa5xc3xe1 -> xxxxXooooooO
13 size tower at 23 plies.
c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5 a3-b4 c5xa3 c1-b2 a3xc1 e3-d4 c1xe3xc5 c3-b4 c5xa3 e1-d2 a5-b4 a1-b2 a3xc1xe3 f2xd4 e5xc3 e3xc5xa7 c3-b2 a7xc5xa3xc1 -> xxxxXoooooooO
14 size tower at 25 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 c1-d2 a5xc3xe1 e3-d4 e5xc3 b2xd4 c5xe3 c3xa5xc7 a7-b6 c7xe5xc3 e7-d6 c3-b2 g5-f4 b2-c1 e3-d2 c1xe3xg5xe7xc5xa7 -> xxxxxXxxxooooO
15 size tower at 25 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-f4 c7xe5xg7 a7-b6 g7xe5xc7xa5 -> xxxXxxxoooooooO
16 size tower at 27 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-f4 c7xe5xg7 a7-b6 g7xe5xc7xa5 c5-b4 a5xc3 -> xxxxXxxxoooooooO
17 size tower at 27 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-d4 c7xe5xg7 a7-b6 g7xe5xc7xa5 c5-b4 a5xc3xe5 -> xxxxxXxxxoooooooO
18 size tower at 29 plies.
c3-d4 e5xc3 d2xb4 d6-e5 b4xd6 e7xc5 a3-b4 c5xa3 e3-d4 g5-f4 c1-d2 a3xc1 c3-b4 c1xe3xc5xa3 e1-d2 b6-c5 a1-b2 a3xc1xe3 f2xd4xb6 a7xc5 e3xg5xe7 a5-b4 e7-f6 c5-d4 f6-e7 e5-f4 e7xc5xa7 g7-f6 a7xc5xe3xg5xe7xc5xa3 -> xxxxxxXxxxoooooooO
19 size tower at 33 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 g3-f4 e5xg3 c3-d4 a5-b4 e1-d2 g3xe1 e3-f4 e1xc3xe5xg3 c1-d2 f6-e5 g1-f2 g3xe1xc3 b2xd4xf6 g7xe5 c3xa5xc7 e5-f4 c7xe5xg7 a7-b6 g7xe5xc7xa5 e7-f6 a5-b4 f4-g3 b4xd6 g5-f4 d6-e7 g3-f2 e7xg5xe3xg1 -> xxxxxxxXxxxoooooooO

above known to be optimal short sequences. Below also very likely to be optimal

20 size tower at 37 plies.
c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 c1xe3 b6-a5 b4xd6 e7xc5 e3-d4 c5xe3xc1 a3-b4 c1xa3xc5 f2-e3 d4xb2 a1xc3 a7-b6 e3-d4 c5xe3xc1xa3 e1-d2 a3-b2 d2-e3 b2xd4xf2 g1xe3 a5-b4 e3-d4 c3-d2 f2-e3 e5xc3 e3xc5xa7 c7-b6 a7xc5xe7 g5-f4 e7xg5xe3xc1 c3-b2 c1xa3xc5xe7 -> xxxxxxxxxXoooooooooO
21 size tower at 39 plies.
c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 d6xf4xd2 c1xe3 b6-a5 b4xd6 e7xc5 e3-d4 c5xe3xc1 a3-b4 c1xa3xc5 f2-e3 d4xb2 a1xc3 a7-b6 e3-d4 c5xe3xc1xa3 e1-d2 a3-b2 d2-e3 b2xd4xf2 g1xe3 a5-b4 e3-d4 c3-d2 f2-e3 e5xc3 e3xc5xa7 c7-b6 a7xc5xe7 g5-f4 e7xg5xe3xc1 c3-b2 c1xa3xc5xe7 g7-f6 e7xg5 -> xxxxxxxxxxXoooooooooO


First occurrence of n - stones of same color on top of tower
------------------------------------------------------------

See theo.md for complete list.


First occurrence of n - captured stones with a single tower
-----------------------------------------------------------

See theo.md for complete list.

First occurrence of n officers
------------------------------------------------------------

1 officers at 8 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 -> o.o.ooX.o|o.o.|o.o.o.|xo..|x.x..x|x.x.x|.x.x.x
2 officers at 10 plies.
g3-f4 e5xg3 c3-d4 d6-e5 d2-c3 e7-d6 c1-d2 g5-f4 e3xg5xe7 c5xe3xc1 -> o.ooX.o.o|o..o|o.o..ox|..|x..x.|x.x.|x.x.xxO.x
3 officers at 13 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 c1-d2 c5xe3xc1 a3xc5xa7 -> o.ooX.ooX.o|o..o|...o|..|..x.xo|.x.x|xxO.x.x.x
4 officers at 15 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 g3-f4 e1xg3 c1-d2 e5xc3xe1 a3-b4 c5xa3xc1 e3xc5xe7 -> o.ooX.ooX.o|..|...oooX|..o|...x|x..x|x.x.xxO.x
5 officers at 18 plies.
c3-b4 a5xc3 d2xb4 b6-a5 c1-d2 a7-b6 c3-d4 a5xc3 e3-f4 c5xe3xc1 a3xc5xa7 c1xa3 e1-d2 g5xe3xc1 a1-b2 c3xa1 g3-f4 e5xg3xe1 -> ooX.ooX.ooX.o|..|oooX...|..|...|.x.x|xxO.x.x.x
6 officers at 22 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 c3-d4 e5xc3xe1 b2-c3 c5-d4 e3xc5xa7 d6-e5 a1-b2 e1-d2 c1xe3 e7-d6 c3-d4 a5xc3xa1 a3xc5xe7 e5xc3xe1 e3-f4 g5xe3xc1 -> ooX.ooX.ooX.o|..o|...o|..X|...|..x|xxO.x.xxO.x
7 officers at 26 plies. (up to symmetric one possibly only solution at 26 plies.) 
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c5-d4 e3xc5 c7-b6 g3-f4 e5xg3xe1 g1-f2 b6xd4 c3xe5xc7 c5xe3xg1 c7-d6 e7xc5 d2-c3 c5-d4 c3xe5xc7 a5xc3 a3xc5xe7 e1-d2 c1xe3 c3xe1 e3-f4 g5xe3xc1 -> o.ooX.ooX.ooX|o..|...|.O.X|...|..x|x.xxO.xxO.x
8 officers at 29 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 c1-d2 e1xc3 g3-f4 c5xe3 a3xc5xa7 e5xg3xe1 b2xd4 e1-f2 c3-b4 e3-d2 g1xe3 d2-c1 e3-f4 d6-c5 b4xd6 e7xc5xe3xg1 g5xe7 c7xe5xg3xe1 d6-c7 -> o.oX.oooX.oooX|..|...|.X.X|...|..|xxO.xO.xxO.x
9 officers at 33 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 c7-b6 c3-d4 a5xc3xe1 c1-d2 e1xc3 g3-f4 c5xe3 b2xd4 b4xd2 c3-b4 e5xg3xe1 g1-f2 e3xg1 a1-b2 d6-e5 b4-a5 e5xc3xa1 a5xc7 d2-c1 c7-d6 e7xc5 g5xe7 a7-b6 d6-c7 c5-b4 a3xc5xa7 -> ooX.oX.ooX.ooX|..|...|O.X.|...|..|xxO.xO.xxO.x

above optimal short. results below up to 14 officers derived from mc searches. 15+ constructed from promising ply 18 positions

10 officers at 41 plies. (*) 21
e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 g1-f2 c5xe3xg1 e1-f2 g1xe3 d2xf4xd6 d4xf2 c3-b4 c7xe5 e3-f4 a5xc3 b2xd4 d6-c5 f4xd6 c5xe3 c3-b4 e7xc5 g5xe7 e3-d2 c1xe3 f2-e1 a1-b2 d4xf2 d6-c7 g7-f6 e5xg7 b6-a5 b4xd6 a7-b6 c5xa7 f2-g1 a3-b4 a5xc3xa1 e7-f6 d2-c1 d6-e7 -> ooX.oX.oX.oX|..|..X.o|..|...|..xxO|xO.xO.xO.xO
11 officers at 47 plies.
e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 g1-f2 c5xe3xg1 e1-f2 g1xe3 d2xf4xd6 d4xf2 c3-b4 c7xe5 e3-f4 a5xc3 b2xd4 d6-c5 f4xd6 c5xe3 c3-b4 e7xc5 g5xe7 e3-d2 c1xe3 f2-e1 a1-b2 d4xf2 d6-c7 c5-d4 c7xa5 f2-g1 e5-d6 d2-c1 a5-b6 a7xc5 b4-a5 c5-b4 a5xc7 d4-c3 a3xc5xa7 c3xa1 e7-f6 g7xe5 d6-e7 e5-f4 g3xe5xg7 -> ooX.oX.oX.oX|..|..X.|O..O|...|..x|xxO.xO.O.xxO
12 officers at 55 plies.
e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 g1-f2 c5xe3xg1 e1-f2 g1xe3 d2xf4xd6 d4xf2 c3-b4 c7xe5 e3-f4 a5xc3 b2xd4 d6-c5 f4xd6 c5xe3 c3-b4 e7xc5 g5xe7 e3-d2 c1xe3 f2-e1 a1-b2 d4xf2 d6-c7 g7-f6 e5xg7 b6-a5 b4xd6 a5-b4 e7-f6 f2-g1 d6-e7 d2-c1 c7-b6 e3-d4 g3-f4 b4-c3 c5-d6 a7xc5 f4-e5 c3xa1 d6-c7 c5-b4 a3xc5xa7 d4-e3 e5-d6 e3-f2 c7-b6 c1-b2 d6-c7 -> oX..oX.oX|oX..X|...|O..|...|O..xxO|xxO.O.xO.xO
13 officers at 61 plies.
e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 g1-f2 c5xe3xg1 e1-f2 g1xe3 d2xf4xd6 d4xf2 c3-b4 c7xe5 e3-f4 a5xc3 b2xd4 d6-c5 f4xd6 c5xe3 c3-b4 e7xc5 g5xe7 e3-d2 c1xe3 f2-g1 a1-b2 d4xf2 b2-c3 g7-f6 e5xg7 b6-a5 d6-c7 d2-c1 b4xd6 f2-e1 c3-d4 a5-b4 e7-f6 b4-c3 d6-e7 c3-d2 c7-b6 c1-b2 c5-d6 a7xc5 d6-c7 b6-a5 d4xb6 a5-b4 b6-a7 b4-c3 a3-b4 d2-c1 b4-a5 c3-d2 a5-b6 e1-f2 c7-d6 d2-e1 b6-c7 -> .X.X.oX|oX..oX|..X.o|..|.O..|.O.xxO|xO.O.xO.xO
14 officers at 66 plies.
e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 g1-f2 c5xe3xg1 e1-f2 g1xe3 d2xf4xd6 d4xf2 c3-b4 c7xe5 e3-f4 a5xc3 b2xd4 d6-c5 f4xd6 c5xe3 a1-b2 e7xc5 g5xe7 c5-b4 c3xa5xc7 f2-g1 c7-b6 a7xc5 b4-a5 c5-b4 a3xc5xa7 b6-c5 b2-a3 e3-d2 c1xe3 d4xf2 a5-b6 c5-d4 d6-c7 d4-c3 e5-d6 c3-b2 b4-a5 f2-e1 a3-b4 e3-f2 e7-f6 g7xe5 b4-c5 e5-f4 g3xe5xg7 f6-e5 f4-g5 e5-d4 d6-e7 d4-e3 c5-d6 b2-a1 e7-f6 d2-c1 d6-e7 e3-d2 c7-d6 c1-b2 b6-c7 d2-c1 -> X.X.oX.oX|oX..X|...|..|O...O|.xO.O|xxO.O.O.xxO
15 officers at 78 plies. (*) 55 100m
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 a5xc3xe1 c1-d2 e1xc3 b2xd4xb6 c7xa5 e5xc7 b4xd2 c7-d6 e7xc5 c3-d4 d6-e5 g3-f4 e5xc3 f4-e5 a5-b4 e5-d6 d2-c1 d6-c7 b6-a5 d4xb6 c3-b2 a1xc3 b4xd2 e3-d4 g5-f4 f2-e3 f6-g5 d4-e5 f4-g3 e3-d4 g5-f4 e5-d6 g7-f6 d6-e7 f6-g5 d4-e5 d2-e1 e5-d6 e1-f2 g1xe3 g3xe1 f2-g3 f4xd2 g3-f4 a5-b4 f4-e5 g5-f4 e5-f6 b2-a1 f6-g7 c3-b2 e7-f6 b4-c3 d6-e7 f4-g3 c7-d6 c3-d4 a3-b4 g3-f2 b4-a5 d4-c3 b6-c7 f2-g1 a5-b6 e1-f2 d6-e5 d2-e1 c7-d6 c1-d2 b6-c7 b2-c1 -> oX.X.oX.X|.oX.oX|.xX.X.|..|.O.O.|.xO.O|x.O.O.O
16 officers at 82 plies. (*) 61
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 c1xe3 e5-d4 b4xd6 e7xc5 d2-c3 d6-e5 c3-b4 e5-f4 b4xd6 c7xe5 d6-e7 f4xd2 e1xc3 d2-e1 e7-d6 e3-d2 d6xf4 g5xe3 a3-b4 f6-g5 b2-a3 d4xb2 f2xd4 d2-c1 e3-f2 e1-d2 c5-d6 d2-e3 d6-c7 e3xc5 b4xd6 b6-a5 c5-b6 a7xc5 d6-e7 f4-e3 e5-d6 c1-d2 e7-f6 g7xe5 b6-a7 f6-g7 d6-e7 e5-f4 g3xe5 b2-c1 f2-g3 e3-f2 g1xe3 f2-g1 a3-b4 d4xf2 b4xd6 f2-e1 c7-b6 a5-b4 a1-b2 c3xa1 d6-c7 b4-c3 e5-d6 c1-b2 f4-e5 b2-a3 e7-f6 c3-b2 d6-e7 b2-c1 c5-d6 c1-b2 b6-c5 d2-c3 c7-b6 e3-d2 d6-c7 d2-c1 -> oX.X.oX.X|X..|oX.X..O|..|.O.O.x|xO..O|O.O.xO.X
17 officers at 93 plies. (*) 67   1d
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 c1xe3 e5-d4 b4xd6 e7xc5 d2-c3 d6-e5 c3-b4 e5-f4 b4xd6 c7xe5 a3-b4 f4xd2 e1xc3 g5-f4 d6-c7 b6-a5 c5-d6 f6-g5 b2-a3 d4xb2 f2xd4xf6 a7-b6 f6-e7 b6-c5 e7-f6 c5-d4 b4-c5 f4-e3 a3-b4 b2-c1 c5-b6 d2-e1 d6-e7 c3-d2 e5-d6 g7xe5 b4-c5 c1-b2 a1xc3 e5-f4 g3xe5xg7 f6-e5 b2-a3 d4xb2 f4-g3 e5-d4 a3-b4 b2-a1 b6-a7 d2-c1 c5-b6 e3-d2 b4-c5 d4-e3 e7-f6 c1-b2 d6-e7 d2-c1 c5-d6 e3-d2 g1-f2 a5-b4 f2-e3 b4-a3 f6-e5 c3-b4 e3-d4 g5-f4 e7-f6 f4-e3 g3-f4 e3-f2 d6-e7 f2-g1 c7-d6 e1-f2 d4-c5 d2-e1 b6-c7 c1-d2 a7-b6 b2-c3 b6-a5 a3-b2 c5-b6 b2-c1 b6-a7 -> oX.X.X.X|.X.oX|.X..|X..O|O..O.|.xO.O|O.O.O.xxO
18 officers at 98 plies. (*) 69  96m
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 c1xe3 e5-d4 b4xd6 e7xc5 d2-c3 d6-e5 c3-b4 e5-f4 b4xd6 c7xe5 d6-e7 f4xd2 e1xc3 d2-e1 e7-d6 e3-d2 d6xf4 g5xe3 a3-b4 f6-g5 b2-a3 d4xb2 f2xd4 d2-c1 e3-f2 e1-d2 c5-d6 d2-e3 d6-c7 e3xc5 b4xd6 b6-a5 c5-b6 a7xc5 d6-e7 f4-e3 e5-d6 c1-d2 e7-f6 g7xe5 b6-a7 f6-g7 d6-e7 e5-f4 g3xe5 b2-c1 f2-g3 e3-f2 g1xe3 f2-g1 a3-b4 d4xf2 b4xd6 f2-e1 c7-b6 a5-b4 d6-c7 b4-a3 e5-d6 e3-f2 f4-e5 d2-e3 e7-f6 c3-d2 a1-b2 g5-f4 b2-c3 a3-b2 c3-b4 b2-a1 b4-a5 c1-b2 d6-e7 d2-c1 c5-d6 e3-d2 b6-c5 f4-e3 a5-b6 b2-a3 f6-g5 c1-b2 e7-f6 d2-c3 g3-f4 e3-d2 d6-e7 d2-c1 c7-d6 e1-d2 b6-c7 f2-e1 -> X.X.X.X|X.oX.|oX.X..|..O|.O.O.O|.xO.xO|O.O.O.X
19 officers at 116 plies. (*) 83  9m
e3-d4 c5xe3 f2xd4 e5-f4 g3xe5 d6xf4 e1-f2 f4-g3 c3-b4 g3xe1 e3-f4 e5xc3 f4-e5 g5-f4 b4-c5 f6-g5 d2xb4 e7-f6 c5-d6 f4-e3 b4-c5 e1-f2 d6-e7 e3-d2 g1xe3 d2-e1 f2-g3 d4xf2 c3-d4 a5-b4 g3-f4 b6-a5 e7-d6 c7-b6 d6-c7 b4-c3 e5-d6 f2-g1 f4-e5 g5-f4 a3-b4 f6-g5 e5-f6 g7xe5 b2-a3 f4-g3 f6-e7 g3-f2 d4xf6 b6xd4 f6-g7 e3-f4 e7-f6 d4-e3 c1-b2 c5-d4 b4-c5 f4-g3 d6-e7 e3-d2 c5-d6 a7-b6 a3-b4 b6-c5 b2-a3 d4-e3 c7-b6 c5-d4 b4-c5 g5-f4 a3-b4 d2-c1 d6-c7 c1-b2 c5-d6 c3-d2 a1xc3 d2-c1 b2-a3 d4xb2 b4-c5 e3-d2 a3-b4 f4-e3 f6-g5 b2-a1 e7-f6 c1-b2 d6-e7 d2-c1 c5-d6 e3-d2 b4-c5 a5-b4 b6-a5 b2-a3 c5-b6 c1-b2 b6-a7 d2-c1 a5-b6 c3-d4 b6-a5 b4-c3 c7-b6 e1-d2 d6-c7 f2-e1 c7-d6 e1-f2 d6-c5 d2-e3 e5-d6 c3-d2 d6-c7 d2-e1 -> oX.X.X.oX|X..X|X..X.X|.X.|O.O..O|O..O|O.O.O.xO

(*) searched from mentioned plies for better variation

the authors proved that 20 officers are impossible to achive.
only remaining task left is to reduce the number of plies to
something at least close to optimal.


Maximum number of promotions
----------------------------

it is possible to promote all 22 private stones to officers.
see [theory stuff](theo.md)


First occurrence of n promotions
--------------------------------

slightly different approach. all 22 stones can promote, as shown above.

1 promotions at 8 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 -> o.o.ooX.o|o.o.|o.o.o.|xo..|x.x..x|x.x.x|.x.x.x
2 promotions at 10 plies.
g3-f4 e5xg3 c3-d4 d6-e5 d2-c3 e7-d6 c1-d2 g5-f4 e3xg5xe7 c5xe3xc1 -> o.ooX.o.o|o..o|o.o..ox|..|x..x.|x.x.|x.x.xxO.x
3 promotions at 13 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 e1-d2 a7-b6 c3-d4 a5xc3xe1 c1-d2 c5xe3xc1 a3xc5xa7 -> o.ooX.ooX.o|o..o|...o|..|..x.xo|.x.x|xxO.x.x.x
4 promotions at 15 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 g3-f4 e1xg3 c1-d2 e5xc3xe1 a3-b4 c5xa3xc1 e3xc5xe7 -> o.ooX.ooX.o|..|...oooX|..o|...x|x..x|x.x.xxO.x
5 promotions at 18 plies.
c3-b4 a5xc3 d2xb4 b6-a5 c1-d2 a7-b6 c3-d4 a5xc3 e3-f4 c5xe3xc1 a3xc5xa7 c1xa3 e1-d2 g5xe3xc1 a1-b2 c3xa1 g3-f4 e5xg3xe1 -> ooX.ooX.ooX.o|..|oooX...|..|...|.x.x|xxO.x.x.x
6 promotions at 21 plies. (first difference to list above)
e3-d4 c5xe3 f2xd4 d6-c5 g1-f2 c7-d6 e3-f4 g5xe3xg1 g3-f4 c5xe3 e1-f2 e5xg3xe1 c3xe5xc7 e1xc3 b2xd4 e3-f2 c1-d2 f2-e1 c7-d6 e7xc5xe3xc1 c3xe5xc7 -> o.ooOX.oX.ooX|..|o...|..|x...|x.x.x|x.xXooO..x
7 promotions at 23 plies.
e3-d4 c5xe3 f2xd4 d6-c5 g1-f2 c7-d6 e3-f4 g5xe3xg1 g3-f4 c5xe3 e1-f2 e5xg3xe1 c3xe5xc7 e1xc3 b2xd4 e3-f2 c1-d2 f2-e1 c7-d6 e7xc5xe3xc1 c3xe5xc7 a5-b4 a3xc5xe7 -> o.ooOX.oX.ooX|..|...|..|...|x..x|x.xXooO.xxO.x
8 promotions at 27 plies.
e3-d4 c5xe3 f2xd4 d6-c5 g1-f2 c7-d6 e3-f4 g5xe3xg1 g3-f4 c5xe3 e1-f2 e5xg3xe1 c3xe5xc7 e1xc3 b2xd4 e3-f2 c1-d2 f2-e1 c7-d6 e7xc5xe3xc1 c3xe5xc7 c1-b2 a1xc3 f6-e5 c3-b4 a5xc3xa1 a3xc5xe7 -> OoX..oX.ooX|oo..|...|..|..x.|x..|x.xXooO.xXO.x

above optimal.

9 promotions at 30 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 g5-f4 a1-b2 e1-f2 g1xe3xg5xe7 d2-e3 d6xf4xd2 b6-c5 e3-d4 c5xe3xc1 e5-d6 c7xe5xc3xa1 d6-c7 d2-e1 -> oooX.OoX.XXxX.|..oo|...|..|...|..|x.ooO.xxXO.x
10 promotions at 33 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 a3-b4 c5xa3xc1 e3xc5xe7 g5-f4 e7xg5xe3 e5-d6 e3-d4 b6-a5 d4-e3 e1-f2 e3-d2 c1xe3 a1-b2 e3-d4 g1xe3xc5xe7 c7xe5xc3xa1 d6-c7 d2-e1 c7-b6 a7xc5xe3xg1 b6-a7 -> oooX..xxxX.ooOX|..o|...|.O.|x...|..|oO..XXXO.x

above likely optimal. but only 28 plies completely searched.

11 promotions at 35 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 b2-c3 c5-b4 a3xc5xe7 e1-d2 a1-b2 d2xb4 g3-f4 e5xg3xe1 e7-d6 c7xe5xc3xa1 e3xc5xe7 e1-d2 c1xe3 b6-a5 e7xc5xa3 a5xc3xe1 e3-f4 g5xe3xc1 a3-b2 c1xa3xc5 b4xd6 a7-b6 c5xa7 b2-c1 d6-c7 e1-f2 g1xe3xg5xe7 -> ooOX.XxxX..|..oo|...|..|...|..|xoOoO.XO.xXXO.x
12 promotions at 40 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 b2-c3 c5-b4 a3xc5xe7 e1-d2 a1-b2 d2xb4 g3-f4 e5xg3xe1 e7-d6 c7xe5xc3xa1 e3xc5xe7 e1-d2 c1xe3 b6-a5 e7xc5xa3 a5xc3xe1 e3-f4 g5xe3xc1 a3-b2 c1xa3xc5 b4xd6 a7-b6 c5xa7 b2-c1 d6-c7 c1-d2 a7-b6 d2-e3 b6-a7 e3-f2 g1xe3xg5xe7 f2-g1 -> ooOX..ooX.XxX|..|...|..|...|..|xoOoO.XO.xXXO.x
13 promotions at 43 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 c3-b4 a5xc3xe1 b2-c3 c5-b4 a3xc5xe7 e1-d2 a1-b2 d2xb4 g3-f4 e5xg3xe1 e7-d6 c7xe5xc3xa1 e3xc5xe7 e1-d2 c1xe3 b6-a5 e7xc5xa3 a5xc3xe1 e3-f4 g5xe3xc1 a3-b2 c1xa3xc5 b4xd6 b2-c1 c5-b6 a7xc5 b6-a7 c1-d2 d6-c7 e1-f2 g1xe3xg5xe7 d2-e3 f2xd4xb6 e3-f2 c7-d6 f2-g1 b6-c7 -> ooOX...XxX|..|...|..|.o..|.XO.|oOO.xXoO.xXXO.x
14 promotions at 46 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 f2-g1 c7-b6 a7xc5xe3xc1 b6-a7 c1-b2 a1xc3 d2-e1 c3-d4 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xa1 f6-e7 a3xc1 -> OoOX.oX.XXxX.ooX|O..|...|.X.|...x|..|XxO..OoO.
15 promotions at 50 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c1-b2 a1xc3 b2-c1 c3-b4 a3-b2 b4xd6 f2-g1 d6-c7 b2-a1 -> X.XXX.OoOX.ooX|..|...|..|.O..x|..|XxO.xXO.OoO.
16 promotions at 53 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 f2-g1 c7-b6 a7xc5xe3xc1 b6-a7 a5-b4 d6-e7 d2-e1 e7-f6 g7xe5 f6-e7 c1-b2 a1xc3xa5 g1-f2 b2-c3 e1-d2 c3xe1xg3 d2-c1 g3-f4 e5xg3xe1 a5-b6 g5xe3xg1 b6-c7 -> .XXX.oOOX.ooX|..|...|..XXO|...|..|XxO.xXO.OoO.
17 promotions at 57 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2 a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 f2-g1 f6-e5 g1-f2 d4-e3 f2xd4xf6 e5xg7 b2-a1 f6-e7 -> X.XXX.OoOX.|..|...|..|...x|..|XxO.xXO.OOoO.XOO
18 promotions at 62 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2 a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 g5-f4 f6-g5 f4-e3 g5-f4 f2-g1 d4xf2 g1xe3xg5 f4-e5 g5-f6 e5xg7 f2-g1 f6-e7 b2-a1 -> X.XXX.OoOX.X|..|...|..|...|..|XxO.xXO.OOoO.XOO
19 promotions at 69 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2 a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 g5-f4 f6-g5 f4-g3 g5-f6 f2-g1 c7-b6 e1-d2 b6-a5 g1-f2 f6-e5 f2-e3 d4xf2 d2xf4xd6 e5xc7 g3xe1 d6-e5 f2xd4xf6 e5xg7 b2-a1 f6-e7 -> X.XXX.OX.|..|...|..|xXO...|..|XxO.XOO.OO.XOoOO
20 promotions at 76 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2 a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 g5-f4 f6-g5 f4-g3 g5-f6 f2-g1 c7-b6 e1-d2 b6-a5 g1-f2 f6-e5 f2-e3 d4xf2 d2xf4xd6 e5xc7 g3xe1 d6-e5 f2xd4xf6 e5xg7 b2-a1 f6-e7 e1-d2 a5-b4 d2-c3 b4xd2 c1xe3 c3xe1 d2-c1 -> X.XX.XO.|..|..OXXX.|..|...|..|XxO.XOO.OO.XOoOO
21 promotions at 80 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2 a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 g5-f4 f6-g5 f4-g3 g5-f6 f2-g1 c7-b6 e1-d2 b6-a5 g1-f2 f6-e5 f2-e3 d4xf2 d2xf4xd6 e5xc7 g3xe1 d6-e5 f2xd4xf6 e5xg7 b2-a1 f6-e7 a1-b2 a5-b4 c1-d2 b4-c3 b2xd4 a7-b6 d4-c5 b6xd4xb2 c3xa1 c5-b6 b2-c1 -> OX.XXXX.OX.|.XXX.|...|..|...|O..|.XOO.OO.XOoOO
22 promotions at 86 plies.
e3-d4 c5xe3 f2xd4 d6-c5 e1-f2 e7-d6 g3-f4 e5xg3xe1 c3-b4 e1xc3xe5 c1-d2 a5xc3xe1 g1-f2 e1xg3 a3-b4 c5xa3xc1 e3xc5xe7 c1-d2 e7-d6 d2-e3 d6xf4xd2 f6xd4xf2 e3-d4 c7-d6 e5xc7 b6-a5 c7-d6 g3-f4 d6-e5 f4xd6 e5xc7 a5-b4 d6-e7 b4-a3 e7-f6 g7xe5xc3xe1 f6-e7 d2-c1 c7-b6 a7xc5 b6-a7 c5-d4 e7-f6 c1-b2 a1xc3xe5 b2-c1 e5-d6 a3-b2 d6-c7 g5-f4 f6-g5 f4-g3 g5-f6 f2-g1 c7-b6 e1-d2 b6-a5 g1-f2 f6-e5 f2-e3 d4xf2 d2xf4xd6 e5xc7 g3xe1 d6-e5 f2xd4xf6 e5xg7 b2-a1 f6-e7 a1-b2 a5-b4 c1-d2 b4-c3 d2xb4 a7-b6 b2-a3 g7-f6 b4-c5 b6xd4xb2 c3xa1 e7-d6 c5xe7xg5 d6-e5 g5xe7 f6-g7 b2-c1 -> OX.XXXX.OX.|..|X...|..|..O.|..|.XOO.OOOOXX.XOO



leaves at given ply without promotion
-------------------------------------

ply leafs
1  3
2  3
3  9
4  27
5  58
6  162
7  423
8  1021
9  2556
10  5770
11  14047
12  33563
13  83368
14  200770
15  496564
16  1158813
17  2775423
18  6371031
19  15031271
20  34664699
21  81611970
22  188406110
23  438938224
24  1005932956
25  2308043361
26  5236611323
27  11906548514

If you calculate the branching factor, you find it nearly constant:
#27 / #26 = 2.2737
#26 / #25 = 2.2688
#25 / #24 = 2.2944
#24 / #23 = 2.2917

We had hoped the number of positions without promotion would collapse faster.
We still assume(d) that not a lot more than 30 plys are possible. How wrong we were!

The longest we found  using a specially crafted version of laska is this
91-ply sequence without promotion. This is now though to be the
longest possible sequence in no-officer Laska.

We had to introduce a simplification to make these calculations
feasible. The assumption "double jumps must lead to loss of potential"
was made, i.e. no sequences with double jumps were considered.
A prove of this assumption is unknown.

There are several permutation of this move sequences, we did not log.

a3-b4 c5xa3 g3-f4 e5xg3 c3-d4 a5-b4 d4-e5 d6xf4 e3-d4 b4-c3 d2xb4 f4-e3 d4-e5
f6xd4 b4-c5 d4-c3 b2xd4 b6-a5 c5-b6 a7xc5 d4-e5 a5-b4 c3xa5 c5-b4 f2xd4 g5-f4
e3xg5 b4-c3 d4-c5 b6xd4 e5-d6 c7xe5 a5-b6 c3-b2 a1xc3 d4-e3 c3-b4 e5-d4 b4-a5
d4-c3 b2xd4 e3-d2 c1xe3 a3-b2 c3-b4 c5xa3 d4-c5 d6xb4 e3-f4 c5-d4 f4-e5 b4-c3
d2xb4 d4-e3 e5-f6 g7xe5 b4-c5 g3-f2 e1xg3 e5-f4 f2xd4 f4-e3 d4-e5 f6xd4 c3-b4
e3-f2 g1xe3 d4-c3 e3-f4 e5-d4 f4-e5 c3-d2 e5-f6 d4-c3 c5-d6 e7xc5 g3-f4 c5-d4
f4-e5 d6xf4 b4-c5 d4-e3 f2xd4 f4-g3 c5-d6 g3-f2 d4-c5 e5-d4 e3-f4 d4-e3 f4-e5

In the meantime we could solve this problem completely by brute
forcing the game. We calculated *all* possible sequences using around
4000 d CPU times on a 64 core machine.

The program generated around 1.6 * 10^6 N/s. Therefore all in all
we visited 60*64*24*3600*1.6 * 10^6 = 5.3 * 10^14 positions.
Incredible!

The breakthrough in reducing the complexity for the computing of all
branches was twofold. First we invented a simple transposition
recognizer, second we resorted to splitting the sequences into all
*unique* position after ply 25. The branching factor for a sequence
was
reduced from around 2.2-2.3 to around 1.6-1.7 depending on starting
positions. That means with n plys we defer the split deeper in the 
search tree we only need (1.6/2.2)^n of the original computing time.

Using a deeper split than 25 plies induced problems with the number of
files generated etc, so 25 was kind of a sweet spot for this machine.

For example for sequence number 1 "a3-b4 c5xa3 c3-b4 a5xc3 b2xd4"
the number of sequences after ply 25 reduced from 7439488 to 1071145,
or to 14.4%, if only unique positions after 25 plys are researched,
not unique positions after 5 plys!

For supervising the running searches some nice shell invocations were
used:

find . -maxdepth 1 ! -empty -mmin +1 -name "*log" |\
xargs -I {} sh -c "echo -n {} >> records ;  grep -A1 record {} |\
 tail -2 >> records ; bzip2 -9 {}" ;\
 grep record records | sort -n -r -k3 > records.sorted ;\
 head records.sorted

A small R script that gave a current estimation of remaining execution 
time every minute:

joblog_analyser.R:

no_pos <- 442187
no_cpus <- 62
delay <- 60

jobtable <- data.frame()

while (TRUE) {
newjobs <- read.table("joblog", header=FALSE,
		      skip=1+NROW(jobtable), 
		      colClasses=c("integer", "NULL", "NULL", 
					  "double", rep("NULL", 12)))
if (NROW(newjobs) > 0) {
jobtable <- rbind(jobtable, newjobs)
cat("Complete jobs: ", NROW(jobtable), "\n")
	cat("Total CPU Time per CPU  (d) ", sum(jobtable[,2])/86400/no_cpus, "\n")
	cat("Expected total time (d) ", mean(jobtable[,2])*no_pos/no_cpus/86400, "\n") 
	cat("Expected waiting time left (d) ", mean(jobtable[,2])*no_pos/no_cpus/86400 - sum(jobtable[,2])/86400/no_cpus, "\n")
	cat("Last output at ", as.character(Sys.time()), "\n\n")
}
Sys.sleep(delay)
}

For generating all unique positions after n plies the following may
be used (in portions according to unique positions after 5 plies)

POS=37
for i in `seq 6 25` 
do nice -19 ./laska -A "c3-d4 e5xc3 b2xd4 a5-b4 c3xa5" \
	    -g -d $((i-5)) -b > sequences-pos${POS}.${i}plys 2>&1
			;\
nice -19 fgrep -A2 too sequences-pos${POS}.${i}plys |\
nice -19 grep -v too |\
nice -19 xargs -n2 -d "\n" > sequences-pos${POS}.${i}plys-oneline;\
nice -19 sort -u -k $((i+1)) sequences-pos${POS}.${i}plys-oneline >\
sequences-pos${POS}.${i}-unique 
wc sequences-pos${POS}.${i}-unique 
done

After we have learned so much a clean check of the records would
involve creating all unique positions starting with a3-b4, c3-b4, or
c3-d4 after say 25 oder 26 plys
and solve 1 million of them at a time to avoid the limitations of
the file system. The time to calculate all these positions probably
can be significantly reduced, if the uniquification is performed after 
each ply or every second(third) ply. (The overhead of restarting laska
forbids  each ply, even if theoretically perfect.)



Branch noofficers
-----------------
This special version shall solve the problem how many moves are possible, until a
promotion occures.

here we only search moves of privates and therefore
many, many modifications and speedups became possible.

Today, 1.2.2016, this version is able to search 6.5 MN/s on a single  thread on a 4.4 GHz i7.

* Files
** pos-after-5
contains the 58 positions after 5 plys starting with a3-b4, c3-b4, c3-d4
** Loop over that file and save the output
i=0; for line in `cat pos-after-5` ; do echo $line ; i=$((i +1)); echo $i ; (./laska -p "$line" -g -d10 > /tmp/$i.txt 2>&1  &) ;done	

** further improvement using GNU parallel:
parallel --joblog ./joblog --results ./tmp -j50 "nice -19 ./laska -A \"{}\" -d100 -g > ./tmp/{#}.log 2>&1"  < sequences-pos21.20ply

dabei muss sequences* in jeder Zeile eine Sequenz der gewüschten Tiefe enthalten.


Move permutations
-----------------

Shortest sequences from beginning leading to the same position with different
order of moves occurs at 6 plies:

a3-b4 c5xa3 e3-d4 g5-f4 f2-e3 d6-c5
a3-b4 c5xa3 e3-d4 d6-c5 f2-e3 g5-f4

And of course two symmetric sequences:

g3-f4 e5xg3 c3-d4 a5-b4 b2-c3 d6-e5
g3-f4 e5xg3 c3-d4 d6-e5 b2-c3 a5-b4

Next new 16 (32) cases occur at ply 8.

It is also possible to reach the same position with a different number of plies.
First example at 8 / 10 plies:
g3-f4 e5xg3 c3-d4 f6-e5 d4xf6 g7xe5 d2-c3 g5-f4
g3-f4 e5xg3 e3-f4 g5xe3 d2xf4 f6-e5 f4-g5 e5-f4 g5-f6 g7xe5


Positions with maximum number of possible moves
-----------------------------------------------

there are positions with 32 possible moves.  see [theo.md](theo.md) for detail.
from where it is impossible to construct positions with 35 possible moves,
due to forced capturing move or lack of a necessary 12th stone.

1 step moves at 10 plies.
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 f6xd4 c1-d2 b6-a5 g3-f4 a5xc3
f2-g3 

2 step moves at 8 plies.
c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 f6xd4 e1-d2 b6-a5
e3-f4 f2-g3 

3 step moves at 5 plies.
e3-d4 c5xe3 d2xf4 a5-b4 c3xa5
e5-d4 b6-c5 d6-c5 

4 step moves at 3 plies.
c3-b4 a5xc3 b2xd4
e5-f4 c5-b4 g5-f4 b6-a5

5 step moves at 2 plies.
g3-f4 e5xg3
c3-b4 e3-d4 a3-b4 c3-d4 e3-f4 

6 step moves at 4 plies.
c3-b4 a5xc3 d2xb4 b6-a5
e1-d2 c1-d2 e3-d4 g3-f4 c3-d4 e3-f4 

7 step moves at 5 plies.
g3-f4 e5xg3 e3-d4 c5xe3 f2xd4
a5-b4 g5-f4 g3-f2 b6-c5 d6-e5 d6-c5 f6-e5 

8 step moves at 6 plies.
c3-b4 a5xc3 b2xd4 b6-a5 d4xb6 a7xc5
c1-b2 a1-b2 c3-b4 e3-d4 g3-f4 a3-b4 c3-d4 e3-f4 

9 step moves at 8 plies.
e3-d4 c5xe3 d2xf4 b6-c5 c3-d4 e5xc3 b2xd4xb6 a7xc5
c1-b2 e1-d2 a1-b2 c1-d2 c3-b4 e3-d4 a3-b4 c3-d4 f4-e5 

10 step moves at 9 plies.
e3-d4 c5xe3 f2xd4 d6-c5 g3-f4 e5xg3 d4-e5 f6xd4xf2 g1xe3
c7-d6 e7-f6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5

11 step moves at 12 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 c3-d4 e5xc3 b2xd4xb6 a7xc5 f2-e3 f6-e5
c1-b2 e1-d2 g1-f2 a1-b2 c1-d2 e1-f2 e3-d4 g3-f4 c3-d4 e3-f4 g5-f6 

12 step moves at 13 plies.
g3-f4 e5xg3 c3-b4 a5xc3 d2xb4 f6-e5 e1-d2 g3xe1 e3-f4 e5xg3 b4-a5 d6-e5 d2-e3
c7-d6 e7-f6 e7-d6 g7-f6 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 g3-f2 e1-d2 e1-f2 

13 step moves at 13 plies.
e3-d4 c5xe3 f2xd4 d6-c5 g3-f4 e5xg3 e1-f2 g3xe1 g1-f2 e1xg3 d4-e5 f6xd4xf2 d2-e3
c7-d6 e7-f6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 f2-g1 f2-e1 g3-f4 

14 step moves at 16 plies.
e3-d4 c5xe3 f2xd4 g5-f4 e3xg5 d6-c5 d2-e3 e7-d6 g5xe7 g7-f6 e7xg5 c5-b4 a3xc5xe7 b6-c5 d4xb6 a7xc5
e1-d2 g1-f2 c1-d2 e1-f2 c3-b4 e3-d4 g3-f4 c3-d4 e3-f4 g5-f6 b2-a3 e7-f6 e7-d6 g5-f4 

15 step moves at 15 plies.
c3-b4 a5xc3 d2xb4 b6-a5 g3-f4 e5xg3 c1-d2 f6-e5 e3-d4 c5xe3xc1 b4-c5 d6xb4xd2 e1xc3 g3xe1 d2-e3
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 e1-d2 c1-d2 e1-f2 

16 step moves at 17 plies.
c3-b4 a5xc3 d2xb4 b6-a5 c1-d2 a7-b6 e3-d4 c5xe3xc1 f2-e3 e5-d4 e3xc5xa7 f6-e5 e1-d2 c1xe3 b4-c5 d6xb4xd2 b2-c3
c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 e3-f2 d2-e1 d2-c1 e3-d4 e3-f4 

17 step moves at 19 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 g3-f4 e5xg3 c1-d2 f6-e5 c3-d4 c5xe3xc1 e1-d2 a5xc3xe1 a3xc5 d6xb4 b2-a3 b4-c3 f2-e3
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 c5-d4 e5-f4 c5-b4 e5-d4 c3-d2 c3-b2 g3-f2 c1-b2 e1-d2 c1-d2 e1-f2 

18 step moves at 19 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 d6-c5 c1-d2 c5-d4 e3xc5 g5xe3xc1 c3-d4 f6-g5 e1-d2 g3xe1 g1-f2 e1xc3xe5 f2-e3 b6xd4xf2 b2-c3
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 c5-b4 e5-d4 f4-g3 f4-e3 f2-g1 f2-e1 c1-b2 c1-d2 e5-d6 e5-f6 

19 step moves at 21 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 g3-f4 e5xg3 c1-d2 f6-e5 c3-d4 c5xe3xc1 e1-d2 g3xe1 b2-c3 c1xe3 b4-c5 d6xb4xd2 a1-b2 d2-c1 b2-c3
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 e5-f4 c5-b4 e5-d4 e3-f2 e3-d2 c1-b2 e1-d2 c1-d2 e1-f2 e3-d4 e3-f4 

20 step moves at 23 plies.
c3-b4 a5xc3 d2xb4 b6-a5 g3-f4 e5xg3 e1-d2 g3xe1 c3-d4 e1xc3xe5 e3-d4 c5xe3 g1-f2 e3xg1 b2-c3 g1-f2 c1-b2 g5-f4 b4-c5 d6xb4xd2 b2-c3 f6-g5 a1-b2
f2-e3 e5-d6 f2-g3 e5-f6 d2-e1 f2-g1 f4-g3 a5-b4 c5-d4 a7-b6 c7-d6 e7-f6 d2-c1 f2-e1 f4-e3 c5-b4 e5-d4 c7-b6 e7-d6 g7-f6

21 step moves at 25 plies.
c3-b4 a5xc3 d2xb4 b6-a5 c3-d4 e5xc3 b2xd4xb6 a7xc5 e1-d2 f6-e5 g3-f4 e5xg3xe1 c3-d4 e1xc3xe5 a1-b2 a5xc3xa1 c1-d2 a1-b2 e3-d4 c5xe3xc1 a3xc5xa7 c1-d2 a7xc5 d6xb4 g1-f2
b2-a3 d2-c3 e5-d6 b2-c3 d2-e3 e5-f6 b2-c1 d2-e1 b4-c3 c5-d4 e5-f4 c7-d6 e7-f6 b2-a1 d2-c1 b4-a3 e5-d4 g5-f4 c7-b6 e7-d6 g7-f6

22 step moves at 25 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 c1-d2 e5-d4 c3xe5 d6-c5 e3-d4 c5xe3xc1 e1-d2 g3xe1 g1-f2 g5xe3xg1 d2-c3 e1-d2 c3-d4 g1-f2 d4-c5 b6xd4 a3-b4 c1xa3xc5 a1-b2
d2-c3 f2-e3 c5-b6 d2-e3 f2-g3 c5-d6 d2-e1 f2-g1 d4-e3 f4-g3 a5-b4 a7-b6 c7-d6 e7-f6 d2-c1 f2-e1 d4-c3 f4-e3 c5-b4 c7-b6 e7-d6 g7-f6

23 step moves at 27 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 c7-b6 c3-d4 a5xc3xe1 b2-c3 b4xd2 a1-b2 e5xc3xa1 g3-f4 e1xg3xe5 g1-f2 a1-b2 e3-d4 c5xe3xg1 c1xe3 g1-f2 e3-f4 g5xe3xc1 a3-b4 c1-d2 b4-a5 e5-d4 a5xc7
b2-a3 d2-c3 f2-e3 d4-c5 b2-c3 d2-e3 f2-g3 d4-e5 b2-c1 d2-e1 f2-g1 d4-e3 f4-g3 d6-e5 f6-g5 a7-b6 b2-a1 d2-c1 f2-e1 d4-c3 f4-e3 d6-c5 f6-e5

24 step moves at 29 plies.
g3-f4 e5xg3 a3-b4 c5xa3 e3-f4 g5xe3 d2xf4 b6-c5 e1-d2 g3xe1 f4-g5 e1-f2 e3-f4 f6-e5 g1xe3 e5xg3xe1 e3-d4 c5xe3xg1 c3xe5 d6xf4 d2-c3 g1-f2 c1-d2 a3xc1 c3-d4 c1xe3xc5 a1-b2 e1-d2 b2-a3
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 a5-b4 c5-d4 c5-b4 e5-d4 f4-g3 f4-e3 d2-e1 f2-g1 d2-c1 f2-e1 c5-b6 e5-d6 c5-d6 e5-f6 d2-c3 f2-e3 d2-e3 f2-g3

25 step moves at 29 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 e3-d4 c5xe3 g1-f2 e3xg1 d2-e3 d6-c5 c1-d2 g1-f2 e3-d4 c5xe3xc1 a3-b4 c1xa3xc5 f4-e5 f6xd4xb2 a1xc3 e1-d2 c3-b4 a5xc3xa1 b2-c3 a1-b2 c3xa5
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 g7-f6 c5-d4 e5-f4 c5-b4 e5-d4 g5-f4 b2-c1 d2-e1 f2-g1 b2-a1 d2-c1 f2-e1 c5-b6 c5-d6 b2-a3 d2-c3 f2-e3 b2-c3 d2-e3 f2-g3

26 step moves at 33 plies.
g3-f4 e5xg3 e3-d4 c5xe3 f2xd4 f6-e5 d4xf6 g7xe5 e1-f2 g3xe1 a3-b4 e1-f2 c3-d4 a5xc3xe1 b2-c3 e1-d2 a1-b2 d2xb4 c1-d2 e5xc3xa1 e3-f4 g5xe3xc1 g1xe3 d6-c5 e3-d4 c5xe3xg1 f2-g3 a1-b2 g3-f4 g1-f2 f4-e5 c1-d2 e5xg7 
c7-d6 e7-d6 b6-c5 f6-g5 b6-a5 f6-e5 b4-c3 d4-e3 b4-a3 d4-c3 b2-c1 d2-e1 f2-g1 b2-a1 d2-c1 f2-e1 b2-a3 d2-c3 f2-e3 b2-c3 d2-e3 f2-g3 b4-a5 d4-c5 b4-c5 d4-e5

above optimal

27 step moves at 35 plies.
g3-f4 e5xg3 c3-d4 g5-f4 e3xg5 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 b2-c3 e1-f2 a3-b4 f2xd4xb2 c1xa3 a5xc3xe1 b2-c3 e1-f2 g1xe3 b6-a5 c3-d4 f6-e5 d4xf6 g7xe5xg3xe1 e3-d4 c5xe3xg1 a1-b2 e1-d2 a3-b4 a5xc3xa1 f6-g7 g1-f2 g7-f6 a1-b2 f6-g7
a7-b6 c7-d6 e7-f6 c7-b6 e7-d6 b4-c3 d4-e3 f4-g3 b4-a3 d4-c3 f4-e3 b2-c1 d2-e1 f2-g1 b2-a1 d2-c1 f2-e1 b2-a3 d2-c3 f2-e3 b2-c3 d2-e3 f2-g3 b4-a5 d4-c5 b4-c5 d4-e5

28 step moves at 47 plies. Derived from 27 moves example.
g3-f4 e5xg3 c3-d4 g5-f4 e3xg5 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 b2-c3 e1-f2 a3-b4 f2xd4xb2 c1xa3 a5xc3xe1 b2-c3 e1-f2 g1xe3 b6-a5 c3-d4 f6-e5 d4xf6 g7xe5xg3xe1 e3-d4 c5xe3xg1 a1-b2 e1-d2 a3-b4 a5xc3xa1 f6-g7 g1-f2 g7-f6 a1-b2 f6-g7 f2-g3 g7-f6 f4-e3 f6-g7 e3-f2 g7-f6 g3-f4 f6-g7 f2-g1 g7-f6 g1-f2 f6-g7
b2-a3 d2-c3 f2-e3 b4-a5 d4-c5 f4-e5 b2-c3 d2-e3 f2-g3 b4-c5 d4-e5 b2-c1 d2-e1 f2-g1 b4-c3 d4-e3 f4-g3 a7-b6 c7-d6 e7-f6 b2-a1 d2-c1 f2-e1 b4-a3 d4-c3 f4-e3 c7-b6 e7-d6

29 step moves at 85 plies. Derived from 27 moves example.
g3-f4 e5xg3 c3-d4 g5-f4 e3xg5 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 b2-c3 e1-f2 a3-b4 f2xd4xb2 c1xa3 a5xc3xe1 b2-c3 e1-f2 g1xe3 b6-a5 c3-d4 f6-e5 d4xf6 g7xe5xg3xe1 e3-d4 c5xe3xg1 a1-b2 e1-d2 a3-b4 a5xc3xa1 f6-g7 g1-f2 g7-f6 a1-b2 f6-g7 f4-e3 g7-f6 f2-g3 f6-g7 e3-f2 g7-f6 f2-g1 f6-g7 g1-f2 g7-f6 g3-f4 f6-g7 a7-b6 g7-f6 c7-d6 f6-g7 d6-e5 g7-f6 b6-a5 f6-g7 d4-c5 g7-f6 e5-d4 f6-g7 d4-e3 g7-f6 f4-g3 f6-g7 d2-c3 g5-f6 e7xg5 g7xe5 e3-d2 e5-f6 d2-e1 f6-g7 c5-b6 g7-f6 b4-c5 f6-g7 c5-d6 g7-f6 c3-d4 f6-g7 a5-b4 g7-f6 e1-d2 f6-g7 g3-f4 g7-f6
b2-a3 d2-c3 f2-e3 d4-c5 f4-e5 b6-a7 d6-c7 b2-c3 d2-e3 f2-g3 d4-e5 b6-c7 d6-e7 b2-c1 d2-e1 f2-g1 b4-c3 d4-e3 f4-g3 b6-c5 d6-e5 b2-a1 d2-c1 f2-e1 b4-a3 d4-c3 f4-e3 b6-a5 d6-c5

30 step moves at 83 plies. Derived from "27 step moves at 69 plies." moves example.
e3-f4 g5xe3 d2xf4 a5-b4 c3xa5 c5-d4 e3xc5 b6xd4 b2-c3 d4xb2 a1xc3 c7-b6 a5xc7 c5-b4 a3xc5 d6xb4xd2 c1xe3 c3xa1 f4xd6 a1xc3 d2xb4 c5xa3 c3-d4 e7xc5 c7xe5 d6xf4xd2 e1xc3 e3xc1 d4xb6 c1xe3 f2xd4 a3-b2 b6-c7 f6-g5 d4xf6 b2-a1 c5-d6 e5-d4 e3xc5 d4xb2 g1-f2 g7xe5 g3-f4 e5xg3xe1 c5-d4 b2-c1 d4xb2 c1xa3xc5xe7 c7-d6 a1xc3 d6-e5 f6xd4 e5-f6 e1-d2 f6-g7 b2-c1 g7-f6 a7-b6 f6-g7 e7-d6 g7-f6 d2-e1 f6-g7 c1-b2 g7-f6 e1-f2 f6-g7 c3-d2 g7-f6 g5-f4 f6-g7 f4-e3 g7-f6 f2-g3 f6-g7 e3-f2 g7-f6 f2-g1 f6-g7 g1-f2 g7-f6 g3-f4 f6-g7
b2-a3 d2-c3 f2-e3 b4-a5 d4-c5 f4-e5 d6-c7 b2-c3 d2-e3 f2-g3 b4-c5 d4-e5 f4-g5 d6-e7 b2-c1 d2-e1 f2-g1 b4-c3 d4-e3 f4-g3 b6-c5 d6-e5 b2-a1 d2-c1 f2-e1 b4-a3 d4-c3 f4-e3 b6-a5 d6-c5

31 step moves at 97 plies. Derived from 27 moves example.
g3-f4 e5xg3 c3-d4 g5-f4 e3xg5 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 b2-c3 e1-f2 a3-b4 f2xd4xb2 c1xa3 a5xc3xe1 b2-c3 e1-f2 g1xe3 b6-a5 c3-d4 f6-e5 d4xf6 g7xe5xg3xe1 e3-d4 c5xe3xg1 a1-b2 e1-d2 a3-b4 a5xc3xa1 f6-g7 g1-f2 g7-f6 a1-b2 f6-g7 f4-e3 g7-f6 f2-g3 f6-g7 e3-f2 g7-f6 f2-g1 f6-g7 g1-f2 g7-f6 g3-f4 f6-g7 a7-b6 g7-f6 c7-d6 f6-g7 d6-e5 g7-f6 b6-a5 f6-g7 d4-c5 g7-f6 e5-d4 f6-g7 d4-e3 g7-f6 f4-g3 f6-g7 d2-c3 g5-f6 e7xg5 g7xe5 e3-d2 e5-f6 d2-e1 f6-g7 c5-b6 g7-f6 b4-c5 f6-g7 c5-d6 g7-f6 c3-d4 f6-g7 a5-b4 g7-f6 e1-d2 f6-g7 g3-f4 g7-f6 b4-c3 f6-g7 b2-a3 g7-f6 c3-b2 f6-g7 b2-a1 g7-f6 a1-b2 f6-g7 a3-b4 g7-f6
b6-c5 d6-e5 b6-a5 d6-c5 b4-c3 d4-e3 f4-g3 b4-a3 d4-c3 f4-e3 b2-c1 d2-e1 f2-g1 b2-a1 d2-c1 f2-e1 b2-a3 d2-c3 f2-e3 b2-c3 d2-e3 f2-g3 b4-a5 d4-c5 f4-e5 b4-c5 d4-e5 b6-a7 d6-c7 b6-c7 d6-e7

32 step moves at 100 plies. Derived from another 30 moves example.
c3-d4 e5xc3 b2xd4 d6-e5 c1-b2 e5-f4 g3xe5 c7-d6 e5xc7 g5-f4 e3xg5 c5xe3xc1 c3xe5 f6xd4 e1-d2 c1xe3 c7-d6 e7xc5 f2-g3 g7-f6 g5xe7 d4-c3 b2xd4xf6 e3-d4 c3xe5xc7 c5xe3 e7xc5 e3-d2 d4-e5 b6xd4 c7-d6 d2-e1 d6xb4 a5xc3 g3-f4 e1-d2 f6-g7 c3-b2 a1xc3xa5 d2-c3 g7-f6 c3xa1 a5-b6 d4-e3 f4-g5 e3-d2 e5-d6 a1-b2 b6-c7 c5-d4 a3xc5 d4-c3 g1-f2 b4-a3 f2-e3 b2-a1 d6-e7 a1-b2 e7-d6 a7-b6 c5xa7 b2-c1 e3-f4 c3-b2 f6-e7 d2-e1 d6-e5 e1-d2 e7-d6 d2-e1 g5-f6 b2-a1 a7-b6 a3-b2 f6-e7 e1-d2 b6-c5 d2-c3 e7-f6 c1-d2 c7-b6 c3-b4 c5xa3xc1xe3 a1-b2 e5-d4 b2-a1 e3-f2 a1-b2 f4-e5 b2-a1 f6-g5 a1-b2 e5-f6 b2-a1 f6-e7 a1-b2 e7-f6 b2-a1 g5-f4 a1-b2
d2-c3 f2-e3 b4-a5 d4-c5 f4-e5 b6-a7 d6-c7 f6-e7 d2-e3 f2-g3 b4-c5 d4-e5 f4-g5 b6-c7 d6-e7 f6-g7 d2-e1 f2-g1 b4-c3 d4-e3 f4-g3 b6-c5 d6-e5 f6-g5 d2-c1 f2-e1 b4-a3 d4-c3 f4-e3 b6-a5 d6-c5 f6-e5


Positions with maximum number of possible capturing moves
---------------------------------------------------------

most likely the maximum number of capturing moves in a position is 46.
this follows moreover directly from the 29 moves example below.
42 such moves in a position are reached even easier.

1 capturing move at 2 plies.
c3-b4
a5xc3 

2 capturing moves at 3 plies.
c3-b4 a5xc3
d2xb4 b2xd4 

3 capturing moves at 5 plies.
e3-d4 c5xe3 d2xf4 e5-d4 e3xc5
g5xe3 b6xd4 d6xb4xd2 

4 capturing moves at 7 plies.
e3-d4 c5xe3 d2xf4 e5-d4 e3xc5 g5xe3 g3xe5
b6xd4 d6xf4 d6xb4xd2 f6xd4 

5 capturing moves at 11 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 e5-d4 c3xe5 a5xc3xe1 c1-d2 e1xc3 b2xd4xb6
a7xc5 c7xa5 d6xf4xd2 f6xd4xb2 b4xd2 

6 capturing moves at 11 plies.
c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 b6-a5 f2-e3 e5-d4 c3xe5 a5xc3 b2xd4xb6
b4xd2 d6xf4xd2 a7xc5 f6xd4xb2 f6xd4xf2 c7xa5

7 capturing moves at 15 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 c1-d2 e7-f6 a3-b4 b6-c5 e3-d4 c5xe3xc1 e1-d2 g3xe1 c3-d4
c1xa3xc5xe3xc1 e1xc3 c1xe3xc5xa3xc1 a5xc3 e5xg3 e5xc3 g5xe3

8 capturing moves at 17 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 c1-d2 e7-f6 a3-b4 b6-c5 e3-d4 c5xe3xc1 e1-d2 g3xe1 g1-f2 c1xa3xc5 c3-d4
e1xc3 e1xg3 c5xe3xc1 c5xe3xg1 e5xg3 e5xc3 g5xe3xc1 g5xe3xg1

9 capturing moves at 21 plies.
c3-b4 a5xc3 b2xd4 e5-f4 g3xe5 d6xf4 c1-b2 f4-g3 e3-f4 g5xe3xc1 a3-b4 c5xa3 f2-e3 b6-a5 g1-f2 c1-d2 c3-b4 d2xf4 e1-d2 a3xc1 a1-b2
c1xa3xc5xe3xc1 c1xa3xc5xe3xg1 c1xe3xg1 c1xe3xc5xa3xc1 a5xc3xa1 a5xc3xe1 g3xe1 e5xc3xa1 e5xc3xe1

10 capturing moves at 21 plies.
e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 c7-b6 c3-d4 e5xc3xe1 b2-c3 d6-e5 f4xd6 e1-d2 a3-b4 d2xf4 g3xe5 c5xa3 c1-d2 f6xd4xb2 a1xc3 a3xc1 c3-d4
c1xa3 e5xc7 c1xe3xc5 e5xg3xe1xc3xa1 e5xg3xe1xc3xe5xg3 e5xg3xe1xc3xe5xc7 e5xc3xa1 e5xc3xe1xg3xe5xc7 g5xe3 e7xc5xe3 

11 capturing moves at 24 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 d6-c5 e1-d2 g3xe1 c3-b4 a5xc3 b2xd4 e1-f2 a1-b2 f6-e5 d4xf6 f2xd4 c3xe5 c5xe3 a3-b4 b6-a5 g1-f2 e3xg1 e5-d6 e1xg3
c7xe5xc3xa1 c7xe5xc3xe1 c7xe5xg3 e7xc5xa3 e7xc5xe3 g7xe5xc3xa1 g7xe5xc3xe1 g7xe5xg3 a5xc3xa1 a5xc3xe1 g5xe3 

12 capturing moves at 23 plies.
e3-d4 c5xe3 d2xf4 b6-c5 c1-d2 a7-b6 e3-d4 c5xe3xc1 f2-e3 c1-d2 a3-b4 d6-c5 f4xd6 d2xf4 g3xe5 c5xa3xc1 e1-d2 f6xd4xb2 a1xc3 c1xa3 c3-d4 e5xg3 g1-f2
g3xe5xc3xa1 g3xe5xc3xe1xg3xe5 a3xc1xe3xg1 a3xc1xe3xc5 c7xe5xc3xa1 c7xe5xc3xe1 g3xe1xc3xa1 g3xe1xc3xe5xg3 g5xe3xc1 g5xe3xg1 e7xc5xe3xc1 e7xc5xe3xg1

13 capturing moves at 24 plies.
e3-d4 c5xe3 d2xf4 b6-c5 c3-b4 a5xc3 b2xd4xb6 c7xa5 c3-d4 e5xc3 a3-b4 d6-e5 f4xd6 e7xc5xa3 e3-d4 a5-b4 d4-c5 b6xd4 c1-d2 a3-b2 d2-e3 c3-d2 e3xc5xe7 g5-f4
e1xc3xa5 e1xc3xe5xc7 g3xe5xc7 a1xc3xa5 a1xc3xe5xc7 e7xg5xe3xc1xa3xc5xe3 e7xg5xe3xc1xa3xc5xe7 e7xg5xe3xc5xa3xc1xe3 e7xg5xe3xc5xe7 e7xc5xa3xc1xe3xc5 e7xc5xa3xc1xe3xg5xe7 e7xc5xe3xc1xa3xc5 e7xc5xe3xg5xe7

14 capturing moves at 25 plies.
g3-f4 e5xg3 c3-d4 d6-e5 d2-c3 c7-d6 c1-d2 a5-b4 c3xa5xc7 e5xc3 d2xb4 c5-d4 e3xc5 d6-e5 e1-d2 g3xe1 c7-d6 e1-f2 g1xe3 e5-f4 c3-d4 f4-g3 c5-b6 g3xe1 e3-f4
e1xc3xa5xc7xe5xc3 e1xc3xa5xc7xe5xg3xe1 e1xc3xe5xg3xe1 e1xc3xe5xc7xa5xc3 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5 a7xc5xe3xc1 a7xc5xe3xg1 g5xe3xc1 g5xe3xg1 e7xc5xe3xc1 e7xc5xe3xg1

15 capturing moves at 27 plies. (Note, this takes one ply more than needed for 16 capturing moves.)
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 e1xc3 c5-d4 b4-a5 b6-c5 g3-f4 g5xe3 d2xf4xd6 c7xe5 c3-b4 f6-g5 c1-d2 e5-f4 a5-b6 d4-c3 b2xd4 e7-f6 a1-b2 f4-g3 e3-f4 g3xe1 g1-f2
e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xg3xe1 e1xc3xe5xg3xe1 e1xc3xe5xc7xa5xc3xa1 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5 c5xe3xc1 c5xe3xg1 g5xe3xc1 g5xe3xg1

16 capturing moves at 26 plies.
c3-b4 a5xc3 d2xb4 c5-d4 e3xc5 b6xd4 e1-d2 c7-b6 g3-f4 e5xg3xe1 c3xe5xc7 c5xe3 d2xf4 e1-d2 c7xa5 e7-d6 e3-d4 g5xe3 d4-e5 f6xd4 a5-b6 a7xc5 b4-a5 e3-f2 a5xc7 c5-b4
g1xe3xc5xa7 g1xe3xc5xe7 g1xe3xg5 c1xe3xc5xa7 c1xe3xc5xe7 c1xe3xg5 a3xc5xa7 a3xc5xe7 c7xe5xc3xe1xg3xe5 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5 c7xa5xc3xe1xg3xe5xc3 c7xa5xc3xe1xg3xe5xc7xa5 c7xa5xc3xe5xg3xe1xc3 c7xa5xc3xe5xc7xa5

17 capturing moves at 29 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 f6-e5 a3-b4 g7-f6 e1-d2 g3xe1 e3-d4 g5xe3 b2-a3 f6-g5 d4xf6 f4-g3 d2xf4 e1-d2 a1-b2 d6-c5 b4xd6 d2xb4 a3xc5 b6xd4xf2 g1xe3 g3xe1 e3-d4 c5xa3 c1-d2 
c7xe5xc3xa1 c7xe5xg3 e7xc5xe3xc1 e7xc5xe3xg1 a5xc3xa1 g5xe3xc1 g5xe3xg1 a3xc1xe3xg1 a3xc1xe3xc5xa3 e1xc3xa1 e1xc3xe5xg3xe1 e1xc3xe5xg7 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xg7 a3xc5xe3xc1xa3xc5 a3xc5xe3xg1 

18 capturing moves at 29 plies.
g3-f4 e5xg3 c3-d4 a5-b4 d4-e5 d6xf4 d2-c3 f4xd2 c3xa5 c7-d6 a5xc7 c5-d4 e1xc3xe5 f6xd4 b2-c3 g3xe1 a1-b2 d6-c5 c7-b6 e1-f2 g1xe3 e5-f4 c3xe5 e7-f6 e5-d6 f4-g3 e3-f4 g3xe1 a3-b4 
c5xe3xg1 c5xa3 g5xe3xg1 e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xc3xe1xg3xe5 e1xc3xa5xc7xe5xg3xe1xc3xa1 e1xc3xa5xc7xe5xg3xe1xc3xe5 e1xc3xe5xg3xe1xc3xa1 e1xc3xe5xg3xe1xc3xa5xc7xe5 e1xc3xe5xc7xa5xc3xa1 e1xc3xe5xc7xa5xc3xe1xg3xe5 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5

19 capturing moves at 28 plies.
c3-b4 a5xc3 d2xb4 c5-d4 e3xc5 b6xd4 e1-d2 c7-b6 g3-f4 e5xg3xe1 c3xe5xc7 c5xe3 d2xf4 e1-d2 c7xa5 e7-d6 e3-d4 g5xe3 d4-e5 f6xd4 a5-b6 a7xc5 b4-a5 g7-f6 a5xc7 e3-f2 c7xa5 c5-b4
g1xe3xc5xa7 g1xe3xc5xe7 g1xe3xg5xe7 c1xe3xc5xa7 c1xe3xc5xe7 c1xe3xg5xe7 a3xc5xa7 a3xc5xe7 a5xc7xe5xc3xe1xg3xe5xg7 a5xc7xe5xc3xa5 a5xc7xe5xg3xe1xc3xa5 a5xc7xe5xg3xe1xc3xe5xg7 a5xc7xe5xg7 a5xc3xe1xg3xe5xc3 a5xc3xe1xg3xe5xc7xa5 a5xc3xe1xg3xe5xg7 a5xc3xe5xg3xe1xc3 a5xc3xe5xc7xa5 a5xc3xe5xg7

20 capturing moves at 29 plies.
g3-f4 e5xg3 c3-d4 a5-b4 d4-e5 d6xf4 b2-c3 c7-d6 c3xa5xc7 d6-e5 a1-b2 e5-d4 c7-b6 d4-c3 d2xb4xd6 f4xd2 e1xc3 g3xe1 b6-c5 f6-e5 a3-b4 e1-f2 g1xe3 e5-f4 c3-d4 f4-g3 e3-f4 g3xe1 c5-b6 g1xe3
a7xc5xa3 a7xc5xe3xg1 e7xc5xa3 e7xc5xe3xg1 g5xe3xg1 e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xc3xe1xg3xe5 e1xc3xa5xc7xe5xg3xe1xc3xa1 e1xc3xa5xc7xe5xg3xe1xc3xe5 e1xc3xe5xg3xe1xc3xa1 e1xc3xe5xg3xe1xc3xa5xc7xe5 e1xc3xe5xc7xa5xc3xa1 e1xc3xe5xc7xa5xc3xe1xg3xe5 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1 e1xg3xe5xc7xa5xc3xe5 

21 capturing moves at 30 plies.
e3-d4 c5xe3 f2xd4 a5-b4 c3xa5 e5xc3 e3xc5 b6xd4 d2xb4 f6-e5 g3-f4 e5xg3 c3xe5 d6xf4 b4xd6 e7xc5xe3 e1-d2 c7-b6 a5xc7 g7-f6 d2-c3 g3-f2 c7-b6 a7xc5 c3-b4 e5-d4 b4-a5 c5-b4 a5xc7 e3-d2
g1xe3xc5xa7 g1xe3xc5xe7 c1xe3xc5xa7 c1xe3xc5xe7 a3xc5xa7 a3xc5xe7 c7xe5xc3xe1xg3xe5xc7xa5xc3 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7xe5xg3xe1xc3 c7xe5xc3xa5xc7xe5xg7 c7xe5xg3xe1xc3xa5xc7xe5xc3 c7xe5xg3xe1xc3xa5xc7xe5xg7 c7xe5xg3xe1xc3xe5xc7xa5xc3 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c7xa5xc3xe1xg3xe5xc3 c7xa5xc3xe1xg3xe5xc7 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3 c7xa5xc3xe5xc7 c7xa5xc3xe5xg7

22 capturing moves at 32 plies.
e3-d4 c5xe3 d2xf4 a5-b4 a3xc5 d6xb4xd2 f4xd6 c7xe5 e1xc3 g5-f4 e3xg5 b6-a5 g3-f4 e5xg3xe1 g1-f2 e1xg3 d2-e3 a7-b6 c1-d2 g3-f2 c3-b4 a5xc3xe1 e3-f4 e1-d2 f4-e5 d6xf4 e5-d6 c5-d4 d6-c7 e7-d6 g5xe7 g7-f6
c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5xg3 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 e7xg5xe3xc1 e7xg5xe3xg1 e7xg5xe3xc5xa3 e7xg5xe3xc5xa7 e7xg5xe3xc5xe7 c7xa5xc3xe1xg3xe5xc3 c7xa5xc3xe1xg3xe5xc7 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3 c7xa5xc3xe5xc7 c7xa5xc3xe5xg7 e7xc5xa3 e7xc5xe3xc1 e7xc5xe3xg1 e7xc5xe3xg5xe7 e7xc5xa7

23 capturing moves at 29 plies.
e3-d4 c5xe3 f2xd4 e5-f4 g3xe5 d6xf4 e1-f2 a5-b4 a3xc5 b6-a5 b2-a3 c7-d6 c3-b4 a5xc3xe1 a1-b2 d6xb4 d4xb6 f4xd2 c1xe3 e1xg3 a3xc5 e5-f4 e3-d4 g3-f2 g1xe3 f4-g3 c5-d6 g3xe1 e3-f4 
a7xc5xa3xc1 a7xc5xe3xc1 a7xc5xe3xg1 e7xc5xa3xc1 e7xc5xe3xc1 e7xc5xe3xg1 g5xe3xc1 g5xe3xg1 e1xc3xa1 e1xc3xa5xc7xe5xc3xa1 e1xc3xa5xc7xe5xc3xe1xg3xe5 e1xc3xa5xc7xe5xg3xe1xc3xa1 e1xc3xa5xc7xe5xg3xe1xc3xe5 e1xc3xe5xg3xe1xc3xa1 e1xc3xe5xg3xe1xc3xa5xc7xe5 e1xc3xe5xc7xa5xc3xa1 e1xc3xe5xc7xa5xc3xe1xg3xe5 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1xg3 e1xg3xe5xc3xa5xc7xe5 e1xg3xe5xc7xa5xc3xa1 e1xg3xe5xc7xa5xc3xe1xg3 e1xg3xe5xc7xa5xc3xe5

24 capturing moves at 31 plies.
g3-f4 e5xg3 c3-b4 a5xc3 d2xb4 c5-d4 c3xe5 d6xf4xd2 c1xe3 g5-f4 e3xg5 e5-f4 f2-e3 f6-e5 e1-f2 g3xe1 b2-c3 e7-d6 g5-f6 d6-c5 b4xd6 f4-g3 a3-b4 e1-f2 e3-f4 e5-d4 g1xe3xc5 b6xd4xb2 a1xc3 g3xe1 c3-d4 g3xe1
c7xe5xc3xa1 c7xe5xg3 g7xe5xc3xa1 g7xe5xg3 c5xe3xc1xa3xc5xe7xg5xe3xc1xa3 c5xe3xc1xa3xc5xe7xg5xe3xg1 c5xe3xg1 c5xe3xg5xe7xc5xa3xc1xe3xg1 c5xa3xc1xe3xg1 c5xa3xc1xe3xc5xe7xg5xe3xc1xa3 c5xa3xc1xe3xc5xe7xg5xe3xg1 c5xa3xc1xe3xg5xe7xc5xe3xc1xa3 c5xa3xc1xe3xg5xe7xc5xe3xg1 e1xc3xa1 e1xc3xa5 e1xc3xe5xg3xe1xc3xa1 e1xc3xe5xg3xe1xc3xa5 e1xg3xe5xc3xa1 e1xg3xe5xc3xe1 e1xg3xe5xc3xa5 c5xe7xg5xe3xc1xa3xc5xe3xc1xa3 c5xe7xg5xe3xc1xa3xc5xe3xg1 c5xe7xg5xe3xg1 c5xe7xg5xe3xc5xa3xc1xe3xg1 

25 capturing moves at 32 plies.
g3-f4 e5xg3 c3-b4 a5xc3 d2xb4 f6-e5 e3-f4 g5xe3 f2xd4xf6 e7xg5 e1-d2 c5-d4 e3xc5xe7 f6-e5 d2-e3 g3-f2 e3-f4 g5xe3 b4-a5 e5-f4 e7-d6 c7xe5 a5xc7 g7-f6 c7-b6 a7xc5 c3-b4 e5-d4 b4-a5 c5-b4 a5xc7 e3-d2
g1xe3xc5xa7 g1xe3xc5xe7 g1xe3xg5xe7 c1xe3xc5xa7 c1xe3xc5xe7 c1xe3xg5xe7 a3xc5xa7 a3xc5xe7 c7xe5xc3xe1xg3xe5xc7xa5xc3 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7xe5xg3xe1xc3 c7xe5xc3xa5xc7xe5xg7 c7xe5xg3xe1xc3xa5xc7xe5xc3 c7xe5xg3xe1xc3xa5xc7xe5xg3 c7xe5xg3xe1xc3xa5xc7xe5xg7 c7xe5xg3xe1xc3xe5xg3 c7xe5xg3xe1xc3xe5xc7xa5xc3 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c7xa5xc3xe1xg3xe5xc3 c7xa5xc3xe1xg3xe5xc7 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3 c7xa5xc3xe5xc7 c7xa5xc3xe5xg7

26 capturing moves at 33 plies.
g3-f4 e5xg3 e3-d4 c5xe3 d2xf4 b6-c5 e1-d2 g3xe1 c3-b4 e1xc3 b2xd4xb6 a7xc5 f4-e5 d6xf4xd2 c1xe3 e7-d6 g1-f2 g5-f4 e3xg5xe7 e5-f4 a1-b2 c5-d4 e7xc5xe3xg5 c7-d6 g5-f4 d6-c5 b4xd6 g7-f6 a3-b4 f6-e5 d4xf6 b6-c5 c3-d4
a5xc3xa1 a5xc3xe1 c5xe3xc1xa3xc5xe3xc1 c5xe3xc1xa3xc5xe3xg1 c5xe3xc1xa3xc5xe3xg5xe7xc5xe3xc1 c5xe3xc1xa3xc5xe3xg5xe7xc5xe3xg1 c5xe3xc1xa3xc5xe7xg5xe3xc1 c5xe3xc1xa3xc5xe7xg5xe3xg1 c5xe3xc1xa3xc5xe7xg5xe3xc5 c5xe3xg1 c5xe3xg5xe7xc5xa3xc1xe3xg1 c5xe3xg5xe7xc5xa3xc1xe3xc5 c5xe3xg5xe7xc5xe3xc1xa3xc5xe3xc1 c5xe3xg5xe7xc5xe3xc1xa3xc5xe3xg1 c5xe3xg5xe7xc5xe3xg1 c5xa3xc1xe3xg1 c5xa3xc1xe3xc5xe7xg5xe3xc1 c5xa3xc1xe3xc5xe7xg5xe3xg1 c5xa3xc1xe3xc5xe7xg5xe3xc5 c5xa3xc1xe3xg5xe7xc5xe3xc1 c5xa3xc1xe3xg5xe7xc5xe3xg1 c5xe7xg5xe3xc1xa3xc5xe3xc1 c5xe7xg5xe3xc1xa3xc5xe3xg1 c5xe7xg5xe3xg1 c5xe7xg5xe3xc5xa3xc1xe3xg1 c5xe7xg5xe3xc5xa3xc1xe3xc5 

above optimal. searched up to 34 plies.

27 capturing moves at 35 plies.
g3-f4 e5xg3 e3-f4 g5xe3 d2xf4 f6-e5 e1-d2 g3xe1 f4-g5 e5-d4 c3xe5 e1xc3 b2xd4 d6xf4xd2 c1xe3 c5-b4 d4xf6 g7xe5 a3xc5 b6xd4xb2 g1-f2 e5-f4 c3-d4 f6-g7 a1xc3 e7-f6 g5xe7 c5-b4 e3xg5 c7-b6 e7-d6 b4-a3 c3-b4 a3xc1 g5-f6
g7xe5xc3xa1 g7xe5xc3xe1xg3xe5xc3xa1 g7xe5xc3xe1xg3xe5xc3xe1 g7xe5xc3xe1xg3xe5xc7 g7xe5xg3xe1xc3xa1 g7xe5xg3xe1xc3xe5xc7 g7xe5xc7 a5xc3xa1 a5xc3xe1 c1xa3xc5xe3xc1 c1xa3xc5xe3xg1 c1xa3xc5xe3xg5xe7xc5xe3xc1 c1xa3xc5xe3xg5xe7xc5xe3xg1 c1xa3xc5xe7xg5xe3xc1 c1xa3xc5xe7xg5xe3xg1 c1xa3xc5xe7xg5xe3xc5 c1xe3xg1 c1xe3xc5xa3xc1xe3xg1 c1xe3xc5xa3xc1xe3xc5xe7xg5xe3xg1 c1xe3xc5xa3xc1xe3xg5xe7xc5xe3xg1 c1xe3xc5xe7xg5xe3xc1xa3xc5xe3xg1 c1xe3xc5xe7xg5xe3xg1 c1xe3xc5xe7xg5xe3xc5xa3xc1xe3xg1 c1xe3xg5xe7xc5xa3xc1xe3xg1 c1xe3xg5xe7xc5xa3xc1xe3xc5 c1xe3xg5xe7xc5xe3xc1xa3xc5xe3xg1 c1xe3xg5xe7xc5xe3xg1 

28 capturing moves at 36 plies.
g3-f4 e5xg3 c3-d4 g5-f4 e3xg5 c5xe3 d2xf4 f6-e5 e1-d2 g3xe1 g1-f2 e1xc3 b2xd4xf6 g7xe5xg3xe1 c1-d2 e1-f2 c3-b4 a5xc3xe1 e3-d4 e1-d2 a1-b2 b6-c5 d4xb6 c7xa5xc3xa1 b4-c5 b6xd4 a3-b4 a1-b2 b4-a5 d6-c5 a5-b6 c5-b4 b6-c7 e7-d6 g5xe7 a7-b6
c7xe5xc3xa1 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa1 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 e7xg5xe3xc1xa3xc5xe3xg1 e7xg5xe3xc1xa3xc5xa7 e7xg5xe3xc1xa3xc5xe7 e7xg5xe3xg1 e7xg5xe3xc5xa3xc1xe3xg1 e7xg5xe3xc5xa7 e7xg5xe3xc5xe7 c7xa5xc3xa1 c7xa5xc3xe1xg3xe5xc3xa1 c7xa5xc3xe1xg3xe5xc7 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3xa1 c7xa5xc3xe5xc7 c7xa5xc3xe5xg7 e7xc5xa3xc1xe3xg1 e7xc5xa3xc1xe3xc5xa7 e7xc5xa3xc1xe3xg5xe7 e7xc5xe3xc1xa3xc5xa7 e7xc5xe3xg1 e7xc5xe3xg5xe7 e7xc5xa7 

29 capturing moves at 48 plies. Found using monte carlo methods.
g3-f4 e5xg3 c3-b4 a5xc3 b2xd4 d6-e5 a1-b2 b6-a5 d4xb6 a7xc5 c3-b4 a5xc3xa1 c1-b2 a1xc3 d2xb4xd6 b6xd4xb2 c3-b4 e7xc5 e1-d2 g3xe1 b4-a5 e1xc3 e3-d4 c5xe3 a3-b4 e3-f2 g1xe3 d6-e7 b4-c5 c3-b4 c5-b6 e5-f4 f2-g3 f4xd2 b6-a7 c7-d6 g3-f4 e3-f2 f4-e5 f6xd4 e5xc7 g7-f6 a5-b6 g5-f4 c7-d6 e7xc5 b6-c7 c5-b6 
a7xc5xa3xc1xe3xg1 a7xc5xa3xc1xe3xc5xa7 a7xc5xa3xc1xe3xc5xe7xg5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xa7 a7xc5xe3xc1xa3xc5xa7 a7xc5xe3xc1xa3xc5xe7xg5xe3xg1 a7xc5xe3xg1 a7xc5xe3xg5xe7xc5xa3xc1xe3xg1 a7xc5xe3xg5xe7xc5xa7 a7xc5xe7xg5xe3xc1xa3xc5xe3xg1 a7xc5xe7xg5xe3xc1xa3xc5xa7 a7xc5xe7xg5xe3xg1 a7xc5xe7xg5xe3xc5xa3xc1xe3xg1 a7xc5xe7xg5xe3xc5xa7 c7xe5xc3xa1 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa1 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c7xa5xc3xa1 c7xa5xc3xe1xg3xe5xc3xa1 c7xa5xc3xe1xg3xe5xc7xa5 c7xa5xc3xe1xg3xe5xg7

30 capturing moves at 46 plies. Found using monte carlo methods.
c3-b4 a5xc3 d2xb4 e5-d4 c3xe5 d6xf4xd2 c1xe3 e5-f4 b4xd6 c7xe5 e3-d4 e5xc3 d2xb4 f6-e5 b4-a5 b6-c5 a3-b4 c5xa3xc1 f2-e3 d4xf2 g1xe3 f4xd2 f2xd4xf6 g7xe5 a1-b2 c1xa3 g3-f4 e5xg3 e1-f2 g3xe1 a5-b6 a7xc5 b6-a7 f6-e5 c3-b4 e7-f6 b4-a5 a3-b2 a5-b6 e1-f2 b6-c7 g5-f4 a7-b6 c5-b4 b6-c5 e5-d4 
c5xe7xg5xe3xc1xa3xc5xe3xg1 c5xe7xg5xe3xc1xa3xc5xe7 c5xe7xg5xe3xg1 c5xe7xg5xe3xc5xa3xc1xe3xg1 c5xe7xg5xe3xc5xa3xc1xe3xc5xe7 c5xe7xg5xe3xc5xe7 c7xe5xc3xa1 c7xe5xc3xe1xg3xe5xc3xa1 c7xe5xc3xe1xg3xe5xc3xa5 c7xe5xc3xe1xg3xe5xc7 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5 c7xe5xg3xe1xc3xa1 c7xe5xg3xe1xc3xa5 c7xe5xg3xe1xc3xe5xc7 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c5xe3xc1xa3xc5xe3xg1 c5xe3xc1xa3xc5xe3xg5xe7xc5 c5xe3xc1xa3xc5xe7xg5xe3xg1 c5xe3xc1xa3xc5xe7xg5xe3xc5xe7 c5xe3xg1 c5xe3xg5xe7xc5xa3xc1xe3xg1 c5xe3xg5xe7xc5xa3xc1xe3xc5xe7 c5xe3xg5xe7xc5xe3xc1xa3xc5xe7 c5xe3xg5xe7xc5xe3xg1 c5xa3xc1xe3xg1 c5xa3xc1xe3xc5xe7xg5xe3xg1 c5xa3xc1xe3xc5xe7xg5xe3xc5xe7 c5xa3xc1xe3xg5xe7xc5xe3xg1

31 capturing moves at 58 plies.
c3-d4 e5xc3 b2xd4 d6-e5 a1-b2 a5-b4 c3xa5 e5xc3xa1 c1-b2 a1xc3 d2xb4xd6 e7xc5 c3xe5 d6xf4xd2 e1xc3 f6xd4xb2 f2-e3 d2-e1 g3-f4 e1-f2 a3-b4 f2xd4xf6 b4xd6 g5xe3 d6-e7 b6xd4 e7xg5 e3-d2 g5-f4 e5xg3 f6-e7 d2-c1 e7-d6 c7xe5 d6-c7 g7-f6 c7-b6 a7xc5 b6-a7 c5-b4 a5-b6 g3-f2 g1xe3xc5 b2-a1 c5-d6 a1-b2 d4-c5 f4-e3 f2xd4 e3-f2 b6-c7 c1-d2 c7-b6 e5-f4 b6-a5 c3xe5xc7 c5xe7 c7-b6
a5xc7xe5xc3xa1 a5xc7xe5xc3xe1xg3xe5xg7 a5xc7xe5xc3xa5 a5xc7xe5xg3xe1xc3xa1 a5xc7xe5xg3xe1xc3xa5 a5xc7xe5xg3xe1xc3xe5xg7 a5xc7xe5xg7 a7xc5xa3xc1xe3xg1 a7xc5xa3xc1xe3xc5 a7xc5xa3xc1xe3xg5 a7xc5xe3xc1xa3xc5 a7xc5xe3xg1 a7xc5xe3xg5 e7xg5xe3xc1xa3xc5xe3xg1 e7xg5xe3xc1xa3xc5xe7 e7xg5xe3xg1 e7xg5xe3xc5xa3xc1xe3xg1 e7xg5xe3xc5xe7 e7xc5xa3xc1xe3xg1 e7xc5xa3xc1xe3xc5 e7xc5xa3xc1xe3xg5xe7 e7xc5xe3xc1xa3xc5 e7xc5xe3xg1 e7xc5xe3xg5xe7 a5xc3xa1 a5xc3xe1xg3xe5xc3xa1 a5xc3xe1xg3xe5xc7xa5 a5xc3xe1xg3xe5xg7 a5xc3xe5xg3xe1xc3xa1 a5xc3xe5xc7xa5 a5xc3xe5xg7 

32 capturing moves at 71 plies.
c3-b4 a5xc3 d2xb4 c5-d4 e3xc5 b6xd4 g3-f4 g5xe3 e1-d2 e5-f4 c3xe5 f4-g3 d4xb6 d6xf4 f2xd4 e5xc3xe1 e3xg5 c7xa5xc3 a3xc5 e7-d6 g5xe7 d6xb4 b2-a3 g3-f2 g1xe3 d4xf2 f4-g5 g7-f6 a1-b2 c3xa1 e7-d6 a1-b2 g5xe7 c5-d4 a3xc5 f2-g1 c1xa3 e1-f2 b2-c3 d4xb2 c3-d4 e3-d2 d6-e5 b2-a1 e5-f4 f2-g3 f4-e5 g3-f2 b4-a5 f2-e1 a5xc7 a1-b2 c7-d6 a7-b6 c5xa7 e1-f2 a7-b6 b2-c1 e5-f6 c1-b2 f6-e5 f2-g3 a3-b4 g3-f4 e5xg3 d2-c1 e7-f6 g1-f2 g3xe1 b2-c3 e1-d2
c3xe1xg3xe5xc3xa5xc7xe5xc3 c3xe1xg3xe5xc3xa5xc7xe5xg3 c3xe1xg3xe5xc3xa5xc7xe5xg7 c3xe1xg3xe5xc7xa5xc3xe5xg3 c3xe1xg3xe5xc7xa5xc3xe5xg7 c3xe1xg3xe5xg7 c1xe3xg1 c1xe3xc5xa3 c1xe3xc5xa7 c1xe3xc5xe7xg5xe3xg1 c1xe3xc5xe7xg5xe3xc5xa3 c1xe3xc5xe7xg5xe3xc5xa7 c1xe3xg5xe7xc5xa3 c1xe3xg5xe7xc5xe3xg1 c1xe3xg5xe7xc5xe3xg5 c1xe3xg5xe7xc5xa7 c3xa5xc7xe5xc3xe1xg3xe5xc3 c3xa5xc7xe5xc3xe1xg3xe5xg7 c3xa5xc7xe5xg3xe1xc3xe5xg3 c3xa5xc7xe5xg3xe1xc3xe5xg7 c3xa5xc7xe5xg7 c3xe5xg3xe1xc3xa5xc7xe5xc3 c3xe5xg3xe1xc3xa5xc7xe5xg3 c3xe5xg3xe1xc3xa5xc7xe5xg7 c3xe5xg3xe1xc3xe5xg3 c3xe5xg3xe1xc3xe5xc7xa5xc3 c3xe5xg3xe1xc3xe5xg7 c3xe5xc7xa5xc3xe1xg3xe5xc3 c3xe5xc7xa5xc3xe1xg3xe5xg7 c3xe5xc7xa5xc3xe5xg3xe1xc3 c3xe5xc7xa5xc3xe5xg7 c3xe5xg7 

34 capturing moves at 60 plies. derived from 29 moves example.
g3-f4 e5xg3 c3-b4 a5xc3 b2xd4 d6-e5 a1-b2 b6-a5 d4xb6 a7xc5 c3-b4 a5xc3xa1 c1-b2 a1xc3 d2xb4xd6 b6xd4xb2 c3-b4 e7xc5 e1-d2 g3xe1 b4-a5 e1xc3 e3-d4 c5xe3 a3-b4 e3-f2 g1xe3 d6-e7 b4-c5 c3-b4 c5-b6 e5-f4 f2-g3 f4xd2 b6-a7 c7-d6 g3-f4 e3-f2 f4-e5 f6xd4 e5xc7 g7-f6 a7-b6 b2-a1 b6-a7 a1-b2 c7-b6 b2-c3 b6-c7 g5-f4 a7-b6 d2-c1 b6-a7 c3-d2 a5-b6 c1-b2 c7-d6 e7xc5 b6-c7 c5-b6
a7xc5xa3xc1xe3xg1 a7xc5xa3xc1xe3xc5xa7 a7xc5xa3xc1xe3xc5xe7xg5xe3xc1 a7xc5xa3xc1xe3xc5xe7xg5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xe3xc1 a7xc5xa3xc1xe3xg5xe7xc5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xa7 a7xc5xe3xc1xa3xc5xa7 a7xc5xe3xc1xa3xc5xe7xg5xe3xc1 a7xc5xe3xc1xa3xc5xe7xg5xe3xg1 a7xc5xe3xg1 a7xc5xe3xg5xe7xc5xa3xc1xe3xg1 a7xc5xe3xg5xe7xc5xa7 a7xc5xe7xg5xe3xc1xa3xc5xe3xc1 a7xc5xe7xg5xe3xc1xa3xc5xe3xg1 a7xc5xe7xg5xe3xc1xa3xc5xa7 a7xc5xe7xg5xe3xg1 a7xc5xe7xg5xe3xc5xa3xc1xe3xg1 a7xc5xe7xg5xe3xc5xa7 c7xe5xc3xa1 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa1 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c7xa5xc3xa1 c7xa5xc3xe1xg3xe5xc3xa1 c7xa5xc3xe1xg3xe5xc3xe1 c7xa5xc3xe1xg3xe5xc7xa5 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3xa1 c7xa5xc3xe5xc7xa5 c7xa5xc3xe5xg7 

35 capturing moves at 60 plies. derived from 29 moves example.
g3-f4 e5xg3 c3-b4 a5xc3 b2xd4 d6-e5 a1-b2 b6-a5 d4xb6 a7xc5 c3-b4 a5xc3xa1 c1-b2 a1xc3 d2xb4xd6 b6xd4xb2 c3-b4 e7xc5 e1-d2 g3xe1 b4-a5 e1xc3 e3-d4 c5xe3 a3-b4 e3-f2 g1xe3 d6-e7 b4-c5 c3-b4 c5-b6 e5-f4 f2-g3 f4xd2 b6-a7 c7-d6 g3-f4 e3-f2 f4-e5 f6xd4 e5xc7 g7-f6 a7-b6 b2-a1 b6-a7 a1-b2 a7-b6 b2-c3 b6-a7 b4-a3 a7-b6 c3-b4 b6-a7 a3-b2 a5-b6 g5-f4 c7-d6 e7xc5 b6-c7 c5-b6
a7xc5xa3xc1xe3xg1 a7xc5xa3xc1xe3xc5xa3 a7xc5xa3xc1xe3xc5xa7 a7xc5xa3xc1xe3xc5xe7xg5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xa3 a7xc5xa3xc1xe3xg5xe7xc5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xa7 a7xc5xe3xc1xa3xc5xa7 a7xc5xe3xc1xa3xc5xe7xg5xe3xg1 a7xc5xe3xg1 a7xc5xe3xg5xe7xc5xa3xc1xe3xg1 a7xc5xe3xg5xe7xc5xa7 a7xc5xe7xg5xe3xc1xa3xc5xe3xg1 a7xc5xe7xg5xe3xc1xa3xc5xa7 a7xc5xe7xg5xe3xg1 a7xc5xe7xg5xe3xc5xa3xc1xe3xg1 a7xc5xe7xg5xe3xc5xa7 c7xe5xc3xa1 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa1 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c7xa5xc3xa1 c7xa5xc3xe1xg3xe5xc3xa1 c7xa5xc3xe1xg3xe5xc3xa5xc7xe5xg7 c7xa5xc3xe1xg3xe5xc7xa5xc3xa1 c7xa5xc3xe1xg3xe5xc7xa5xc3xe5xg7 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3xa1 c7xa5xc3xe5xg3xe1xc3xa5xc7xe5xg7 c7xa5xc3xe5xc7xa5xc3xa1 c7xa5xc3xe5xc7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg7

36 capturing moves at 80 plies.
e3-f4 g5xe3 f2xd4 c5-b4 a3xc5 d6xb4 e1-f2 f6-g5 d4xf6 g7xe5 e3-f4 g5xe3 f2xd4 c7-d6 e3-f4 c5xe3 f4-g5 b4-a3 d2xf4 d4xf2 g1xe3 e5-d4 e3xc5 d6xb4xd2 c1xe3 a3xc1 d4-e5 f6xd4 d2-c3 d4xb2 e3-d4 c5xe3xg1 c3xe5 e7-d6 a1xc3 c1-d2 e5xc7 d2xb4 g5-f6 c3-d2 c7-d6 b6-c5 d6-c7 a7-b6 c7-d6 c5-d4 d6-c7 d2-e1 f4-e5 b2-c1 e5-d6 d4-c3 d6-e7 c1-d2 g3-f4 b4-a3 f4-e5 d2-e3 f6-g7 g1-f2 c7-d6 a5-b4 e7-f6 e3-d4 f6-e7 d4xf6 e7xg5 f2-e3 d6-e7 c3-d2 g5-f4 e3xg5 e7-d6 g5xe7xc5 f6-e7 e1-f2 g7-f6 a3-b2 f6-e5 c5-d4
e5xc7xa5xc3xa1 e5xc7xa5xc3xe1xg3xe5xc3xa1 e5xc7xa5xc3xe1xg3xe5xc7 e5xc7xa5xc3xe5xg3xe1xc3xa1 e5xc7xa5xc3xe5xg3xe1xc3xe5xg3 e5xc7xa5xc3xe5xg3xe1xc3xe5xc7 e5xc7xa5xc3xe5xc7 e7xc5xa3xc1xe3xg1 e7xc5xa3xc1xe3xc5xa7 e7xc5xa3xc1xe3xc5xe7 e7xc5xa3xc1xe3xg5 e7xc5xe3xc1xa3xc5xe3xg1 e7xc5xe3xc1xa3xc5xe3xg5 e7xc5xe3xc1xa3xc5xa7 e7xc5xe3xc1xa3xc5xe7 e7xc5xe3xg1 e7xc5xe3xg5 e7xc5xa7 e5xg3xe1xc3xa1 e5xg3xe1xc3xa5xc7xe5xc3xa1 e5xg3xe1xc3xa5xc7xe5xg3 e5xg3xe1xc3xe5xg3 e5xg3xe1xc3xe5xc7xa5xc3xa1 e5xg3xe1xc3xe5xc7xa5xc3xe5xg3 e5xg3xe1xc3xe5xc7xa5xc3xe5xc7 e5xc3xa1 e5xc3xe1xg3xe5xc3xa1 e5xc3xe1xg3xe5xc3xa5xc7xe5xg3 e5xc3xe1xg3xe5xc7xa5xc3xa1 e5xc3xe1xg3xe5xc7xa5xc3xe5xg3 e5xc3xe1xg3xe5xc7xa5xc3xe5xc7 e5xc3xa5xc7xe5xc3xa1 e5xc3xa5xc7xe5xc3xe1xg3xe5xc7 e5xc3xa5xc7xe5xg3xe1xc3xa1 e5xc3xa5xc7xe5xg3xe1xc3xe5xg3 e5xc3xa5xc7xe5xg3xe1xc3xe5xc7 

41 capturing moves at 72 plies. derived from 29 moves example.
g3-f4 e5xg3 c3-b4 a5xc3 b2xd4 d6-e5 a1-b2 b6-a5 d4xb6 a7xc5 c3-b4 a5xc3xa1 c1-b2 a1xc3 d2xb4xd6 b6xd4xb2 c3-b4 e7xc5 e1-d2 g3xe1 b4-a5 e1xc3 e3-d4 c5xe3 a3-b4 e3-f2 g1xe3 d6-e7 b4-c5 c3-b4 c5-b6 e5-f4 f2-g3 f4xd2 b6-a7 c7-d6 g3-f4 e3-f2 f4-e5 f6xd4 e5xc7 b4-a3 c7-b6 b2-c1 b6-c7 c1-b2 c7-b6 b2-c3 b6-c7 c3-b4 c7-b6 d4-c3 b6-c7 c3-b2 c7-b6 b4-c3 b6-c7 c3-d4 c7-b6 a3-b4 b6-c7 g5-f4 c7-b6 e7-f6 b6-c7 f6-e7 a5-b6 g7-f6 c7-d6 e7xc5 b6-c7 c5-b6
a7xc5xa3xc1xe3xg1 a7xc5xa3xc1xe3xc5xa7 a7xc5xa3xc1xe3xc5xe7xg5xe3xg1 a7xc5xa3xc1xe3xc5xe7xg5xe3xc5xa7 a7xc5xa3xc1xe3xg5xe7xc5xe3xg1 a7xc5xa3xc1xe3xg5xe7xc5xa7 a7xc5xe3xc1xa3xc5xe3xg1 a7xc5xe3xc1xa3xc5xe3xg5xe7xc5xa7 a7xc5xe3xc1xa3xc5xa7 a7xc5xe3xc1xa3xc5xe7xg5xe3xg1 a7xc5xe3xc1xa3xc5xe7xg5xe3xc5xa7 a7xc5xe3xg1 a7xc5xe3xg5xe7xc5xa3xc1xe3xg1 a7xc5xe3xg5xe7xc5xa3xc1xe3xc5xa7 a7xc5xe3xg5xe7xc5xe3xc1xa3xc5xa7 a7xc5xe3xg5xe7xc5xe3xg1 a7xc5xe3xg5xe7xc5xa7 a7xc5xe7xg5xe3xc1xa3xc5xe3xg1 a7xc5xe7xg5xe3xc1xa3xc5xa7 a7xc5xe7xg5xe3xg1 a7xc5xe7xg5xe3xc5xa3xc1xe3xg1 a7xc5xe7xg5xe3xc5xa3xc1xe3xc5xa7 a7xc5xe7xg5xe3xc5xa7 c7xe5xc3xa1 c7xe5xc3xe1xg3xe5xc3xa1 c7xe5xc3xe1xg3xe5xc3xa5xc7 c7xe5xc3xe1xg3xe5xg7 c7xe5xc3xa5xc7 c7xe5xg3xe1xc3xa1 c7xe5xg3xe1xc3xa5xc7 c7xe5xg3xe1xc3xe5xg7 c7xe5xg7 c7xa5xc3xa1 c7xa5xc3xe1xg3xe5xc3xa1 c7xa5xc3xe1xg3xe5xc7xa5 c7xa5xc3xe1xg3xe5xg7 c7xa5xc3xe5xg3xe1xc3xa1 c7xa5xc3xe5xg3xe1xc3xe5xc7xa5 c7xa5xc3xe5xg3xe1xc3xe5xg7 c7xa5xc3xe5xc7xa5 c7xa5xc3xe5xg7


Maximum captures with one move
------------------------------

first occurence of n - capture move at m - plies

see [theory stuff](theo.md) for detail.


Longest Sequence of forced moves
--------------------------------

A simple MC search found after a few seconds at least

1 forced moves at 1 plies.
c3-b4 a5xc3
2 forced moves at 5 plies.
e3-f4 g5xe3 d2xf4 e5-d4 c3xe5 f6xd4
3 forced moves at 6 plies.
c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 c5xe3 d2xf4
4 forced moves at 7 plies.
c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 c5xe3 d2xf4 d4xb2
5 forced moves at 8 plies.
c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 c5xe3 d2xf4 d4xb2 a1xc3
6 forced moves at 14 plies.
a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c3-b4 a5xc3 c1-d2 a3xc1 d2xb4 c5xa3 c3xa5xc7 e5xc3 c7xe5
7 forced moves at 15 plies.
a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c3-b4 a5xc3 c1-d2 a3xc1 d2xb4 c5xa3 c3xa5xc7 e5xc3 c7xe5 f6xd4 
8 forced moves at 16 plies.
a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c3-b4 a5xc3 c1-d2 a3xc1 d2xb4 c5xa3 c3xa5xc7 e5xc3 c7xe5 f6xd4 e3xc5
9 forced moves at 21 plies.
a3-b4 c5xa3 c3-b4 a5xc3 b2xd4 g5-f4 e3xg5 e5-f4 g3xe5 d6xf4 c3-b4 e5xc3 d2-e3 f4xd2 c1xe3xc5 b6xd4 b4xd6 c7xe5 d2xb4 d4xb2 a1xc3 a3xc1
10 forced moves at 23 plies.
a3-b4 c5xa3 e3-d4 d6-c5 d2-e3 c7-d6 c1-d2 a3xc1 a1-b2 c1xa3 c3-b4 a5xc3 d2xb4 g5-f4 e3xg5 c5xe3 f2xd4 a3xc5 c3xa5xc7 e5xc3 c7xe5 d4xf2 g1xe3 f6xd4
11 forced moves at 25 plies.
c3-d4 e5xc3 b2xd4 f6-e5 d4xf6 g7xe5 c1-b2 a5-b4 c3xa5 e5-d4 d2-c3 f6-e5 e3-f4 g5xe3 g3-f4 e5xg3 c3xe5 d6xf4 d4xf6 e7xg5 f2xd4 f4xd2 e1xc3 c5xe3xc1 c3xe5xg7 
12 forced moves at 26 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 e3-d4 c5xe3 d2xf4 f6-e5 e3-d4 e5xg3 d4-c5 b6xd4 c3xe5 a5xc3 a3xc5 d6xb4 d4xb6 c7xa5 b2xd4 b4xd2 c1xe3 e1xc3 
13 forced moves at 27 plies.
c3-b4 a5xc3 d2xb4 b6-a5 e1-d2 a7-b6 g3-f4 e5xg3xe1 e3-d4 c5xe3 d2xf4 f6-e5 e3-d4 e5xg3 d4-c5 b6xd4 c3xe5 a5xc3 a3xc5 d6xb4 d4xb6 c7xa5 b2xd4 b4xd2 c1xe3 e1xc3 d2xb4 

above optimal

14 forced moves at 31 plies.
e3-d4 c5xe3 d2xf4 b6-c5 c3-d4 e5xc3 b2xd4xb6 a7xc5 c3-b4 a5xc3 a1-b2 c3xa1 c1-d2 f6-e5 d2-c3 e5-d4 c3xe5 c5-d4 e3xc5xa7 g5xe3 a7xc5 d6xb4 d4xb6 c7xa5 f2xd4 f4xd2 e1xc3 c5xe3xc1 a3xc5xa7 d4xb2 b4xd2 
15 forced moves at 35 plies.
a3-b4 c5xa3 c3-b4 a5xc3 d2xb4 e5-f4 g3xe5 f6xd4 e3xc5 g5-f4 d4xf6 e7xg5 c5xe7 b6-a5 e7-d6 c7xe5 c3-d4 e5xc3 b2xd4 f4-e3 b4-c5 d6xb4xd2 d4xb6 a7xc5 f2xd4 c5xe3 c3xe5 f6xd4 e1xc3 d4xb2 g1-f2 e3xg1 c1xe3 c3xe1 a1xc3 
16 forced moves at 40 plies.
g3-f4 e5xg3 e3-d4 c5xe3 f2xd4 f6-e5 d4xf6 g7xe5 e1-f2 g3xe1 g1-f2 e1xg3 c3-b4 a5xc3xe1 e3-f4 g5xe3 b2-c3 e3-f2 a3-b4 e1-d2 c1xe3 f2-g1 e3-d4 g1-f2 d4-c5 b6xd4xb2 a1xc3 c5xa3xc1xe3 d2xf4 f2xd4 e3xc5 d6xb4xd2 f4xd6 c3xa1 e5xg7 c7xe5xc3 g7xe5xc7 d6xb4 d4xb6 a7xc5 c7xa5
17 forced moves at 41 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 a5-b4 c3xa5xc7 c5-b4 a3xc5 d6xb4 e3-d4 e5xc3xe1 c7-b6 c5-d4 b2-c3 a7xc5 c3xa5xc7 b6-a5 c7-b6 b4-c3 c1-d2 g5-f4 g3xe5 e1xg3 d2xb4xd6 d4xb2 a1xc3 f6xd4 c5xe3 e7xc5 d4xf6 g7xe5 b6xd4 d6xb4xd2 e3xc1 c3xa1 c1xe3 e5xc3xe1 e3xc5 e1xc3
18 forced moves at 42 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 a5-b4 c3xa5xc7 c5-b4 a3xc5 d6xb4 e3-d4 e5xc3xe1 c7-b6 c5-d4 b2-c3 a7xc5 c3xa5xc7 b6-a5 c7-b6 b4-c3 c1-d2 g5-f4 g3xe5 e1xg3 d2xb4xd6 d4xb2 a1xc3 f6xd4 c5xe3 e7xc5 d4xf6 g7xe5 b6xd4 d6xb4xd2 e3xc1 c3xa1 c1xe3 e5xc3xe1 e3xc5 e1xc3 c5xe3
19 forced moves at 43 plies.
e3-d4 c5xe3 d2xf4 d6-c5 f4xd6 c7xe5 e1-d2 a5-b4 c3xa5xc7 c5-b4 a3xc5 d6xb4 e3-d4 e5xc3xe1 c7-b6 c5-d4 b2-c3 a7xc5 c3xa5xc7 b6-a5 c7-b6 b4-c3 c1-d2 g5-f4 g3xe5 e1xg3 d2xb4xd6 d4xb2 a1xc3 f6xd4 c5xe3 e7xc5 d4xf6 g7xe5 b6xd4 d6xb4xd2 e3xc1 c3xa1 c1xe3 e5xc3xe1 e3xc5 e1xc3 c5xe3 d4xf2
20 forced moves at 50 plies.
c3-d4 e5xc3 d2xb4 g5-f4 e3xg5 f6-e5 e1-d2 e7-f6 g5xe7 c5-d4 e7xc5xe3 e5-d4 c3xe5 a5xc3xe1 a3xc5 b6xd4 e3-d2 e1xc3 c1xe3 c3-b4 e5-f6 g7xe5 e3-f4 f6-g5 f4xd6 b4-c3 e5-f6 c7xe5 f6-e7 c3-b4 b2-c3 d4xb2 a1xc3xa5 c5xa3xc1 e7xc5 c1xa3 c5xe7 a3xc5 b4xd6xf4 g5xe3 f2xd4 f4xd6xb4 e3xc1 b4xd6 c1xe3 d6xb4 e3xc1 b4xd6 e7xc5 d6xb4 d4xb6
21 forced moves at 63 plies.
g3-f4 e5xg3 e3-f4 g5xe3 f2xd4 d6-e5 c3-b4 a5xc3 d2xb4xd6 e7xc5 c3-b4 e5xc3 b4-a5 d4xf2 g1xe3 c5-d4 e3xc5xe7 c3-d2 e1xc3 g3xe1 e7xg5 d6-c5 d2-e3 g7-f6 g5xe7 e1-f2 c3-b4 f2-g3 b4xd6 c7xe5xc3 b2xd4 b6-c5 d4xb6 a7xc5 a5xc7 g3-f2 c7xa5 f2xd4xb2 a5-b4 b2xd4 b4-c3 d4xb2 a3-b4 c5xa3 e7xc5 d6xb4 c1-d2 c3xe1 a1xc3xa5 a3xc1 b4xd6 c1xa3 d6xb4 a3xc1 b4xd6 c1xa3 d6xb4 a3xc1 b4xd6 c1xa3 d6xb4 a3xc5 a5-b6 c5xa7
22 forced moves at 75 plies.
e3-f4 g5xe3 f2xd4 f6-g5 d4xf6 g7xe5 e1-f2 e5-d4 c3xe5xg7 d6-e5 g3-f4 e5xc3xe1 g7xe5 e1xg3 e3-d4 g5xe3 e5-d6 c7xe5xc3 b2xd4 g3-f2 a3-b4 c5xa3 c3-b4 a5xc3 d4-e5 d6xf4 a1-b2 c3xa1 b4-a5 e7-f6 a5xc7 f4-g3 c1-b2 a1xc3 c7-d6 a3-b2 d6xf4xd2xb4 f2xd4 c3xa1 f6-e5 b2-c3 d4xb2 a1xc3 a7-b6 g1-f2 g3xe1 b4-a5 e1-f2 a5xc7 f2-e3 c7-d6 e3-f2 d6xf4 f2-e3 f4xd2 e3xc1 c3xe1 c1xa3 e1xc3 a3xc1 c3xe1 c1xa3 e1xc3 a3xc1 c3xe1 c1xa3 e1xc3 d2xb4 c3xa5 a3xc5 a5-b6 c5xa7 b6-c7 a7-b6 c7xa5 b6-c5
23 forced moves at 79 plies.
c3-d4 e5xc3 b2xd4 d6-e5 c3-b4 a5xc3 d2xb4xd6 e7xc5 a1-b2 b6-a5 d4xb6 d6xb4xd2 e1xc3 a7xc5 c3-b4 a5xc3xa1 g3-f4 e5xg3xe1 e3-d4 c5xe3 a3xc5xa7 b6-a5 c1-b2 a1xc3 g1-f2 e3xg1 a7-b6 c7-d6 b6-c7 f6-e5 c7-b6 d6-c5 b6xd4xb2 e5-f4 c3-d4 g1-f2 d4-c5 f2-g1 b2-c3 e1-f2 c3xe1xg3xe5 g1xe3xc1 c5-b6 c1xe3xg1 b6-c7 g5-f4 e5xg3 g7-f6 c7-d6 f6-e5 d6xf4 g1-f2 g3xe1 a5-b4 f4-e3 b4-c3 e1-d2 c3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 d2xf4 e3xg5 g3xe5 g5-f6 e5xg7 f6-e7 g7-f6
24 forced moves at 80 plies.
c3-d4 e5xc3 b2xd4 d6-e5 c3-b4 a5xc3 d2xb4xd6 e7xc5 a1-b2 b6-a5 d4xb6 d6xb4xd2 e1xc3 a7xc5 c3-b4 a5xc3xa1 g3-f4 e5xg3xe1 e3-d4 c5xe3 a3xc5xa7 b6-a5 c1-b2 a1xc3 g1-f2 e3xg1 a7-b6 c7-d6 b6-c7 f6-e5 c7-b6 d6-c5 b6xd4xb2 e5-f4 c3-d4 g1-f2 d4-c5 f2-g1 b2-c3 e1-f2 c3xe1xg3xe5 g1xe3xc1 c5-b6 c1xe3xg1 b6-c7 g5-f4 e5xg3 g7-f6 c7-d6 f6-e5 d6xf4 g1-f2 g3xe1 a5-b4 f4-e3 b4-c3 e1-d2 c3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 g3xe1 e3xc1 e1xg3 c1xe3 d2xf4 e3xg5 g3xe5 g5-f6 e5xg7 f6-e7 g7-f6 e7xg5
25 forced moves at 98 plies.
a3-b4 c5xa3 c3-d4 e5xc3 b2xd4 d6-c5 a1-b2 g5-f4 g3xe5 c5-b4 f2-g3 b6-c5 d4xb6 a7xc5 e3-d4 c5xe3 d2xf4 b4xd2 e1xc3 f6xd4xf2 f4xd6 c7xe5 e3-d4 g7-f6 g1xe3 d6-c5 c3-b4 a5xc3xe1 g3-f4 e1xg3 f4xd6 g3xe1 e5xg7 e1-f2 b2-c3 b4xd2 d4-e5 f2xd4xf6 g7xe5 e7xg5 d6-c7 g5-f4 e5xg3 c5-b4 c7xa5xc3xe1 b4-c3 d2xb4 b6-a5 g3-f2 a5xc3 f2xd4xb2 b4xd2 c1xe3 a3xc1 d2-c3 c1-d2 e1-f2 d2xb4 f4-e5 b4-c3 e3-d4 b2-a1 f6-e7 c3-b4 f2-e3 b4-a5 e5-d6 a1-b2 e7-f6 a5-b6 e3-d2 b6-c5 d4xb6 b2-c3 d2xb4 c3xa5xc7xe5xg7 c5xa7 g7xe5 a7xc5xa3 e5xg7 a3xc5 g7xe5 c5xa3 e5xg7 a3xc5 b4xd6 c5xe7 g7-f6 e7xg5 f6-e5 d6xf4 e5xg3 g5xe3 g3-f2 e3xg1 f2-e1 g1-f2 e1xg3 f2-e3


it is impossible to extend 25 forced moves positions to 26 forced moves.
the latter don't look likely to exist.


Shortest mate sequence from starting position
---------------------------------------------

See [theo.md](theo.md) for detail.


Other board sizes
-----------------

games on smaller board sizes than 7x7 are won for a color playing optimal.
3x3 laska is (easyly) won in 4 plies by black.
5x5 laska is won my white in 16 plies starting with b2-c3 or d2-c3. For example
d2-c3 b4xd2 e1xc3 d4-e3 b2-a3 a5-b4 c3xa5 c5-d4 d2-c3 d4xb2 a1xc3 e3-d2 c1xe3 e5-d4 e3xc5
The other first moves are won by black in twice as many plies.
b2-c3     M15
d2-c3     M15
b2-a3    M-30
d2-e3    M-30

