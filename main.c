
#include "evaltest.h"
#include "mc.h"
#include "movetree.h"
#include "negamax.h"
#include "alphabeta.h"
#include "alphabetaq.h"
#include "alphabetaqtt.h"
#include "alphabetaq-id.h"
#include "pvs.h"
#include "mtdf.h"

#include "position.h"
#include "egtb.h"

#include "tt.h"

#include <list>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

#include <getopt.h>
#include <unistd.h>

/*
 */

int init (string pos) {
//	Position p (pos);

//	std::cerr << p.toString(1) << std::endl;
//	std::cerr << p.toString(3) << std::endl;
//	BitBoard b (7);

//	b.initMoveHashes(7);

		// std::cerr << single_move_str << std::endl;
	Move m (pos);

	std::cout << m << '\n';
	std::cout << std::hex << m << '\n';
	std::cout << std::dec << m << '\n';

	return 0; 
}                 

void compact2readable(string p, std::string moves, unsigned int board_size, unsigned int print_style) {
	Position b(p, board_size);
	b.applyMoves (moves);
	b.print(print_style);
}

void usage () {
	cout << "laska options:"										<< endl;
	cout << "	[-a]		return best move known to laska"					<< endl;
	cout << "	[-A <movedata>]	apply move(s) in <movedata> before starting game"			<< endl;
	cout << "	[-C <movedata>]	use move(s) in <movedata> for position lookup"				<< endl;
	cout << "	[-b]		computer plays black. (default is white, white begins)"			<< endl;
	cout << "	[-d <n>]	tree_depth, search depth of possible moves tree"			<< endl;
	cout << "	[-D]		print (compact) position as a text board, optinal given by -p <pos>"	<< endl;
	cout << "	[-k <n>]	split depth, for long time deep searches"				<< endl;
	cout << "	[-c <n>]	split depth, skip first <n> branches"					<< endl;
	cout << "	[-e]		evaluate position, optinal given by -p <pos>"				<< endl;
	cout << "	[-E]		test evaluation, uses -m and -M "					<< endl;
	cout << "	[-g]		generate the possible moves tree"					<< endl;
	cout << "	[-i]		interactive. human vs. human mode."					<< endl;
	cout << "	[-l]		play a complete game. read opponents moves from stdin."			<< endl;
	cout << "	[-L]		log game to file position-log-<processid>. requires -l option"		<< endl;
	cout << "	[-m <n>]	set maximum moves for a game to <n>, default 200"			<< endl;
	cout << "	[-M <n>]	set number of random games for position evaluation to <n>, default 100"	<< endl;
	cout << "	[-P] 		print possible moves for (static) position"				<< endl;
	cout << "	[-p <p>]	use (compact) position <p> as start position"				<< endl;
	cout << "	[-q <p>]	use (compact) position <q> for position lookup"				<< endl;
	cout << "	[-r]		revert colors in position arg"						<< endl;
	cout << "	[-R]		generate random positions. Uses -M <n> and -d <n>"			<< endl;
	cout << "	[-s]		call special debug function and exit"					<< endl;
	cout << "	[-S <style>]	print style: 0=ascii, 1=compact, 2=hash, 3=postscript"			<< endl;
	cout << "	[-t <secs>	maximum time per move in seconds"					<< endl;
	cout << "	[-T <towerlist>	create EGTB for towerlist"						<< endl;
	cout << "	[-v]		verbose flag"								<< endl;
	cout << "	[-5]		play 5x5 laska"								<< endl;
	cout << "	[-4]		play 4x4 laska (bashni)"						<< endl;
	cout << "	[-3]		play 3x3 laska"								<< endl;
	cout << "	[-?|-h]		print this string"							<< endl;
	exit(0);
}

int main (int argc, char **argv) {
	int c;
	string	position		= "o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x",
		qosition		= "o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x",
		str_move		= "",
		str_move2		= "",
		to			= "",
		egtb_towers		= "";
	unsigned int depth		= 14,
		split_depth		=  0,
		skip			=  0;

	bool	find_best_move		= 0,
		compact2textboard	= 0,
		evaluate_position	= 0,
		test_evaluation		= 0,
		generate_move_tree	= 0,
		debug_possible_moves	= 0,
		revert_colors		= 0,
		generate_random_positions	= 0,
		input_game		= 0,
		batch_input		= 0,
		play_complete_game	= 0;
	bool	special_debug_function	= 0,
		verbose			= 0;
	int	board_size		= 7;
	unsigned int print_style	= 0,
		max_moves_per_game	= 1000,
		number_of_random_games	= NUM_ROLL_OUTS,
		sesc_per_move		= 60;
	unsigned int turn		= 0;	// white

	if (std::string(argv[0]).find("laska5"  ) != std::string::npos) { board_size	   = 5; }
	if (argc == 1) { usage() ; exit(0); }
	
	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"depth",	 1, 0, 0},
			{"position",	 1, 0, 0},
			{"file",	 1, 0, 0},
			{0, 0, 0, 0}
		};

		c = getopt_long (argc, argv, "aA:BbC:d:c:DeEilL:m:M:p:q:ghHjk:PrRsS:t:T:tvw543?", long_options, &option_index);

		if (c == -1) { break; }

		switch (c) {
		case 'a': find_best_move = 1;	break;
		case 'A': str_move = optarg;	break;
		case 'B': batch_input = true;	break;
		case 'C': str_move2 = optarg;	break;
		case 'b': turn = 1;		break;	// play as black
		case 'd':       depth = atoi(optarg); if (      depth <= 0) { cerr <<         "depth <= 0 not an option\n"; return -1; } break;
		case 'k': split_depth = atoi(optarg); if (split_depth <= 0) { cerr << "(split) depth <= 0 not an option\n"; return -1; } break;
		case 'c': skip	      = atoi(optarg); if (skip        <= 0) { cerr << "         skip <= 0 not an option\n"; return -1; } break;
		case 'D': compact2textboard	= 1;	break;
		case 'e': evaluate_position	= 1;	break;
		case 'E': test_evaluation	= 1;	break;
		case 'g': generate_move_tree	= 1;	break;
		case 'i': input_game		= 1;	break;
		case 'l': play_complete_game	= 1;	break;
		case 'm': max_moves_per_game	 = atoi(optarg); if (max_moves_per_game	    <= 0) { cerr <<     "max_moves_per_game <= 0 not an option\n"; return -1; } break;
		case 'M': number_of_random_games = atoi(optarg); if (number_of_random_games <= 0) { cerr << "number_of_random_games <= 0 not an option\n"; return -1; } break;
		case 'p': position		= optarg;	break;
		case 'q': qosition		= optarg;	break;
		case 'P': debug_possible_moves	= 1;	break;
		case 'r': revert_colors		= 1;	break;
		case 'R': generate_random_positions	= 1;	break;
		case 's': special_debug_function= 1;	break;
		case 'S': print_style		= atoi(optarg);	break;
		case 't': sesc_per_move		= atoi(optarg);	break;
		case 'T': egtb_towers		= optarg;	break;
		case 'v': verbose		= 1;	break;
		case '5': board_size		= 5;	break;
		case '4': board_size		= 4;	break;
		case '3': board_size		= 3;	break;
		case 'h': usage();	break;
		case '?': usage();	break;
		default: cerr << "what is -" << c << " for" << endl; usage();
		}
	}

	if (optind < argc) {
		cout << "Non Options/Values in ARGV: ";
		while (optind < argc)
			cout << argv[optind++] << ' ';
		cout << endl;
	}

	if (position == "o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x") {	// maybe set another default position, if there was position arg, don't change anything
		if (board_size == 5) {		// on smaller board sizes...
			position = "o|o|o|o|o||||x|x|x|x|x";
		} else if (board_size == 4) {
			position = "o|o|||||x|x";
		} else if (board_size == 3) {
			position = "o|o||x|x";
		} else if ((std::string(argv[0]).find("mill") != std::string::npos || std::string(argv[0]).find("nmm")!= std::string::npos)) {
			position = "........*........*........";
		}
	}


	// when starting through autoplay.sh (scripted args, programms connect by named pipes, i.e. dump terminal) then shell leaves the quotes around position.
	// remove quotes
	if (position[0] == '"')			position.erase(position.begin());
	if (position[position.size()-1] == '"') position.erase(position.end()-1);

	TranspositionTable tt;
#ifdef USE_POSITION_KEY
	tt.resize(26);
#endif
	initializeGame (board_size);

	BitBoard bb(board_size);
	bb.initMoveHashes(board_size);

	if (batch_input) {
		MoveTree mt(position, board_size);
		mt.setTranspositionTable(&tt);
		mt.setTreeDepth(depth);
		char line[2048];
		unsigned int c = 0;
		while (std::cin.getline (line, 2046)) {
			// std::cerr << c << ' ' << line << '\n';
			// std::cerr << '.';

			MoveTree b0 = mt;
			b0.applyMoves (line);
			b0.countPromotions ();
			b0.generateMoveTree(depth);
			c++;
			     if (c%100000 == 0) { std::cerr << ' ' << c << std::endl; }
			else if (c%10000  == 0) { std::cerr << '+'; }
			else if (c%1000   == 0) { std::cerr << '*'; }
			else if (c%100    == 0) { std::cerr << '-'; }
			else if (c%10     == 0) { std::cerr << ' '; }
			else std::cerr << '.';
		}
		//std::cerr << ' ' << c << std::endl;
		return 0;
	}

	if (revert_colors) {		// revert debug position (switich colors)
		unsigned int i = 0;
		while (i < position.length()) {
			switch (position[i]) {
			case 'O': position[i] = 'X'; break;
			case 'o': position[i] = 'x'; break;
			case 'X': position[i] = 'O'; break;
			case 'x': position[i] = 'o'; break;
			}
			++i;
		}
		cerr << "p: " << position << endl;
	}
	// cout << argv[0] << ": tree depth " << depth << endl;

	if (special_debug_function)	{ init(position);					return 0; } 
	if (compact2textboard)		{ compact2readable(position, str_move, board_size, print_style);	return 0; } 
	if (print_style == 3)		{ Position p(position); p.print(print_style);		return 0; }

	if (evaluate_position) {
		Position p(position);
		p.applyMoves (str_move);
		if (turn) p.setTurn(turn);
		cout << p.toString(1)  << ' ' << p.evaluatePosition () << endl;
		return 0;
	}

	if (debug_possible_moves) {
		cerr << "possibleMoves for " << (turn ? "black" : "white") << endl;
		Position p(position);
		p.applyMoves (str_move);
		if (turn) p.setTurn(turn);
		p.print();
		for (const auto& m: p.possibleMoves()) { cout <<  m.toString() << ' '; } cout << endl;

		return 0;
	}

	if (false ==	(batch_input		||
		generate_move_tree		||
		generate_random_positions	||
		test_evaluation			||
		find_best_move			||
		play_complete_game		||
		input_game)) {
		std::cerr << "hhmm, are you sure about the options? this run probably does nothing." << std::endl;
	}
	
	if (generate_random_positions) {
		MCBoard mcb(position);
		mcb.applyMoves (str_move);
		for (unsigned int i = 0; i<number_of_random_games; i++) {
			MCBoard b0 = mcb;
			b0.setMaxGameLength	(max_moves_per_game);
			b0.setNumberOfRollouts	(number_of_random_games);
			b0.genetrateRandomPosition(depth);
		}
		return 0;
	}

	if (generate_move_tree) {
		MoveTree mt(position, board_size);
		mt.setTranspositionTable(&tt);
		mt.applyMoves (str_move);
		mt.countPromotions ();
		if (turn) mt.setTurn(turn);		// if not otherwise stated program take white, see below
		mt.setTreeDepth(depth);
//		Position qos(qosition);
//		qos.setTranspositionTable(&tt);
//		if (str_move2 != "") {
//	 		qos.applyMoves (str_move2);
//		}
//		if (str_move2 != "" || qosition != "o|o|o|o|o|o|o|o|o|o|o||||x|x|x|x|x|x|x|x|x|x|x") {
//			mt.setReferencePosition (qos.getBitBoard(), qos);
//		}

		if (split_depth == 0) {
			mt.generateMoveTree(depth);
		} else {
			mt.generateMoveTreeSplitAt(depth, split_depth, skip);
		}
		// mt.writeConfig();
		return 0;
	}

	if (test_evaluation) {
		EvalTest evt(position, number_of_random_games, max_moves_per_game, depth);
		return 0;
	}

	// NegaMax
	// AlphaBeta
	// AlphaBetaQuiesce
	AlphaBetaQTT
	// AlphaBetaQId
	// PVS
	// MTDF
	b(position, board_size);
	b.readConfig ();
	b.setTranspositionTable(&tt);
	b.setTreeDepth(depth);
	b.applyMoves (str_move);
	if (turn) b.setTurn(turn);		// overwrite, program takes black. usually not usefull with str_move != ""

	if (find_best_move) {
		cerr << "b.findBestMove on position" << endl;
		b.print();
		Move m = b.findBestMove();
		if(m.empty()) {
			cerr << "no move. game over." << endl;
			return 0;
		}
		b.move(m);
		cout << "pm: "; for (const auto& m: b.possibleMoves()) { cout <<  m.toString() << ' '; } cout << endl;	// list possible moves to ease interfaces
		cout << m.toString()  << endl;			// and move
		cout << b.toString(1) << endl;			// and resulting position

		return 0;
	}

	if (play_complete_game) {	// program vs. outside world
		string line;
		unsigned int mc = 0;
		// ofstream out;
		if (turn == 0) {
			if (verbose) { b.print(); }
			Move m = b.findBestMove();
			if (verbose) { cout  << "I play "; } else { mc++; cerr << mc << ". " << m << endl; }
			cout << m << endl;
			b.moveRecorded(m);
			// b.writeConfig();
		} else {	// prog plays b
			b.switchTurn();
		}
		do {
			list<Move> l = b.possibleMoves();
			if (l.empty()) {
				if (verbose) {
					b.print();
					cout << "looks like i won. thx for the game." << endl;
				} else {
					cerr << (b.getTurn() ? "white" : "black") << " won." << endl;	// switchTurn() already done
				}
				return 2;
			} else if (l.size() == 1 && verbose) {	// not when prog vs. prog
				cout << "forced move " << l.front().toString() << endl;
				b.print();
				b.moveRecorded(l.front());
				b.print();
			} else {
				if (verbose) {
					b.print();
					for (const auto& m: l) { cout <<  m.toString() << ' '; } cout << endl;
					cout << "enter your move please > ";
				}
				if (!getline(cin, line)) {	// get input
					return 0;
				}
				if (line == "q" || line == "quit") return 0;
				if (line.substr(0, 10) == "set depth ") {
					depth = atoi (line.substr(10).c_str());
					cout << "setting search depth to " << depth << endl;
					b.setTreeDepth(depth);
					continue;
				}
				if (line.substr(0, 8) == "timeout ") {
					sesc_per_move = atoi (line.substr(8).c_str());
					cout << "setting time per move " << sesc_per_move << endl;
					// b.setTimePerMove(sesc_per_move);
					continue;
				}
				if (line == "compact") {
					b.print(1);
					continue;
				}
				if (line == "w" || line == "write") {
					b.writeConfig();
					continue;
				}

				Move m(line);
				b.moveRecorded(m);
				if (verbose) { b.print(); }
			}
			Move m = b.findBestMove();
			if (m.empty()) {
				if (verbose) {
					b.print();
					cout  << "looks like you won. thx for the game." << endl;
				} else {
					cerr << (b.getTurn() ? "white" : "black") << " won." << endl;	// switchTurn() already done
				}
				return 1;
			}
			if (verbose) { cout  << "I play "; } else { mc++; cerr << mc << ". " << m << endl; }
			cout << m << endl;
			b.moveRecorded(m);
			// b.writeConfig();
			if (mc > max_moves_per_game) {
				cerr << "maximum number of moves per games reached. I declare this a draw." << endl;
				return 0;

			}
		} while (true);
	}

	if (input_game) {
		string line;
		Position b (position, board_size);
		b.applyMoves (str_move);
		if (turn) b.setTurn(turn);
		do {
			b.print();
			list<Move> l = b.possibleMoves();
			if (l.empty()) {
				if (verbose) { cout << "no more moves. thx for the game." << endl; }
				else { cerr << "too many moves, declare game draw." << endl; }
				return 0;
			}
			for (const auto& m: l) { cout <<  Move(m).toString() << ' '; } cout << endl;
			cout << "enter your move please > ";
			if (!getline(cin, line)) { return 0; }	// get input
			if (line == "q" || line == "quit") return 0;
			if (line == "u" || line == "undo") {
				cout << "undo is ..." << endl;
				b.undo();
				continue;
			}
			if (line == "compact") {
				b.print(1);
				continue;
			}

			Move m(line);
			b.moveRecorded(m);
		} while (true);
	}

	if (egtb_towers != "") {
		// EGTB egtb(egtb_towers, position);

		// cerr << "position index: " << egtb.positionIndex() << endl;
		// cerr << "index size: " << egtb.indexSize() << endl;
	}
	return 0;
}

