#include "movetree.h"
#include "readme.h"

#include <algorithm>
#include <chrono>

// testing possibleMoves() finder

// #define __MT_LIST_COMPACT
// #define __MT_LIST_MOVES

// #define __MT_ALL_NODES
// #define __MT_LEAVES

// #define __MT_JUMPSEARCH
// #define __MT_MAXMOVESEARCH

// #define __MT_NOOFFICER

enum RecordType { MaxStepMoves, MaxJumpMoves, LongJump, ForcedMoveSequence, MaxTower, OwnStones, CapturedStones };

std::list<Move>	pm_stack[MaxPly];

#ifdef __MT_JUMPSEARCH
	static int n_offi   = 0;
#endif
static unsigned int max_offset	= 500;
static unsigned int     offset	=   0;
static unsigned int need_o	=  10;


// static unsigned int ref_bits[4];
// static Tower ref_tower[24];
// static string ref_string;

MoveTree::MoveTree (const unsigned int board_size)		: Position(board_size)				{ } 
MoveTree::MoveTree (string pos, const unsigned int board_size)	: Position(pos, board_size), move_count(0)	{
	countPromotions ();
}

static unsigned int calcOffset (const unsigned int b0, const unsigned int b1) {
	unsigned int o = 0;

	o += 1 * __builtin_popcount(b0 & ( 7<<18));
	o += 1 * __builtin_popcount(b1 &   7<< 4 );
	o += 2 * __builtin_popcount(b0 & (15<<14));
	o += 2 * __builtin_popcount(b1 & (15<< 7));
	o += 3 * __builtin_popcount(b0 & ( 7<<11));
	o += 3 * __builtin_popcount(b1 & ( 7<<11));
	o += 4 * __builtin_popcount(b0 & (15<< 7));
	o += 4 * __builtin_popcount(b1 & (15<<14));
	o += 5 * __builtin_popcount(b0 &   7<< 4 );
	o += 5 * __builtin_popcount(b1 & ( 7<<18));
	o += 6 * __builtin_popcount(b0 &  15     );
	o += 6 * __builtin_popcount(b1 & (15<<21));

	return o;
}

void MoveTree::countPromotions () const {
#ifdef __MT_JUMPSEARCH
	unsigned int o = 0;
	for (auto m: command_line_moves) {
		if (m.isPromotion()) { o++; }
	}
	n_offi = o;
#endif
	offset = calcOffset(bitb.bits[0] & ~bitb.bits[2], bitb.bits[1] & ~bitb.bits[3]);
	max_offset = 116 - command_line_moves.size() - 1;

//	std::cerr << "max_offset: " << 116 << " - " << command_line_moves.size() << " - 1 = " << max_offset << "\n";
}

void MoveTree::store(const int move, const int turn, const int depth, const int value) {
#ifdef USE_POSITION_KEY
//	std::cerr << "MT::store " << key() << ' ' << Move(move) << " " << value << " " << depth << ": ";
//	printMoveStack(depth, true, std::cerr);
	tt->store (key(), move, turn, depth, value);
#endif
}

bool MoveTree::probe(int &v, const int d) {
#ifdef USE_POSITION_KEY
 // std::cerr << "MT::probe " << key() << ' ' << d << ' ';
//	printMoveStack(d, true, std::cerr);
	const TTEntry *e = tt->probe(key());
	if (e != NULL && e->key != 0 && e->key == key()  && e->depth >= d && e->turn == turn) {	// tt move searched with greater depth (depth counted descending). on par, also stop searching
		// std::cerr << "TT::probe " << key() << ' ' << d << ' ';
		// std::cerr << e->value << " yes " << std::endl;
		// v = (e->turn == turn) ? e->value : -e->value; return true;
		v = e->value;
		return true;
	}
	// std::cerr << " no " << std::endl;
#endif
	return false;
}

void MoveTree::setTreeDepth		(const unsigned int d)	{
	tree_depth = d;
}

/*
void MoveTree::setReferencePosition		(const BitBoard &b, const Position &p)	{
//	ref_pos = b;
	for (unsigned int i=0; i<4; i++) {
		ref_bits[i] = b.bits[i];
	}
	for (unsigned int i=0; i<25; i++) {
		ref_tower[i] = b.tower[i];
	}
	ref_string = p.toString(1);
} */

void MoveTree::generateMoveTree(unsigned int depth) {
	// cerr << "	generateMoveTree(" << depth << ")" << endl;

#ifdef __MT_MAXMOVESEARCH
	static unsigned int max_steps = 25;
	static unsigned int max_jumps = 20;
#endif

#ifdef __MT_NOOFFICER
	static unsigned int node_count	 = 0;
	static unsigned int record_depth = 1000;
#endif
 
	if (!depth) {
		unsigned int np = __builtin_popcount(bitb.bits[0]) + __builtin_popcount(bitb.bits[1]); 
		if (np < need_o) { return; }

//		int d = __builtin_popcount(bitb.bits[0]) - __builtin_popcount(bitb.bits[1]);
//		if (d > 1 || d < -1) { return; }

// if (offset < 116 - command_line_moves.size() - 1 - tree_depth) {
#ifdef __MT_LEAVES
#ifdef __MT_LIST_MOVES
		printMoveStack (1, false);
		// std::cout << (bitb.bits[0] & ~bitb.bits[2]) << ' ' << (bitb.bits[1] & ~bitb.bits[3])<< ' ';

	//	int bo =  __builtin_popcount(bitb.bits[0] & ((1<<11)-1)) + __builtin_popcount(bitb.bits[1] &  (((1<<25)-1) - ((1<<14)-1)));
		// std::cout << __builtin_popcount(bitb.bits[0] & ((1<<11)-1)) << ' ' << __builtin_popcount(bitb.bits[1] &  (((1<<25)-1) - ((1<<14)-1))) << ' ' << bo << ' ';
		// std::cout << "\033[31m" << offset << "\033[0m" << ' ';
//H		std::cout << offset << ' ';
		// for (unsigned int i = tree_depth+1; i > 0; i--) { std::cerr << move_stack[i-1] << ' '; } std::cerr << endl;

#endif
#ifdef __MT_LIST_COMPACT
#ifdef __HEX_IO
//		print (3);
#else
		print (1);
#endif
#endif
#endif
// }
/*		Move m = move_stack[depth+1];
		if (m.isPromotion() || m.isJump()) {
		static unsigned int max_officers[]   = { 1, 8, 10, 13, 15, 18, 22, 26, 29, 35, 50, 62, 75, 87, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000 };
			bitb.bits[2]&=0x1ffffff;
			bitb.bits[3]&=0x1ffffff;
			int no = __builtin_popcount(bitb.bits[2]) + __builtin_popcount(bitb.bits[3]); 
			if (no > 9 && max_officers[no] > (move_history.size()+(tree_depth-depth)+1)) {
				max_officers[no] = (move_history.size()+(tree_depth-depth)+1);
				int cc[] = { 0, 37, 36, 35, 34, 33, 32, 31, 37, 36, 35, 34, 33,  93,96,95,94,97,92,91 };
				// std::cout << std::dec << "\033[" << cc[no] << "m" << no << " officers at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies.\033[0m" << std::endl;
				std::cout << std::dec << no << " officers at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies." << std::endl;
				printMoveStack (depth, false); cout  << "-> "; print(1);
			}
		} */
		return;
	}

	list<Move> l = possibleMoves();

	pm_stack[depth] = l;

	if (l.empty()) { /* cerr << "game over." << endl; */ return; }		// game over

#ifdef __MT_MAXMOVESEARCH
	// unsigned int plies = (move_history.size()+(tree_depth-depth));
	if (l.size() == 1) {
		unsigned int fs = 1;
		for (unsigned int i = depth+1; i<tree_depth; i++) {
			if (pm_stack[i].size() != 1) { break; }
			fs ++;
		}
		if (fs > 13 && forc_move_records[fs] >= (move_history.size()+(tree_depth-depth))) {
			forc_move_records[fs] = move_history.size()+(tree_depth-depth);
			std::cout << std::dec << fs       << " forced moves at " << std::dec << (move_history.size()+(tree_depth-depth)) << " plies." << std::endl;
			printMoveStack (depth+1, false); std::cout << l.front() << std::endl;
		}
	}
	if (l.front().isStep()) {
		if (l.size() > max_steps && step_move_records[l.size()] >= (move_history.size()+(tree_depth-depth))) {
			std::cout << std::dec << l.size() << " step moves at " << std::dec   << (move_history.size()+(tree_depth-depth)) << " plies." << std::endl;
			printMoveStack (depth, false); cout  << "-> "; printMovelist (l);
		}
	} else {
		if (l.size() > max_jumps && jump_move_records[l.size()] >= (move_history.size()+(tree_depth-depth))) {
			std::cout << std::dec << l.size() << " jump moves at " << std::dec   << (move_history.size()+(tree_depth-depth)) << " plies." << std::endl;
			printMoveStack (depth, false); cout  << "-> "; printMovelist (l);
		}
	} 
#endif

//	static Position qos ("...............OxxX.o..xxXXxXx.OooOoooooX.....");

	for (auto m: l) {
		// move_count ++; cerr << move_count << ". ";
		// cerr << m.toString() << endl;
#ifdef __MT_NOOFFICER
		if (m.isOnPromotionSquare()) { continue; }
#endif

//		if (pos_stack[depth+4] == key() || pos_stack[depth+8] == key() || pos_stack[depth+12]) {
		//	std::cerr << m << " is repetition\n";
//			continue;
//		}
//		if (offset > max_offset) {
// 			std::cerr << m << " offset cutoff " << offset << ' ' << max_offset << "\n";
//			continue;
//		}

//H		if (!m.isJump() && !bitb.tower[m.from()].isOfficer()) {
// 			std::cerr << m << " offset dec " << offset << ' ' << max_offset << "\n";
//H			offset --;
//H		}

		move(m);
		move_stack[depth] = m;
		pos_stack[depth]  = key();

//H		if (m.isJump()) {
//H			offset = calcOffset(bitb.bits[0] & ~bitb.bits[2], bitb.bits[1] & ~bitb.bits[3]);
// 			std::cerr << m << " offset calc " << offset << ' ' << max_offset << "\n";

		//	if (offset > max_offset) {
// 				std::cerr << "offset cutoff 2\n";
		//		undo(m);
		//		offset = calcOffset(bitb.bits[0] & ~bitb.bits[2], bitb.bits[1] & ~bitb.bits[3]);
		//		continue;
		//	}
//H		}

// 		std::cerr << m << " to go " << offset << " " << max_offset << "\n"; 
//		if (qos.bitb.bits[0] == bitb.bits[0]) {
//			std::cerr << "white match " << bitb.bits[0] << std::endl;
//		}
		// if (toString(1) == "...|..|...|..|.OxxX.o.|xxXXxXx.OooOoooooX.|...") {
		// if (toString(1) == ".oooX..|.ooX.xxxx|..OoX.|oOoxxX..|..xO.|..|...") {
//		bool matches = true;

//		for (unsigned int i=0; i<2; i++) {
//			if (ref_bits[i] != bitb.bits[i]) { matches = false; }
//		}
		// if (bitb.bits[0] == ref_pos.bits[0] && bitb.bits[1] == ref_pos.bits[1]){
		// if (matches || toString(1) == ref_string) {
//		if (toString(1) == ref_string) {
//			std::cout << "position match at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies." << std::endl;
//			printMoveStack (depth, false); cout  << "-> "; print(1);
//		}
#ifdef USE_POSITION_KEY
		int w; 
		if (probe(w, depth)) {
	//		cerr << "is permutation ";
	//		printMoveStack (depth, true, std::cerr);
			undo(m);
			if (m.isJump()) {
				offset = calcOffset(bitb.bits[0] & ~bitb.bits[2], bitb.bits[1] & ~bitb.bits[3]);
			} else {
				if (!bitb.tower[m.from()].isOfficer()) {
					offset ++;
				}
			}
			continue;
		}
#endif

		unsigned int np = __builtin_popcount(bitb.bits[0]) + __builtin_popcount(bitb.bits[1]); 
		if (np < need_o) {
			undo(m);
//H			if (m.isJump()) {
//H				offset = calcOffset(bitb.bits[0] & ~bitb.bits[2], bitb.bits[1] & ~bitb.bits[3]);
//H			} else {
//H				if (!bitb.tower[m.from()].isOfficer()) {
//H					offset ++;
//H				}
//H			}
			continue;
		}

//		int d = __builtin_popcount(bitb.bits[0]) - __builtin_popcount(bitb.bits[1]);
//		if (d > 1 || d < -1) { undo(m); continue; }
#ifdef __MT_JUMPSEARCH
		static int max_offi = 8;
		static unsigned int max_promotions[] = { 1, 8, 10, 13, 15, 18, 21, 23, 27, 30, 35, 38, 50, 61, 65, 73, 79, 83, 94, 107, 118, 129, 143 };
		static int num_off = 8;
		static unsigned int max_officers[]   = { 1, 8, 10, 13, 15, 18, 22, 26, 29, 35, 50, 62, 75, 87, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000 };

		if (m.isPromotion() || m.isJump()) {
			bitb.bits[2]&=0x1ffffff;
			bitb.bits[3]&=0x1ffffff;
			int no = __builtin_popcount(bitb.bits[2]) + __builtin_popcount(bitb.bits[3]); 
			if (no > num_off && max_officers[no] > (move_history.size()+(tree_depth-depth)+1)) {
				max_officers[no] = (move_history.size()+(tree_depth-depth)+1);
				// int cc[] = { 0, 37, 36, 35, 34, 33, 32, 31, 37, 36, 35, 34, 33,  93,96,95,94,97,92,91 };
				// std::cout << std::dec << "\033[" << cc[no] << "m" << no << " officers at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies.\033[0m" << std::endl;
				std::cout << std::dec << no << " officers at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies." << std::endl;
				printMoveStack (depth, false); cout  << "-> "; print(1);
			}
		}
		if (m.isPromotion()) {
			n_offi ++;
			if (n_offi > max_offi && max_promotions[n_offi] > (move_history.size()+(tree_depth-depth)+1)) {
				max_promotions[n_offi] = (move_history.size()+(tree_depth-depth)+1);
				std::cout << std::dec << n_offi << " promotions at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies." << std::endl;
				printMoveStack (depth, false); cout  << "-> "; print(1);
			}
		}

		if (max_tower_records[bitb.tower[m.to()].size()] > (move_history.size()+(tree_depth-depth)+1) ) {
			std::cout << std::dec << bitb.tower[m.to()].size() << " size tower at " << std::dec << (move_history.size()+(tree_depth-depth)+1) << " plies." << std::endl;
			max_tower_records[bitb.tower[m.to()].size()] = (move_history.size()+(tree_depth-depth)+1);
			printMoveStack (depth, false); cout  << "-> " << bitb.tower[m.to()].toString() << std::endl;
		}
#endif

#ifdef __MT_ALL_NODES
#ifdef __MT_LIST_MOVES
		// printMovelist (move_history);
		printMoveStack (depth, false);
#endif
#ifdef __MT_LIST_COMPACT
#ifdef __SEARCH
		print (3);
#else
		print (1);
#endif
#endif
#endif
		// printMoveStack (depth, false);
		// print (3);

		generateMoveTree(depth-1);

#ifdef USE_POSITION_KEY
		store(m, turn, depth, 0);	// value not used here
#endif
		undo(move_stack[depth]);
//H		if (m.isJump()) {
//H			offset = calcOffset(bitb.bits[0] & ~bitb.bits[2], bitb.bits[1] & ~bitb.bits[3]);
//H		} else {
//H			if (!bitb.tower[m.from()].isOfficer()) {
//H				offset ++;
//H			}
//H		}
#ifdef __MT_JUMPSEARCH
		if (m.isPromotion()) { n_offi --; }
#endif
		//undo(m);
		//move_history.pop_back();
	}
}

static auto start_time = std::chrono::steady_clock::now();

void MoveTree::generateMoveTreeSplitAt(unsigned int depth, unsigned int ply, unsigned int skip) {
	// cerr << "generateMoveTreeSplit(" << depth << ", " << ply << ", " << skip << ")" << endl;

	static unsigned int once = 0;
	if (once == 0) {
		std::time_t tt = std::chrono::system_clock::to_time_t (std::chrono::system_clock::now());
		std::cerr << "0." << ctime(&tt);
		once = 1;
	}

	static unsigned int c = 0;

	if (!depth) {
		// print (1);
		return;
	}

	list<Move> l = possibleMoves();

	if (l.empty()) { /* cerr << "game over." << endl; */ return; }		// game over

	for (auto m: l) {
		//for (const auto& mi: move_history) { cerr << (mi) << ' '; }
		// move_count ++; cerr << move_count << ". ";
		//cerr << m.toString() << endl;

		move(m);
		move_stack[depth] = m;
		pos_stack[depth]  = key();

		// print(1);

		if (depth > ply) {
			generateMoveTreeSplitAt(depth-1, ply, skip);
		} else if (depth == ply) {
			std::time_t tt = std::chrono::system_clock::to_time_t (std::chrono::system_clock::now());
			std::cerr << c << ". ";
			printMoveStack (depth, false, std::cerr);
			c++;
			if (skip >= c) {
				cerr << "skip" << std::endl;
				// undo();
				// undo(m);
				undo(move_stack[depth]);
				continue;
			}
			std::cerr << ctime(&tt);

			generateMoveTree(depth-1);

			// auto now = std::chrono::steady_clock::now();
			// std::cerr << c << ". " << ctime(&tt) << "Time elapsed: " << std::chrono::duration_cast<std::chrono::microseconds>(now - start_time).count() << "us.\n";
			// std::cerr << "descend " << ply << std::endl;
			
		} else {
			generateMoveTree(depth-1);
		}


		//undo();
		//undo(m);
		undo(move_stack[depth]);
		// move_history.pop_back();
	if (c==3) {
		return;
	}
	}
}

void MoveTree::printMoveStack (int e, bool new_line, std::ostream &out) const {
	printMovelist(command_line_moves, false, out);
#ifdef __HEX_IO
	for (int i=tree_depth; i>=e; i--) { out << std::hex << move_stack[i].index(); /* if (move_stack[i].isPromotion()) { out << '+'; } */ out << ' '; }
#else
	for (int i=tree_depth; i>=e; i--) { out << move_stack[i]; /* if (move_stack[i].isPromotion()) { out << '+'; } */ out << ' '; }
#endif
	if (new_line) { out << std::endl; }
}

