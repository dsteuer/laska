#ifndef _TT_H
#define _TT_H

#include <vector>

#include <stdlib.h>

enum Bound	{ Bound_Lower = 1, Bound_Upper, Bound_Exact };

typedef unsigned long long Key;

struct TTEntry {
	Key			key;
	unsigned int		move;
	unsigned int		turn;
		 int		value;
		 int		depth;
};

class TranspositionTable {
public:
	TranspositionTable ();

	const TTEntry*	probe	(const Key key) const;
	void		store	(const Key key, const int move, const int turn, const unsigned int depth, const int value);
	void		resize	(unsigned int to_size);
	void		clear	();
private:
	std::vector<TTEntry>	table;	// adr alligned and renamed
};

#endif

