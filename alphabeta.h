
#if !defined(__AB_BOARD_H)
#define __ALPHA_BETA_H

#include "movetree.h"

class AlphaBeta : public MoveTree {
public:
	AlphaBeta	(const unsigned int board_size = 7);
	AlphaBeta	(std::string	pos, const unsigned int board_size = 7);
	Move		findBestMove	();

protected:
	int		evaluateMoveTree (unsigned int depth, int alpha, int beta);
};

#endif
