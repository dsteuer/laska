
#include "alphabetaq-id.h"

#include <utility>
#include <iomanip>
#include <chrono>

// #define NDEBUG
#include <assert.h>

// negamax with alpha/beta pruning and quiescence search and transposition table
// and interative deepening loop

AlphaBetaQId::AlphaBetaQId (const unsigned int board_size)			: MoveTree (board_size)    	{ } 
AlphaBetaQId::AlphaBetaQId (std::string pos, const unsigned int board_size)	: MoveTree (pos, board_size)	{ }

void AlphaBetaQId::store(const int move, const int turn, const int value, const int depth) {
#ifdef USE_POSITION_KEY
	tt->store (key(), move, turn, depth, value);
#endif
}

bool AlphaBetaQId::probe(int &v, int d) {
#ifdef USE_POSITION_KEY
	const TTEntry *e = tt->probe(key());
	if (e->key != 0 && e->key == key()  && e->depth <= d) {	// tt move searched with greater depth (depth counted descending). on par, also stop searching
		v = e->value; return true;
	}
#endif
	return false;
}

int AlphaBetaQId::evaluateMoveTreeQuiesce(int depth, int alpha, int beta) {
#ifdef USE_POSITION_KEY
	int w;
	if (probe(w, depth)) return w;
#endif
	std::list<Move> l = possibleMoves();

	if (l.empty())		 { return -mateValue(tree_depth-depth);	}	// game over, node is terminal. return mate in plies
	if (l.front().isQuiet()) { return evaluatePosition();		}	// no jumps, so quiesence search over. node is (at least) quiet

	for (auto m: l) {						// play forced moves
		move(m);
		int v = -evaluateMoveTreeQuiesce(depth-1, -beta, -alpha);
		store(m, turn, v, depth);				// allow negative depthes in TT
		undo(m);

		if (v >= beta)  { return v;  }
		if (v >  alpha) { alpha = v; }
	}

	return alpha;
}

int AlphaBetaQId::evaluateMoveTree(unsigned int depth, int alpha, int beta) {
#ifdef USE_POSITION_KEY
	int w;
	if (probe(w, depth)) return w;
#endif
	if (depth == stop_depth) { return evaluateMoveTreeQuiesce(depth, alpha, beta); }	// end of search depth. just forward all args to qsearch.

	std::list<Move> l = possibleMoves();

	if (l.empty()) { return -mateValue(tree_depth-depth);		 }	// game over, node is terminal

	for (auto m: l) {
		move(m);
		int v = -evaluateMoveTree(depth-1, -beta, -alpha);
		store(m, turn, v, depth);
		undo(m);

		if (v >= beta)  { return v;  }
		if (v >  alpha)	{ alpha = v; }
	}

	return alpha;
}

Move AlphaBetaQId::findBestMove() {
	auto start_time = std::chrono::steady_clock::now();

	std::list<Move> l = possibleMoves();

	if (l.empty())		return NoMove;		// game over. i lost
	if (l.size() == 1)	return l.back();	// if only one move possible, return at once

	std::list<std::pair<int, Move> > sl;
	for (const auto& m: l) { sl.push_back(std::pair<int, Move>(value, m)); }	// copy possible moves

	for (stop_depth = tree_depth - 5; stop_depth; stop_depth--) {
		std::cerr << "stop search at depth=" << std::setw(2) << stop_depth << "\t";
		for (auto &p: sl) {
			move(p.second);
			p.first =  -evaluateMoveTree(tree_depth-1, -maxValue(), maxValue());
			undo(p.second);

			std::cerr << moveValueString	(p.second, p.first) << "\t";
		}
		auto end_time  = std::chrono::steady_clock::now();
		std::cerr << "(" << std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count() << " s)" << "\r"; // std::endl;
		sl.sort(std::greater<std::pair<int, Move> >());
	}
	tree_depth--;	// restore from last ++, for next run
	std::cerr << std::endl;
	auto end_time  = std::chrono::steady_clock::now();
	std::cerr << "Time elapsed " << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count() << "us.\n";

	return sl.front().second;	// best move;
}
