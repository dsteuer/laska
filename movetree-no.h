
#if !defined(__MOVETREE_NO_H)
#define __MOVETREE_NO_H

#include <iostream>
#include <list>
#include <string.h>

#include "games/laska-no/position.h"

class MoveTreeNoOfficer : public PositionNoOfficer {
public:
	MoveTreeNoOfficer (		     const unsigned int board_size = 7);
	MoveTreeNoOfficer (std::string	pos, const unsigned int board_size = 7);

	void		generateMoveTree	(unsigned int depth);
	void		setTreeDepth		(const unsigned int d);

protected:
	bool		isPermutation 		(const Move m);

	unsigned int	tree_depth;
	unsigned int	move_count;

private:
	void		initStats		();
};

#endif

