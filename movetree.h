
#if !defined(__MOVETREE_H)
#define __MOVETREE_H

#include <iostream>
#include <list>
#include <string.h>

#include "position.h"

using namespace std;

class MoveTree : public Position {
public:
	MoveTree	(		     const unsigned int board_size = 7);
	MoveTree	(std::string	pos, const unsigned int board_size = 7);

	void		generateMoveTreeSplitAt	(unsigned int depth, unsigned int ply, unsigned int skip);
	void		generateMoveTree	(unsigned int depth);
	void		setTreeDepth		(const unsigned int d);
	void		countPromotions		() const;	// usefull only for record searches
	// void		setReferencePosition	(const BitBoard &b, const Position &p);

protected:
	bool		isPermutation		(Move m, const int depth, const std::list<Move> &current);

	void		printMoveStack		(int e=0, bool new_line = true, std::ostream &out = std::cout) const;
	void		store			(const    int move,  const int turn, const int value, const int depth);
	bool		probe			(         int &v,    const int depth);

	unsigned int	tree_depth;
	unsigned int	move_count;
};

#endif

