
#if !defined(__ABQTT_H)
#define __ABQTT_H

#include "movetree.h"

class AlphaBetaQTT : public MoveTree {
public:
	AlphaBetaQTT	(const unsigned int board_size = 7);
	AlphaBetaQTT	(std::string	pos, const unsigned int board_size = 7);
	Move	findBestMove	();

protected:
	int	evaluateMoveTree	(unsigned int depth, int alpha, int beta);
	int	evaluateMoveTreeQuiesce	(	  int depth, int alpha, int beta);
//	void	store			(const    int move,  const int turn, const int value, const int depth);
//	bool	probe			(         int &v,          int depth);
};

#endif
