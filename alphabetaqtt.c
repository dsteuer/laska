
#include "alphabetaqtt.h"

#include <iomanip>
#include <chrono>

// min/max with alpha/beta pruning with quiescence

// static int stored = 0;

AlphaBetaQTT::AlphaBetaQTT (const unsigned int board_size)			: MoveTree (board_size)    	{ } 
AlphaBetaQTT::AlphaBetaQTT (std::string pos, const unsigned int board_size)	: MoveTree (pos, board_size)	{ }

// void AlphaBetaQTT::store(const int move, const int turn, const int depth, const int value) {
// #ifdef USE_POSITION_KEY
	// std::cerr << "TT::store " << key() << ' ' << Move(move) << " " << value << " " << depth << std::endl;
// 	tt->store (key(), move, turn, depth, value);
// 	stored ++;
// #endif
// }

// bool AlphaBetaQTT::probe(int &v, int d) {
// #ifdef USE_POSITION_KEY
// 	// std::cerr << "TT::probe " << key() << ' ' << d << ' ';
// 	const TTEntry *e = tt->probe(key());
// 	if (e != NULL && e->key != 0 && e->key == key()  && e->depth >= d && e->turn == turn) {	// tt move searched with greater depth (depth counted descending). on par, also stop searching
// 		// std::cerr << "TT::probe " << key() << ' ' << d << ' ';
// 		// std::cerr << e->value << " yes " << std::endl;
// 		// v = (e->turn == turn) ? e->value : -e->value; return true;
// 		v = e->value;
// 		return true;
// 	}
// 	// std::cerr << " no " << std::endl;
// #endif
// 	return false;
// }

int AlphaBetaQTT::evaluateMoveTreeQuiesce(int depth, int alpha, int beta) {
#ifdef USE_POSITION_KEY
	int w;
	if (probe(w, depth)) return w;
#endif
	std::list<Move> l = possibleMoves();

	if (l.front().isQuiet()) { return evaluatePosition();		}	// no jumps, so quiesence search over. node is (at least) quiet
	if (l.empty())		 { return -mateValue(tree_depth-depth);	}	// game over, node is terminal. return mate in plies

	for (auto m: l) {						// forced part, moves hit
		moveRecorded(m);
		// move(m);
		int v = -evaluateMoveTreeQuiesce(depth-1, -beta, -alpha);
		// for (auto i: move_history) { std::cerr << i << ' '; } std::cerr << v << std::endl;
		undo();
		// undo(m);

		if (v >= beta)  { return v; }
		if (v >  alpha) { store(m, turn, depth, v); alpha = v; }				// allow negative depthes in TT
	}

	return alpha;
}

int AlphaBetaQTT::evaluateMoveTree(unsigned int depth, int alpha, int beta) {
#ifdef USE_POSITION_KEY
	int w;
	if (probe(w, depth)) return w;
#endif
	if (!depth) { return evaluateMoveTreeQuiesce(depth, alpha, beta); }	// end of search depth. just forward all args to qsearch.

	std::list<Move> l = possibleMoves();

	if (l.empty()) { return -mateValue(tree_depth-depth);		}	// game over, node is terminal

	for (auto m: l) {
		moveRecorded(m);
		//move(m);
		int v = -evaluateMoveTree(depth-1, -beta, -alpha);
		// for (auto i: move_history) { std::cerr << i << ' '; } std::cerr << v << std::endl;
		undo();
		//undo(m);

		if (v >= beta)  { return v; }
		if (v >  alpha)	{ store(m, turn, depth, v); alpha = v; }
	}

	return alpha;
}

Move AlphaBetaQTT::findBestMove() {
	auto start_time = std::chrono::steady_clock::now();

	std::list<Move> l = possibleMoves();

	if (l.empty())	   return NoMove;					// no more moves, game over.
	if (l.size() == 1) return l.back();					// only one move possible in this position. return at once

	int	best_value = -maxValue();					// start with worst possible outcome
	Move	best_move  = NoMove;						// and no move

#ifdef USE_POSITION_KEY
	tt->clear ();
#endif

	for (auto m: l) {
		moveRecorded(m);
		// move(m);
		int v = -evaluateMoveTree(tree_depth-1, -maxValue(), maxValue());
		for (auto i: move_history) { std::cout << i << ' '; } std::cout << v << std::endl;
		undo();
		// undo(m);

		std::cerr << moveValueString(m, v) << "\t";

		if (v > best_value) {
			best_value = v;
			best_move  = m;
		}
	}
	std::cerr << std::endl;
	auto end_time  = std::chrono::steady_clock::now();
	std::cerr << "Time elapsed " << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count() << /* " " << stored << */ " us.\n";

	std::cout << "result: " << best_move << ' ' << best_value << std::endl;

	return best_move;
}

