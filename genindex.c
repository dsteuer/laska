#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

#include <bitset>
#include <vector>
#include <map>

#include <getopt.h>
#include <unistd.h>

// #include "over.h"

const unsigned int Step = 0, Jump = 1;
const unsigned int squares[] = { 0, 0, 0, 5, 8, 13, 18, 25 };

const unsigned int ranks[][7] = {
	{
	}, {
	}, {
	}, {
		((1<< 0)|(1<< 1)),		// board bits of ranks
		((1<< 2)        ),
		((1<< 0)|(1<< 1))  <<  3,
	}, {
		((1<< 0)|(1<< 1)),		// board bits of ranks
		((1<< 0)|(1<< 1))  <<  2,
		((1<< 0)|(1<< 1))  <<  4,
		((1<< 0)|(1<< 1))  <<  6,
	}, {
		((1<< 0)|(1<< 1)|(1<< 2)),		// board bits of ranks
		((1<< 3)|(1<< 4)        ),
		((1<< 0)|(1<< 1)|(1<< 2))  <<  5,
		((1<< 3)|(1<< 4)        )  <<  5,
		((1<< 0)|(1<< 1)|(1<< 2))  << 10,
	}, {
	}, {
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3)),		// board bits of ranks
		((1<< 4)|(1<< 5)|(1<< 6)        ),        
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3))  <<  7,
		((1<< 4)|(1<< 5)|(1<< 6)        )  <<  7,
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3))  << 14,
		((1<< 4)|(1<< 5)|(1<< 6)        )  << 14,
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3))  << 21,
	}
};

const unsigned int files[][7] = {
	{
	}, {
	}, {
	}, {
		((1<< 0)|(1<< 3)),
		((1<< 2)        ),        
		((1<< 0)|(1<< 3))  << 1,
	}, {
		((1<< 0)|(1<< 4)),
		((1<< 2)|(1<< 6)),        
		((1<< 1)|(1<< 5)),
		((1<< 3)|(1<< 7)),
	}, {
		((1<< 0)|(1<< 5)|(1<<10)),
		((1<< 3)|(1<< 8)        ),        
		((1<< 0)|(1<< 5)|(1<<10))  << 1,
		((1<< 3)|(1<< 8)        )  << 1,
		((1<< 0)|(1<< 5)|(1<<10))  << 2,
	}, {
	}, {
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)),
		((1<< 4)|(1<<11)|(1<<18)        ),        
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)) << 1,
		((1<< 4)|(1<<11)|(1<<18)        ) << 1,        
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)) << 2,
		((1<< 4)|(1<<11)|(1<<18)        ) << 2,        
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)) << 3,
	}
};

const unsigned int PromotionBottom	=   4;	// if square number a  black  private reaches with a move is below           4, private promotes, 7x7 board
const unsigned int PromotionTop		=  20;	// if square number a  white  private reaches             is greater than   20, private promotes

unsigned int	   StepMoveCounter	=   0;	// number of step moves listed in move_index (moveindex.h)
unsigned int	   LongJumpsIndex	=   0;	// index of first jump of length 5
unsigned int	   MoveIndexSize	=   0;	// size of move index

const unsigned int NoMove		= 255;

struct MoveData {
	MoveData () {
		n = 0;
		steps.clear();
		own_squares =  0;
		opp_squares =  0;
		contfrom[0] =  0;
		contfrom[1] =  0;
		contfrom[2] =  0;
		contto[0]   = -1;
		contto[1]   = -1;
		contto[2]   = -1;
	}

	unsigned char   		n;      // count, hit- and promotion flag
	std::vector<unsigned int>	steps;
	unsigned int			own_squares, opp_squares;
	short   contfrom[3];
	short   contto[3];
	bool operator<(const MoveData &m) const {
		if (n <m.n) return true;
		if (n >m.n) return false;
		for (unsigned int i=0; i<=n; i++) {
			if (steps[i] < m.steps[i]) return true;
		}
		return false;
	}
	bool operator==(const MoveData &m) const {
		if (n <m.n) return false;
		if (n >m.n) return false;
		for (unsigned int i=0; i<=n; i++) {
			if (steps[i] != m.steps[i]) return false;
		}
		return true;
	}
};

// there is/was a bug (?) in libc, if automatically move_index resized (to double) at push_back or emplace_back
// some of the subvectors of steps (most at index 2^n+1 or 2^n+2) lost their iterator and damaged the index
// so resize it here to a value large enough.
std::vector<MoveData>		move_index(4096, MoveData());			// enough for up to 8 jumps
std::vector<unsigned int>	double_check;					// ToDo: store the list of squares instead of the bitmask of squares

void printHeader (bool over_header) {
	if (over_header) {
		std::cout << "#ifndef __OVER_INDEX_H" << std::endl;
		std::cout << "#define __OVER_INDEX_H" << std::endl;
	} else {
		std::cout << "#ifndef __MOVE_INDEX_H" << std::endl;
		std::cout << "#define __MOVE_INDEX_H" << std::endl;
	}
	std::cout << "" << std::endl;
//	std::cout << "struct MoveData {" << std::endl;
//	std::cout << "	MoveData () {" << std::endl;
//	std::cout << "		n = 0;" << std::endl;
//	std::cout << "		steps.clear();" << std::endl;
//	std::cout << "		own_squares =  0;" << std::endl;
//	std::cout << "		opp_squares =  0;" << std::endl;
//	std::cout << "		contfrom[0] =  0;" << std::endl;
//	std::cout << "		contfrom[1] =  0;" << std::endl;
//	std::cout << "		contfrom[2] =  0;" << std::endl;
//	std::cout << "		contto[0]   = -1;" << std::endl;
//	std::cout << "		contto[1]   = -1;" << std::endl;
//	std::cout << "		contto[2]   = -1;" << std::endl;
//	std::cout << "	}" << std::endl;
//	std::cout << "" << std::endl;
//	std::cout << "	unsigned char   		n;      // count, hit- and promotion flag" << std::endl;
//	std::cout << "	std::vector<unsigned int>	steps;" << std::endl;
//	std::cout << "	unsigned int			own_squares, opp_squares;" << std::endl;
//	std::cout << "	short   contfrom[3];" << std::endl;
//	std::cout << "	short   contto[3];" << std::endl;
//	std::cout << "	bool operator<(const MoveData &m) const {" << std::endl;
//	std::cout << "		if (n <m.n) return true;" << std::endl;
//	std::cout << "		if (n >m.n) return false;" << std::endl;
//	std::cout << "		for (unsigned int i=0; i<=n; i++) {" << std::endl;
//	std::cout << "			if (steps[i] < m.steps[i]) return true;" << std::endl;
//	std::cout << "		}" << std::endl;
//	std::cout << "		return false;" << std::endl;
//	std::cout << "	}" << std::endl;
//	std::cout << "	bool operator==(const MoveData &m) const {" << std::endl;
//	std::cout << "		if (n <m.n) return false;" << std::endl;
//	std::cout << "		if (n >m.n) return false;" << std::endl;
//	std::cout << "		for (unsigned int i=0; i<=n; i++) {" << std::endl;
//	std::cout << "			if (steps[i] != m.steps[i]) return false;" << std::endl;
//	std::cout << "		}" << std::endl;
//	std::cout << "		return true;" << std::endl;
//	std::cout << "	}" << std::endl;
//	std::cout << "};" << std::endl << std::endl;
}

void printFooter () {
	std::cout << "#endif" << std::endl;
}

// check if a jump from -> to is allowed, just technically cause of bit order etc
static const bool check (unsigned int from, int to, unsigned int from2, unsigned int board_size) {
	unsigned int corner[][4] = {
		{   0,  0,  0,  0 },
		{   0,  0,  0,  0 },
		{   0,  0,  0,  0 },
		{   0,  1,  3,  4 },
		{   0,  1,  6,  7 },
		{   0,  2, 10, 12 },
		{   0,  0,  0,  0 },
		{   0,  3, 21, 24 },
	};

	unsigned int b = board_size;

	for (unsigned int i=0; i<4; i++) {						// can't step back from corner
		if (from == corner[b][i]) {
			return false;
		}
	}
	if (from2 == (unsigned) to)		{ return false; }				// can't jump 2 times over same tower
	if ((unsigned) to > squares[b]-1 || to < 0)	{ return false; }				// to is off board

	int diff = to - from;

	unsigned int e = board_size-1;
	unsigned int f = board_size-2;
	if (diff == (signed)  (b+1) && (1<<from & files[b][e] || 1<<from & files[b][f] || 1<<from & ranks[b][e] || 1<<from & ranks[b][f])) { return false; }		// impossible directions, off board
	if (diff == (signed)  (b-1) && (1<<from & files[b][0] || 1<<from & files[b][1] || 1<<from & ranks[b][e] || 1<<from & ranks[b][f])) { return false; }
	if (diff == (signed) -(b+1) && (1<<from & files[b][0] || 1<<from & files[b][1] || 1<<from & ranks[b][0] || 1<<from & ranks[b][1])) { return false; }
	if (diff == (signed) -(b-1) && (1<<from & files[b][e] || 1<<from & files[b][f] || 1<<from & ranks[b][0] || 1<<from & ranks[b][1])) { return false; }

	return true;
}

static void printIndex(std::vector<MoveData>   &move_index, int limit, unsigned int board_size) {
	int i = 0;

	std::cout << "static  MoveData move_index" << board_size << 'x' << board_size << "[] = {" << std::endl;

	for (auto m: move_index) {
		if (m.n == 0) {
			std::cout << "{ " << std::to_string(m.n) << ",\t{ 255,255 ";
		} else {
			std::cout << "{ " << std::to_string(m.n) << ",\t{ ";
		}
		for (auto s: m.steps) { std::cout << std::setw(2) << s << ", "; }
		std::cout << " }, ";
		std::cout << std::setw(8) << m.own_squares << ", " << std::setw(8) << m.opp_squares << ", { ";
		std::cout << '\t' << std::setw(4) << m.contfrom[0] << ", " << std::setw(4) << m.contfrom[1] << ", " << std::setw(4) << m.contfrom[2] << " }, { ";
		std::cout << '\t' << std::setw(4) << m.contto[0]   << ", " << std::setw(4) << m.contto[1]   << ", " << std::setw(4) << m.contto[2]   << " }},";
		std::cout << "\t// " << std::setw(4) << i << '\n';
		if (i == limit-1) { break; }
		i++;
	}
	std::cout << "};" << std::endl << std::endl;
}

MoveData dummyEntry (unsigned int n) {
	return MoveData();
}

void genMoveIndex(const unsigned int board_size, bool over_header) {

	const unsigned int	generate_dummy_entries = true;

	unsigned int  b = board_size;
	unsigned int sq	= squares[b];

	int offsets[]	 = {				// move directions upper left = 4, upper right = 3 ...
		 ((signed) (b+b%2)/2-1),
		 ((signed) (b+b%2)/2  ),
		-((signed) (b+b%2)/2-1),
		-((signed) (b+b%2)/2  )
	};
	unsigned int count = 0;

	StepMoveCounter = 0;					// 3 globals to calc
	MoveIndexSize   = 0;
	LongJumpsIndex  = 0;

	unsigned int e = b-b%2;
	unsigned int f = b-2;
	if (b%2 == 0) { e--; f--; }
	for (unsigned int d=0; d<4; d++) {
	for (int from=0, to=offsets[d]; (unsigned) from!=sq; from++,to++) {
			if (b == 4 && d == 3) { std::cerr << files[b][0] << ' ' << ranks[b][0] << '\n'; }
		     if (d == 0 && (1<<from & files[b][0] || 1<<from & ranks[b][e])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }
		else if (d == 1 && (1<<from & files[b][e] || 1<<from & ranks[b][e])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }
		else if (d == 3 && (1<<from & files[b][0] || 1<<from & ranks[b][0])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; } 
		else if (d == 2 && (1<<from & files[b][e] || 1<<from & ranks[b][0])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }

		int addo = 0, addp = 0;
		if ((d&2) == 0 && b%2 == 0 && (from/2)%2) { addo = 1; }
		if ((d&2) == 2 && b%2 == 0 && (from/2)%2) { addp = 1; }
 
		move_index[count].n		= 1;
		move_index[count].steps.clear();
		move_index[count].steps.emplace_back(from+addp);
		move_index[count].steps.emplace_back(to+addo);
		move_index[count].own_squares	= (1<<(to+addo));
		move_index[count].opp_squares	= 0;
		count ++;
	}
	}

	// unsigned int StepMoveCounter = move_index.size();
	StepMoveCounter = count;

	// now generate the list of single jump moves, just one hop
	offsets[0] =  ((board_size-1));
	offsets[1] =  ((board_size+1));
	offsets[2] = -((board_size-1));
	offsets[3] = -((board_size+1));

	for (unsigned int d=0; d<4; d++) {
	for (int from=0, to=offsets[d]; (unsigned) from!=sq; from++,to++) {
		     if (offsets[d] ==  ((signed) (board_size-1)) && (1<<from & files[b][0] || 1<<from & files[b][1] || 1<<from & ranks[b][e] || 1<<from & ranks[b][f])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }
		else if (offsets[d] ==  ((signed) (board_size+1)) && (1<<from & files[b][e] || 1<<from & files[b][f] || 1<<from & ranks[b][e] || 1<<from & ranks[b][f])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }
		else if (offsets[d] == -((signed) (board_size+1)) && (1<<from & files[b][0] || 1<<from & files[b][1] || 1<<from & ranks[b][0] || 1<<from & ranks[b][1])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }
		else if (offsets[d] == -((signed) (board_size-1)) && (1<<from & files[b][e] || 1<<from & files[b][f] || 1<<from & ranks[b][0] || 1<<from & ranks[b][1])) { if (generate_dummy_entries) { move_index[count] = dummyEntry(1); count++; } continue; }

		move_index[count].n		= 1;
		move_index[count].steps.clear();
		move_index[count].steps.emplace_back(from);
		move_index[count].steps.emplace_back(to);
		move_index[count].own_squares	= 1<<to;
		move_index[count].opp_squares	= 1<<((from+to)/2);

		count ++;
	}
	}

	// std::cout << "----------------------------" << std::endl;
	// printIndex(move_index, count);
	// std::cout << "size = " << move_index.size() << std::endl;
	// std::cout << "----------------------------" << std::endl;

	offsets[0] = -((board_size+1));	// old indexes relied on ascending order. keep it.
	offsets[1] = -((board_size-1));
	offsets[2] =  ((board_size-1));
	offsets[3] =  ((board_size+1));

	unsigned int first = StepMoveCounter;			// range of last jumps created. 
	unsigned int last  = count;

	unsigned int jmax[] = { 0, 0, 0, 1, 1, 5, 5, 11 };
	// extend the single jumps to double, triple, and so on ... jumps
	// through jumps round a square this might theoretically extend to 11 hops
	// in actual game a jump length of 3 hops might be sure win/loss.
	// when testing, 9 hop moves were found.
	// the more hops the bigger the index. so for an actual game 6 or 7 hops should be enough
	// (movelist is prababy wrong, but, no opponent will trap into such move, and sure win anyway)
	// for testing use at least 9 hops
	for (unsigned int jumpno = 0; jumpno < jmax[b]-1; jumpno++) {	// recurse over # of steps (hops), counter is steps-1
		for (unsigned int i = first; i<last; i++) {	// for the previously generate list
			MoveData *m = &move_index[i];
			if (m->n == 0) continue;		// skip dummies
			unsigned int f = m->steps[jumpno];	// from square to square of last hop
			unsigned int t = m->steps[jumpno+1];	//  of a previously generated move

			// up to three, out of 4 directions, possible continuations:
			for (auto o: offsets) {			// directions to jump to
				int to = t + o;			// square to hop to
				if (check (t, to, f, board_size)) {		// allowed move ?
					MoveData next_m;	// if so, this is a new entry
					move_index[count].n     = m->n + 1;		// one further hop
					move_index[count].steps = m->steps;		// old hops
					move_index[count].steps.emplace_back(to);	// plus new one
					count ++;
				}
			}
		}
		first = last;		// the new range of moves to elonginate, the moves just created
		last  = count;
	}

	MoveIndexSize = count;		// done, so far. moves created.

	//  own_squares: bits of squares used by own tower. including all fields that must be empty for a move
	//  opp_squares: bits of all squares to jump over, that must be occupied by opponents towers.

	// for all jump moves, generate a mask of square visited and jumped over
	for (unsigned int i = StepMoveCounter; i< MoveIndexSize; i++) {
		MoveData *md = &move_index[i];
		if (md->n == 0) continue;		// skip dummies
		for (unsigned int j=1; j<=move_index[i].n; j++) {		// startsquare needs no check.
			move_index[i].own_squares |= (1<< (move_index[i].steps[j]));
			move_index[i].opp_squares |= (1<<((move_index[i].steps[j]+move_index[i].steps[j-1])/2));
		}

		// if startsquare occurs multiple times within a move, it must be removed from the bitmask
		// startsquare is not empty in the board, but it is always empty during the move, so no check needed
		md->own_squares &= ~(1<<(md->steps[0]));		// remove startbit again, with jump length > 4 it may be visited again
	}

	// if a square is jumped over twice, the tower on that square must have at least 2 opponent stones on top
	// for these long jumps mask square visite twice 

	std::string os = "std::map<unsigned int,unsigned int> over_check[] = {\n";
	// std::cerr << "std::map<unsigned int,unsigned int> over_check[] = {" << std::endl;
	for (unsigned int i = 0; i<StepMoveCounter; i++) {
		os += "{ },\t// " + std::to_string(i) + '\n';
	}
	for (unsigned int i = StepMoveCounter; i< MoveIndexSize; i++) {
		std::map<unsigned int, unsigned int> over_count;
		for (unsigned int j=0; j<move_index[i].n; j++) {
			unsigned int over = (move_index[i].steps[j] + move_index[i].steps[j+1])/2;
			over_count[over] ++;
		}
		os += "{ ";
		// std::cerr << "{ ";	// vector index, map 
		for (auto o: over_count) {
			os += "{ " + std::to_string(o.first) + ", " + std::to_string(o.second) + "}, ";
		// 	std::cerr << "{ " << o.first << ", " << o.second << "}, ";
		}
		os += "},\t// " + std::to_string(i) + '\n';
		// std::cerr << "},\t// " << i << std::endl;
		
		if (move_index[i].n > 4) {			// create bitmask, for squares with multiple passes.
			if (LongJumpsIndex == 0) {		// by the way, store the index of the first of this moves globally
				LongJumpsIndex = i;
			}
			unsigned int double_mask = 0;
			for (unsigned int j=1; j< 4; j++) { // only 1, 2, 3 relevant
				unsigned int over = (move_index[i].steps[j-1]+move_index[i].steps[j])/2;
				double_mask |= (1<<over);
			}
			unsigned int r = 0;
			for (unsigned int j=5; j<=move_index[i].n; j++) { // only 5,6,7 can be second passes
				unsigned int over = (move_index[i].steps[j-1]+move_index[i].steps[j])/2;

				if (double_mask & (1<<over)) {
					// double_check[(i-LongJumpsIndex)] |= (1<<over);
					r |= (1<<over);
				}
			}
			double_check.emplace_back (r);	// anyway
		}
	}
	os += "};\n\n";
	// std::cerr << "};" << std::endl;

	// printIndex(move_index, count-1);

	for (unsigned int i = StepMoveCounter; i< MoveIndexSize-1; i++) {	// there are eventually leftovers from last run, and not affected by calulations below
		move_index[i].contfrom[0] =  0;			// store result of continuation search.
		move_index[i].contfrom[1] =  0;
		move_index[i].contfrom[2] =  0;
		move_index[i].contto[0]   = -1;
		move_index[i].contto[1]   = -1;
		move_index[i].contto[2]   = -1;
	}
	// now enter the continuations of the moves.
	// connect each move of n steps the the exdended moves of n+1 step (hop)
	for (unsigned int i = StepMoveCounter; i< MoveIndexSize-1; i++) {
		MoveData *m = &move_index[i];

		if (m->n == 0) continue;			// skip dummies
		if (m->n == jmax[b]) break;			// max move length can't be extended any more, so terminate loop
		// if (m->n == 9) break;			// max move length can't be extended any more, so terminate loop

		unsigned int cs =  0, csb =  0, csw =  0;	// start index of continuation moves, officer, black, white
		int          ce = -1, ceb = -1, cew = -1;	//   end index "
		bool is_w = true, is_b = true;			// potential move of white/black private

		for (unsigned int j=0; j < m->steps.size()-1; j++) {	// maybe move a private can do, if
			if (m->steps[j] < m->steps[j+1]) {	// all hops down the board so far ?
				is_b = false;			// no
			}
			if (m->steps[j] > m->steps[j+1]) {	// all hops up   the board so far
				is_w = false;
			}
		}

		for (unsigned int k=i+1; k < MoveIndexSize-1; k++) {	// search rest of index for continuation
			MoveData *n = &move_index[k];

			if (n->n < m->n+1) { continue; }		// wrong hop counter, so examine next move
			if (n->n > m->n+1) { break;    }

			bool found = true;
			for (unsigned int j=0; j < m->steps.size(); j++) {	// compare # of hops so far
				if (m->steps[j] != n->steps[j]) {
					found = false;				// don't match
					break;
				}
			}
			if (found == false) { continue; }			// so examine next move

			cs = k;							// store continuation index for officers
			ce = k;
			if (is_w && n->steps[n->steps.size()-2] < n->steps[n->steps.size()-1]) {
				csw = k;					// white private only if this hop is also to the top   of the board
				cew = k;
			}
			if (is_b && n->steps[n->steps.size()-2] > n->steps[n->steps.size()-1]) {
				csb = k;					// black private only if this hop is also to the bottom of the board
				ceb = k;
			}
			k++;
			while (found) {					// there may be sevaral continuation moves
				MoveData *n = &move_index[k];
				// std::cerr << i << " steps m = " << m->steps.size() << " " << k << " n = " << n->steps.size() << '\n';
				for (unsigned int j=0; j < m->steps.size(); j++) {
					if (m->steps[j] != n->steps[j]) {	// again match the steps
						found = false;
						break;
					}
				}
				if (found) {					// if so
					if (is_b && n->steps[n->steps.size()-2] > n->steps[n->steps.size()-1] ) {	// white private can do this
						if (csb == 0) { csb = k; ceb = k; } else { ceb++; }			// if previous move wasn't for a white private, maybe this one
					}
					if (is_w && n->steps[n->steps.size()-2] < n->steps[n->steps.size()-1] ) { 	// white private can do this
						if (csw == 0) { csw = k; cew = k; } else { cew++; }			// same for black private
					}
					k++;
					ce++;					// store officers en index
					if (k == MoveIndexSize-1) { break; }
				} else {
					break;					// terminate looking for continuations of this move
				}
			}
		}
		// std::cerr << i << " cont " << cs << " " << ce << '\n';
		if (cew > -1) { cew++; }
		if (ceb > -1) { ceb++; }
		if (ce  > -1) { ce ++; }
		m->contfrom[0] = csw;			// store result of continuation search.
		m->contfrom[1] = csb;
		m->contfrom[2] = cs;
		m->contto[0]   = cew;
		m->contto[1]   = ceb;
		m->contto[2]   = ce;
	}

	if (over_header == false) {
		printIndex(move_index, count, board_size);			// for debugging
	} else {
		if (board_size == 7) { std::cout << os; }
	}
}

int main (int argc, char **argv) {
	if (argc > 1) {
		printHeader(true);
		genMoveIndex(7, true);
	} else {
		printHeader(false);
		genMoveIndex(7, false);
		genMoveIndex(5, false);
		genMoveIndex(4, false);
		genMoveIndex(3, false);
	}
	printFooter();

	return 0;
}
