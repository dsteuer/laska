
#if !defined(__MONTECARLO_H)
#define __MONTECARLO_H

#include "position.h"

#define NUM_ROLL_OUTS  100

class MCBoard : public Position {
public:
	MCBoard		();
	MCBoard		(std::string pos);

	Move		findBestMove		();
	unsigned int	randomGame		();
	Move		randomMove		();
	int		evaluatePosition	(const unsigned int col);

	void		setNumberOfRollouts	(const unsigned int n);
	void		setMaxGameLength	(const unsigned int n);
	void		genetrateRandomPosition	(const unsigned int n_moves);			// for testing evals

private:
	void		init();

	std::string	start_pos;
 	unsigned int	num_roll_outs;
 	unsigned int	max_game_length;
};

#endif

