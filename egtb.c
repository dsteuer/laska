
#include "egtb.h"

static int index_size[] = {	// 25 over n stones
	1,
	25,
	300,
	2300,
	12650,
	53130,
	177100,
	480700,
	1081575,
	2042975,
	3268760,
	4457400,
	5200300,
	5200300,
	4457400,
	3268760,
	2042975,
	1081575,
	480700,
	177100,
	53130,
	12650,
	2300,
	300,
	25,
	1,
};

EGTB::EGTB(string top_stones, string pos) : MoveTree(pos) {
// count stones
// define index
// open table file
// write possible moves
// mark positions with moves that hit, i.e., switch to another table
// recurse through moves until no more moves to look at
	// connect pm with prevously examined moves

	tower_count[0] = 0;
	tower_count[1] = 0;
	tower_count[2] = 0;
	tower_count[3] = 0;

	for (auto &i: top_stones) {
		switch (i) {
                case 'o':       tower_count[0]++; break;
                case 'O':       tower_count[1]++; break;
                case 'x':       tower_count[2]++; break;
                case 'X':       tower_count[3]++; break;
                default:
                        cerr << "What is " << (i) << endl;

                }
	}

	cerr << "o: " << tower_count[0] << endl;
	cerr << "O: " << tower_count[1] << endl;
	cerr << "x: " << tower_count[2] << endl;
	cerr << "X: " << tower_count[3] << endl;
}

unsigned int EGTB::positionIndex () {
	unsigned int n_stones = tower_count[0]+ tower_count[1]+ tower_count[2]+tower_count[3];
	unsigned int pos_index = 0;

	for (unsigned int i = 0; i<25; i++) {
		if (tower[i].size()) {
			if (!tower[i].color() && !tower[i].isOfficer()) {
				pos_index += (n_stones*i);
				n_stones --;
			}
		}
	}
	for (unsigned int i = 0; i<25; i++) {
		if (tower[i].size()) {
			if (!tower[i].color() && tower[i].isOfficer()) {
				pos_index += n_stones * i;
				n_stones --;
			}
		}
	}
	for (unsigned int i = 0; i<25; i++) {
		if (tower[i].size()) {
			if (tower[i].color() && !tower[i].isOfficer()) {
				pos_index += i * (25-n_stones);
				n_stones --;
			}
		}
	}
	for (unsigned int i = 0; i<25; i++) {
		if (tower[i].size()) {
			if (tower[i].color() && tower[i].isOfficer()) {
				pos_index += n_stones * i;
				n_stones --;
			}
		}
	}
	
	cerr << "position index is " << pos_index << endl;

	return pos_index;
}

unsigned int EGTB::indexSize() {
	return index_size[tower_count[0]+ tower_count[1]+ tower_count[2]+tower_count[3]];
}

