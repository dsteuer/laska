
#ifndef __BITB_H
#define __BITB_H

#include <string>

struct BitBoard {
	unsigned int bits[2];

	BitBoard (const unsigned int board_size);
	void		clear		();
	void		move		(const unsigned int from, const unsigned int to, const unsigned int color);
	void		switchColor	(const unsigned int at,   const unsigned int color);
	void		erase		(const unsigned int at,   const unsigned int color);
	void		set		(const unsigned int at,   const unsigned int color);

	bool		collides	(const unsigned int move) const;
	bool		isNotOccupied	(const unsigned int squares, const unsigned int color) const;
	bool		hasJumps	(const unsigned int color) const;
	unsigned int	color		(const unsigned int square) const;
	const unsigned int both		()	  const	{ return (bits[0] | bits[1]); }

	std::string		toString	();
};

#endif

