
#ifndef __POSITION_H
#define __POSITION_H

#include <list>
#include <vector>
#include <map>

#include "ring.h"
#include "move.h"

#include "../../tt.h"

enum GameValue	{ MateValue = 65535 };					// max short

extern void initializeGame (const unsigned int board_size);

class Position {
public:
	Position (const unsigned int board_size = 7);
	Position (std::string	pos, const unsigned int board_size = 7);

	void		init		();
	void		setTurn		(const unsigned int t)	{ turn = t;		value = (turn) ? -value : value; 	}	// TODO: set sign correctly
	void		switchTurn	()			{ turn = 1^turn;	value = -value;				}
	std::string	toString	(const unsigned int style = 0);
	unsigned int	getTurn		() const { return turn;		}
	unsigned int	getValue	() const { return value;	}
	bool		isLegal		(const Move m);
	std::list<Move>	removables	() const;
	std::list<Move>	possibleMoves	();
	std::list<Move>	possibleSteps	();
	std::list<Move>	possibleJumps	();

	// template <bool do_eval>
	void		move		(const Move);
	void		moveRecorded	(const Move);
	void		undo		();
	// template <bool do_eval>
	void		undo		(const Move);

	int		evaluatePosition() const;
	int		mateValue	() const			 { return MateValue;		}
	int		mateValue	(const unsigned int depth) const { return MateValue-depth;	}	// mate in depth plies

	void		applyMoves	(std::string str_moves);
	void		setConfigValues	(const int *v);
	int		getConfigValue	(const int v);

	const unsigned long long key () const;	// just return position hash key
	unsigned long long	generateKey	();		// generate hash key
	void		setTranspositionTable (TranspositionTable* t);
	void		readConfig	(const std::string file_name = "cfg.txt");
	void		writeConfig	(const std::string file_name = "cfg2.txt");
	void		print		(unsigned int style = 0, std::ostream &out = std::cout);
	
protected:
	int		evaluateTriangles() const;

	bool		longJumpsCheck	(const Move m) const;

	void		setPosition	(std::string pos);
	std::string	moveValueString	(const Move m, const int v) const;

	union {
		unsigned long long	pos_key;	// current key
		Ring			ring[3];	// the rings
	};
	unsigned int	turn;
	unsigned int	stones[2];
	unsigned int	captured[2];
	unsigned int	phase;
	int		value;
	unsigned int	move_count;			// depending on start position may differ vom history size
	std::list<Move>	move_history;

#ifdef USE_POSITION_KEY
	TranspositionTable*	tt;
#endif
};

#endif
