#include "move.h"

#include <vector>
#include <algorithm>
#include <iomanip>

bool		Move::isStep		() const { return true;			}
bool		Move::isJump		() const { return false;		}
bool		Move::isQuiet		() const { return true;			}
bool		Move::empty		() const { return (index() == NoMove);	}

// store: to, from, except in opening, removed stone, if any
unsigned char	Move::from		() const { return (index() & (31<< 5))>> 5;	}
unsigned char	Move::to		() const { return  index() &  31;		}
unsigned char	Move::remove		() const { return (index() & (31<<10))>>10;	}

static std::vector<std::string> square2string = {
	"a7", "d7", "g7", "g4", "g1", "d1", "a1", "a4",
	"b6", "d6", "f6", "f4", "f2", "d2", "b2", "b4",
	"c5", "d5", "e5", "e4", "e3", "d3", "c3", "c4"
};

std::string Move::toString () const {
	std::string s = "";
	if (empty()) { return s; }
	if (isOpening()) {
		s  = "+" + square2string[to()];
	} else if (isMidgame()) {
		s += square2string[from()] + "-" + square2string[to()];
	} else if (isEndgame()) {
		s += square2string[from()] + "^" + square2string[to()];
	}
	if (isRemove()) {
		s += "x" + square2string[remove()];
	}
	return s;
}

unsigned int oneCoord(std::string c) {
	auto i = std::find(square2string.begin(), square2string.end(), c);
	if (i == square2string.end()) {
		std::cerr << "Move (" << c << ") could not parse anything useful out of this string." << std::endl;
		return (unsigned) -1;
	}
	return i - square2string.begin();
}

Move::Move(std::string m) {
	unsigned int t = 0;
	if (m[0] == '+') {
		m.erase(m.begin());
		t = OpeningMove;
	}

	unsigned int r[3] = { 0, 0, 0 };
	for (unsigned int s = 0, i=0; s<m.length() && s < 9; s+=3, i++) {
		r[i] = oneCoord(m.substr(s,2));
	}

	if (m.length() == 2) {	// an opening move
		_index = r[0] | OpeningMove;
	} else if (m.length() == 5) { 
		if (t == OpeningMove) {	// an opening move making a mill
			_index = r[0] | (r[1]<<10) | OpeningMove | RemoveMove;
			
		} else {		// "normal" <from>-<to> move
			_index = r[1] | (r[0]<< 5) | MidgameMove;
		}
	} else if (m.length() == 8) {	// mid or endgame <from>-<to>x<remove> move
		_index = r[1] | (r[0]<< 5) | (r[2]<<10) | MidgameMove | RemoveMove;	// further cheks needed. don't care right now
	} else {
		std::cerr << "Move (" << m << ") could not parse anything useful out of this string." << std::endl;
		_index = NoMove;
	}
	// std::cerr << "Move: " << m << " -> "  << index() << " = " << toString() << std::endl;
}

static std::vector<unsigned int> rank_index = { 0, 4, 7, 11, 14, 18, 21 };	// square number in 7x7 board, where a new rank begins
static std::vector<unsigned int> file_index = { 0, 0, 1, 1, 2, 2, 3	};	// number to add to rank_index to get the actual square

unsigned int Move::square(std::string s) {
	return rank_index[(s[1]-'1')] + file_index[(s[0] - 'a')];
}

std::ostream& operator<<(std::ostream& s, const Move &m) {
        s << m.toString();
        return s;
}

