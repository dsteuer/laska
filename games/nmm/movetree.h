
#if !defined(__MOVETREE_H)
#define __MOVETREE_H

#include <iostream>
#include <list>
#include <string.h>

#include "position.h"

using namespace std;

class MoveTree : public Position {
public:
	MoveTree(const unsigned int board_size = 7);
	MoveTree(std::string	pos, const unsigned int board_size = 7);

	void		generateMoveTree	(unsigned int depth);
	void		setTreeDepth		(const unsigned int d)	{ tree_depth = d; }
protected:
	bool		isPermutation 		(const Move m);

	unsigned int	tree_depth;
	unsigned int	move_count;
};

#endif

