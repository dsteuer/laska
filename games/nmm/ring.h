#ifndef __RING_H
#define __RING_H

#include "move.h"

#include <list>
#include <string>

using namespace std;

extern void initRingIndex();

class Ring {
public:
	Ring	() : index(0), _value(0) {}
	Ring	(string	s);
	Ring	(unsigned int d);

	bool		empty		() const		{ return (index == 0) ? true : false; }
	void		clear		()			{ index = 0; _value = 0; };
	void		clear		(const unsigned int at, const unsigned int color);
	void		set		(const unsigned int at, const unsigned int color);
	void		move		(const unsigned int m,  const unsigned int color);
	unsigned int	ownStones	() const;

	unsigned int	color		(const unsigned int i) const;
	int		value		() const		{ return _value; }
	unsigned int	key		() const		{ return index; };
	unsigned char	laterals	(const unsigned int color) const;
	list<Move>	removables	(const unsigned int color, const bool        forced, const unsigned int offset, const unsigned int lateral_mask) const;
	list<Move>	possibleMoves	(const unsigned int color, const unsigned int phase, const unsigned int offset, const unsigned int lateral_mask, const list<Move> removable) const;
	string		toString	() const;
	string		toString	(const unsigned int i) const;

private:
	void		init		();
	int		evaluate	() const;	// for position evaluation
	unsigned int	index;
		 int	_value;
};

#endif
