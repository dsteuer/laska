
#include "bitb.h"

#include <iostream>
#include <iomanip>

// #define NDEBUG
#include <assert.h>

/*
  an n x n (n=(2m+1)) laska board has (n^2+1)/2 squares
  at start position there are (n-1)^2 + foor(n/2) squares to fill with white and black stones.
  whites first stone is at square 0, blacks at square (n^2+1)/2 - (n-1)^2 + foor(n/2)
  rows are filled i=0..(n-1)/2 alternating with (n+1)/2, (n-1)/2 ... stones.

 n    white rows  stones
 3          2      2 =  1 + 1 
 5         3 2      5 =  4 + 1 
 7        4 3 4     11 =  9 + 2  
 9       5 4 5 4     18 = 16 + 2
11      6 5 6 5 6     28 = 25 + 3  
13     7 6 7 6 7 6     39 = 36 + 3
15    8 7 8 7 8 7 8     53 = 49 + 4
17   9 8 9 8 9 8 9 8     68 = 64 + 4
 */

static unsigned int numberOfStonesOfPlayer	(unsigned int n) { assert(n<8&&n%2==1); return ((n-1)/2)*((n-1)/2) + ((n-1)/2+1)/2; }
static unsigned int boardSquares		(unsigned int n) { return ((2*((n-1)/2)+1)*(2*((n-1)/2)+1)+1)/2; } 
static unsigned int bitsToShift			(unsigned int n) { return boardSquares(n)-numberOfStonesOfPlayer(n); } 

static unsigned int bitsToSetWhite(unsigned int n) { return (1<<((numberOfStonesOfPlayer(n))-1)); }
static unsigned int bitsToSetBlack(unsigned int n) { return (bitsToSetWhite(n))<<(bitsToShift(n)); }

BitBoard::BitBoard(const unsigned int board_size) {
	bits[0] = bitsToSetWhite(board_size);
	bits[1] = bitsToSetBlack(board_size);
}

void BitBoard::clear () {
	bits[0] = 0;
	bits[1] = 0;
}

void BitBoard::move(const unsigned int from, const unsigned int to, const unsigned int color) {
	assert(bits[color] & (1<<from));

	bits[color] &= ~(1<<from);
	bits[color] |=  (1<<to  );
}

void BitBoard::switchColor(const unsigned int at, const unsigned int color) {
	erase(at, !color);
	bits[color] |=  (1<<at);
}

void BitBoard::erase (const unsigned int at, const unsigned int color) { bits[color]  &= ~(1<<at); } 
void BitBoard::set   (const unsigned int at, const unsigned int color) { bits[color]  |=  (1<<at); }

bool BitBoard::collides		(const unsigned int move)				const { return (both()        & move); }
bool BitBoard::isNotOccupied	(const unsigned int squares, const unsigned int color)	const { return ((bits[color]  & squares) != squares); }

unsigned int  BitBoard::color		(const unsigned int square)	const {
	return	(bits[0] & (1<<square)) ? 0 :
		(bits[1] & (1<<square)) ? 1 : 2;
}

bool BitBoard::hasJumps (const unsigned int color) const {	// noofficer check, not too usefull to adopt for concurrent game
	unsigned int me    =  bits[color];
	unsigned int other =  bits[1^color];
	if (color) {    // black
		const unsigned int mask6 = ~((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<10)|(1<<13)|(1<<17)|(1<<20)|(1<<24)) & me;
		if (mask6 & (other << 3) & (~(both()) << 6)) { return true; }
		const unsigned int mask8 = ~((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<< 7)|(1<<11)|(1<<14)|(1<<18)|(1<<21)) & me;
		if (mask8 & (other << 4) & (~(both()) << 8)) { return true; }
	} else {
		const unsigned int mask6 = ~((1<<0)|(1<<4)|(1<< 7)|(1<<11)|(1<<14)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24)) & me;
		if (mask6 & (other >> 3) & (~(both()) >> 6)) { return true; }
		const unsigned int mask8 = ~((1<<3)|(1<<6)|(1<<10)|(1<<13)|(1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24)) & me;
		if (mask8 & (other >> 4) & (~(both()) >> 8)) { return true; }
	}

	return false;
}

std::string BitBoard::toString() {
	std::string s = "";
	for (unsigned int j=0; j< 2; j++) {
	for (unsigned int i=0; i<25; i++) {
		s += (bits[j] & (1<<i)) ?  '|' : '.';
		if (i%7 == 6 || i%7 == 3) s += ' ';
	}
	s+= '\n';
	}
	return s;
}

