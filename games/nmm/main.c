
#include "ring.h"
#include "position.h"

#include "movetree.h"

#include <list>
#include <iostream>
#include <sstream>
#include <fstream>

#include <getopt.h>
#include <unistd.h>

/*
 */

int init (string pos) {
	return 0; 
}                 

void compact2readable(string p, std::string moves, unsigned int board_size) {
}

void usage () {
	cout << "nmm options:"											<< endl;
	cout << "	[-a]		return best move known to laska"					<< endl;
	cout << "	[-A <movedata>]	apply move(s) in <movedata> before starting game"			<< endl;
	cout << "	[-b]		computer plays black. (default is white, white begins)"			<< endl;
	cout << "	[-d <n>]	tree_depth, search depth of possible moves tree"			<< endl;
	cout << "	[-D]		print (compact) position as a text board, optinal given by -p <pos>"	<< endl;
	cout << "	[-e]		evaluate position, optinal given by -p <pos>"				<< endl;
	cout << "	[-E]		test evaluation, uses -m and -M "					<< endl;
	cout << "	[-g]		generate the possible moves tree"					<< endl;
	cout << "	[-i]		interactive. human vs. human mode."					<< endl;
	cout << "	[-l]		play a complete game. read opponents moves from stdin."			<< endl;
	cout << "	[-L]		log game to file position-log-<processid>. requires -l option"		<< endl;
	cout << "	[-m <n>]	set maximum moves for a game to <n>, default 200"			<< endl;
	cout << "	[-M <n>]	set number of random games for position evaluation to <n>, default 100"	<< endl;
	cout << "	[-p <p>]	use (compact) position <p> as start position"				<< endl;
	cout << "	[-P] 		debug possible move generator"						<< endl;
	cout << "	[-r]		revert colors in position arg"						<< endl;
	cout << "	[-R]		generate random positions. Uses -M <n> and -d <n>"			<< endl;
	cout << "	[-s]		call special debug function and exit"					<< endl;
	cout << "	[-S <style>]	print style: 0=ascii, 1=compact, 2=hash, 3=postscript"			<< endl;
	cout << "	[-t <secs>	maximum time per move in seconds"					<< endl;
	cout << "	[-T <towerlist>	create EGTB for towerlist"						<< endl;
	cout << "	[-v]		verbose flag"								<< endl;
	cout << "	[-5]		play 5x5 laska"								<< endl;
	cout << "	[-3]		play 3x3 laska"								<< endl;
	cout << "	[-?|-h]		print this string"							<< endl;
	exit(0);
}

int main (int argc, char **argv) {
	int c;
	string	position		= "........*........*........",
		str_move		= "",
		to			= "",
		egtb_towers		= "";
	string	position_log_file	= "";
	unsigned int depth = 14;

	bool	find_best_move		= 0,
		compact2textboard	= 0,
		evaluate_position	= 0,
		test_evaluation		= 0,
		generate_move_tree	= 0,
		debug_possible_moves	= 0,
		revert_colors		= 0,
		generate_random_positions	= 0,
		input_game		= 0,
		play_complete_game	= 0;
	bool	special_debug_function	= 0,
		verbose			= 0;
	bool	position_log		= 0;
	int	board_size		= 7;
	unsigned int print_style	= 0,
		max_moves_per_game	= 100,
		number_of_random_games	= 100,
		sesc_per_move		= 60;
	unsigned int turn		= 0;	// white

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"depth",	 1, 0, 0},
			{"position",	 1, 0, 0},
			{"file",	 1, 0, 0},
			{0, 0, 0, 0}
		};

		c = getopt_long (argc, argv, "aA:Bbd:DeEilL:m:M:p:ghHjPrRsS:t:T:tvw53?", long_options, &option_index);

		if (c == -1) break;

		switch (c) {
		case 'a': find_best_move = 1;	break;
		case 'A': str_move = optarg;	break;
		case 'b': turn = 1;		break;	// play as black
		case 'd': depth = atoi(optarg); if (depth <= 0) { cerr << "treedepth <= 0 not an option\n"; return -1; } break;
		case 'D': compact2textboard	= 1;	break;
		case 'e': evaluate_position	= 1;	break;
		case 'E': test_evaluation	= 1;	break;
		case 'g': generate_move_tree	= 1;	break;
		case 'i': input_game		= 1;	break;
		case 'l': play_complete_game	= 1;	break;
		case 'L': position_log		 = 1; 	position_log_file = optarg;	break;
		case 'm': max_moves_per_game	 = atoi(optarg); if (max_moves_per_game	    <= 0) { cerr << "max_moves_per_game <= 0 not an option\n"; return -1; } break;
		case 'M': number_of_random_games = atoi(optarg); if (number_of_random_games <= 0) { cerr << "number_of_random_games <= 0 not an option\n"; return -1; } break;
		case 'p': position		= optarg;	break;
		case 'P': debug_possible_moves	= 1;	break;
		case 'r': revert_colors		= 1;	break;
		case 'R': generate_random_positions	= 1;	break;
		case 's': special_debug_function= 1;	break;
		case 'S': print_style		= atoi(optarg);	break;
		case 't': sesc_per_move		= atoi(optarg);	break;
		case 'T': egtb_towers		= optarg;	break;
		case 'v': verbose		= 1;	break;
		case '5': board_size		= 5;	break;
		case '3': board_size		= 3;	break;
		case 'h': usage();	break;
		case '?': usage();	break;
		default: cerr << "what is -" << c << " for" << endl; usage();
		}
	}

	if (optind < argc) {
		cout << "Non Options/Values in ARGV: ";
		while (optind < argc)
			cout << argv[optind++] << ' ';
		cout << endl;
	}

	initRingIndex();

	// for (unsigned int i=0; i<6561; i++) {
	// 	std::cout << Ring(i).toString() << std::endl;
	// }
	// Position p(position);

	// std::cout << p.toString(1) << std::endl;
	// p.print();

	if (debug_possible_moves) {
		cerr << "possibleMoves for " << (turn ? "black" : "white") << endl;
		Position p(position);
		p.applyMoves (str_move);
		if (turn) p.setTurn(turn);
		p.print();
		for (const auto& m: p.possibleMoves()) { cout <<  m.toString() << ' '; } cout << endl;

		return 0;
	}

	if (generate_move_tree) {
		MoveTree mt(position, board_size);
		mt.applyMoves (str_move);
		if (turn) mt.setTurn(turn);		// if not otherwise stated program take white, see below
		mt.generateMoveTree(depth);
		// optim.writeConfig(mt);
		return 0;
	}

	if (input_game) {
		string line;
		Position b (position);
		b.applyMoves (str_move);
		if (turn) b.setTurn(turn);
		do {
			b.print();
			list<Move> l;
			l = b.possibleMoves();
			if (l.empty()) {
				if (verbose) { cout << "no more moves. thx for the game." << endl; }
				else { cerr << "too many moves, declare game draw." << endl; }
				return 0;
			}
			for (const auto& m: l) { cout <<  m.toString() << ' '; } cout << endl;
			cout << "enter your move please > ";
			if (!getline(cin, line)) { return 0; }	// get input
			if (line == "q" || line == "quit") return 0;
			if (line == "u" || line == "undo") {
				cout << "undo is ..." << endl;
				b.undo();
				continue;
			}

			b.moveRecorded(Move(line));
		} while (true);
	}

	return 0;
}

// 6 12 30 84 201 524 1360 3555 9024 45662 57212

