
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <utility>

// #define NDEBUG
#include <assert.h>

#include "position.h"

enum GamePhase { Opening = 0, MidGame = 1, EndgameWhite = 2, EndgameBlack = 4, EndgameBoth = 6 };

void	Position::setTranspositionTable (TranspositionTable* t) {
#ifdef USE_POSITION_KEY
	tt = t;
#endif
}

const unsigned long long Position::key () const {
	return pos_key;
} 

void Position::init () {				// same for all constructors
	turn = 0;
	ring[0] = 0;
	ring[1] = 0;
	ring[2] = 0;
	stones[0] = 0;
	stones[1] = 0;
	captured[0] = 0;
	captured[1] = 0;
	move_count  = 0;
	phase = Opening;
}

Position::Position (const unsigned int board_size) {	// default position, unused. correct only for 7x7.
	init ();
}

Position::Position (std::string pos, const unsigned int board_size) {	// std. constructor used
	init ();
	setPosition (pos);
}

void Position::setPosition (std::string pos) {							// convert string to internal representation
	if (pos == "") return;

	// parse string
	unsigned int c = 8;
	if (pos[c] == '|' || pos[c] == ' ' || pos[c] == '*') { c = 9; }	// separator ?
	ring[0] = Ring(pos.substr(0, 8));
	ring[1] = Ring(pos.substr(c, 8));
	ring[2] = Ring(pos.substr(pos.length()-8, 8));
}

std::string	Position::moveValueString	(const Move m, const int v) const {					// format move/value pair
	std::string r = (v >  mateValue()-100) ? std::string("M") + std::to_string( mateValue()-v) :			// we never get a mate(d) in plies > 100 ?
			(v < -mateValue()+100) ? std::string("M") + std::to_string(-mateValue()-v) : std::to_string(v);

	if (r.length() < 6) r.insert(r.begin(), 6 - r.length(), ' ');

	return m.toString() + "  " + r;
}

void Position::applyMoves (std::string str_moves) {									// set list of moves given in string
	if (str_moves == "") { return; }

	print();

	std::istringstream iss(str_moves);
	std::string single_move_str;

	bool first = true;
	while (iss >> single_move_str) {
		std::cerr << single_move_str << std::endl;
		Move m(single_move_str);
		if (first) {
			if (turn == 2) {
				std::cerr << "this move list does not apply to this position, i fear. i better do nothing with this moves ... " << std::endl;
				return;
			}
			first = false;
		}
		if (!isLegal(m)) {
			std::cerr << "looks like " << m.toString() << " is not a legal move here. i better do nothing with this moves ... " << std::endl;
			return;
		}
		moveRecorded(m);
	 	print();
	}
}

void Position::setConfigValues (const int *v) {
	value = evaluatePosition();	// config values might differ from defaults, so re-evaluate. (can't be done at init() currently)
}

int Position::getConfigValue (const int v) {
	return 0;	// unreached
}

void Position::readConfig	(const std::string file_name) { }
void Position::writeConfig	(const std::string file_name) { }

void Position::print(unsigned int style, std::ostream &out) {
	out << toString(style);

	if (!style && !move_history.empty()) {	// TODO; find color of startmove, so white moves are always on the left. (needs bitboard?)
		unsigned int j = 1;
		for (const auto& m: move_history) {
			if (move_history.size() - j >20) { j++; continue; }
			if (j%2) { out << std::setw(2) << (j/2 + 1) << ". "; }
			// out << m.toString() << ' ';
			out << m;
			if (j%2) { out << " "; } else { out << std::endl; }
			++j;
		}
	}
	out << std::endl;
}

std::string Position::toString(unsigned int style) {
	if (style == 1) {
		return ring[0].toString() + "*" + ring[1].toString() + "*" + ring[2].toString();
	}

	std::string r[3]  = { ring[0].toString(), ring[1].toString(), ring[2].toString() };
	std::string mo = " ------------- ";
	std::string mc = " --------- ";
	std::string mi = " ----- ";
	std::string mm = " - ";
	string
	t  = std::string(" 7 ") + r[0][0] + mo + r[0][1] + mo + r[0][2] + "\n";
	t += "   |               |               |\n";
	t += std::string(" 6 |   ") + r[1][0] + mc + r[1][1] + mc + r[1][2] + "   |\n";
	t += "   |   |           |           |   |\n";
	t += std::string(" 5 |   |   ") + r[2][0] + mi + r[2][1] + mi + r[2][2] + "   |   |\n";
	t += "   |   |   |               |   |   |\n";
	t += std::string(" 4 ") + r[0][7] + mm + r[1][7] + mm + r[2][7] + "               " + r[2][3] + mm + r[1][3] + mm + r[0][3] + "\n";
	t += "   |   |   |               |   |   |\n";
	t += std::string(" 3 |   |   ") + r[2][6] + mi + r[2][5] + mi + r[2][4] + "   |   |\n";
	t += "   |   |           |           |   |\n";
	t += std::string(" 2 |   ") + r[1][6] + mc + r[1][5] + mc + r[1][4] + "   |\n";
	t += "   |               |               |\n";
	t += std::string(" 1 ") + r[0][6] + mo + r[0][5] + mo + r[0][4] + "\n";
	t += "   A   B   C       D       E   F   G\n";
	// std::cerr << t << std::endl;

	return t;
}

bool Position::isLegal(const Move m) {					// for applyMoves and isPermutation
	return true;
}

std::list<Move>	Position::possibleSteps	() { std::list<Move> l; return l; }	// empty list for MoveTree
std::list<Move>	Position::possibleJumps	() { std::list<Move> l; return l; }	// empty list for MoveTree

std::list<Move>	Position::possibleMoves() {
	std::cerr << "Position::possibleMoves " << move_count << " " << phase << std::endl;
	std::list<Move> l;
	if (stones[turn] == 2 && phase != Opening) {
		std::cerr << "game over. " << ((turn) ? "black" : "white") << " lost." << std::endl;
		return l;
	}
	unsigned int laterals[3];
	laterals[0] = ring[1].laterals((turn)+1) & ring[2].laterals((turn)+1);	// for check if move makes a mill
	laterals[1] = ring[0].laterals((turn)+1) & ring[2].laterals((turn)+1);
	laterals[2] = ring[0].laterals((turn)+1) & ring[1].laterals((turn)+1);
	list<Move> remos = removables();
	if (phase == Opening) {
		l =		    ring[0].possibleMoves(turn, phase,  0, laterals[0], remos);
		l.splice(l.end(),   ring[1].possibleMoves(turn, phase,  8, laterals[1], remos));
		l.splice(l.end(),   ring[2].possibleMoves(turn, phase, 16, laterals[2], remos));
	} else if ((turn == 0 && (phase & EndgameWhite)) || (turn == 1 && (phase & EndgameBlack))) {
		// std::cerr << "Position::possibleMoves endgame for " << turn << " " << phase << std::endl;
		std::list<Move>
		l2 =		     ring[0].possibleMoves(turn, Opening,  0, laterals[0], remos);	// get list of free spots
		l2.splice(l2.end(),  ring[1].possibleMoves(turn, Opening,  8, laterals[1], remos));
		l2.splice(l2.end(),  ring[2].possibleMoves(turn, Opening, 16, laterals[2], remos));
		std::list<Move>
		s =		     ring[0].removables(turn, true,  0, 0);	// get list of (three) stones
		s.splice(s.end(),    ring[1].removables(turn, true,  8, 0));
		s.splice(s.end(),    ring[2].removables(turn, true, 16, 0));
		// std::cerr << "merging.\n3 stones:" << std::endl;
		// for (const auto& m:  s) { cout << m.toString() << ' '; } cout << endl;
		// std::cerr << "with free spots:" << std::endl;
		// for (const auto& m: l2) { cout << m.toString() << ' '; } cout << endl;
		for (auto &m:  s) {						// for each stone
		for (auto &f: l2) {						// move to free spots
			// std::cerr << (int) f.to() << " | " << (int) m.to() << "    ";
			l.push_back ((f.to()|(m.to()<<5)) | EndgameMove);	// dont care, who's endgame it is
		}					// TODO: consider removes!
		}
		std::cerr << "merging done." << std::endl;
	} else {	// midgame
		l =		    ring[0].possibleMoves(turn, phase,  0, laterals[0], remos);
		l.splice(l.end(),   ring[1].possibleMoves(turn, phase,  8, laterals[1], remos));
		l.splice(l.end(),   ring[2].possibleMoves(turn, phase, 16, laterals[2], remos));
		// add cross ring moves here.
		for (unsigned int b = ring[0].laterals(turn+1) & ring[1].laterals(0); b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			l.push_back(((at<<5)|((at+8) | MidgameMove)));
		}
		for (unsigned int b = ring[1].laterals(turn+1) & ring[0].laterals(0); b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			l.push_back((((at+8)<<5)|((at+0) | MidgameMove)));
		}
		for (unsigned int b = ring[1].laterals(turn+1) & ring[2].laterals(0); b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			l.push_back((((at+8)<<5)|((at+16) | MidgameMove)));
		}
		for (unsigned int b = ring[2].laterals(turn+1) & ring[1].laterals(0); b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			l.push_back((((at+16)<<5)|((at+8) | MidgameMove)));
		}
	}
	
	// for (const auto& m: l) { cout <<  (int) m.index() << ' '; } cout << endl;
	return l;
}

std::list<Move>	Position::removables()  const {
	unsigned int lat = ring[0].laterals((1^turn)+1) & ring[1].laterals((1^turn)+1) & ring[2].laterals((1^turn)+1);
	list<Move>
	l =		    ring[0].removables(1^turn, false,  0, lat);
	l.splice(l.end(),   ring[1].removables(1^turn, false,  8, lat));
	l.splice(l.end(),   ring[2].removables(1^turn, false, 16, lat));
	if (l.empty()) {
	l =		    ring[0].removables(1^turn, true,  0, lat);
	l.splice(l.end(),   ring[1].removables(1^turn, true,  8, lat));
	l.splice(l.end(),   ring[2].removables(1^turn, true, 16, lat));
	}
	// for (const auto& m: l) { cout <<  Move(m).toString() << ' '; } cout << endl;

	return l;
}

void Position::moveRecorded(const Move m) {	// move and add move to history
	move_history.push_back(m);
	return move(m);
}

void Position::move(const Move m) {
	std::cerr << "Position::move(" << m.toString() << ");" << std::endl;

	// move_history.push_back(m);

	if (m.isOpening()) {
		unsigned int t = m.to();
		ring[t/8].move(t%8, turn);
		stones[turn] ++;
	} else if (m.isMidgame()) {
		     if (m.from() <  8) { ring[0].clear(m.from()   , turn); }
		else if (m.from() < 16) { ring[1].clear(m.from()- 8, turn); }
		else if (m.from() < 24) { ring[2].clear(m.from()-16, turn); }
		     if (m.to()   <  8) { ring[0].move (m.to()     , turn); }
		else if (m.to()   < 16) { ring[1].move (m.to()  - 8, turn); }
		else if (m.to()   < 24) { ring[2].move (m.to()  -16, turn); }
	}
	if (m.isRemove()) {
		     if (m.remove() <  8) {	 ring[0].clear(m.remove()   , 1^turn); }
		else if (m.remove() < 16) {	 ring[1].clear(m.remove()- 8, 1^turn); }
		else if (m.remove() < 24) {	 ring[2].clear(m.remove()-16, 1^turn); }
		stones[1^turn] --;
		// std::cerr << "post remove: " << stones[0] << " " << stones[1] << std::endl;
		if (stones[1^turn] == 3 && phase != Opening) {
			(1^turn) ? phase |= EndgameBlack : phase |= EndgameWhite;
			std::cerr << "endgame for " << (1^turn) << " started. phase=" << phase << std::endl;
		}
	}

	move_count ++;
	if (move_count == 18) { phase = MidGame; }
	switchTurn();
}

void Position::undo() {				// remove last move in history, cut history
	if (move_history.size() == 0) return;

	undo(move_history.back());

	move_history.pop_back();
}

void Position::undo(const Move m) {
	std::cerr << "Position::undo(" << m.toString() << ");" << std::endl;

	move_count --;
	if (move_count < 18) { phase = Opening; }
	switchTurn();
	if (m.isOpening()) {
		unsigned int t = m.to();
		ring[t/8].clear(t%8, turn);
	} else if (m.isMidgame()) {
		     if (m.from() <  8) { ring[0].move (m.from()   , turn); }
		else if (m.from() < 16) { ring[1].move (m.from()- 8, turn); }
		else if (m.from() < 24) { ring[2].move (m.from()-16, turn); }
		     if (m.to()   <  8) { ring[0].clear(m.to()     , turn); }
		else if (m.to()   < 16) { ring[1].clear(m.to()  - 8, turn); }
		else if (m.to()   < 24) { ring[2].clear(m.to()  -16, turn); }
	}
	if (m.isRemove()) {
		     if (m.remove() <  8) { ring[0].move(m.remove()   , 1^turn); }
		else if (m.remove() < 16) { ring[1].move(m.remove()- 8, 1^turn); }
		else if (m.remove() < 24) { ring[2].move(m.remove()-16, 1^turn); }
		stones[1^turn] ++;
		if (stones[1^turn] == 3 && phase != Opening) { (1^turn) ? phase |= EndgameBlack : phase |= EndgameWhite; }
	}

}

int Position::evaluatePosition() const {
	return stones[turn] - stones[1^turn];
	return value;
}

extern void initRingIndex();

void initializeGame (const unsigned int board_size) {
	initRingIndex();

}

