#include "movetree.h"

// testing possibleMoves() finder

MoveTree::MoveTree (const unsigned int board_size)		: Position(board_size)				{ } 
MoveTree::MoveTree (string pos, const unsigned int board_size)	: Position(pos, board_size), move_count(0)	{ }

bool MoveTree::isPermutation	(const Move m) {
	// std::cerr << "MoveTree::symmetryCheck " << Move(m).toString() << std::endl;

	if (move_history.size() < 3) return false;	// need at least three moves

	Move other (move_history.back());
	std::list<Move>::const_reverse_iterator i = ++(move_history.rbegin());
	Move m0(*i);							// second last move, undo 2 times, and reverse moves (m0, other, m) to (m, other, m0)
	
	if (m0.index  () <  m.index  ()) { return false; }		// the variation to keep 
	if (m0.isStep () != m.isStep ()) { return false; }		// exchanded moves both be steps or jumps
	if (m0.to     () == m.from   ()) { return false; }		// current move is a continuation of former, cannot exchange

	undo(other);							// undo 2 moves and apply in reversed order below
	undo(m0);

	if (!isLegal(m)) {						// m not legal in this position
		move(m0);						// restore position
		move(other);
		return false;
	}
	move(m);
	if (!isLegal(other)) {						// opponents move not legal after playing m
		undo(m);						// restore position
		move(m0);
		move(other);
		return false;
	}
	if (other.isStep()) {
		if (!possibleJumps().empty()) {				// m provokes a jump, but m0 doesn't. (move above already called switchTurn() )
			undo(m);
			move(m0);
			move(other);
			return false;
		}
	}
	move(other);
	if (!isLegal(m0)) {
		undo(other);
		undo(m);
		move(m0);
		move(other);
		return false;
	}

	undo(other);							// restore position
	undo(m);
	move(m0);
	move(other);

	// std::cerr << "MoveTree::symmetryCheck is permutation." << std::endl;

	return true;
}

void MoveTree::generateMoveTree(unsigned int depth) {
	// cerr << "generateMoveTree(" << depth << ")" << endl;

	if (!depth) {
		print (1);
		return;
	}

	list<Move> l = possibleMoves();

	if (l.empty()) { /* cerr << "game over." << endl; */ return; }		// game over

	for (const auto& m: l) {
		// if (isPermutation (m)) { continue; }

		//for (const auto& mi: move_history) { cerr << (mi) << ' '; }
		// move_count ++; cerr << move_count << ". ";
		//cerr << m.toString() << endl;

		//moveRecorded(m);
		move(m);

		// print(1);

		generateMoveTree(depth-1);

		//undo();
		undo(m);
		// move_history.pop_back();
	}
}

