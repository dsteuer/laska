
#ifndef __MOVE_H
#define __MOVE_H

#include <vector>
#include <iostream>

enum MoveType { OpeningMove = (1<<16), MidgameMove = (1<<17), EndgameMove = (1<<18), RemoveMove = (1<<19), NoMove = (unsigned) -1 };

struct Move {
public:
	Move ()			: _index(NoMove) { }
	Move (unsigned int i)	: _index(i)	 { }
	Move (std::string m);

	bool            isStep          () const;
	bool		isJump		() const;
	bool            isQuiet         () const;
	bool		isOpening	() const	{ return (index() & OpeningMove); }
	bool		isMidgame	() const	{ return (index() & MidgameMove); }
	bool		isEndgame	() const	{ return (index() & EndgameMove); }
	bool		isRemove	() const	{ return (index() & RemoveMove);  }
	bool		empty		() const;

	unsigned char	from		() const;
	unsigned char	to		() const;
	unsigned char	remove		() const;	// the stone to remove in case of a mill

	std::string	toString	() const;

	unsigned int	index		() const	{ return _index; };

	bool operator<(const Move &m) const { return index() < m.index(); }
	operator	unsigned int	() const { return _index; }
	Move&		operator++	() { ++_index; return *this; }

private:
	unsigned int	square		(std::string s);

	unsigned int	_index;
};

std::ostream& operator<<(std::ostream& s, const Move &m);
#endif

