
#include "ring.h"

#include <iostream>
#include <iomanip>

const unsigned int pow3[8] = { 1, 3, 9, 27, 81, 243, 729, 2187 };

struct RingEntry {
	unsigned char	bits[3], laterals;
	short		value;
	unsigned short	pm[2][8];
};

static  RingEntry	ring_index[6561];

Ring::Ring	(string	s) {
	index = 0;
	int n = 1;
	for(basic_string<char>::const_iterator i = s.begin(); i!= s.end(); i++) {
		switch (*i) {
		case 'w':
		case 'W':
		case 'o':
		case 'O':	index += n;	break;
		case 'b':
		case 'B':
		case 'x':
		case 'X':	index += 2*n;	break;
		case '.':
		case '-':
		case '|':			break;
		default:
			cerr << "What is " << (*i) << endl;

		}
		n *=3;
	}
//	cerr << toString() << endl;
	_value = evaluate();
}

Ring::Ring	(unsigned int d) : index(d) {
	_value = evaluate();
}

unsigned int Ring::color	(const unsigned int i)	const { return (index / pow3[i])%3; 		}

unsigned int Ring::ownStones	() const	{	// good question
	return 0;
}

void Ring::set (const unsigned int at, const unsigned int color) {
	move (at, color);
}

unsigned int mills[4] = { 7, 7<<2, 7<<4, 1|(3<<6) };
 
void Ring::move (const unsigned int at, const unsigned int color) {
	index += (pow3[at] * (color+1));
	_value = evaluate();
}

void Ring::clear (const unsigned int at, const unsigned int color) {
	index -= (pow3[at] * (color+1));
	_value = evaluate();
}

unsigned char Ring::laterals(const unsigned int color) const {
	return ring_index[index].bits[color] & (2|8|32|128);
}

list<Move> Ring::removables(const unsigned int color, const bool forced, const unsigned int offset, const unsigned int lateral_mask) const {
	list<Move> l;
	// std::cerr << "Ring::removables " << index << " " << toString() << " " << color << std::endl;
	for (unsigned int b = (int) ring_index[index].bits[color+1]; b; b &= b-1) {
		unsigned int at = __builtin_ffs(b)-1;
		// std::cerr << "check " << at << std::endl;
		if (!forced) {
			// std::cerr << at << " in lateral mask " << lateral_mask << std::endl;
			if ((1<<at) & lateral_mask) {
				// std::cerr << "Ring::removables " << at << " is in lateral mill." << std::endl;
				continue;
			}
			if (at%2) {
				if (((ring_index[index].bits[color+1]) & (mills[((at-1)/2)])) == mills[((at-1)/2)]) {
					// std::cerr << " is in mill. 1" << std::endl;
					continue;
				}
			} else {
				unsigned int a = at/2;
				if (a == 0) a = 3;
				// std::cerr << a << " " << mills[a] << " " << (int) ring_index[index].bits[color+1] << std::endl;
				if (((ring_index[index].bits[color+1]) & (mills[a])) == mills[a]) {
					// std::cerr << at << " is in mill. 2" << std::endl;
					continue;
				}
				a = (at+2)/2;
				if (a == 4) a = 0;
				if (((ring_index[index].bits[color+1]) & (mills[a])) == mills[a]) {
					// std::cerr << " is in mill. 3" << std::endl;
					continue;
				}
			}
		}
		// std::cerr << (at+offset) << " adding." << std::endl;
		l.push_back((at+offset) | RemoveMove);
	}
	// for (const auto& m: l) { cerr <<  m << " " << Move(m).toString() << ' '; } cerr << endl;

	return l;
}

list<Move> Ring::possibleMoves(const unsigned int color, const unsigned int phase, const unsigned int offset, const unsigned int lateral_mask, const list<Move> removable) const {
	list<Move> l;
	if (phase == 0) {
		unsigned int bits = ring_index[index].bits[color+1];			// actual stones of color on this ring
		// std::cerr << "Ring::possibleMoves " << color << " "  << phase << " "  <<offset << " "  << lateral_mask << " bits=" << bits << std::endl;
		for (unsigned int b = (int) ring_index[index].bits[0]; b; b &= b-1) {	// free spots on this ring
			unsigned int at = __builtin_ffs(b)-1;
			// std::cerr << "at " << at << " " << (1<<at) << std::endl;
			unsigned int will_mill = 0;
			for (unsigned int i=0; i<4; i++) {
				// std::cerr << "milltest " << mills[i] << " " << (bits|(1<<at)) << std::endl; 
				if (((mills[i] & (1<<at)) && (((bits|(1<<at)) & (mills[i])) == mills[i])) || ((1<<at) & lateral_mask)) {
					// std::cerr << at << " will mill." << std::endl;
					will_mill = RemoveMove;
					break;
				}
			}
			if (will_mill) {
				for (const auto& m: removable) {
					l.push_back((at+offset) | (m<<10) | OpeningMove | RemoveMove);
				}
			} else {
				l.push_back((at+offset) | OpeningMove);
			}
		}
	} else if (phase == 1 || (phase == 3 && color == 1) || (phase == 4 && color == 0)) {
		// std::cerr << "Ring::possibleMoves " << color << " " << phase << " " << offset << std::endl;
		// std::cerr << "index " << index << std::endl;
		unsigned int bits = ring_index[index].bits[color+1];
		for (unsigned int n= 0; ring_index[index].pm[color][n]; n++) {
			unsigned int at = ring_index[index].pm[color][n];
			unsigned int will_mill = 0;
			for (unsigned int i=0; i<4; i++) {
				if (at%2) {	// mill check: shuffling on odd index can only make lateral mills, to is in first 5 bits
					if ((1<<(at&31)) & lateral_mask) {
						// std::cerr << at << " will mill." << std::endl;
						will_mill = RemoveMove;
						break;
					}
				} else {
					if (( (mills[i] & (1<< (at& 31    )    ))  && // to is in mill
					     !(mills[i] & (1<<((at&(31<<5))>>5)))) && // but not from
					    (((bits|(1<<at)) & (mills[i])) == mills[i])) { // with my other stones so make a mill
						// std::cerr << at << " will mill." << std::endl;
						will_mill = RemoveMove;
						break;
					}
				}
			}
			if (will_mill) {
				for (const auto& m: removable) {
					l.push_back((at+(offset|offset<<5))| (m<<10) | MidgameMove | RemoveMove);
				}
			} else {
				l.push_back((at+(offset|offset<<5)) | MidgameMove);	// at contains fromn->to pair, both to add ring offest to
			}
		}
	} else {	// must be sort of endgame
		std::cerr << "Ring::possibleMoves: this situiation not implemented, yet. phase=" << phase << " color=" << color << std::endl;
	}

	return l;
}

string		Ring::toString	() const {
	std::string s;
	static std::string c[3] = { ".", "o", "x" };
	for (unsigned int i = index; i; i/=3) {
		s += c[i%3];
	}
	if (s.length() < 8) s.insert(s.end(), 8-s.length(), '.');
	return s;
}

int Ring::evaluate() const {
	return 0;
}

void initRingIndex () {
	for (unsigned int r = 0; r<6561; r++) {
		ring_index[r].bits[0] = 0;
		ring_index[r].bits[1] = 0;
		ring_index[r].bits[2] = 0;
		// for opening
		for (unsigned int i = r, n=0; n<8; i/=3, n++) {
			ring_index[r].bits[i%3] |= (1<<n);
		}
		// midgame shift moves
		unsigned int color = 0;
		// std::cerr << "Ring::possibleMoves " << r << " " << Ring(r).toString() << std::endl;
		// std::cerr << "w" << std::endl;
		unsigned int bits[3];
		bits[0] = (int) ring_index[r].bits[0];
		bits[1] = (int) ring_index[r].bits[1];
		bits[2] = (int) ring_index[r].bits[2];
		unsigned int n = 0;
		for (unsigned int b = bits[color+1]; b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			if (at   && (bits[0]&(1<<(at-1)))) { /* std::cerr << "m1 " << ((at-1)|(at<<5)) << std::endl;  */ ring_index[r].pm[color][n] = ((at-1)|(at<<5)); n++; } 	// to <- from save
			if (at<7 && (bits[0]&(1<<(at+1)))) { /* std::cerr << "m2 " << ((at+1)|(at<<5)) << std::endl;  */ ring_index[r].pm[color][n] = ((at+1)|(at<<5)); n++; }		// to <- from save
		}
		if ((bits[color+1] &     1 ) && (bits[0] & (1<<7))) { /* std::cerr << "m0a " << (7)    << std::endl;  */ ring_index[r].pm[color][n] =  7;     n++; }
		if ((bits[color+1] & (1<<7)) && (bits[0] &  1    )) { /* std::cerr << "m0b " << (7<<5) << std::endl;  */ ring_index[r].pm[color][n] = (7<<5); n++; }
		// std::cerr << "b" << std::endl;
		n = 0;
		color = 1;
		for (unsigned int b = bits[color+1]; b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			if (at   && (bits[0]&(1<<(at-1)))) { /* std::cerr << "m1 " << ((at-1)|(at<<5)) << std::endl;  */ ring_index[r].pm[color][n] = ((at-1)|(at<<5)); n++; } 	// to <- from save
			if (at<7 && (bits[0]&(1<<(at+1)))) { /* std::cerr << "m2 " << ((at+1)|(at<<5)) << std::endl;  */ ring_index[r].pm[color][n] = ((at+1)|(at<<5)); n++; }		// to <- from save
		}
		if ((bits[color+1] &     1 ) && (bits[0] & (1<<7))) { /* std::cerr << "m0a " << (7)    << std::endl;  */ ring_index[r].pm[color][n] =  7;     n++; }
		if ((bits[color+1] & (1<<7)) && (bits[0] &  1    )) { /* std::cerr << "m0b " << (7<<5) << std::endl;  */ ring_index[r].pm[color][n] = (7<<5); n++; }
	}
}

