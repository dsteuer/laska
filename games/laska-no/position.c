
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <utility>

// #define NDEBUG
#include <assert.h>

#include "position.h"

PositionNoOfficer::PositionNoOfficer (			const unsigned int board_size) : Position(     board_size)  { } 
PositionNoOfficer::PositionNoOfficer (std::string pos,	const unsigned int board_size) : Position(pos, board_size)  { }

std::list<Move> PositionNoOfficer::possibleSteps() {
	std::list<Move> l;

	// in no officers case we don't need to consider towers on the
	// second last line, cause if they move they will promote,
	// so mask them out.
	unsigned int no_promotion[2] = { (unsigned) ~((1<<18)|(1<<19)|(1<<20)), (unsigned) ~((1<<4)|(1<<5)|(1<<6)) };

	for (unsigned int turn_bits = bitb.bits[turn] & no_promotion[turn]; turn_bits; turn_bits &= turn_bits-1) {	// for turn's towers
		unsigned int at = __builtin_ffs(turn_bits)-1;                                   // number of first bit set - 1, square of tower

		int stone_type = (tower[at].isOfficer()) ? OFFICER : turn;

		for (Move m = startIndexSteps(at, stone_type); m.index() < stopIndexSteps(at, stone_type); ++m){
			if (bitb.collides(m.stepMask())) { continue; }			// road blocked
			l.push_back (m);		// step is legal.
		}	
	}

	// std::cerr << "PositionNoOfficer::possibleSteps: "; for (auto& i: l) { std::cerr << i << " "; } std::cerr << std::endl;
	return l;
}

std::list<Move>	PositionNoOfficer::possibleJumps() {	
	// std::cerr << "PositionNoOfficer::possibleJumps " << turn << std::endl;

	std::list<Move> l;

        // in no officers case we don't need to consider towers on the
        // third last line, cause if they move they will promote,
        // so mask them out.
        unsigned int no_promotion[2] = {
                (unsigned) ~((1<<14)|(1<<15)|(1<<16)|(1<<17)|(1<<18)|(1<<19)|(1<<20)),
                (unsigned) ~((1<< 4)|(1<< 5)|(1<< 6)|(1<< 7)|(1<< 8)|(1<< 9)|(1<<10))
        };

	for (unsigned int turn_bits = bitb.bits[turn] & no_promotion[turn]; turn_bits; turn_bits &= turn_bits-1) {
		unsigned int at = __builtin_ffs(turn_bits)-1;						// tower is at square, shape does not matter

		int stone_type = (tower[at].isOfficer()) ? OFFICER : turn;				// except, officer has more possibilites, (stored in move_index)
		
		if (tower[at].size() == 2) { continue; }						// no more than 2 stones in a tower allowed

		for (Move m = startIndexJumps(at, stone_type); m.index() < stopIndexJumps(at, stone_type); ++m){	// for possible moves in move_index
			if ((bitb.collides	(m.stepMask())))		 {  continue; }		// bitboard check: squares to move on is occupied already
			if ((bitb.isNotOccupied	(m.overMask(), 1^turn)))	 {  continue; }		// square this move will jump over is empty

			if (m.contFrom(stone_type)) {					// index says, there may be continuations
				l.splice(l.end(), continueJumps(stone_type, m));
			} else {					// single jump move is complete (by move_index)
				l.push_back (m);		// jump is legal.
			}
		}
	}
	// std::cerr << "PositionNoOfficer::possibleJumps: "; for (auto& i: l) { std::cerr << i << " "; } std::cerr << std::endl;
	return l;
}

std::list<Move> PositionNoOfficer::continueJumps(const unsigned int stone_type, const Move index) const {
	std::list<Move> l;
	
	for (Move m = index.contFrom(stone_type); m.index() < index.contTo(stone_type); ++m) {
		if ((bitb.collides	(m.stepMask())))	 { continue; }		// square to jump on is occupied by another tower
		if ((bitb.isNotOccupied	(m.overMask(), 1^turn))) { continue; }		// square this move will jump over is empty
		
		// if (longJumpsCheck(m) == false) continue;
		
		return l;	// if we get here, this is at least a double jump. we do not consider this moves. since this function essentially adds a move to the possible moves list, everything is ok.
	}
	l.push_back (index);		// jump is legal. and remains a single jump. (decide add here, not in parent.)
	return l;
}

std::list<Move>	PositionNoOfficer::possibleMoves() {
	std::list<Move> l = possibleJumps ();

	if (l.empty()) {
		if (bitb.hasJumps(turn)) {	// we consider no move, but there are some.
			return l;		// return empty list
		}
	}

	return  (l.empty()) ? possibleSteps() : l;
}

void PositionNoOfficer::moveRecorded(const Move m) {	// move and add move to history
	move(m);
	move_history.push_back(m);
}

void PositionNoOfficer::move(const Move m) {
	// std::cerr << "PositionNoOfficer::move(" << m.toString() << ");" << std::endl;

	// move_history.push_back(m);

	bitb.move(m.from(), m.to(), turn);
	std::swap(tower[m.to()], tower[m.from()]);			// swap towers

	assert (bitb.isOfficer(m.to(), turn) == tower[m.to()].isOfficer());
	if (m.isOnPromotionSquare() && !tower[m.to()].isOfficer()) {	// move promotes
		tower[m.to()].promote();
		bitb.bits[turn+2] |= (1<<m.to());
	}

	if (m.isJump()) {
		unsigned int other = !turn;
		for (unsigned int j = 0; j<m.size(); j++) {		// captures
			unsigned int over = m.over(j);
			const bool iso = tower[over].isOfficer();
			tower[m.to()].push_back(tower[over].pop());	// add captured stone ot turns tower
			if      ( iso && !tower[over].isOfficer()) bitb.degrade (over, other);
			else if (!iso &&  tower[over].isOfficer()) bitb.promote (over, other);
			if (tower[over].empty()) {			// last stone of tower captured
				bitb.erase(over, other);
			} else if (tower[over].color() == turn) {	// color switch
				bitb.switchColor(over, turn);
			}						// if color remains same after hit, nothing to do.
		}
	}

	switchTurn();
}

void PositionNoOfficer::undo() {				// remove last move in history, cut history
	if (move_history.size() == 0) return;

	undo(move_history.back());

	move_history.pop_back();
}

void PositionNoOfficer::undo(const Move m) {
	// std::cerr << "PositionNoOfficer::undo(" << m.toString() << ");" << std::endl;

	// move_history.pop_back();
	switchTurn();

	bitb.move(m.to(), m.from(), turn);
	std::swap(tower[m.to()], tower[m.from()]);

	if (m.isPromotion()) {
		tower[m.from()].degrade();
		bitb.bits[turn+2] &= ~(1<<m.from());
	}	

	if (m.isJump()) {
		unsigned int other = !turn;
		for (unsigned int j = m.size(); j; j--) {	// cause unsigned
			unsigned int over = m.over(j-1);	// substract here
			if (tower[over].empty()) {			// last stone of tower was captured
				bitb.set(over, tower[m.from()].back());
			} else if (tower[over].color() == turn) {	// color change
				bitb.switchColor(over, other);
			}
			const bool iso = tower[over].isOfficer();
			tower[over].push(tower[m.from()].pop_back());	// restore stone on top of tower
			if      ( iso && !tower[over].isOfficer()) bitb.degrade (over, other);
			else if (!iso &&  tower[over].isOfficer()) bitb.promote (over, other);
		}
	}
}

