
#ifndef __POSITION_NO_H
#define __POSITION_NO_H

#include "../laska/position.h"

class PositionNoOfficer : public Position {
public:
	PositionNoOfficer (const unsigned int board_size = 7);
	PositionNoOfficer (std::string	pos, const unsigned int board_size = 7);

	std::list<Move>	possibleMoves	();
	std::list<Move>	possibleJumps	();
	std::list<Move>	possibleSteps	();

	void		move		(const Move);
	void		moveRecorded	(const Move);
	void		undo		();
	void		undo		(const Move);

protected:
	std::list<Move>	continueJumps(const unsigned int stone_type, const Move index) const;
};

#endif
