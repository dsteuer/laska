
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <utility>
#include <string>

// #define NDEBUG
#include <assert.h>

#include "position.h"

enum GamePhase { Opening = 0, MidGame = 1, EndgameWhite = 2, EndgameBlack = 4, EndgameBoth = 6 };

void	Position::setTranspositionTable (TranspositionTable* t) {
#ifdef USE_POSITION_KEY
	tt = t;
#endif
}

const unsigned long long Position::getKey () const {
	return pos_key;
} 

void Position::init () {				// same for all constructors
	turn = 0;
	stones[0] = 0;
	stones[1] = 0;
	captured[0] = 0;
	captured[1] = 0;
	move_count  = 0;
	phase = Opening;
}

Position::Position (const unsigned int board_size) {	// default position, unused. correct only for 7x7.
	init ();
}

Position::Position (std::string pos, const unsigned int board_size) {	// std. constructor used
	init ();
	setPosition (pos);
}

void Position::setPosition (std::string pos) {							// convert string to internal representation
	if (pos == "") return;
}

std::string	Position::moveValueString	(const Move m, const int v) const {					// format move/value pair
	std::string r = (v >  mateValue()-100) ? std::string("M") + std::to_string( mateValue()-v) :			// we never get a mate(d) in plies > 100 ?
			(v < -mateValue()+100) ? std::string("M") + std::to_string(-mateValue()-v) : std::to_string(v);

	if (r.length() < 6) r.insert(r.begin(), 6 - r.length(), ' ');

	return m.toString() + "  " + r;
}

void Position::applyMoves (std::string str_moves) {									// set list of moves given in string
	if (str_moves == "") { return; }

	print();

	std::istringstream iss(str_moves);
	std::string single_move_str;

	bool first = true;
	while (iss >> single_move_str) {
		std::cerr << single_move_str << std::endl;
		Move m(single_move_str);
		if (first) {
			if (turn == 2) {
				std::cerr << "this move list does not apply to this position, i fear. i better do nothing with this moves ... " << std::endl;
				return;
			}
			first = false;
		}
		if (!isLegal(m)) {
			std::cerr << "looks like " << m.toString() << " is not a legal move here. i better do nothing with this moves ... " << std::endl;
			return;
		}
		moveRecorded(m);
	 	print();
	}
}

void Position::setConfigValues (const int *v) {
	value = evaluatePosition();	// config values might differ from defaults, so re-evaluate. (can't be done at init() currently)
}

int Position::getConfigValue (const int v) {
	return 0;	// unreached
}

void Position::readConfig	(const std::string file_name) { }
void Position::writeConfig	(const std::string file_name) { }

void Position::print(unsigned int style, std::ostream &out) {
	out << toString(style);

	if (!style && !move_history.empty()) {	// TODO; find color of startmove, so white moves are always on the left. (needs bitboard?)
		unsigned int j = 1;
		for (const auto& m: move_history) {
			if (move_history.size() - j >20) { j++; continue; }
			if (j%2) { out << std::setw(2) << (j/2 + 1) << ". "; }
			// out << m.toString() << ' ';
			out << m;
			if (j%2) { out << " "; } else { out << std::endl; }
			++j;
		}
	}
	out << std::endl;
}

std::string Position::toString(unsigned int style) {
	if (style == 1) {
		// return std::string s("compact position (fen)");
		return "compact position (fen)";
	}

	std::string mo = " ------------- ";
	std::string mc = " --------- ";
	std::string mi = " ----- ";
	std::string mm = " - ";
	std::string
	t  = std::string(" 7 ") + mo + mo + "\n";
	t += "   |               |               |\n";
	t += "   |   |           |           |   |\n";
	t += "   |   |   |               |   |   |\n";
	t += "   |   |   |               |   |   |\n";
	t += "   |   |           |           |   |\n";
	t += "   |               |               |\n";
	t += std::string(" 1 ") + mo + mo + "\n";
	t += "   A   B   C       D       E   F   G\n";
	// std::cerr << t << std::endl;

	return t;
}

bool Position::isLegal(const Move m) {					// for applyMoves and isPermutation
	return true;
}

std::list<Move>	Position::possibleSteps	() { std::list<Move> l; return l; }	// empty list for MoveTree
std::list<Move>	Position::possibleJumps	() { std::list<Move> l; return l; }	// empty list for MoveTree

std::list<Move>	Position::possibleMoves() {
	std::cerr << "Position::possibleMoves " << move_count << " " << phase << std::endl;
	std::list<Move> l;
	if (stones[turn] == 2 && phase != Opening) {
		std::cerr << "game over. " << ((turn) ? "black" : "white") << " lost." << std::endl;
		return l;
	}
	if (phase == Opening) {
	} else if ((turn == 0 && (phase & EndgameWhite)) || (turn == 1 && (phase & EndgameBlack))) {
	} else {	// midgame
	}
	
	// for (const auto& m: l) { cout <<  (int) m.index() << ' '; } cout << endl;
	return l;
}

std::list<Move>	Position::removables()  const {
	std::list<Move> l;
	// for (const auto& m: l) { cout <<  Move(m).toString() << ' '; } cout << endl;

	return l;
}

void Position::moveRecorded(const Move m) {	// move and add move to history
	move_history.push_back(m);
	return move(m);
}

void Position::move(const Move m) {
	std::cerr << "Position::move(" << m.toString() << ");" << std::endl;

	// move_history.push_back(m);

	if (m.isOpening()) {
		stones[turn] ++;
	} else if (m.isMidgame()) {
	}
	if (m.isRemove()) {
		stones[1^turn] --;
		// std::cerr << "post remove: " << stones[0] << " " << stones[1] << std::endl;
		if (stones[1^turn] == 3 && phase != Opening) {
			(1^turn) ? phase |= EndgameBlack : phase |= EndgameWhite;
			std::cerr << "endgame for " << (1^turn) << " started. phase=" << phase << std::endl;
		}
	}

	move_count ++;
	if (move_count == 18) { phase = MidGame; }
	switchTurn();
}

void Position::undo() {				// remove last move in history, cut history
	if (move_history.size() == 0) return;

	undo(move_history.back());

	move_history.pop_back();
}

void Position::undo(const Move m) {
	std::cerr << "Position::undo(" << m.toString() << ");" << std::endl;

	move_count --;
	if (move_count < 18) { phase = Opening; }
	switchTurn();
	if (m.isOpening()) {
	} else if (m.isMidgame()) {
	}
	if (m.isRemove()) {
		stones[1^turn] ++;
		if (stones[1^turn] == 3 && phase != Opening) { (1^turn) ? phase |= EndgameBlack : phase |= EndgameWhite; }
	}

}

int Position::evaluatePosition() const {
	return stones[turn] - stones[1^turn];
	return value;
}

void initializeGame (const unsigned int board_size) {
}

