#ifndef __BITB_H
#define __BITB_H

#include <list>
#include <string>

#include "tower.h"
#include "move.h"

enum MoveType { Step  = 0, Jump  = 1 };
enum Color    { White = 0, Black = 1 };

struct BitBoard {
	unsigned int	board_size;
	unsigned int	bits[4];
	Tower		tower[25];	// the towers

	// construct / init
			BitBoard	(const unsigned int boardsize);

	void		initMoveHashes	(const unsigned int boardsize);
	void		clear		();

	// forward from position
	void		move		(      Move &m, const unsigned int turn);
	void		undo		(const Move m, const unsigned int turn);
	// for moves
	void		move		(const unsigned int from, const unsigned int to, const unsigned int color);
	void		switchColor	(const unsigned int at,   const unsigned int color);
	void		erase		(const unsigned int at,   const unsigned int color);
	void		degrade		(const unsigned int at,   const unsigned int color);
	void		set		(const unsigned int at,   const unsigned int color);
	void		setTower	(const unsigned int at,   const Tower	     t);
	void		promote		(const unsigned int at,   const unsigned int color);
	void		setGrade	(const unsigned int at,   const unsigned int color, const unsigned int grade);	// post capture, set officer flag according to tower new top stone

	// checks for Position
	bool		collides	(const unsigned int move)    const;
	bool		isNotOccupied	(const unsigned int squares, const unsigned int color) const;
	const bool	isOfficer	(const unsigned int square,  const unsigned int color) const;
	unsigned int	color		(const unsigned int square)  const;
	const unsigned int both		() const;
	const unsigned int all		(const unsigned int color)   const;						// privates and officers (top stone color of pieces/towers of color)
	const unsigned int privates	(const unsigned int color)   const;
	const unsigned int officers	(const unsigned int color)   const;
	std::list<Move>	hashMoves	(const Color color, const MoveType movetype, const unsigned int p, const unsigned int o, const unsigned int e, const unsigned int b) const;
	std::list<Move> generateMoves	(const Color color, const MoveType movetype)   const;
	std::list<Move> continueJumps	(const unsigned int color,   const Move index, const unsigned int stone_type) const;
	void		listMoves	(unsigned int moves, const unsigned int color, const unsigned int skip, const bool do_promotion, std::list<Move> &l) const;
	bool		longJumpsCheck	(const Move m)		     const;

	// evaluation
	int 		adjacent	(const Color color)	     const;
	int		evaluateTower	(const int at)		     const;
	int		evaluateTowers	(const Color color)	     const;
	void		readConfig	(const std::string file_name);
	void		writeConfig	(const std::string file_name);

	const bool	hasSteps	(const unsigned int color)   const;			// for qsearch )obsolete)
	bool		hasJumps	(const unsigned int color)   const;			// no officer game only

	std::string	toString	() const;
};

#endif

