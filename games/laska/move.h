
#ifndef __MOVE_H
#define __MOVE_H

#include <vector>
#include <iostream>

enum MoveBits { NoMove = -1, BIT_PROMOTE = 13, PROMOTE = (1<<BIT_PROMOTE) };

struct Move {
public:
	Move ()			: _index(NoMove) { }
	Move (unsigned int i)	: _index(i)	 { }
	Move (std::string m);

	bool            isStep          () const;
	bool		isJump		() const;
	bool            isQuiet         () const;
	bool		isPromotion	() const;
	void		setPromotion	();		// case position initilized by movelist
	bool		empty		() const;
	unsigned int	size		() const;
	unsigned char	step		(const unsigned int i) const;
	unsigned int	stepMask	() const;
	unsigned int	overMask	() const;
	bool		isOnPromotionSquare()const;

	unsigned char	from		() const;
	unsigned char	from		(const unsigned int i) const;

	unsigned char	to		() const;
	unsigned char	to		(const unsigned int i) const;
	unsigned int	over		(const unsigned int i) const;
	unsigned int	contFrom	(const unsigned int t) const;				// first index of possible continuation steps in move_index
	unsigned int	contTo		(const unsigned int t) const;				// last  index of possible continuation steps in move_index

	std::string	toString	() const;

	unsigned int	index		() const { return _index & 4095; };	// mask promotion flag

	bool operator<(const Move &m) const { return 0; } // dummy for sort, sort is done by a move value
	operator	unsigned int	() const { return _index; }
	Move&		operator++	() { ++_index; return *this; }

private:
	unsigned int	square		(std::string s);

	unsigned int	_index;
};

std::ostream& operator<<(std::ostream& s, const Move &m);

enum MoveDirections { WHITE_PRIVATE = 0, BLACK_PRIVATE = 1, OFFICER = 2 };

extern unsigned int doubleCheckMask(const unsigned int i);

#endif

