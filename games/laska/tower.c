
#include "move.h"
#include "tower.h"

#include <map>

#include <iostream>
#include <fstream>
#include <algorithm> // std::reverse

static 	int	val_private[16] = { 1000, 450, 200,  90,  60,  50, 40, 30, 20, 10, 10, 10, 10, 10, 10, 10 };
static 	int	val_officer[16] = { 2000, 900, 400, 180, 160, 100, 80, 60, 40, 20, 20, 20, 20, 20, 20, 20 };
static 	int	own_private[16] = { 1000, 450, 200,  90,  60,  50, 40, 30, 20, 10, 10, 10, 10, 10, 10, 10 };
static 	int	own_officer[16] = { 2000, 900, 400, 180, 160, 100, 80, 60, 40, 20, 20, 20, 20, 20, 20, 20 };

const unsigned long long count_mask = 1<<0 | 1<<3 | 1<<6 | 1<<9 | 1<<12 | 1<<15 | 1<<18 | 1<<21 | 1<<24 | 1<<27 | 1<<30 |
	1ULL<<33 | 1ULL<<36 | 1ULL<<39 | 1ULL<<42 | 1ULL<<45 | 1ULL<<48 | 1ULL<<51 | 1ULL<<54 | 1ULL<<57 | 1ULL<<60 | 1ULL<<63;

static std::map<Tower, unsigned long long> tower_count;
static std::map<unsigned int, int> tower_value;

void towerInit(std::string filename) {
	std::string s;
	int v;
	std::ifstream f(filename);
	while (f >> s >> v)
		tower_value[Tower(s).key()] = v;

	// for (auto p: tower_value) { std::cerr << p.first.toString() << " " << p.second << std::endl; }
}

Tower::Tower	(std::string	s) {
	int n = 0;
	data = 0;
	std::reverse(s.begin(), s.end());
	for(std::basic_string<char>::const_iterator i = s.begin(); i!= s.end(); i++) {
		switch (*i) {
		case 'o':	data |= (1 << n); n += 3; break;
		case 'O':	data |= (5 << n); n += 3; break;
		case 'x':	data |= (3 << n); n += 3; break;
		case 'X':	data |= (7 << n); n += 3; break;
		default:
			std::cerr << "What is " << (*i) << std::endl;
		}
	}

	value = evaluate();
}

Tower::Tower	(unsigned int d) : data(d) {
	value = evaluate();
}

void Tower::updateMaps() {
	// value = evaluate();
	value = tower_value[valueKey()];
/*	if (tower_count.count(key()) == 0) {
		tower_count[key()] = 1;
	} else {	
		tower_count[key()] += 1;
	}
	if (tower_value.count(key()) == 0) {
		tower_value[key()] = value;
	} */
}

unsigned int Tower::size	()			const { return (__builtin_popcountll (data & count_mask));	}
unsigned int Tower::color	()			const { return ((data >> 1) & 1);				}
bool	     Tower::isOfficer	()			const { return  (data & 4);					}
unsigned int Tower::color	(const unsigned int i)	const { return ((data & (1<<(i+i+i+1))) >> (i+i+i+1));		}
unsigned int Tower::isOfficer	(const unsigned int i)	const { return ((data & (1<<(i+i+i+2))) >> (i+i+i+2));		}
unsigned int Tower::moveType	()			const { return (isOfficer()) ? 2 : color();			}
const unsigned int Tower::top	()			const { return (data & 7) >> 1;					}	// for bitb
const unsigned int Tower::back	()			const { return (data >> (3*(size()-1)+1)) & 3;			}	// for setting in bitb


const unsigned int Tower::ownStones	() const	{
	unsigned long long color_mask = count_mask<<1;

	if (color()) {
		return __builtin_popcountll (data & color_mask); 
	} else {
		return size () - __builtin_popcountll (data & color_mask); // size minus black stones
	}
}

void Tower::push_back (const unsigned int stone) {		// put stone under tower
	data |= (((unsigned long long) stone) << 3*size());
	value = tower_value[valueKey()];
}

void Tower::push (const unsigned int stone) {			// put stone on top 
	data <<=3;
	data |= stone;
	value = tower_value[valueKey()];
}

unsigned int Tower::pop () {					// remove top stone, return top stone
	unsigned int s = (data & 7);
	data >>= 3;
	value = tower_value[valueKey()];

	return s;
}

unsigned int Tower::pop_back	() {				// remove bottom stone, return bottom stone
	unsigned int s = (data >> (3*(size()-1))) & 7;
	data &=	~(7ULL << (3*(size()-1)));
	value = tower_value[valueKey()];

	return s;
}

void Tower::promote () { data |=  4; value = tower_value[valueKey()]; }
void Tower::degrade () { data &= ~4; value = tower_value[valueKey()]; }

void Tower::changeColor	() {
	unsigned long long color_mask = count_mask<<1;
	unsigned long long    on_mask = ((1ULL<<(size()*3+1))-1) & color_mask;

	data ^= on_mask;
}

void Tower::setConfigValues	(const int *v) {
	for (unsigned int i = 0; i<16; i++) {
		val_private[i] = v[i];
		val_officer[i] = v[i+16];
	}
}

void Tower::printMaps	() const {
return;
	std::cerr << "tower counter:" << std::endl;
	for (auto p: tower_count) { std::cerr << p.first.toString() << " = " << p.second << std::endl; }
	std::cerr << "tower values:" << std::endl;
	for (auto p: tower_value) { std::cerr << Tower(p.first).toString() << " = " << p.second << std::endl; }
}

int Tower::getConfigValue	(const int v) const {
	if (v < 16) {
		return val_private[v];
	} else if (v < 32) {
		return val_officer[v-16];
	} else {
		std::cerr << "Tower::getConfigValue (" << v << "): index out of range. (0..31)" << std::endl;
	}

	return 0; 	//unreached
}

std::string Tower::toString	() const {
	std::string r;
	unsigned long long d = data;

	while (int s = d&7) {
		switch 	(s) {
			case 1: r = "o" + r; break;
			case 5: r = "O" + r; break;
			case 3: r = "x" + r; break;
			case 7: r = "X" + r; break;
		}
		d >>= 3;
	}

	return r;
}

int Tower::evaluate() {
	value = tower_value[valueKey()];
	return value;
	// return  tower_value[valueKey()];

	int v = 0;
	
	unsigned int n = 0;
	unsigned int o = ownStones();
	for (unsigned long long d = data; d; d >>=3) {	// stones from top to bottom (top stone is in lowest bits)
		v += (d & 4) ? val_officer[n] : val_private[n]; 
		if (n<=o && size() > 1) 
		v += (d & 4) ? own_officer[n] : own_private[n]; 
		n++;
	}
	return v;
}
