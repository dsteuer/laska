
#ifndef __POSITION_H
#define __POSITION_H

#include <list>
#include <vector>
#include <map>
#include <stack>

#include "move.h"
#include "tower.h"
#include "bitb.h"

#include "../../tt.h"

enum GameValue	{ MaxPly = 1024, MaxValue = 65535 };					// max short

extern void initializeGame (const unsigned int board_size);

class HashStack : public std::stack<unsigned int> {
public:
	using std::stack<unsigned int>::c;
};


class Position {
public:
	Position (const unsigned int board_size = 7);
	Position (std::string	pos, const unsigned int board_size = 7);

	void		init		(const unsigned int board_size = 7);
	void		setTurn		(const unsigned int t)	{ turn = t;	value = (turn) ? -value : value; 	  }	// TODO: set sign correctly
	void		switchTurn	();
	std::string	toString	(const unsigned int style = 0) const;
	unsigned int	getTurn		() const { return turn;		}
	unsigned int	getValue	() const { return value;	}
	BitBoard	getBitBoard	() const { return bitb;		}
	Tower		getTower	(const unsigned int i) const { return bitb.tower[i];		}
	bool		isLegal		(const Move m);
	virtual std::list<Move>	possibleMoves	();
	virtual std::list<Move>	possibleJumps	();	// for quiescence
	virtual std::list<Move>	possibleSteps	();

	virtual void	move		(Move &m);
	virtual void	moveRecorded	(Move &m);
	virtual void	undo		();
	virtual void	undo		(const Move m);

	int		evaluatePosition() const;
	int		maxValue	() const			 { return MaxValue;		}
	int		mateValue	() const			 { return MaxValue;		}
	int		mateValue	(const unsigned int depth) const { return MaxValue-depth;	}	// mate in depth plies

	void		applyMoves	(std::string str_moves);
	void		setConfigValues	(const int *v);
	int		getConfigValue	(const int v);

	const unsigned long long key	() const;		// just return position hash key
	unsigned long long	generateKey	();		// generate hash key
	void		setTranspositionTable (TranspositionTable* t);
	void		readConfig	(const std::string file_name = "cfg.txt");
	void		writeConfig	(const std::string file_name = "cfg2.txt");
	void		print		(unsigned int style = 0, std::ostream &out = std::cout);
	void		printMovelist	(std::list<Move> l, bool new_line = true, std::ostream &out = std::cout) const;
	
	void		mirror		();

protected:
	int		evaluateTowers	() const;
	int		evaluateSquares	() const;
	int		evaluateTriangles() const;

	void		makeTowerKeys	(const unsigned int tower_type);
	void		checkTowerKeys	(const Move m);
	void		updateHashKey	(const Move m, const unsigned int square);
	int		moveValue	(const Move m, const unsigned int square) const;

	bool		isRepetition	() const;

	void		setPosition	(std::string pos);
	std::string	moveValueString	(const Move m, const int v) const;

	BitBoard	bitb;		// and a bitboard of theirs tops (and the empty squares)
	unsigned int	turn;
	int		value;
	std::list<Move>	move_history;
	std::list<Move>	command_line_moves;
	unsigned long long	pos_stack[MaxPly];
	Move		move_stack[MaxPly];

	// define anyway for assert (or else #idef asserts)
	unsigned long long	pos_key;	// current key

#ifdef USE_POSITION_KEY
	unsigned long long	turn_key;	// key for color to move
	// a tower is stored in an int. which is mapped to a list of 25 keys, one for each square, for each type of tower in the game
	std::map<unsigned int, std::vector<unsigned long long> >	tower_keys;	// keys for tower on squares

	TranspositionTable*	tt;
#endif
};

#endif
