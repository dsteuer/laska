
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <utility>
#include <bitset>

// #define NDEBUG
#include <assert.h>

#include "position.h"
#include "rkiss.h"
// #include "nn.h"

static unsigned int squares;			// for odd board_size a laska board has ((board_size*board_size)+1)/2; squares, see init()

void	Position::setTranspositionTable (TranspositionTable* t) {
#ifdef USE_POSITION_KEY
	tt = t;
#endif
}

const unsigned long long Position::key () const {
#ifdef USE_POSITION_KEY
	return pos_key;
#else
	return 0ULL;
#endif
} 

unsigned long long Position::generateKey () {	// generate key
#ifdef USE_POSITION_KEY
	unsigned long long r = 0ULL;
	for(unsigned int i=0; i<squares; i++) {
		if (bitb.tower[i].empty()) { continue; }
		if (tower_keys.count(bitb.tower[i].key()) == 0) {
			makeTowerKeys (bitb.tower[i].key());
		}
		// std::map<unsigned int, std::vector<unsigned long long> >::const_iterator ki = tower_keys.find(tower[i].key());
		r ^= tower_keys[bitb.tower[i].key()][i];
	}

	if (turn) {
		r ^= turn_key;
	}
	return r;
#else
	return 0ULL;
#endif
}

// static PRNG rk(1);
static PRNG rk(time(0));

void Position::makeTowerKeys (const unsigned int tower_type) {
#ifdef USE_POSITION_KEY
	std::vector<unsigned long long> new_keys(squares);

	for (unsigned int i = 0; i< squares; i++) {
		new_keys[i] = rk.rand<unsigned long long>();
	}
	tower_keys[tower_type] = new_keys;
#endif
}

void Position::checkTowerKeys (const Move m) {			// add (eventually new) keys of towers involved in move to pos_key
								// tower has to be on the to() square of move
#ifdef USE_POSITION_KEY
	if (!tower_keys.count(bitb.tower[m.to()].key())) {
		makeTowerKeys (bitb.tower[m.to()].key());
	}

	if (Move(m).isJump()) {					// on jump only
		for (unsigned int j = 0; j<m.size(); j++) {
			unsigned int over = m.over(j);
			if (bitb.tower[over].size()) {
				if (!tower_keys.count(bitb.tower[over].key())) {	// happens on some undo conditions
					makeTowerKeys (bitb.tower[over].key());
				}
			}
		}
	}
#endif
}

void Position::updateHashKey (const Move m, const unsigned int square) {	// remove keys of towers involved in move from pos_key, square = tower that moves is currently on
#ifdef USE_POSITION_KEY
	pos_key ^= tower_keys.find(bitb.tower[square].key())->second[square];

	if (m.isJump()) {	// on jump only
		for (unsigned int j = m.overMask(); j; j &= (j-1)) {		// for movelength > 4, ensure each key is removed only once.
			unsigned int over = __builtin_ffs(j) - 1;
			if (bitb.tower[over].size()) {
				assert (tower_keys.count(bitb.tower[over].key()));
				pos_key ^= tower_keys.find(bitb.tower[over].key())->second[over];
			}
		}
	}
#endif
}

void Position::init (const unsigned int board_size) {				// same for all constructors
	squares	 = ((board_size*board_size)+1)/2;				// count squares where towers might move
	value    = bitb.evaluateTowers((Color) turn);
#ifdef USE_POSITION_KEY
	tower_keys.clear();
	turn_key = rk.rand<unsigned long long>();
	pos_key = generateKey();	// init pos_key
#endif
}

Position::Position (const unsigned int board_size) : bitb(board_size)  {	// default position, unused. correct only for 7x7.
	turn = 0;
	for(unsigned int i=0; i<11; i++){ 
		bitb.tower[   i] = Tower("o");
		bitb.tower[24-i] = Tower("x");
	}

	init (board_size);
}

Position::Position (std::string pos, const unsigned int board_size) : bitb(board_size)  {	// std. constructor used
	setPosition (pos);
	init (board_size);
}

void Position::setPosition (std::string pos) {							// convert string to internal representation
	turn = 0;										// clear relevant data
	bitb.clear();
	for(unsigned int i=0; i<squares; i++) {
		bitb.tower[i].clear();
	}

	if (pos != "") {									// parse string
		unsigned int c = 0;
		unsigned int start = 0;
		if (pos[0] == '|' || pos[0] == ' ' || pos[0] == '.') { start = 1; c = 1;}	// first square empty ?
		std::string s = "";
		for (unsigned int i = start; i < pos.length(); ++i) {
			if (pos[i] == '|' || pos[i] == ' ' || pos[i] == '.') {
				if (s.length()) {
					bitb.setTower (c, Tower(s));
					s = "";
				}
				c ++;
				if (c>24) {							// # squares not jet known :/ todo
					std::cerr << "Position: \"" << pos << "\" has more than " << squares << " squares " << std::endl;
					break;
				}
			} else {
				s += pos[i];
			}
		}
		if (s.length()) {								// last square not empty ?
			bitb.setTower (c, Tower(s));
		}
	}
}

std::string	Position::moveValueString	(const Move m, const int v) const {					// format move/value pair
	std::string r = (v >  mateValue()-100) ? std::string("M") + std::to_string( mateValue()-v) :			// we never get a mate(d) in plies > 100 ?
			(v < -mateValue()+100) ? std::string("M") + std::to_string(-mateValue()-v) : std::to_string(v);

	if (r.length() < 6) r.insert(r.begin(), 6 - r.length(), ' ');

	return m.toString() + "  " + r;
}

void Position::applyMoves (std::string str_moves) {									// set list of moves given in string
	if (str_moves == "") { return; }

	// print();

	std::istringstream iss(str_moves);
	std::string single_move_str;
	bool first = true;
	while (iss >> single_move_str) {
		// std::cerr << single_move_str << std::endl;
		if (single_move_str.size() == 46) { continue; }	// a position
		if (single_move_str.size() == 28 && single_move_str[2] != 'x') { continue; }	// a hex position

		Move m;
		if (single_move_str.size() > 2 && (single_move_str[2] == 'x' || single_move_str[2] == '-')) {
			m = Move (single_move_str);
		} else {
			std::istringstream iss2(single_move_str);
			int ind;
			iss2 >> std::hex >> ind;
			m = Move (ind);
		}
		if (first) {
			turn = bitb.color(m.from());
			if (turn == 2) {
				std::cerr << single_move_str << ": this move list does not apply to this position, i fear. i better do nothing with this moves ... " << std::endl;
				return;
			}
			first = false;
		}
		if (!isLegal(m.index())) {
			std::cerr << "looks like " << m.toString() << " is not a legal move here. i better do nothing with this moves ... " << std::endl;
			return;
		}
		moveRecorded(m);
		command_line_moves.emplace_back(m);
#ifdef USE_POSITION_KEY
		// std::cerr << "Position store " << key() << ' ' << m << "\n";
	//	tt->store (key(), m, turn, 100, 0);
#endif
	 	// print();
	}
	// print(1);
}

void Position::readConfig	(const std::string file_name) {
	bitb.readConfig (file_name);
	value = bitb.evaluateTowers((Color) turn);	// config values might differ from defaults, so re-evaluate. (can't be done at init() currently)
}
void Position::writeConfig	(const std::string file_name) {
	bitb.writeConfig (file_name);
}

void Position::print(unsigned int style, std::ostream &out) {
	out << toString(style);

	if (!style && !move_history.empty()) {	// TODO; find color of startmove, so white moves are always on the left. (needs bitboard?)
		unsigned int j = 1;
		for (const auto& m: move_history) {
			if (move_history.size() - j >20) { j++; continue; }
			if (j%2) { out << std::setw(2) << (j/2 + 1) << ". "; }
			// out << m.toString() << ' ';
			out << m;
			if (j%2) { out << " "; } else { out << std::endl; }
			++j;
		}
	}
	out << std::endl;
}

void Position::printMovelist (std::list<Move> l, bool new_line, std::ostream &out) const {
	for (const auto& m: l) { out << m << ' '; }
	if (new_line) { out << std::endl; }
}

void Position::mirror () {
	// 0 -> 24
	// 1 -> 23
	// ...
	// 13
	for (unsigned int i = 0; i<12; i++) {
		std::swap(bitb.tower[i], bitb.tower[24-i]);
	}
	for (unsigned int i = 0; i<25; i++) {
		bitb.tower[i].changeColor();
	}
}

std::string pre  (const char c, const unsigned int w, const char n = ' ') {
	std::string s = "";
	for (unsigned int i=0; i< w/2;   i++) { s += c; }
	s += n;
	for (unsigned int i=0; i< w/2-1; i++) { s += c; }
	return s;
}

std::string post (const char c = '|') {
	std::string s = "";

	s += c;
	s += "\n";

	return s;
}

std::string topLine(const unsigned int w) {
	std::string s = pre (' ', 8);
	for (unsigned int i=0; i< 8*w-1; i++) { s += '_'; }
	s += '\n';

	return s;
}

std::string piece (const std::string t, const unsigned int w) {
	std::string s = "|" + t;
	for (unsigned int j=s.length(); j<=w; j++) { s += ' '; }
	return s;
}

std::string part (const char c, const unsigned int w) {
	std::string s = "|";
	for (unsigned int i=0; i< w; i++) { s += c; }
	return s;
}

std::string letter (const unsigned int w, const char c) {
	std::string s = "";
	for (unsigned int i=0; i< w/2+1; i++) { s += ' '; }
	s += c;
	for (unsigned int i=0; i< w/2;   i++) { s += ' '; }
	return s;
}

unsigned int pieceIndex (const unsigned int board_size, const unsigned int n) {
	if (board_size == 7) {
		unsigned int pieceindex[] = { 21,22,23,24,18,19,20,14,15,16,17,11,12,13,7,8,9,10,4,5,6,0,1,2,3 };
		return pieceindex[n];
	} else if (board_size == 5) {
		unsigned int pieceindex[] = { 10,11,12,8,9,5,6,7,3,4,0,1,2 };
		return pieceindex[n];
	} else if (board_size == 3) {
		unsigned int pieceindex[] = { 3,4,2,0,1 };
		return pieceindex[n];
	}

	return 0;
}
std::string Position::toString(unsigned int style) const {
	if (style == 3) {
		std::stringstream stream;
		stream << std::setfill ('0') << std::setw(7) << std::hex << bitb.bits[0];
		stream << std::setfill ('0') << std::setw(7) << std::hex << bitb.bits[1];
		stream << std::setfill ('0') << std::setw(7) << std::hex << bitb.bits[2];
		stream << std::setfill ('0') << std::setw(7) << std::hex << bitb.bits[3];
		return stream.str();
	}
	std::string s[squares];
	std::string t = "";
	for(unsigned int j=0; j<squares; j++) { s[j] =  bitb.tower[j].toString(); }

	if (style == 2) {
		unsigned int last = 0;
		for (unsigned int i  = bitb.bits[0]; i; i &= (i - 1)) {
			unsigned int at = __builtin_ffs(i)-1;
			if (at && at-last>1) {
				unsigned int d = at-last;
				if (last) d--;
				t += std::to_string(d);
			} else {
				if (at) t+=".";
			}
			t += s[at];
			last = at;
		}
		if (24-last)
		t += std::to_string(24-last);
		std::bitset<361> bs;
		int bsi = 0;		
		for (unsigned int i=0; i<t.length(); i++) {
			char cc = t[i];
			for (int j=0; j<8; j++) {
				if (cc&(1<<j))
					bs.set(bsi);
				bsi++;
			}
		}
		std::cerr << bs << '\n';

		t += "\n";
		last = 0;
		for (unsigned int i  = bitb.bits[1]; i; i &= (i - 1)) {
			unsigned int at = __builtin_ffs(i)-1;
			if (at && at-last>1) {
				unsigned int d = at-last;
				if(last) d--;
				t += std::to_string(d);
			} else {
				if (at) t+=".";
			}
			t += s[at];
			last = at;
		}
		if (24-last)
		t += std::to_string(24-last);
		t += "\n";
		return t;
	}
	if (style) {		// return compact string
		std::vector<unsigned int> ranks;
		     if (squares == 25) ranks = { 0, 3, 6, 10, 13, 17, 20 };
		else if (squares == 13) ranks = { 0, 2, 4, 7, 9 };
		else if (squares ==  5) ranks = { 0, 1, 2 };
		unsigned int r = 1;
		for(unsigned int i=0; i<squares-1; i++) {
			t += s[i];
			if (ranks[r] == i) {
				t+= '|';
				r++;
			} else {
				t += '.';
			}
		}
		t += s[squares-1];

		// t += " " + std::to_string(bitb.bits[2]) + " " + std::to_string(bitb.bits[3]);
		return t;
	}

	for(unsigned int i=0; i<squares; i++) {		// fill strings with blanks. string class most likely has a better way?
		for (int j=s[i].length(); j<7; j++) { s[i] += ' '; }
	}
	char rno[] = "123456789";
	char fno[] = "ABCDEFGHI";
	unsigned int bw = bitb.board_size;
	unsigned int sh = 3;
	t = topLine(bw);
	
	unsigned int pi = 0;
	for (unsigned int l=0; l< bw; l++) {
	for (unsigned int j=0; j< sh; j++) {
		char c = ' ';
		if (j == 1) { c = rno[bw-l-1]; }
		t += pre (' ', 7, c);
		for (unsigned int i=0; i< bw; i++) {
			const char c[] = { ' ', '_' };
			if (j == 2 && (l+i)%2 == 0) {
				t += piece (s[pieceIndex(bw, pi)], 7);
				pi++;
			} else {
				t += part (c[(l+i)%2], 7);
			}
		}
		t += post ();
	}
	t += pre (' ', 7);
	for (unsigned int i=0; i< bw; i++) {
		t += part ('_', 7);
	}
	t += post ();
	}
	t += post (' ');
	t += pre (' ', 7);
	for (unsigned int i=0; i< bw; i++) {
		t += letter (7, fno[i]);
	}
	t += post (' ');
/*
	if (squares == 25) {
		t += "        _______________________________________________________\n";
		t += "       |       |_______|       |_______|       |_______|       |\n";
		t += "  7    |       |_______|       |_______|       |_______|       |\n";
		t += "       |" + s[21] + "|_______|" + s[22] + "|_______|" + s[23] + "|_______|" + s[24] + "|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "       |_______|       |_______|       |_______|       |_______|\n";
		t += "  6    |_______|       |_______|       |_______|       |_______|\n";
		t += "       |_______|" + s[18] + "|_______|" + s[19] + "|_______|" + s[20] + "|_______|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "       |       |_______|       |_______|       |_______|       |\n";
		t += "  5    |       |_______|       |_______|       |_______|       |\n";
		t += "       |" + s[14] + "|_______|" + s[15] + "|_______|" + s[16] + "|_______|" + s[17] + "|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "       |_______|       |_______|       |_______|       |_______|\n";
		t += "  4    |_______|       |_______|       |_______|       |_______|\n";
		t += "       |_______|" + s[11] + "|_______|" + s[12] + "|_______|" + s[13] + "|_______|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "       |       |_______|       |_______|       |_______|       |\n";
		t += "  3    |       |_______|       |_______|       |_______|       |\n";
		t += "       |" + s[7] + "|_______|" + s[8] + "|_______|" + s[9] + "|_______|" + s[10] + "|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "       |_______|       |_______|       |_______|       |_______|\n";
		t += "  2    |_______|       |_______|       |_______|       |_______|\n";
		t += "       |_______|" + s[4] + "|_______|" + s[5] + "|_______|" + s[6] + "|_______|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "       |       |_______|       |_______|       |_______|       |\n";
		t += "  1    |       |_______|       |_______|       |_______|       |\n";
		t += "       |" + s[0] + "|_______|" + s[1] + "|_______|" + s[2] + "|_______|" + s[3] + "|\n";
		t += "       |_______|_______|_______|_______|_______|_______|_______|\n";
		t += "\n";
		t += "           A       B       C       D       E       F       G\n";
	} else if (squares == 13) {
		t += "        _______________________________________\n";
		t += "  5    |       |_______|       |_______|       |\n";
		t += "       |" + s[10] + "|_______|" + s[11] + "|_______|" + s[12] + "|\n";
		t += "       |_______|_______|_______|_______|_______|\n";
		t += "       |_______|       |_______|       |_______|\n";
		t += "  4    |_______|       |_______|       |_______|\n";
		t += "       |_______|" + s[8] + "|_______|" + s[9]  + "|_______|\n";
		t += "       |_______|_______|_______|_______|_______|\n";
		t += "       |       |_______|       |_______|       |\n";
		t += "  3    |       |_______|       |_______|       |\n";
		t += "       |" + s[5] + "|_______|" + s[6] + "|_______|" + s[7] + "|\n";
		t += "       |_______|_______|_______|_______|_______|\n";
		t += "       |_______|       |_______|       |_______|\n";
		t += "  2    |_______|       |_______|       |_______|\n";
		t += "       |_______|" + s[3] + "|_______|" + s[4] + "|_______|\n";
		t += "       |_______|_______|_______|_______|_______|\n";
		t += "       |       |_______|       |_______|       |\n";
		t += "  1    |       |_______|       |_______|       |\n";
		t += "       |" + s[0] + "|_______|" + s[1] + "|_______|" + s[2] + "|\n";
		t += "       |_______|_______|_______|_______|_______|\n";
		t += "\n";
		t += "           A       B       C       D       E\n";
	} else if (squares == 5) {
		t += "        _______________________\n";
		t += "       |       |_______|       |\n";
		t += "  3    |       |_______|       |\n";
		t += "       |" + s[3] + "|_______|" + s[4] + "|\n";
		t += "       |_______|_______|_______|\n";
		t += "       |_______|       |_______|\n";
		t += "  2    |_______|       |_______|\n";
		t += "       |_______|" + s[2] + "|_______|\n";
		t += "       |_______|_______|_______|\n";
		t += "       |       |_______|       |\n";
		t += "  1    |       |_______|       |\n";
		t += "       |" + s[0] + "|_______|" + s[1] + "|\n";
		t += "       |_______|_______|_______|\n";
		t += "\n";
		t += "           A       B       C\n";
	}
 */

	return t;
}

bool Position::isLegal(const Move m) {					// for applyMoves and isPermutation
	if (bitb.collides(m.stepMask())) { return false; }		// road blocked
	if (!(bitb.bits[turn]	& (1<<m.from())))		 { return false; }	// tower has not color of turn (only for permutation checks)
	if (m.isStep()) {
	  if (bitb.hasJumps(turn))    { return false; }
	} else {	// a jump
		if ((bitb.isNotOccupied	(m.overMask(), 1^turn)))	 { return false; }	// square this move will jump over is empty

		return bitb.longJumpsCheck(m);		// if last hurdle ok, everything is ok
	}

	return true;
}

std::list<Move> Position::possibleSteps() {
	std::list<Move> l = bitb.hashMoves((Color) turn, Step, bitb.bits[turn], bitb.bits[turn+2], bitb.bits[1^turn], bitb.both());
	return l;

	//	return bitb.generateMoves((Color) turn, Step);
}

std::list<Move>	Position::possibleJumps() {
	std::list<Move> l = bitb.hashMoves((Color) turn, Jump, bitb.bits[turn], bitb.bits[turn+2], bitb.bits[1^turn], bitb.both());
	return l;

	//	return bitb.generateMoves((Color) turn, Jump);
}

std::list<Move>	Position::possibleMoves() {
	std::list<Move> l = possibleJumps ();

	return  (l.empty()) ? possibleSteps() : l;
}

int Position::moveValue(const Move m, const unsigned int square) const {	// value difference of a move / undo
#ifdef NOEVAL
	return 0;
#endif
	int v  = bitb.evaluateTower(square);
	v += bitb.adjacent((Color) turn);

	if (m.isJump()) {
		for (unsigned int j = m.overMask(); j; j &= (j-1)) {		// for movelength > 4, ensure each key is removed only once.
			unsigned int over = __builtin_ffs(j) - 1;
			if (bitb.tower[over].size()) {
				if (bitb.tower[over].color() == turn) {	// color switch
					v += bitb.evaluateTower(over);
				} else {
					v -= bitb.evaluateTower(over);
				}
			}
		}
	}
	return v;
}

void Position::switchTurn () {
	turn  = 1^turn;
	value = -value;
#ifdef USE_POSITION_KEY
	pos_key ^= turn_key;
#endif
}

void Position::moveRecorded(Move &m) {	// move and add move to history
	move(m);
	move_history.emplace_back(m);
}

void Position::move(Move &m) {
	// std::cerr << "Position::move(" << m.toString() << ");" << std::endl;

	updateHashKey		(m, m.from());				// needs to know square where moving tower is, i.e., pre or post move

	value -= moveValue	(m, m.from());				// "
	bitb.move(m, turn);
	value += moveValue	(m, m.to());				// "

	switchTurn();							// order: after updateHashKey and moveValue

	checkTowerKeys		(m);					// beside constructor only place where hash keys for a tower may not exist
	updateHashKey		(m, m.to());				// needs to know square where moving tower is, i.e., pre or post move

	assert(pos_key == generateKey());
	assert(value == bitb.evaluateTowers((Color) turn));
}

void Position::undo() {				// remove last move in history, cut history
	if (move_history.size() == 0) return;

	undo(move_history.back());

	move_history.pop_back();
}

void Position::undo(const Move m) {
	// std::cerr << "Position::undo(" << m.toString() << ");" << std::endl;

	updateHashKey		(m, m.to());
	switchTurn();

	value -= moveValue	(m, m.to());
	bitb.undo (m, turn);
	value += moveValue	(m, m.from());

	updateHashKey		(m, m.from());

	assert(pos_key == generateKey());
	assert(value == bitb.evaluateTowers((Color) turn));
}

int Position::evaluatePosition() const {
	return value;
}

// global stuff, per session init

extern void initMoveIndex (const unsigned int board_size);
extern void towerInit	  (const unsigned int board_size);

void initializeGame (const unsigned int board_size) {
	towerInit("towervalues.ini");
	initMoveIndex (board_size);
}

