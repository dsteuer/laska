
#ifndef __TOWER_H
#define __TOWER_H

#include <cctype>

#include <list>
#include <string>

// for speedup of hashes, a bitbased tower
class Tower {
public:
	Tower	() : data(0), value(0) {}
	Tower	(std::string	s);
	Tower	(unsigned int d);

	unsigned int	size		() const;
	bool		empty		() const		{ return (data == 0) ? true : false; }
	unsigned int	color		() const;
	void		clear		()			{ data = 0; value = 0; };
	bool		isOfficer	() const;
	unsigned int	moveType	() const;
	const unsigned int	back		() const;
	const unsigned int	top		() const;
	const unsigned int	ownStones	() const;
	// for move
	void		promote		();
	void		push_back	(const unsigned int stone);
	unsigned int	pop		();
	// for undo
	void		push		(const unsigned int stone);
	unsigned int	pop_back	();
	void		degrade		();

	void		changeColor	();						// swap stones color

	unsigned int	key		() const		{ return data;	};	// for position hashing
	unsigned int	valueKey	() const		{ return data & 32767;	};	// to get value from values map, reduced to top 4 (or 5) stones, 3 bits per stone: 2^12-1 = 4095 (or 2^15-1 = 32767)

	int		getValue	() const		{ return value; }
	int		setValue        ()                      { updateMaps(); /* value = evaluate(); */ return value; } ;
	void		setConfigValues	(const int *v);
	int		getConfigValue	(const int  v) const;
	void		printMaps	() const;
	std::string	toString	() const;
	operator	unsigned int	() const		{ return data;	};
	operator	int		() const		{ return getValue();	};
	bool		operator<	(const Tower &t) const	{ return key() < t.key();	};
private:
	int		evaluate	();	// for position evaluation
	void		updateMaps	();

	unsigned long long	data;
	int			value;

	unsigned int	color		(const unsigned int i) const;
	unsigned int	isOfficer	(const unsigned int i) const;
};

extern void towerInit(std::string filename);

#endif
