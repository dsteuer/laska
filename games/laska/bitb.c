
#include <iostream>
#include <iomanip>
#include <fstream>

#include <algorithm>
#include <map>

// #define NDEBUG
#include <assert.h>

#include "bitb.h"
#include "over.h"

/*
  an n x n (n=(2m+1)) laska board has (n^2+1)/2 squares
  at start position there are (n-1)^2 + foor(n/2) squares to fill with white and black stones.
  whites first stone is at square 0, blacks at square (n^2+1)/2 - (n-1)^2 + foor(n/2)
  rows are filled i=0..(n-1)/2 alternating with (n+1)/2, (n-1)/2 ... stones.

 n    white rows  stones
 3          2      2 =  1 + 1 
 5         3 2      5 =  4 + 1 
 7        4 3 4     11 =  9 + 2  
 9       5 4 5 4     18 = 16 + 2
11      6 5 6 5 6     28 = 25 + 3  
13     7 6 7 6 7 6     39 = 36 + 3
15    8 7 8 7 8 7 8     53 = 49 + 4
17   9 8 9 8 9 8 9 8     68 = 64 + 4
 */

static unsigned int numberOfStonesOfPlayer	(unsigned int n) { assert(n<8&&n%2==1); return ((n-1)/2)*((n-1)/2) + ((n-1)/2+1)/2; }
static unsigned int boardSquares		(unsigned int n) { return ((2*((n-1)/2)+1)*(2*((n-1)/2)+1)+1)/2; } 
static unsigned int bitsToShift			(unsigned int n) { return boardSquares(n)-numberOfStonesOfPlayer(n); } 

static unsigned int bitsToSetWhite(unsigned int n) { return (1<<((numberOfStonesOfPlayer(n))-1)); }
static unsigned int bitsToSetBlack(unsigned int n) { return (bitsToSetWhite(n))<<(bitsToShift(n)); }

static unsigned int squares = 25;		

static unsigned int skip[2][4] = {				// [Step|Jump][direction], offsets in moveindex
	{ 25*0,       25*1,       25*3,       25*2       },
	{ 25*0 + 100, 25*1 + 100, 25*3 + 100, 25*2 + 100 }
};

// hashes for jump and step move lookup for given position. see hashMoves() function
// p: upward, m: downward. 0: even rank, 1: odd rank. [rank][turn bits][other bits]
std::list<Move> s_p0[3][16][ 8];
std::list<Move> s_p1[3][ 8][16];
std::list<Move> s_m0[3][16][ 8];
std::list<Move> s_m1[3][ 8][16];

std::list<Move> j_p0[3][16][ 8][16];
std::list<Move> j_p1[2][ 8][16][ 8];
std::list<Move> j_m0[3][16][ 8][16];
std::list<Move> j_m1[2][ 8][16][ 8];

BitBoard::BitBoard(const unsigned int boardsize) {
	board_size = boardsize;
	bits[0] = bitsToSetWhite(board_size);
	bits[1] = bitsToSetBlack(board_size);
	bits[2] = 0;
	bits[3] = 0;

	for(unsigned int i=0; i<numberOfStonesOfPlayer(board_size); i++){ 
		tower[   i] = Tower("o");
		tower[boardSquares(board_size)-i] = Tower("x");
	}

	squares = boardSquares(board_size);			// squares of board_size

	for (unsigned int i=0; i<4; i++) {
		skip[0][i] = squares * i;			// index has 4*squares step move entries, ordered by direction. skip entries of "first" directions
		skip[1][i] = squares * i + 4 * squares;		// as many move entries, as well ordered by direction after step moves. skip all step moves and entries of "first" directions
	}
}

void BitBoard::clear () {
	bits[0] = 0;
	bits[1] = 0;
	bits[2] = 0;
	bits[3] = 0;

	for(unsigned int i=0; i<25; i++){ 
		tower[i].clear ();
	}
}

void BitBoard::move(Move &m, const unsigned int turn) {
	// std::cerr << "BitBoard::move(" << m << ", " << turn << ' ' << m.isPromotion() << ")\n";
	move(m.from(), m.to(), turn);			// set bits
	std::swap(tower[m.to()], tower[m.from()]);	// move tower

	if (m.isOnPromotionSquare() && !tower[m.to()].isOfficer()) {	// move promotes (don't use m.isPromotion (), moves from command line can't know)
		tower[m.to()].promote();
		promote (m.to(), turn);
		m.setPromotion();					// the only place, where a promotion is actually checked and set. (generate moves knows by moveindex.)
	}

	if (m.isJump()) {
		unsigned int other = !turn;
		for (unsigned int j = 0; j<m.size(); j++) {		// captures
			unsigned int over = m.over(j);
			const bool iso = tower[over].isOfficer();
			tower[m.to()].push_back(tower[over].pop());	// add captured stone to turns tower
			if      ( iso && !tower[over].isOfficer()) degrade (over, other);
			else if (!iso &&  tower[over].isOfficer()) promote (over, other);
			if (tower[over].empty()) {			// last stone of tower captured
				erase(over, other);
			} else if (tower[over].color() == turn) {	// color switch
				switchColor(over, turn);
			}						// if color remains same after hit, nothing to do.
		}
	}
}

void BitBoard::undo(const Move m, const unsigned int turn) {
	move(m.to(), m.from(), turn);			// set bits
	std::swap(tower[m.to()], tower[m.from()]);	// move tower

	if (m.isPromotion()) {
		tower[m.from()].degrade();
		degrade (m.from(), turn);
	}

	if (m.isJump()) {
		unsigned int other = !turn;
		for (unsigned int j = m.size(); j; j--) {	// cause unsigned
			unsigned int over = m.over(j-1);	// substract here
			if (tower[over].empty()) {			// last stone of tower was captured
				set(over, tower[m.from()].back());
			} else if (tower[over].color() == turn) {	// color change
				switchColor(over, other);
			}
			const bool iso = tower[over].isOfficer();
			tower[over].push(tower[m.from()].pop_back());	// restore stone on top of tower
			if      ( iso && !tower[over].isOfficer()) degrade (over, other);
			else if (!iso &&  tower[over].isOfficer()) promote (over, other);
		}
	}
}

void BitBoard::move(const unsigned int from, const unsigned int to, const unsigned int color) {
	assert(bits[color] & (1<<from));

	bits[color] ^= (1<<from)^(1<<to);

	if (bits[color+2] & ((1<<from))) {
		bits[color+2] ^= (1<<from)^(1<<to);
	}
}

void BitBoard::switchColor(const unsigned int at, const unsigned int color) {
	bits[color] |=  (1<<at);
	if (bits[(1^color)+2] & (1<<at)) bits[color+2] |= (1<<at);
	erase(at, !color);
}

void BitBoard::erase (const unsigned int at, const unsigned int color) {
	bits[color  ]  &= ~(1<<at);
	degrade (at, color);
}

void BitBoard::degrade (const unsigned int at, const unsigned int color) {
	bits[color+2]  &= ~(1<<at);
}

void BitBoard::set   (const unsigned int at, const unsigned int color) {
	bits[color&1] |= (1<<at);
	if(color&2)	// officer
		bits[(color&1)+2] |= (1<<at);
}

void BitBoard::setTower   (const unsigned int at, const Tower t) {
	tower[at] = t;

	unsigned int color = tower[at].color();

	bits[color&1] |= (1<<at);
	if(tower[at].isOfficer())
		bits[(color&1)+2] |= (1<<at);
}

void BitBoard::setGrade (const unsigned int at, const unsigned int color, const unsigned int grade) {
	bits[color+2] &= (~(1<<at)) | (grade<<at);
}

void BitBoard::promote (const unsigned int at, const unsigned int color) {
	bits[color+2] |= (1<<at);
}

bool BitBoard::collides		(const unsigned int move)				const { return (both()        & move); }
bool BitBoard::isNotOccupied	(const unsigned int squares, const unsigned int color)	const { return ((bits[color]  & squares) != squares); }

unsigned int  BitBoard::color		(const unsigned int square)	const {
	return	(bits[0] & (1<<square)) ? 0 :
		(bits[1] & (1<<square)) ? 1 : 2;
}

const bool BitBoard::isOfficer		(const unsigned int square, const unsigned int color)	const {
	return	(bits[color+2] & (1<<square)) ? true : false;
}

const unsigned int BitBoard::both	() const { return (bits[0] | bits[1]); }
const unsigned int BitBoard::all	(const unsigned int color) const { return bits[color];			}	// all (top stone color of) pieces/towers of color
const unsigned int BitBoard::privates	(const unsigned int color) const { return bits[color] & ~bits[color+2]; }	// all, but not officer
const unsigned int BitBoard::officers	(const unsigned int color) const { return		 bits[color+2]; }

const unsigned int ranks[][7] = {
	{
		((1<< 0)|(1<< 1)),				// board bits of ranks, 3x3 board
		((1<< 2)        ),
		((1<< 0)|(1<< 1))  <<  3,
	}, {
		((1<< 0)|(1<< 1)|(1<< 2)),			// board bits of ranks, 5x5 board
		((1<< 3)|(1<< 4)        ),
		((1<< 0)|(1<< 1)|(1<< 2))  <<  5,
		((1<< 3)|(1<< 4)        )  <<  5,
		((1<< 0)|(1<< 1)|(1<< 2))  << 10,
	}, {
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3)),		// board bits of ranks, 7x7 board
		((1<< 4)|(1<< 5)|(1<< 6)        ),
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3))  <<  7,
		((1<< 4)|(1<< 5)|(1<< 6)        )  <<  7,
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3))  << 14,
		((1<< 4)|(1<< 5)|(1<< 6)        )  << 14,
		((1<< 0)|(1<< 1)|(1<< 2)|(1<< 3))  << 21,
	}
};

const unsigned int files[][7] = {				// same for files
	{
		((1<< 0)|(1<< 3)),
		((1<< 2)        ),
		((1<< 0)|(1<< 3))  << 1,
	}, {
		((1<< 0)|(1<< 5)|(1<<10)),
		((1<< 3)|(1<< 8)        ),
		((1<< 0)|(1<< 5)|(1<<10))  << 1,
		((1<< 3)|(1<< 8)        )  << 1,
		((1<< 0)|(1<< 5)|(1<<10))  << 2,
	}, {
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)),
		((1<< 4)|(1<<11)|(1<<18)        ),
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)) << 1,
		((1<< 4)|(1<<11)|(1<<18)        ) << 1,
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)) << 2,
		((1<< 4)|(1<<11)|(1<<18)        ) << 2,
		((1<< 0)|(1<< 7)|(1<<14)|(1<<21)) << 3,
	}
};

enum MoveDirection { UpperLeft = 0, UpperRight = 1, LowerRight = 2, LowerLeft = 3 };

// #define __SEARCH

std::list<Move> BitBoard::hashMoves (const Color color, const MoveType movetype, const unsigned int p, const unsigned int o, const unsigned int e, const unsigned int b) const {
	// std::cerr << "BitBoard::hashMoves (color=" << color << " type=" << movetype << " p=" << p << " o=" << o << " e=" << e << " b=" << b << ")\n";
	
	std::list<Move> l;
#ifdef __SEARCH
	if (__builtin_popcount(bits[0]) + __builtin_popcount(bits[1]) < 19) {
		return l;
	}
#endif

	if (color == White) {
		if (movetype == Jump) {
		std::list<Move> l2;
		std::list<Move> *
		lc = &j_p0[0][ p     &15][(e>> 4)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p0[1][(p>> 7)&15][(e>>11)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p0[2][(p>>14)&15][(e>>18)& 7][(b>>21)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p1[0][(p>> 4)& 7][(e>> 7)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p1[1][(p>>11)& 7][(e>>14)&15][(b>>18)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2));

		if (o) {
		lc = &j_m0[0][(o>>21)&15][(e>>18)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m0[1][(o>>14)&15][(e>>11)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m0[2][(o>> 7)&15][(e>> 4)& 7][(b>> 0)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m1[0][(o>>18)& 7][(e>>14)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m1[1][(o>>11)& 7][(e>> 7)&15][(b>> 4)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2));
		}
		for (auto m: l2) {
			unsigned int at = m.from();
			int stone_type = (tower[at].isOfficer()) ? OFFICER : (color == White) ? 0 : 1;				// except, officer has more possibilites, (stored in move_index)
			l.splice(l.end(), continueJumps(color, m, stone_type));
		}
		} else {
		std::list<Move> *
		lc = &s_p0[0][p      &15][(b>> 4)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p0[1][(p>> 7)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p0[2][(p>>14)&15][(b>>18)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p1[0][(p>> 4)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p1[1][(p>>11)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		lc = &s_p1[2][(p>>18)& 7][(b>>21)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));

		if (o) {
		lc = &s_m0[0][(o>>21)&15][(b>>18)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m0[1][(o>>14)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m0[2][(o>> 7)&15][(b>> 4)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m1[0][(o>>18)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m1[1][(o>>11)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		lc = &s_m1[2][(o>> 4)& 7][(b>> 0)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		}
		}
	} else {
		if (movetype == Jump) {
		std::list<Move> l2;
		std::list<Move> *
		lc = &j_m0[0][(p>>21)&15][(e>>18)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m0[1][(p>>14)&15][(e>>11)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m0[2][(p>> 7)&15][(e>> 4)& 7][(b>> 0)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m1[0][(p>>18)& 7][(e>>14)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_m1[1][(p>>11)& 7][(e>> 7)&15][(b>> 4)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2));

		if (o) {
		lc = &j_p0[0][ o     &15][(e>> 4)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p0[1][(o>> 7)&15][(e>>11)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p0[2][(o>>14)&15][(e>>18)& 7][(b>>21)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p1[0][(o>> 4)& 7][(e>> 7)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2)); 
		lc = &j_p1[1][(o>>11)& 7][(e>>14)&15][(b>>18)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l2));
		}
		for (auto m: l2) {
			unsigned int at = m.from();
			int stone_type = (tower[at].isOfficer()) ? OFFICER : (color == White) ? 0 : 1;				// except, officer has more possibilites, (stored in move_index)
			l.splice(l.end(), continueJumps(color, m, stone_type));
		}
		} else {
		std::list<Move> *
		lc = &s_m0[0][(p>>21)&15][(b>>18)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m0[1][(p>>14)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m0[2][(p>> 7)&15][(b>> 4)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m1[0][(p>>18)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_m1[1][(p>>11)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		lc = &s_m1[2][(p>> 4)& 7][(b>> 0)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		if (o) {
		lc = &s_p0[0][ o     &15][(b>> 4)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p0[1][(o>> 7)&15][(b>>11)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p0[2][(o>>14)&15][(b>>18)& 7]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p1[0][(o>> 4)& 7][(b>> 7)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l)); 
		lc = &s_p1[1][(o>>11)& 7][(b>>14)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		lc = &s_p1[2][(o>>18)& 7][(b>>21)&15]; std::copy(lc->begin(), lc->end(), std::back_inserter(l));
		}
		}
	}

	// std::cerr << "return: "; for (auto i: l) { std::cerr << i << ' '; } std::cerr << '\n' << std::endl;
	return l;
}

// move_index: bits to index translation:  (index lists step moves first, then jumps.)
// ordered by MoveDirection enum, i.e,
// moves are ordered in 4 directions.
// 'skip' holds offset in index for each direction, for jump and step moves likewise
void BitBoard::listMoves (unsigned int moves, const unsigned int color, const unsigned int skip, const bool do_promotion, std::list<Move> &l) const {
	for (unsigned int m = moves; m; m &= m-1) {
		unsigned int at = __builtin_ffs(m)-1;
		unsigned int p = 0;		// promotion
#ifdef NOOFFICER
		if (skip > 99 && tower[at].size() == 2) { continue; }	// further captures results in too big a tower
#endif
		l.emplace_back (Move((at+skip)|p));
	}
}

std::list<Move> BitBoard::generateMoves (const Color color, const MoveType movetype) const {
	const unsigned int ones = 0x1ffffff;				// all 25 square bits set
	const unsigned int bi = (board_size >> 1) - 1;			// board_size -> array index for 3, 5, 7, ...
	const unsigned int e = board_size-1;
	const unsigned int f = board_size-2;

#ifdef NOOFFICER
	// in no officers case we don't need to consider towers on the
	// second last rank, (with capturing moves, the third last rank also),
	// cause if they move they will promote, so mask them out.
	const unsigned int mask[2][4] = {
		{ ~(ranks[bi][e] | ranks[bi][f] | ranks[bi][4] | files[bi][0]) & ones,					// squares where a stone can shift to lower left etc.
		  ~(ranks[bi][e] | ranks[bi][f] | ranks[bi][4] | files[bi][e]) & ones,					// leaves 18 bits set
		  ~(ranks[bi][0] | ranks[bi][1] | ranks[bi][2] | files[bi][e]) & ones,
		  ~(ranks[bi][0] | ranks[bi][1] | ranks[bi][2] | files[bi][0]) & ones },
		{ ~(ranks[bi][e] | ranks[bi][f] | ranks[bi][4] | files[bi][1] | files[bi][0]) & ones,		// squares where a stone can jump  to lower left etc.
		  ~(ranks[bi][e] | ranks[bi][f] | ranks[bi][4] | files[bi][f] | files[bi][e]) & ones,		// leaves 13 bits
		  ~(ranks[bi][0] | ranks[bi][1] | ranks[bi][2] | files[bi][f] | files[bi][e]) & ones,
		  ~(ranks[bi][0] | ranks[bi][1] | ranks[bi][2] | files[bi][1] | files[bi][0]) & ones }
	};
#else
	const unsigned int mask[2][4] = {
		{ ~(ranks[bi][e] | files[bi][0]) & ones,					// squares where a stone can shift to lower left etc.
		  ~(ranks[bi][e] | files[bi][e]) & ones,					// leaves 18 bits set
		  ~(ranks[bi][0] | files[bi][e]) & ones,
		  ~(ranks[bi][0] | files[bi][0]) & ones },
		{ ~(ranks[bi][e] | ranks[bi][f] | files[bi][1] | files[bi][0]) & ones,		// squares where a stone can jump  to lower left etc.
		  ~(ranks[bi][e] | ranks[bi][f] | files[bi][f] | files[bi][e]) & ones,		// leaves 13 bits
		  ~(ranks[bi][0] | ranks[bi][1] | files[bi][f] | files[bi][e]) & ones,
		  ~(ranks[bi][0] | ranks[bi][1] | files[bi][1] | files[bi][0]) & ones }
	};
#endif

	unsigned int offsets[2][4]	 = { {				// move directions upper left = 4, upper right = 4 ...
		 (board_size-1)/2,					// 1, 2, 3, ... for board_size 3, 5, 7, ...
		 (board_size+1)/2,					// 2, 3, 4, ... for board_size 3, 5, 7, ...
		 (board_size-1)/2,
		 (board_size+1)/2 }, {
		 (board_size-1),					// 2, 4, 6, ... for board_size 3, 5, 7, ...
		 (board_size+1),					// 4, 6, 8, ... for board_size 3, 5, 7, ...
		 (board_size-1),
		 (board_size+1) }
	};


	const unsigned int  directions[4]         =     {   UpperLeft, UpperRight, LowerRight,  LowerLeft }; 
	const bool          check_promotion[2][4] =   { {        true,       true,      false,      false },
						        {       false,      false,       true,       true } };
	unsigned int        nb                    = ~both() & ones;
	unsigned int        empty[2][4]           = { { nb >> offsets[Step][UpperLeft],
							nb >> offsets[Step][UpperRight],
							nb << offsets[Step][LowerRight],
							nb << offsets[Step][LowerLeft] },
						      { nb >> offsets[Jump][UpperLeft],
							nb >> offsets[Jump][UpperRight],
							nb << offsets[Jump][LowerRight],
							nb << offsets[Jump][LowerLeft] } };
	unsigned int	    other                 =  all(1^color);
	unsigned int	    over[2][4]            = { {        ones,       ones,       ones,       ones },	// step
						      { other >> offsets[Step][UpperLeft],
							other >> offsets[Step][UpperRight],
							other << offsets[Step][LowerRight],
							other << offsets[Step][LowerLeft]  } };	// jump
	unsigned int        who_moves[2][4]	  = { {       all(color),      all(color), officers(color), officers(color) },
						      {  officers(color), officers(color),      all(color),      all(color) } };

	std::list<Move> l;	// list of moves to return
	std::list<Move> l2;

	// std::cerr << "BitBoard::generateMoves (" << color << ", " << movetype << ")\n";
	for (auto d: directions ) {
		listMoves (mask[movetype][d]  & who_moves[color][d] & over[movetype][d]  & empty[movetype][d], color, skip[movetype][d], check_promotion[color][d],  l2);	// moves to lower right
	}
	// for (auto i: l2) { std::cerr << i << " " << i.index() << " "; } std::cerr << " done." << std::endl;

	if (movetype == Jump) {
	for (auto m: l2) {
		unsigned int at = m.from();
		int stone_type = (tower[at].isOfficer()) ? OFFICER : (color == White) ? 0 : 1;				// except, officer has more possibilites, (stored in move_index)
		l.splice(l.end(), continueJumps(color, m, stone_type));
	}
	} else {
		return l2;
	}
	// for (auto i: l) { std::cerr << i << " " << i.index() << " "; } std::cerr << " done." << std::endl;
	return l;
}

bool BitBoard::hasJumps (const unsigned int color) const {	// noofficer check, not too usefull to adopt for concurrent game
	unsigned int me    =  bits[color];
	unsigned int other =  bits[1^color];

	if (color) {    // black
		const unsigned int mask6 = ~((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<10)|(1<<13)|(1<<17)|(1<<20)|(1<<24)) & me;
		if (mask6 & (other << 3) & (~(both()) << 6)) { return true; }
		const unsigned int mask8 = ~((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<< 7)|(1<<11)|(1<<14)|(1<<18)|(1<<21)) & me;
		if (mask8 & (other << 4) & (~(both()) << 8)) { return true; }
	} else {
		const unsigned int mask6 = ~((1<<0)|(1<<4)|(1<< 7)|(1<<11)|(1<<14)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24)) & me;
		if (mask6 & (other >> 3) & (~(both()) >> 6)) { return true; }
		const unsigned int mask8 = ~((1<<3)|(1<<6)|(1<<10)|(1<<13)|(1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24)) & me;
		if (mask8 & (other >> 4) & (~(both()) >> 8)) { return true; }
	}

	return false;
}

std::string BitBoard::toString() const {
	std::string s = "";
	for (unsigned int i=0; i<25; i++) {
		     if (bits[3] & (1<<i)) { s += 'X'; }
		else if (bits[2] & (1<<i)) { s += 'O'; }
		else if (bits[1] & (1<<i)) { s += 'x'; }
		else if (bits[0] & (1<<i)) { s += 'o'; }
		if (i<24) s += '.';
		// if (i%7 == 6 || i%7 == 3) s += ' ';
	}

	return s;
}

std::list<Move> BitBoard::continueJumps(const unsigned int color, const Move index, const unsigned int stone_type) const {
	std::list<Move> l;

	// std::cerr << "continueJumps " << index << " " << index.contFrom(stone_type)  << " " << (signed) index.contTo(stone_type) << '\n';

	for (Move m = index.contFrom(stone_type); (signed) m.index() < (signed) index.contTo(stone_type); ++m) {
		if (isNotOccupied	(m.overMask(), 1^color)) { continue; }		// square this move will jump over is empty
		if (collides		(m.stepMask()))		 { continue; }		// square to jump on is occupied by another tower

		if (longJumpsCheck(m) == false) continue;

		if (m.contFrom(stone_type)) {			// maybe there are further continuations
			l.splice(l.end(), continueJumps(color, m, stone_type));
		} else {					// if move has no possible continuation, add to movelist
			l.emplace_back (m);			// jump is legal.
		}
	}
	if (l.size() == 0) {					// if move has no possible further continuation, add index to movelist
		l.emplace_back (index);				// jump is legal.
	}

	return l;
}

bool BitBoard::longJumpsCheck(const Move m) const {			// for applyMoves and isPermutation
	for (auto o: over_check[m.index()]) {
		if (tower[o.first].ownStones() < o.second) {
			return false;
		}
	}
	return true;
}


// position evaluation stuff
 
static	int	square_value[4][25] = {
	{ 0,   1,   1,   0,
	     1,   2,   1,
	  2,   3,   3,   2,
	     3,   4,   3,
	  4,   5,   5,   4,
	     5,   6,   5,
	  6,   7,   7,   6},
	{ 6,   7,   7,   6,
	     5,   6,   5,
	  4,   5,   5,   4,
	     3,   4,   3,
	  2,   3,   3,   2,
	     1,   2,   1,
	  0,   1,   1,   0},
	{ 0,   2,   2,   0,
	     5,  10,   5,
	 10,  15,  15,  10,
	    15,  20,  15,
	 10,  15,  15,  10,
	     5,  10,   5,
	  0,   2,   2,   0},
	{ 0,   2,   2,   0,
	     5,  10,   5,
	 10,  15,  15,  10,
	    15,  20,  15,
	 10,  15,  15,  10,
	     5,  10,   5,
	  0,   2,   2,   0},
};

int adjacent_value[25] = {
	   55,   60,   60,    55,
	     70,   70,   70,
	  60,   80,   80,   60,
	     70,   90,   70,
	  60,   80,   80,   60,
	     70,   70,   70,
	   55,   60,   60,    55
};

int BitBoard::adjacent (const Color color) const {
	const unsigned int ones = 0x1ffffff;				// all 25 square bits set
	const unsigned int bi = (board_size >> 1) - 1;			// board_size -> array index for 3, 5, 7, ...
	const unsigned int e = board_size-1;

	const unsigned int mask[4] = {
		~(ranks[bi][e] | files[bi][0]) & ones,					// squares where a stone can shift to lower left etc.
		~(ranks[bi][e] | files[bi][e]) & ones,					// leaves 18 bits set
		~(ranks[bi][0] | files[bi][e]) & ones,
		~(ranks[bi][0] | files[bi][0]) & ones,
	};

	unsigned int offsets[4] = {
		 (board_size-1)/2,					// 1, 2, 3, ... for board_size 3, 5, 7, ...
		 (board_size+1)/2,					// 2, 3, 4, ... for board_size 3, 5, 7, ...
		 (board_size-1)/2,
		 (board_size+1)/2 
	};

	unsigned int	    me 			=	all(  color);
	unsigned int	    other 		=	all(1^color);
	unsigned int	    m_adj[4]		= {	me    >> offsets[UpperLeft]	& mask[0],
							me    >> offsets[UpperRight]	& mask[1],
							me    << offsets[LowerRight]	& mask[2],
							me    << offsets[LowerLeft]	& mask[3]  };
	unsigned int	    o_adj[4]		= {	other >> offsets[UpperLeft]	& mask[0],
							other >> offsets[UpperRight]	& mask[1],
							other << offsets[LowerRight]	& mask[2],
							other << offsets[LowerLeft]	& mask[3]  };
	int m = 0;
	int o = 0;

	for (unsigned int i = 0; i<4; i++) {
		for (unsigned int b = m_adj[i]; b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			m += adjacent_value[at];
		}
		for (unsigned int b = o_adj[i]; b; b &= b-1) {
			unsigned int at = __builtin_ffs(b)-1;
			o += adjacent_value[at];
		}
	}

	return m-o;
}

int BitBoard::evaluateTower(const int at) const {
	return tower[at].getValue() + square_value[tower[at].top()][at];
}

int BitBoard::evaluateTowers(const Color color) const {
	int v = 0;

	// std::cerr << " #  tower  value  square" << std::endl;
	// std::cerr << "------------------------" << std::endl;
	for (unsigned int b = bits[ color]; b; b &= (b - 1)){
		unsigned int at = __builtin_ffs(b) - 1;
		// std::cerr << std::setw(2) << at << std::setw(7) << tower[at].toString() << std::setw(7) << tower[at].getValue() << std::setw(7) << square_value[tower[at].top()][at] << std::endl;
		v += tower[at].getValue(); 
		v += square_value[tower[at].top()][at];
	}

	for (unsigned int b = bits[!color]; b; b &= (b - 1)){
		unsigned int at = __builtin_ffs(b) - 1;
		// std::cerr << std::setw(2) << at << std::setw(7) << tower[at].toString() << std::setw(7) << -tower[at].getValue() << std::setw(7) << -square_value[tower[at].top()][at] << std::endl;
		v -= tower[at].getValue();
		v -= square_value[tower[at].top()][at];
	}
	// std::cerr << "-----------------------" << std::endl;
	// std::cerr << "                " << std::setw(7) << v << std::endl;

	// std::cerr << "adjvalue: " << adjacent((Color) turn) << std::endl;
	v += adjacent(color);

	return v;
}

void BitBoard::readConfig       (const std::string file_name) {
	int     v[256];

	for (unsigned int i=0; i<256; i++) { v[i] = 0; }

	std::ifstream f;
	f.open(file_name);

	int i = 0, j = 0;

	std::string dummy;

	f >> dummy; while (f >> j) { v[i] = j; ++i; if (f.eof() || i ==  16) break; }
	f >> dummy; while (f >> j) { v[i] = j; ++i; if (f.eof() || i ==  32) break; }
	f >> dummy; while (f >> j) { v[i] = j; ++i; if (f.eof() || i ==  57) break; }
	f >> dummy; while (f >> j) { v[i] = j; ++i; if (f.eof() || i ==  82) break; }
	f >> dummy; while (f >> j) { v[i] = j; ++i; if (f.eof() || i == 107) break; }
	f >> dummy; while (f >> j) { v[i] = j; ++i; if (f.eof() || i == 132) break; }

	f. close();


	tower[0].setConfigValues (v);

	for (unsigned int i = 32   ; i<32+ 25; i++) { adjacent_value[i-32]	= v[i]; }
	for (unsigned int i = 32+25; i<32+ 50; i++) { square_value[0][i-32-25]	= v[i]; }
	for (unsigned int i = 32+50; i<32+ 75; i++) { square_value[1][i-32-50]	= v[i]; }
	for (unsigned int i = 32+75; i<32+100; i++) { square_value[2][i-32-75]	= square_value[3][i-32-75] = v[i]; }
}

void BitBoard::writeConfig       (const std::string file_name) {
	std::ofstream f;
	
	f.open(file_name);

	f << "privates: ";         for (int i= 0; i <   16; i++) { f << tower[0].getConfigValue (i) << ' '; } f << std::endl;
	f << "officers: ";         for (int i=16; i < 2*16; i++) { f << tower[0].getConfigValue (i) << ' '; } f << std::endl;
	f << "adjacent: ";         for (int i= 0; i <   25; i++) { f << adjacent_value[i]  << ' ';	    } f << std::endl;
	f << "squares_white: ";    for (int i= 0; i <   25; i++) { f << square_value[0][i] << ' ';	    } f << std::endl;
	f << "squares_black: ";    for (int i= 0; i <   25; i++) { f << square_value[1][i] << ' ';	    } f << std::endl;
	f << "squares_officers: "; for (int i= 0; i <   25; i++) { f << square_value[2][i] << ' ';	    } f << std::endl;

	f. close();
}



void  BitBoard::initMoveHashes (const unsigned int boardsize) {
	unsigned int bak_bits[4];
	for (unsigned int v = 0; v < 4; v++) { bak_bits[v] = bits[v]; }

/*
	for (unsigned int v = 0; v < (1<<12); v++) {
		bits[0]  = (v& 15) | (v&(15<<4)<<3) | (v&(15<<8)<<6);
		for (unsigned int w = 0; w < (1<<9); w++) {
			bits[1]  = (w& 7   ) << 4;
			bits[1] |= (w&(7<<3))<< 8;
			bits[1] |= (w&(7<<6))<<12;
			std::list<Move> l = generateMoves (White, Jump);
			std::cerr  << std::setw(3) << v << " "<< std::setw(3) << w << ' ' << toString() << " -> ";
			for (const auto& i: l) { std::cerr << i << " "; } std::cerr << '\n';
			// j_p0[i][(v&15)][w][((v&(15<<4))>>4)] = l;
		}
	}
return;
 */
	for (unsigned int i = 0; i < 3; i+=1) {
	for (unsigned int v = 1; v < 16; v+=1) {
		bits[0] = v<<(7*i);
		for (unsigned int w = 0; w < 8; w++) {
			bits[1] = w<<(4+7*i);
			std::list<Move> l = generateMoves (White, Step);
			s_p0[i][v][w]   = l;
			// std::cerr << i << ' ' << v << ' ' << w << " -> "; for (auto& j: l) { std::cerr << j << " "; } std::cerr << '\n';
		}
	}
	}
	for (unsigned int i = 0; i < 3; i+=1) {
	for (unsigned int v = 1; v < 8; v++) {
		bits[0] = v<<(4+7*i);
		for (unsigned int w = 0; w < 16; w++) {
			bits[1] = w<<(7+7*i);
			std::list<Move> l = generateMoves (White, Step);
			s_p1[i][v][w]   = l;
			// std::cerr << i << ' ' << v << ' ' << w << " -> "; for (auto& j: l) { std::cerr << j << " "; } std::cerr << '\n';
		}
	}
	}
	for (unsigned int i = 0; i < 3; i+=1) {
	for (unsigned int v = 1; v <16; v++) {
		bits[1] = v<<(21-7*i);
		for (unsigned int w = 0; w < 8; w++) {
			bits[0] = w<<(18-7*i);
			std::list<Move> l = generateMoves (Black, Step);
			s_m0[i][v][w]   = l;
			// std::cerr << i << ' ' << v << ' ' << w << " -> "; for (auto& j: l) { std::cerr << j << " "; } std::cerr << '\n';
		}
	}
	}
	for (unsigned int i = 0; i < 3; i+=1) {
	for (unsigned int v = 1; v < 8; v++) {
		bits[1] = v<<(18-7*i);
		for (unsigned int w = 0; w < 16; w++) {
			bits[0] = w<<(14-7*i);
			std::list<Move> l = generateMoves (Black, Step);
			s_m1[i][v][w]   = l;
			// std::cerr << i << ' ' << v << ' ' << w << " -> "; for (auto& j: l) { std::cerr << j << " "; } std::cerr << '\n';
		}
	}
	}
	// for (unsigned int v = 0; v < 4; v++) { bits[v] = bak_bits[v]; }
	// return;

	bits[2] = 0;	// no officers needed.
	bits[3] = 0;
	for (unsigned int i = 0; i < 3; i++) {
	for (unsigned int v = 0; v < (1<<8); v++) {
		bits[0]  = (v& 15)    << (  7*i);
		bits[0] |= (v&(15<<4))<< (3+7*i);	// dummy stones, which may block, two ranks above. color doesn't matter here.
		for (unsigned int w = 0; w < (1<<3); w++) {
			bits[1] = w<<(4+7*i);
			std::list<Move> l = generateMoves (White, Jump);
			// std::cerr  << std::setw(3) << v << " "<< std::setw(3) << w << " " << ((v&(15<<4))>>4) << ' ' << toString() << " -> ";
			// for (const auto& i: l) { std::cerr << i << " "; } std::cerr << '\n';
			j_p0[i][(v&15)][w][((v&(15<<4))>>4)] = l;
		}
	}
	}
	for (unsigned int i = 0; i < 2; i++) {
	for (unsigned int v = 0; v < (1<<6); v++) {
		bits[0]  = (v&  7)    << (4+7*i);
		bits[0] |= (v&( 7<<3))<< (8+7*i);	// dummy stones, blocking
		for (unsigned int w = 0; w < (1<<4); w++) {
			bits[1] = w<<(7+7*i);
			std::list<Move> l = generateMoves (White, Jump);
			// std::cerr  << std::setw(3) << v << " "<< std::setw(3) << w << " " << (v&(7<<3)>>3) << ' ' << toString() << " -> ";
			// for (const auto& i: l) { std::cerr << i << " "; } std::cerr << '\n';
			j_p1[i][(v&7)][w][((v&(7<<3))>>3)] = l;
		}
	}
	}
	for (unsigned int i = 0; i < 3; i++) {
	for (unsigned int v = 0; v < (1<<8); v++) {
		bits[1]  = ( v& 15)        <<(21-7*i);
		bits[1] |= ((v&(15<<4))>>4)<<(14-7*i);	// dummy stones, which may block, two ranks below. color doesn't matter here.
		for (unsigned int w = 0; w < (1<<3); w++) {
			bits[0] = w<<(18-7*i);
			std::list<Move> l = generateMoves (Black, Jump);
			// std::cerr  << std::setw(3) << v << " "<< std::setw(3) << w << ' ' << toString() << " -> ";
			// for (const auto& i: l) { std::cerr << i << " "; } std::cerr << '\n';
			j_m0[i][(v&15)][w][((v&(15<<4))>>4)] = l;
		}
	}
	}
	for (unsigned int i = 0; i < 2; i++) {
	for (unsigned int v = 0; v < (1<<6); v++) {
		bits[1]  = ( v& 7)        << (18-7*i);
		bits[1] |= ((v&(7<<3))>>3)<< (11-7*i);	// dummy stones, blocking
		for (unsigned int w = 0; w < (1<<4); w++) {
			bits[0] = w<<(14-7*i);
			std::list<Move> l = generateMoves (Black, Jump);
			// std::cerr  << std::setw(3) << v << " "<< std::setw(3) << w << ' ' << toString() << " -> ";
			// for (const auto& i: l) { std::cerr << i << " "; } std::cerr << '\n';
			j_m1[i][(v&7)][w][((v&(7<<3))>>3)] = l;
		}
	}
	}

	for (unsigned int v = 0; v < 4; v++) { bits[v] = bak_bits[v]; }
}

