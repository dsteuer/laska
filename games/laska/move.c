/*
        _______________________________________________________
       |       |_______|       |_______|       |_______|       |
  7    |       |_______|       |_______|       |_______|       |
       |x 21   |_______|x 22   |_______|x 23   |_______|x 24   |
       |_______|_______|_______|_______|_______|_______|_______|
       |_______|       |_______|       |_______|       |_______|
  6    |_______|       |_______|       |_______|       |_______|
       |_______|x 18   |_______|x 19   |_______|x 20   |_______|
       |_______|_______|_______|_______|_______|_______|_______|
       |       |_______|       |_______|       |_______|       |
  5    |       |_______|       |_______|       |_______|       |
       |x 14   |_______|x 15   |_______|x 16   |_______|x 17   |
       |_______|_______|_______|_______|_______|_______|_______|
       |_______|       |_______|       |_______|       |_______|
  4    |_______|       |_______|       |_______|       |_______|
       |_______|  11   |_______|  12   |_______|  13   |_______|
       |_______|_______|_______|_______|_______|_______|_______|
       |       |_______|       |_______|       |_______|       |
  3    |       |_______|       |_______|       |_______|       |
       |o  7   |_______|o  8   |_______|o  9   |_______|o 10   |
       |_______|_______|_______|_______|_______|_______|_______|
       |_______|       |_______|       |_______|       |_______|
  2    |_______|       |_______|       |_______|       |_______|
       |_______|o  4   |_______|o  5   |_______|o  6   |_______|
       |_______|_______|_______|_______|_______|_______|_______|
       |       |_______|       |_______|       |_______|       |
  1    |       |_______|       |_______|       |_______|       |
       |o  0   |_______|o  1   |_______|o  2   |_______|o  3   |
       |_______|_______|_______|_______|_______|_______|_______|

           A       B       C       D       E       F       G

   by this table a move a1-b4 has steps 0 -> 4 in move_index
   a jump d4xf6 das steps 12, 20 and jumps over square 16.
   bitboard own_masks are (1<<0|1<<4) for step move and
   (1<<12|1<<20), with opp_squares mask (1<<16) for the jump
*/

#include <vector>
#include <algorithm>
#include <iomanip>
#include <sstream>


#include "move.h"

struct MoveData {
	unsigned char   n;      // count, hit- and promotion flag
	unsigned char   steps[15];
	unsigned int    own_squares, opp_squares;
	short   contfrom[3];
	short   contto[3];
	bool operator<(const MoveData &m) const {
		if (n <m.n) return true;
		if (n >m.n) return false;
		for (unsigned int i=0; i<=n; i++) {
			if (steps[i] < m.steps[i]) return true;
		}
		return false;
	}
	bool operator==(const MoveData &m) const {
		if (n <m.n) return false;
		if (n >m.n) return false;
		for (unsigned int i=0; i<=n; i++) {
			if (steps[i] != m.steps[i]) return false;
		}
		return true;
	}
};

#include "moveindex.h"

static unsigned int PromotionBottom	=  4;	// if square number a black private reaches with a move is below 4, private promotes, 7x7 board
static unsigned int PromotionTop	= 20;	// if square number a white private reaches is greater than     20, private promotes
static unsigned int StepCounter		=  0;	// number of step moves listed in move_index (moveindex.h)
static unsigned int MoveIndexSize	=  0;	// remember size of static move index (pointer cannot sizeof)

static MoveData     *move_index;

bool		Move::isStep		() const { return (index() <  StepCounter)    ? true : false; }
bool		Move::isJump		() const { return (index() > (StepCounter-1)) ? true : false; }
bool		Move::isQuiet		() const { return isStep();			 } // alias for isStep
bool		Move::isPromotion	() const { return (_index & PROMOTE);						}
void		Move::setPromotion	()	 { 	   _index |= PROMOTE;						}
bool		Move::empty		() const { return index() == 4095;						}
bool		Move::isOnPromotionSquare()const { return (to() < PromotionBottom || to() > PromotionTop);		}	// this check does not need color
unsigned int	Move::size		() const { return move_index[index()].n;					}
unsigned int	Move::stepMask		() const { return move_index[index()].own_squares;				}
unsigned int	Move::overMask		() const { return move_index[index()].opp_squares;				}

unsigned char	Move::from		() const { return from(0);							}
unsigned char	Move::from		(const unsigned int i) const { return  move_index[index()].steps[i];		}
unsigned char	Move::step		(const unsigned int i) const { return  move_index[index()].steps[i];		}	// alias for from

unsigned char	Move::to		() const { return move_index[index()].steps[size()];				}
unsigned char	Move::to		(const unsigned int i) const { return  move_index[index()].steps[i+1];		}
unsigned int	Move::over		(const unsigned int i) const { return (move_index[index()].steps[i]+move_index[index()].steps[i+1])/2;	}
unsigned int	Move::contFrom		(const unsigned int t) const { return  move_index[index()].contfrom[t];		}
unsigned int	Move::contTo		(const unsigned int t) const { return  move_index[index()].contto[t];		}

static std::vector<std::string> square2string = {
	"a1", "c1", "e1", "g1", "b2", "d2", "f2",
	"a3", "c3", "e3", "g3", "b4", "d4", "f4",
	"a5", "c5", "e5", "g5", "b6", "d6", "f6",
	"a7", "c7", "e7", "g7"
};

std::string Move::toString () const {
	MoveData data = move_index[index()];

	std::string s = "";
	if (empty()) { return s; }
	std::string sep = (isJump()) ? "x" : "-";
	s  = square2string[data.steps[0]] + sep;
	s += square2string[data.steps[1]];
	for (unsigned int j = 2; j<=size(); j++) {
		s += 'x';	// is a continued jump
		s += square2string[ data.steps[j] ];
	}

	return s;
}

Move::Move(std::string m) {
	if (m.size() > 2 && (m[2] == 'x' || m[2] == '-')) {
		MoveData	data;

		data.n = -1;

		for (unsigned int s = 0; s < m.length(); s += 3) {
			data.n ++;
			data.steps[data.n] = square(m.substr(s, 2).data());
		}

		auto f = std::find(move_index, move_index+MoveIndexSize, data);
		if (f == move_index+MoveIndexSize) {
			std::cerr << "Move (" << m << ") could not parse anything useful out of this string." << std::endl;
			_index = NoMove;
			return;
		}
		_index = f - move_index;
	} else {	// a hex digit as move index
		std::istringstream iss(m);
		iss >> std::hex >> _index;
	}
}

static std::vector<unsigned int> rank_index = { 0, 4, 7, 11, 14, 18, 21 };	// square number in 7x7 board, where a new rank begins
static std::vector<unsigned int> file_index = { 0, 0, 1, 1, 2, 2, 3	};	// number to add to rank_index to get the actual square

unsigned int Move::square(std::string s) {
	return rank_index[(s[1]-'1')] + file_index[(s[0] - 'a')];
}

std::ostream& operator<<(std::ostream& s, const Move &m) {
	if (s.flags() & std::ios_base::hex) {
		s << m.index();
	} else {
		s << m.toString();
	}
        return s;
}

void initMoveIndex(const unsigned int board_size) {
	StepCounter	= 100;							// 8*((board_size-1)/2)*(  ((board_size-1)/2));		// number of step moves on a n x n laska is 8m^2, with m=(n-1)/2. steps listed in move_index (moveindex.h)
	PromotionTop	=   ((board_size+1)/2)*(2*((board_size+1)/2)-3);	// 2, 9, 20, ...
	PromotionBottom	=    (board_size+1)/2;					// 2, 3, 4, ...

	      if (board_size == 7) {
		move_index = move_index7x7;
		square2string = {
			"a1", "c1", "e1", "g1", "b2", "d2", "f2",
			"a3", "c3", "e3", "g3", "b4", "d4", "f4",
			"a5", "c5", "e5", "g5", "b6", "d6", "f6",
			"a7", "c7", "e7", "g7"
		};
		rank_index = { 0, 4, 7, 11, 14, 18, 21 };	// square number in 7x7 board, where a new rank begins
		MoveIndexSize	= sizeof(move_index7x7)/sizeof(MoveData);
	} else if (board_size == 5) {
		move_index = move_index5x5;
		square2string = {
			"a1", "c1", "e1", "b2", "d2",
			"a3", "c3", "e3", "b4", "d4",
			"a5", "c5", "e5"
		};
		rank_index = {0, 3, 5, 8, 10, 0, 0 };
		StepCounter	= 52;							// 8*((board_size-1)/2)*(  ((board_size-1)/2));		// number of step moves on a n x n laska is 8m^2, with m=(n-1)/2. steps listed in move_index (moveindex.h)
		MoveIndexSize	= sizeof(move_index5x5)/sizeof(MoveData);
	} else if (board_size == 4) {
		move_index = move_index4x4;
		square2string = {
			"a1", "c1", "b2", "d2",
			"a3", "c3", "b4", "d4",
		};
		rank_index = {0, 2, 4, 6, 0, 0, 0 };
		StepCounter	= 45;							// 8*((board_size-1)/2)*(  ((board_size-1)/2));		// number of step moves on a n x n laska is 8m^2, with m=(n-1)/2. steps listed in move_index (moveindex.h)
		MoveIndexSize	= sizeof(move_index4x4)/sizeof(MoveData);
	} else if (board_size == 3) {
		move_index = move_index3x3;
		square2string = {
			"a1", "c1", "b2",
			"a3", "c3",
		};
		rank_index =  { 0, 2, 3, 0, 0, 0, 0 };
		StepCounter	= 39;							// 8*((board_size-1)/2)*(  ((board_size-1)/2));		// number of step moves on a n x n laska is 8m^2, with m=(n-1)/2. steps listed in move_index (moveindex.h)
		MoveIndexSize	= sizeof(move_index3x3)/sizeof(MoveData);
	}
}

