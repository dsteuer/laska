<html>
<body>

<?php

session_start();
include("lb-config.php");
global $laskabin;

function appendMvMvlst ($mv) {
    global $laskabin;
    //echo "appenMvMvlst called </br>";
    // updates Mvlst with new move, if $mv is the old move from Mvlst,
    // just increase curmv counter. If $mv is new cut old Mvlst here.
    // $mv comes from a click on a possible mv, so should be possible 
    
    $lmvlst = $_SESSION['mvlst'];
    $lcurmv = $_SESSION['curmv'];

    if ($lmvlst[count($lmvlst)-1] != $mv){
        //print_r ($lmvlst);
        //print $mv;
        //print "</br>";
        
        if ( (count($lmvlst) > $curmv) & ($lmvlst[ $lcurmv +1 ] == $mv)){
            // middle of mvlst
            $lcurmv++;
        } else {
            $lmvlst = array_slice($lmvlst, 0, $lcurmv);
            //// new last move , real append
            $lcurmv++;
            array_push($lmvlst , $mv);
        }
        
        $laskacmd = $laskabin . ' -p "' . $_SESSION['curpos'] . '" -A "' . $mv . '" -S1 -D' ;
        //echo $laskacmd . "</br>";
        
        $lcurpos = exec($laskacmd, $lines);
        //echo "CurPosition: " . $lcurpos . "</br>";
        
        $_SESSION['curpos'] = $lcurpos;
        $_SESSION['mvlst'] = $lmvlst;
        $_SESSION['curmv'] = $lcurmv;
    }
}

if (isset($_REQUEST['setstartpos']) && ($_REQUEST['setstartpos'] != '')   ) {
    $_SESSION['startpos'] = $_REQUEST['setstartpos'] ; unset($_REQUEST['setstartpos']);
    $_SESSION['curpos'] = $_SESSION['startpos'];
    $_SESSION['mvlst'] = array();
    $_SESSION['curmv'] = 0;
}

if (isset($_REQUEST['applymvlst'])  && ($_REQUEST['applymvlst'] != '') ) {
    $imvstring = $_REQUEST['applymvlst'] ; unset($_REQUEST['applymvlst']) ;
    
    $mvs = explode(" ", $imvstring) ;
    
    $lmvlst = $_SESSION['mvlst'];
    $lcurmv = $_SESSION['curmv'];
    
    if ($lmvlst[count($lmvlst)-1] != $mvs[count($mvs)-1]) {
        for ($i=0; $i< count($mvs);$i++){
            array_push($lmvlst , $mvs[$i] );
            $lcurmv++;
        }
        $laskacmd = $laskabin . ' -p "' . $_SESSION['curpos'] . '" -A "' . $imvstring . '" -S1 -D' ;
        //echo $laskacmd ; 
        $lcurpos = exec($laskacmd, $lines);
        //echo $lcurpos ;
        $_SESSION['curpos'] = $lcurpos;
        $_SESSION['mvlst'] = $lmvlst;
        $_SESSION['curmv'] = $lcurmv;
    }
}


if (isset($_REQUEST['mv'])) {appendMvMvlst($_REQUEST['mv']); unset($_REQUEST['mv']); } else { $lbnextmv="";}

if (isset($_REQUEST["resetgame"])) {
    unset($_SESSION['startpos']);
    unset($_SESSION['curpos']);
    unset($_SESSION['mvlst']);
    unset($_SESSION['curmv']);
}

if (isset($_REQUEST['targetmv']) ||  isset($_REQUEST['ply'])  ) {
    if (isset($_REQUEST['targetmv'])) {
        $targetmv = $_REQUEST['targetmv']; unset($_REQUEST['targetmv']);
    }
    if (isset($_REQUEST['ply'])) {
        $targetmv = $_REQUEST['ply']+1; unset($_REQUEST['ply']);
    }
    //echo 'Targetmove ', $targetmv ;
    if ($targetmove == "0") {
        $_SESSION['curpos'] = $_SESSION['startpos'];
        $_SESSION['curmv'] = 0;
    } else {
        $lmvlst = array_slice($_SESSION['mvlst'], 0, $targetmv);
        $laskacmd = $laskabin . ' -p "' . $_SESSION['startpos'] . '" -A "' . implode(" ", $lmvlst) . '" -S1 -D' ;
        //echo $laskacmd . "</br>";
        $lcurpos = exec($laskacmd, $lines);
        $_SESSION['curmv'] = $targetmv;
        $_SESSION['curpos'] = $lcurpos;
    }
}

if (empty($_SESSION['startpos'])) {$_SESSION['startpos'] = "o.o.o.o|o.o.o|o.o.o.o|..|x.x.x.x|x.x.x|x.x.x.x" ;}
if (strlen($_SESSION['curpos']) != 46) {$_SESSION['curpos'] = $_SESSION['startpos'];}
if (empty($_SESSION['mvlst'])) {$_SESSION['mvlst'] = array() ;}
if (empty($_SESSION['curmv'])) {$_SESSION['curmv'] = 0 ;}

echo 'LaskaBoard 0.1 / Position after move ' . $_SESSION['curmv'] . '</br>';

//print 'Got move ' . $_REQUEST["mv"] . '</br>'; 
//print "Startpos " . $_SESSION['startpos'] . "</br>";
//print "curpos " . $_SESSION['curpos'] . "</br>";

//print_r ( $_SESSION["mvlst"]  );

print "<table>";
print '<tr>';
print '<th> position </th> <th> possible moves </th><th> move history </th>' ;
print '</tr>';
print "<tr>";
print "<td>";
print '<img src="plotboard.php"></img>';
print "</td>";
print '<td style="vertical-align: top;">';
$laskacmd = $laskabin . ' -p "' . $_SESSION["curpos"] . '"' ;
if ( fmod($_SESSION['curmv'], 2) != 0 ) { // black to move
    $laskacmd .= ' -b ' ;
}
$laskacmd .= ' -P 2>&1 | tail -1';
//print $laskacmd . "</br>";
$lr = explode(" ", exec ($laskacmd, $lines));

echo "<form name=\"mvselector\"  method=\"post\">";
for ($i=0 ; $i < count($lr); $i++){
    echo '<input onchange="this.form.submit();" type="radio" id="' . $lr[$i] . '" name="mv" value="' . $lr[$i] .'">';
    echo '<label for="' . $lr[$i] . '">' . $lr[$i] . '</label><br>';
}
echo "</form>";

print "</td>";
print '<td style="vertical-align: top;" >';
echo "<form name=\"plyselector\"  method=\"post\">";
for ($i=0 ; $i < count($_SESSION['mvlst']); $i++){
    echo '<input onchange="this.form.submit();" type="radio" id=h-"' . $_SESSION['mvlst'][$i] . '" name="ply" value="' . $i .'">';
    echo '<label for="h-' . $_SESSION['mvlst'][$i] . '">' . $_SESSION['mvlst'][$i] . '</label>';
    if ( fmod($i,2) != 0) { echo '<br>';}
}
echo "</form>";

print "</td>";
print "</tr>";
print "<tr>";
print "<td>";
echo "<form name=\"mvselector\"  method=\"post\">";

echo '<input onchange="this.form.submit();" type="radio" id="startofgame" name="targetmv" value=' . 0 .'>';
echo '<label for="startofgame">Start of Game</label>';

echo '<input onchange="this.form.submit();" type="radio" id="5back" name="targetmv" value=' . max(0, $_SESSION['curmv'] -5) .'>';
echo '<label for="5back">back 5 moves</label>';

echo '<input onchange="this.form.submit();" type="radio" id="1back" name="targetmv" value=' . max(0, $_SESSION['curmv'] -1) .'>';
echo '<label for="1back">back 1 move</label>';

echo '<input onchange="this.form.submit();" type="radio" id="1for" name="targetmv" value=' . min(count($_SESSION['mvlst']), $_SESSION['curmv'] +1) .'>';
echo '<label for="1for">forward 1 move </label>';

echo '<input onchange="this.form.submit();" type="radio" id="5for" name="targetmv" value=' . min(count($_SESSION['mvlst']), $_SESSION['curmv'] +5) .'>';
echo '<label for="5for">forward 5 moves </label>';

echo '<input onchange="this.form.submit();" type="radio" id="endofgame" name="targetmv" value=' . count($_SESSION['mvlst']) .'>';
echo '<label for="endofgame">End of Game</label>';


echo "</form>";
print "</td>";
print "<td>";
print "</td>";
print "<td>";
print "</td>";
print "</tr>";
print "</table>";


echo '<form  name="reset" method="post">';
echo '<input type="submit" name="resetgame" value="restart">';
echo '</form>';

echo '<form  name="setposormv" method="post">';
echo 'Set position: <input type="text" name="setstartpos" value="" placeholder="paste compact position" size=35>';
echo 'Apply movelist: <input type="text" name="applymvlst" value="" placeholder="paste moves in algebraic notation" size=35>';
echo '<input type="submit" value="Set!">';
echo '</form>';

?>

</body>
</html>
             
