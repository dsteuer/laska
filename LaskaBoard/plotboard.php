<?php
session_start();
// include("lb-config.php");
// not needed right now

$rad=100; // Felddurchmesser
$rsizex = 60;
$rsizey = 14;

// Create an image with the specified dimensions
$image = imageCreate(7.5*$rad,7.5*$rad);
 
// Create a color (this first call to imageColorAllocate
//  also automatically sets the image background color)
$colorBoardBackground = imageColorAllocate($image, 0,180,0);

// Create another color
$circleColor = imageColorAllocate($image,  230, 230, 0);
$wpc = imageColorAllocate($image, 230, 0, 0);
$woc = imageColorAllocate($image, 150, 0, 0);
$bpc = imageColorAllocate($image, 80, 80, 255);
$bpc = imageColorAllocate($image, 0, 0, 180);

for ($zeile = 1 ; $zeile < 8 ; $zeile = $zeile +2) {
    for ($spalte = 1 ; $spalte < 8 ; $spalte +=2 ) {
        imagefilledellipse($image, -$rad/4+$spalte*$rad ,-$rad/4+$zeile*$rad, $rad, $rad, $circleColor);
    }}

for ($zeile = 2 ; $zeile < 7 ; $zeile = $zeile +2) {
    for ($spalte = 2 ; $spalte < 7 ; $spalte +=2 ) {
        imagefilledellipse($image, -$rad/4+$spalte*$rad ,-$rad/4+$zeile*$rad, $rad, $rad, $circleColor); 
    }}

$boardLines = explode("|", $_SESSION['curpos']);
// a laska board has 7 lines

for ($zeile=0; $zeile <7 ; $zeile++){
    
    $xoffset = (3/4 + fmod(6-$zeile, 2))*$rad; 
    $yoffset = -$rad/4 + (7-$zeile)*$rad ;
    
    $fields = explode('.',$boardLines[$zeile]);
    
    for ($f = 0; $f < count($fields); $f++){
        if ($fields[$f] != '') {
            $tower = str_split($fields[$f]);
            // guaranteed >=1
            for ($s=0; $s < count($tower); $s++){
                switch ($tower[count($tower)-1 - $s]) {
                case "o": $stonecolor = $wpc ; break;
                case "O": $stonecolor = $woc ; break;
                case "x": $stonecolor = $bpc ; break;
                default: $stonecolor = $boc;
                }
                $leftborder = $xoffest - $rsisex/2;
                imagefilledrectangle($image, $xoffset+(2*$f)*$rad - $rsizex/2, $yoffset- (count($tower)-1-$s)*$rsizey + 2*$rsizey,
                                             $xoffset+(2*$f)*$rad + $rsizex/2, $yoffset- (count($tower) - $s)*$rsizey + 2*$rsizey, $stonecolor);
            }
        }
    }
}


   // Set type of image and send the output
   header("Content-type: image/jpeg");
   imageJpeg($image);
 
   // Release memory
   imageDestroy($image);
?>
