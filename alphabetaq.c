
#include "alphabetaq.h"

#include <iomanip>

// min/max with alpha/beta pruning with quiescence

AlphaBetaQuiesce::AlphaBetaQuiesce (const unsigned int board_size)			: MoveTree (board_size)    	{ } 
AlphaBetaQuiesce::AlphaBetaQuiesce (std::string pos, const unsigned int board_size)	: MoveTree (pos, board_size)	{ }

int AlphaBetaQuiesce::evaluateMoveTreeQuiesce(int depth, int alpha, int beta) {
	std::list<Move> l = possibleMoves();

	if (l.front().isQuiet()) { return evaluatePosition();		}	// no jumps, so quiesence search over. node is (at least) quiet
	if (l.empty())		 { return -mateValue(tree_depth-depth);	}	// game over, node is terminal. return mate in plies

	for (auto m: l) {						// forced part, moves capture
		// moveRecorded(m);
		move(m);
//		if (isRepetition ()) {
//			//undo(m);
//			for (const auto& m: move_history) { std::cerr << m << ' '; } std::cerr << " Qrep" << std::endl;
//			undo();
//			continue;
//		}
		int v = -evaluateMoveTreeQuiesce(depth-1, -beta, -alpha);
		// for (auto i: move_history) { std::cout << i << ' '; } std::cout << v << std::endl;
		// undo();
		undo(m);
//			for (const auto& m: move_history) { std::cerr << m << ' '; } std::cerr << " Q " << v << std::endl;
		//undo();

		if (v >= beta)  { return v;  }					// fail-soft
		if (v >  alpha) { alpha = v; }
	}

	return alpha;
}

int AlphaBetaQuiesce::evaluateMoveTree(unsigned int depth, int alpha, int beta) {
	if (!depth) { return evaluateMoveTreeQuiesce(depth, alpha, beta); }	// end of search depth. just forward all args to qsearch.

	std::list<Move> l = possibleMoves();

	if (l.empty()) { return -mateValue(tree_depth-depth);		}	// game over, node is terminal

	for (auto m: l) {
		//moveRecorded(m);
		move(m);
//		if (isRepetition ()) {
//			//undo(m);
//			for (const auto& m: move_history) { std::cerr << m << ' '; } std::cerr << " Qrep" << std::endl;
//			undo();
//			continue;
//		}
		int v = -evaluateMoveTree(depth-1, -beta, -alpha);
	//		for (const auto& m: move_history) { std::cerr << m << ' '; } std::cerr << " T " << v << std::endl;
		//for (auto i: move_history) { std::cout << i << ' '; } std::cout << v << std::endl;
		//undo();
		undo(m);

		if (v >= beta)  { return v;  }
		if (v >  alpha) { alpha = v; }
	}

	return alpha;
}

Move AlphaBetaQuiesce::findBestMove() {
	std::list<Move> l = possibleMoves();

	if (l.empty())	   return NoMove;					// no more moves, game over.
	if (l.size() == 1) return l.back();					// only one move possible in this position. return at once

	int	best_value = -mateValue();					// start with worst possible outcome
	Move	best_move  = NoMove;						// and no move

	for (auto m: l) {
		//moveRecorded(m);
		move(m);
		int v = -evaluateMoveTree(tree_depth-1, -maxValue(), maxValue());
		//for (auto i: move_history) { std::cout << i << ' '; } std::cout << v << std::endl;
		//undo();
		undo(m);
//			for (const auto& m: move_history) { std::cerr << m << ' '; } std::cerr << " F " << v << std::endl;

		std::cerr << moveValueString(m, v) << "\t";

		if (v > best_value) {
			best_value = v;
			best_move  = m;
		}
	}
	std::cerr << std::endl;

	return best_move;
}

